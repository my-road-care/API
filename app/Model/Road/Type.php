<?php

namespace App\Model\Road;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{	
	//use SoftDeletes;
    protected $table = 'roads_type';

    public function parts() {
        return $this->hasMany('App\Model\Road\Part', 'type_id');
    }

    public function mRoads() {
        return $this->hasMany('App\Model\Road\Part', 'type_id');
    }
    
    public function roads(){
        return $this->belongsToMany('App\Model\Road\Main', 'road_parts', 'type_id', 'road_id');
    }
    

}
