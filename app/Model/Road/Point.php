<?php

namespace App\Model\Road;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Point extends Model
{	
	use SoftDeletes; 
	use SpatialTrait;
    protected $table = 'road_pk_points';
    protected $fillable = [
        'point',
    ];
     protected $spatialFields = [
        'point',
    ];
    public function pk() { //M-1
        return $this->belongsTo('App\Model\Road\PK', 'pk_id');
    }

    public function potholes() {//1-M
        return $this->hasMany('App\Model\Pothole\Main', 'point_id');
    }

    public function communes() {//1-M
        return $this->hasMany('App\Model\Location\CommunePoint', 'point_id');
    }
}
