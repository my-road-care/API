<?php

namespace App\Model\Road;

use App\Model\Supervision\Projects\Project;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'road';

    protected $fillable = [
        'status'
    ];

    public function potholes() {//1-M
        return $this->hasMany('App\Model\Pothole\Main', 'road_id');
    }

    public function parts() {//1-M
        return $this->hasMany('App\Model\Road\Part', 'road_id');
    }

    public function mos() { //1-M
        return $this->hasMany('App\Model\Authority\MO\Road', 'road_id');
    }

    public function mMos(){ //M-M
    	return $this->belongsToMany('App\Model\Authority\MO\Main', 'mos_roads', 'road_id', 'mo_id');
    }

    public function mts(){ //1-M
        return $this->hasMany('App\Model\Authority\MT\Road', 'road_id');
    }

    public function mMts(){ //M-M
    	return $this->belongsToMany('App\Model\Authority\MT\Main', 'mts_roads', 'road_id', 'mt_id');
    }

    public function provinces() { //1-M
        return $this->hasMany('App\Model\Road\Province', 'road_id');
    }
    public function mProvinces(){ //M-M
        return $this->belongsToMany('App\Model\Location\Province', 'roads_provinces', 'road_id', 'province_id');
    }

    public function pks() {//1-M
        return $this->hasMany('App\Model\Road\PK', 'road_id');
    }

    public function ministries() {//1-M
        return $this->hasMany('App\Model\Authority\Ministry\Road', 'road_id');
    }

    public function mMinistries(){//M-M
        return $this->belongsToMany('App\Model\Authority\Ministry\Main', 'ministries_roads', 'road_id', 'ministry_id');
    }

    public function projects(){
        return $this->hasMany(Project::class,'road_id', 'id');
    }

    
}
