<?php

namespace App\Model\Road;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Part extends Model
{	
	use SoftDeletes; 
    use SpatialTrait;
    protected $table = 'road_parts';

    protected $fillable = [
        'line',
    ];
     protected $spatialFields = [
        'line',
    ];

    public function road() {//M-1
        return $this->belongsTo('App\Model\Road\Main', 'road_id');
    }

    public function type() {//M-1
        return $this->belongsTo('App\Model\Road\Type', 'type_id');
    }

    public function pks() {//1-M
        return $this->hasMany('App\Model\Road\PKPart', 'part_id');
    }

    public function mPKs(){ //M-M
        return $this->belongsToMany('App\Model\Road\PK', 'road_pks_parts', 'part_id', 'pk_id');
    }

}
