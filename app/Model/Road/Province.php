<?php

namespace App\Model\Road;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{	
	//use SoftDeletes;
    protected $table = 'roads_provinces';

    public function road() {
        return $this->belongsTo('App\Model\Road\Main', 'road_id');
    }

    public function province() {
        return $this->belongsTo('App\Model\Location\Province', 'province_id');
    }

    public function mProvinces() {
        return $this->hasMany('App\Model\Road\Province', 'road_id');
    }
    public function provinces(){
        return $this->belongsToMany('App\Model\Location\Province', 'roads_provinces', 'road_id', 'province_id');
    }

}
