<?php

namespace App\Model\Road;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PK extends Model
{	
	//use SoftDeletes; 
    protected $table = 'road_pks';
   

    public function road() {//M-1
        return $this->belongsTo('App\Model\Road\Main', 'road_id');
    }

    public function points() {//1-M
        return $this->hasMany('App\Model\Road\Point', 'pk_id');
    }

    public function parts() {//1-M
        return $this->hasMany('App\Model\Road\PKPart', 'pk_id');
    }

    public function mParts(){ //M-M
    	return $this->belongsToMany('App\Model\Road\Part', 'road_pks_parts', 'pk_id', 'part_id');
    }

    public function potholes() {//1-M
        return $this->hasManyThrough('App\Model\Pothole\Main', 'App\Model\Road\Point', 'pk_id', 'point_id');
    }
}
