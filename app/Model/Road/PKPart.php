<?php

namespace App\Model\Road;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PKPart extends Model
{	
	//use SoftDeletes;
    protected $table = 'road_pks_parts';

   
    public function pk() {//M-1
        return $this->belongsTo('App\Model\Road\PK', 'pk_id');
    }

    public function part() {//M-1
        return $this->belongsTo('App\Model\Road\Part', 'part_id');
    }

    
}
