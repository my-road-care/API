<?php

namespace App\Model\Road;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Data extends Model
{	
	use SpatialTrait;
    protected $table = 'road_data';
     protected $fillable = [
        'shape',
    ];
     protected $spatialFields = [
        'shape',
    ];
   
}
