<?php

namespace App\Model\Member;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Score extends Model
{	
	use SoftDeletes;
    protected $table = 'member_scores';


    public function member() {
        return $this->belongsTo('App\Model\Member\Main', 'member_id');
    }

    

}
