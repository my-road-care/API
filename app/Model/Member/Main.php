<?php

namespace App\Model\Member;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'member';

    public function user(){
        return $this->belongsTo('App\Model\User\Main', 'user_id');
    }

    public function reports() {
        return $this->hasMany('App\Model\Pothole\Report', 'member_id');
    }
    
    public function scroces() {
        return $this->hasMany('App\Model\Member\Score', 'member_id');
    }
}
