<?php

namespace App\Model\Notification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Main extends Model
{   
    use SoftDeletes;
    
    protected $table = 'notifications';

    public function receivers() { //1-M
        return $this->hasMany('App\Model\User\Notification', 'notification_id');
    }



}
