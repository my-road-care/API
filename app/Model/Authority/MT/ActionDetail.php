<?php

namespace App\Model\Authority\MT;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActionDetail extends Model
{	
	use SoftDeletes;
    protected $table = 'mt_action_details';


    public function mt() {
        return $this->belongsTo('App\Model\Authority\MT\Main', 'mt_id');
    }
    public function action() {
        return $this->belongsTo('App\Model\Authority\MT\Action', 'action_id');
    }

   
}
