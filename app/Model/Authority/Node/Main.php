<?php

namespace App\Model\Authority\Node;
use Illuminate\Database\Eloquent\Model;

class Main extends Model
{
   
    protected $table = 'user_node';
    public function user(){
        return $this->belongsTo('App\Model\User\Main', 'user_id');
    }

    public function parent(){
        return $this->belongsTo('App\Model\User\Node', 'parent_id');
    }

    public function children(){
        return $this->hasMany('App\Model\User\Node', 'parent_id');
    }
   
}
