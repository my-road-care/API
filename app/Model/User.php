<?php

namespace App\Model;

use App\Model\Supervision\Projects\ProjectImage;
use App\Model\Supervision\Projects\ProjectIntroLetter;
use App\Model\Supervision\Projects\ProjectLetter;
use App\Model\Supervision\Projects\ProjectPlan;
use App\Model\Supervision\Projects\ProjectProcurement;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Projects\StandardTestReport;
use App\Model\Supervision\Setup\Role;
use App\Model\Supervision\Setup\Organization;
use Illuminate\Notifications\Notifiable;
use App\Model\User\Type;
use App\Model\Supervision\Projects\ProjectWorkingGroup;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'user';

    protected $hidden = [
        'password', 'remember_token', 'password', 'google2fa_secret', 'telegram_chat_id', 'social_id'
    ];

    protected $fillable = [
        'app_token',
        'password'
    ];

    public function organization(){
        return $this -> belongsTo('App\Model\Supervision\Projects\Organization', 'organization_id');
    }

    public function admin() {
        return $this->hasOne('App\Model\User\Admin', 'user_id');
    }
    public function mo() {
        return $this->hasOne('App\Model\Authority\MO\Main', 'user_id');
    }
    public function mt() {
        return $this->hasOne('App\Model\Authority\MT\Main', 'user_id');
    }
    public function ru() {
        return $this->hasOne('App\Model\Member\Main', 'user_id');
    }

    public function standardTest(){
        return $this -> hasMany(ProjectStandardTest::class,'creator_id', 'id');
    }

    public function oranizationRole(){
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function role(){
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function type(){
        return $this->belongsTo(Type::class,'type_id','id');
        
    }

    public function projectPlans(){
        return $this->hasMany(ProjectPlan::class,'creator_id','id');
    }

    public function projectProcurements(){
        return $this->hasMany(ProjectProcurement::class,'creator_id', 'id');
    }

    public function projectWorkingGroups(){
        return $this->hasMany(ProjectWorkingGroup::class,'user_id', 'id');
    }

    public function testReports(){
        return $this->hasMany(StandardTestReport::class,'creator_id', 'id');
    }

    public function projectIntroLetters(){
        return $this -> hasMany(ProjectLetter::class,'creator_id', 'id');
    }

    public function projectImages(){
        return $this -> hasMany(ProjectImage::class,'creator_id', 'id');
    }

}
