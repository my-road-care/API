<?php

namespace App\Model\Supervision\Permission;

use Illuminate\Database\Eloquent\Model;

class PerPart extends Model
{
    protected $table = "sup_per_part";
}
