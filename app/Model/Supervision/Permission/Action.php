<?php

namespace App\Model\Supervision\Permission;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $table = "sup_per_action";
}
