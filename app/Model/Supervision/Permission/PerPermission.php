<?php

namespace App\Model\Supervision\Permission;

use Illuminate\Database\Eloquent\Model;

use App\Model\Supervision\Permission\Action;
use App\Model\Supervision\Permission\PerPart;
use App\Model\Supervision\Projects\SystemRole;

class PerPermission extends Model
{
    protected $table = 'sup_per_permission';

    public function action(){
        return $this -> belongsTo(Action::class, 'action_id');
    }

    public function part(){
        return $this -> belongsTo(PerPart::class, 'part_id');
    }

    public function sys(){
        return $this->belongsTo(SystemRole::class, 'role_id');
    }
}
