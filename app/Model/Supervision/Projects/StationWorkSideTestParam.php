<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;
use App\Model\Supervision\Setup\ProgressStatus;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Setup\TestingMethod;
use App\Model\Supervision\Setup\TestingMethodTypParamater;
use App\Model\User;

class StationWorkSideTestParam extends Model
{
    protected $table = 'sup_pr_station_works_sides_tests_params';


    public function test()
    {
        return $this->belongsTo(StationWorkSideTest::class, 'station_work_side_test_id');
    }

    public function param()
    {
        return $this->belongsTo(TestingMethodTypParamater::class, 'parameter_id');
    }

    public function parameter(){
        return $this->belongsTo(TestingMethod::class,'parameter_id','id');
    }

    


    

}
