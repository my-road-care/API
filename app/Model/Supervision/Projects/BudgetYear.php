<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;

class BudgetYear extends Model
{

    protected $table = 'sup_year';

    public function project()
    {
        return $this->hasMany(Project::class, 'budget_year_id');
    }
   
    
}
