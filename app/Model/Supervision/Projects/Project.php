<?php

namespace App\Model\Supervision\Projects;

use App\Model\Supervision\Setup\Budget;
use App\Model\Supervision\Setup\Category;
use App\Model\Supervision\Setup\Status;
use App\Model\Supervision\Setup\ProjectType;
use App\Model\Supervision\Setup\Organization;
use App\Model\Supervision\Projects\ProjectWorkingGroup;
use App\Model\Location\Province;
use App\Model\Road\Main;
use App\Model\Supervision\Setup\Work;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
	use SoftDeletes;

    protected $table = 'sup_project';

    protected $fillable = [
        'diagram_creator_id',
        'diagram_updater_id',
        'diagram_approver_id',
        'diagram_create_date',
        'diagram_update_date',
        'diagram_approve_date',
        'updated_at',
        'interval',
        'project_status_id'
    ];
    // =========================> Relationship Model <=======================
    public function surfaces()
    {
        return $this->belongsToMany(Work::class, 'sup_project_has_surfaces', 'project_id', 'surface_id');
    }

    public function diagramCreator()
    {
        return $this->belongsTo(User::class, 'diagram_creator_id', 'id');
    }

    public function diagramUpdater()
    {
        return $this->belongsTo(User::class, 'diagram_updater_id', 'id');
    }

    public function diagramApprover()
    {
        return $this->belongsTo(User::class, 'diagram_approver_id', 'id');
    }

    public function standardTest()
    {
        return $this->hasMany(ProjectStandardTest::class, 'project_id', 'id');
    }

    public function budgets()
    {
        return $this->belongsTo(Budget::class, 'budget_id', 'id');
    }

    public function year(){
        return $this->belongsTo(BudgetYear::class,'budget_year_id', 'id');
    }

    public function provinces()
    {
        return $this->belongsTo(Province::class, 'provinces_id', 'id');
    }

    public function types()
    {
        return $this->belongsTo(ProjectType::class, 'type_id', 'id')->select('id', 'name');
    }

    public function organizations()
    {
        return $this->belongsToMany(Organization::class, 'sup_project_mgt_structures', 'project_id', 'organization_id');
    }

    public function projectWorks()
    {
        return $this->hasMany(ProjectWork::class, 'project_id', 'id');
    }

    public function projectWorkingGroups()
    {
        return $this->hasMany(ProjectWorkingGroup::class, 'project_id', 'id')->select('id','project_id','sys_id');
    }

    public function proejctTestPrinciples()
    {
        return $this->hasMany(ProjectTestPrinciple::class, 'project_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id')->select('id', 'name');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'project_status_id', 'id')->select('id', 'kh_name', 'en_name', 'color');
    }

    public function road()
    {
        return $this->belongsTo(Main::class, 'road_id', 'id');
    }

    public function projectProvinces(){
        return $this->hasMany(ProjectProvince::class);
    }
}
