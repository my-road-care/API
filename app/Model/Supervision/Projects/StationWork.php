<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;

use App\Model\Supervision\Setup\Work;
use App\Model\Supervision\Projects\Station; 
use App\Model\Supervision\Projects\StationWorkSide; 

class StationWork extends Model
{

    protected $table = 'sup_project_station_works';

    public function work()
    {
        return $this->belongsTo(Work::class, 'work_id')
        ->select('id', 'type_id', 'kh_name as name', 'color')
        ;
    }

    public function station()
    {
        return $this->belongsTo(Station::class, 'station_id', 'id')

        ->select('id', 'transaction_id', 'number', 'trx', 'station_num');
    }

    public function sides()
    {
        return $this->hasMany(StationWorkSide::class, 'station_work_id')
        ;
    }


}
