<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;
use App\Model\Supervision\Setup\ProgressStatus;
use App\Model\User;

class StationWorkSideStatus extends Model
{
    protected $table = 'sup_pr_station_works_sides_statuses';

    // ==============> Relationship Model <=================
    public function side()
    {
        return $this->belongsTo(StationWorkSide::class, 'station_work_side_id');
    }

    public function progress()
    {
        return $this->belongsTo(ProgressStatus::class, 'progress_status_id')
        ->select('id', 'kh_name as name', 'color', 'icon')
        ;
    }


    public function standardTest()
    {
        return $this->belongsTo(ProjectStandardTest::class, 'standard_test_id')
        ->select('id', 'testing_method_id', 'standard')
        ->with([
            'method:id,name'
        ])
        ;
    }

    public function verifier(){
        return $this->belongsTo(User::class,'verifier_id','id')->select('id', 'name');
    }

    public function tester(){
        return $this->belongsTo(User::class,'tester_id','id')->select('id', 'name');
    }

}
