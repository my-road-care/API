<?php

namespace App\Model\Supervision\Projects;

use App\Model\Supervision\Setup\ProgressStatus;
use App\Model\Supervision\Setup\Side;
use Illuminate\Database\Eloquent\Model;
use App\Model\Supervision\Projects\StationWork;

class StationWorkSide extends Model
{

    protected $table = 'sup_project_station_works_sides';
    protected $fillable = [
        'progress_status_id'
    ];
    
    public function station()
    {
        return $this->belongsTo(StationWork::class, 'station_work_id','id');
    }

    public function side()
    {
        return $this->belongsTo(Side::class, 'side_id')->select('id', 'name', 'abrv');
    }


    public function status()
    {
        return $this->belongsTo(ProgressStatus::class, 'progress_status_id', 'id')
        ->select('id', 'kh_name as name', 'color')
        ;
    }

    public function histories()
    {
        return $this->hasMany(StationWorkSideHistory::class, 'station_work_side_id', 'id')
        ->select('id', 'station_work_side_id', 'progress_status_id', 'updater_id', 'comment', 'created_at','progress_trx_id')
        ->whereBetween('progress_status_id', [2,5])
        ->orderBy('id', 'DESC')
        ;
    }

    public function testingRecords()
    {
        return $this->hasMany(StationWorkSideTest::class, 'station_work_side_id', 'id')
        ->select('id', 'station_work_side_id', 'standard_test_id', 'comment', 'created_at', 'tester_id', 'result', 'is_pass')
        ->where('standard_test_id', '<>', null);
    }
}
