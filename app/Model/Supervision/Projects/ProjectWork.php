<?php

namespace App\Model\Supervision\Projects;

use App\Model\Location\Province;
use App\Model\Supervision\Setup\BuildType;
use App\Model\Supervision\Setup\SlabType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectWork extends Model
{
    // use SoftDeletes;

    protected $table = 'sup_project_works';

    //=====================> Relationship Model <====================


    public function provinces()
    {
        return $this->belongsTo(Province::class, 'provinces_id', 'id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
