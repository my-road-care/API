<?php

namespace App\Model\Supervision\Projects;

use App\Model\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Supervision\Projects\Project;
use App\Model\Location\Province; 
class ProjectProvince extends Model
{


    protected $table = 'sup_project_province';

    // ====================> Relationship Model <====================

    public function provinces()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
