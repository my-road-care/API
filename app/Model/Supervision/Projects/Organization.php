<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;
use App\Model\Supervision\Setup\ProgressStatus;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Setup\TestingMethod;
use App\Model\Supervision\Setup\TestingMethodTypParamater;
use App\Model\User;

class Organization extends Model
{
    protected $table = 'organization';
    
}
