<?php

namespace App\Model\Supervision\Projects;

use App\Model\Supervision\Setup\TestingMethod;
use App\Model\Supervision\Setup\Organization;
use App\Model\Supervision\Setup\Work;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;

class ProjectStandardTest extends Model
{
    protected $table        = "sup_project_standard_tests";
    protected $primarykey   = "id";


    //==========================> Relationship Model <===========================

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function method()
    {
        return $this->belongsTo(TestingMethod::class, 'testing_method_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function work()
    {
        return $this->belongsTo(Work::class, 'work_id', 'id');
    }

    public function entities()
    {
        return $this->belongsTo(Organization::class, 'entity_id', 'id');
    }

}
