<?php

namespace App\Model\Supervision\Projects;

use App\Model\Supervision\Setup\ImageType;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;

class ReferenceReport extends Model
{
    protected $table = "sup_project_reference_reports";
    protected $primarykey = "id";

    //======================> Relationship Model <========================

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id')->select('id','name');
    }

    public function project(){
        return $this->belongsTo(Project::class,'project_id', 'id');
    }
}
