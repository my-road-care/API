<?php

namespace App\Model\Supervision\Projects;

use App\Model\User;
use Illuminate\Database\Eloquent\Model;

class ProjectLetter extends Model
{
    protected $table = 'sup_project_letters';

    //=====================> Relationship Model <=========================

    public function user()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id')->select('id','name');
    }
    public function letter_type(){
      return $this->belongsTo(LetterType::class, 'project_intro_letters_type_id', 'id')->select('id','name');
    }
}
