<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{

    protected $table = 'sup_project_stations';


    public function point()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id', 'id')
        ->select('id', 'pk', 'project_id')
        ;
    }

    public function stationWorks(){
        return $this->hasMany(StationWork::class, 'station_id', 'id');
    }
}
