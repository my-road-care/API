<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectMGT extends Model
{
    protected $table = 'sup_project_mgt_structures';

    protected $fillable = [
        'project_id',
        'organization_id'
    ];

    // ===============> Relationship Model <=================
    public function project(){
        return $this->belongsTo(Project::class,'project_id', 'id')->whereNull('deleted_at');
    }

}


