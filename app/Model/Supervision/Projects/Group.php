<?php

namespace App\Model\Supervision\Projects;

use App\Model\Road\Main;
use App\Model\Supervision\Setup\ImageType;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = "sup_group";
    protected $primarykey = "id";
    protected $fillable = [
        'id',
        'name'
    ];

    public function roads(){
        return $this->hasMany(Main::class,'group_id','id');
    }
}
