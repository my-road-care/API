<?php

namespace App\Model\Supervision\Projects;

use App\Model\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectPlan extends Model
{
    use SoftDeletes;

    protected $table = 'sup_project_plans';

    // ====================> Relationship Model <====================
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id')->select('id', 'name');
    }
}
