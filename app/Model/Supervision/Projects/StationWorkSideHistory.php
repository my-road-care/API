<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;
use App\Model\Supervision\Setup\ProgressStatus;
use App\Model\User;

use App\Model\Supervision\Projects\StationWorkSide;
use Illuminate\Database\Eloquent\SoftDeletes;

class StationWorkSideHistory extends Model
{
	use SoftDeletes;

    protected $table = 'sup_pr_station_works_sides_histories';

    // ==============> Relationship Model <=================
    public function side()
    {
        return $this->belongsTo(StationWorkSide::class, 'station_work_side_id');
    }

    public function progress()
    {
        return $this->belongsTo(ProgressStatus::class, 'progress_status_id')
        ->select('id', 'kh_name as name', 'color', 'icon')
        ;
    }

    public function updater(){
        return $this->belongsTo(User::class,'updater_id','id')->select('id', 'name');
    }

    public function images() {
        return $this->hasMany(ProjectImage::class,'station_work_side_history_id', 'id')
        ->select('id','station_work_side_history_id','image_uri','lat','lng');
    }

    public function progressTrx(){
        return $this->belongsTo(ProgressTRX::class,'progress_trx_id','id');
    }

}
