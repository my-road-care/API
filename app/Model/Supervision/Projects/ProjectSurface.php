<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;

class ProjectSurface extends Model
{
    protected $table = 'sup_project_has_surfaces';
    protected $fillable = [
        'project_id',
        'surface_id'
    ];

    
}
