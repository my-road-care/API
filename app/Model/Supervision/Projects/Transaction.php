<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;

use App\Model\Supervision\Projects\Project;

class Transaction extends Model
{
    protected $table = 'sup_project_transactions_pk';

    public function stations()
    {
        return $this->hasMany(Station::class, 'transaction_id')->select('id', 'station_num as number', 'transaction_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id')
        ->select('id', 'code')
        ;
    }

}
