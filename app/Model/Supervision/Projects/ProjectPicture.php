<?php

namespace App\Model\Supervision\Projects;

use App\Model\Supervision\Setup\ImageType;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;

class ProjectPicture extends Model
{
    protected $table = "sup_project_pictures";
    protected $primarykey = "id";

    //======================> Relationship Model <========================

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function imageTypes(){
        return $this->belongsTo(ImageType::class,'image_type_id', 'id')
        ->select('id','name');
    }

    public function images(){
        return $this->hasMany(ProjectImage::class,'project_picture_id', 'id')
        ->select('id','project_picture_id','image_uri','lat','lng');
    }
}
