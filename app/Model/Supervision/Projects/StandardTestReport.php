<?php

namespace App\Model\Supervision\Projects;

use App\Model\Supervision\Setup\TestingMethod;
use App\Model\Supervision\Setup\TestReportType;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StandardTestReport extends Model
{
    // use SoftDeletes;
    protected $table = 'sup_project_test_reports';

    //=====================> Relationship Model <=========================

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function testingMethod()
    {
        return $this->belongsTo(TestingMethod::class, 'field_testing_id', 'id');
    }

    public function reportTypes(){
        return $this->belongsTo(TestReportType::class,'report_type_id', 'id');
    }
}
