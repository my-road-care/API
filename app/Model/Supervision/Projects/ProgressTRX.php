<?php

namespace App\Model\Supervision\Projects;

use App\Model\Supervision\Setup\Side;
use App\Model\Supervision\Setup\Work;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;

class ProgressTRX extends Model
{

    protected $table = 'sup_progress_trx';

    // ====================> Relationship Model <====================
    public function tester()
    {
        return $this->belongsTo(User::class, 'tester_id', 'id')->select('id', 'name');
    }

    public function pk(){
        return $this->belongsTo(Transaction::class,'pk_id', 'id');
    }

    public function images(){
        return $this->hasMany(ProjectImage::class,'progress_trx_id', 'id')->select('id','progress_trx_id','image_uri', 'lat', 'lng');
    }

    public function work(){
        return $this->belongsTo(Work::class,'work_id', 'id')->select('id', 'kh_name as name');
    }

    public function side(){
        return $this->belongsTo(Side::class,'side_id', 'id')->select('id','name','abrv');
    }
}
