<?php

namespace App\Model\Supervision\Projects;
use Illuminate\Database\Eloquent\Model;

use App\Model\Supervision\Permission\PerPart;

class SystemRole extends Model
{
    protected $table = 'sup_per_system_possition';

    public function PerPermission(){
        return $this->belongsToMany(PerPart::class,'sup_per_permission', 'part_id','role_id');
    }
    
}
