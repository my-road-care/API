<?php

namespace App\Model\Supervision\Projects;

use App\Model\Supervision\Setup\ImageType;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;

class ProjectImage extends Model
{
    protected $table = "sup_project_images";
    protected $primarykey = "id";
    protected $fillable = [
        'progress_trx_id'
    ];

    //======================> Relationship Model <========================

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function histories(){
        return $this->belongsTo(StationWorkSideHistory::class,'station_work_side_history_id', 'id');
    }

    public function test(){
        return $this->belongsTo(StationWorkSideTest::class,'station_work_side_test_id', 'id');
    }
}
