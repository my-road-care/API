<?php

namespace App\Model\Supervision\Projects;

use App\Model\Supervision\Setup\Role;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;
class ProjectWorkingGroup extends Model
{

    protected $table = 'sup_project_working_group';

    //=================> Relationship Mode <========================

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->select('id','role_id','name','avatar','phone','organization_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id')->select('id','role_id','name','avatar','phone','organization_id');
    }

    public function role()
    {
        return $this->belongsTo(ProjectRole::class, 'role_id', 'id')->select('id','name');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function sys(){
        return $this->belongsTo(SystemRole::class, 'sys_id')->select('id','name','abbre','description');
    }

}
