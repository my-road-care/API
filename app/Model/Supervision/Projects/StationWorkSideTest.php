<?php

namespace App\Model\Supervision\Projects;

use Illuminate\Database\Eloquent\Model;
use App\Model\Supervision\Setup\ProgressStatus;
use App\Model\Supervision\Setup\TestingMethodTypParamater;
use App\Model\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class StationWorkSideTest extends Model
{
	use SoftDeletes;

    protected $table = 'sup_pr_station_works_sides_tests';

    // ==============> Relationship Model <=================
    public function side()
    {
        return $this->belongsTo(StationWorkSide::class, 'station_work_side_id');
    }


    public function standardTest()
    {
        return $this->belongsTo(ProjectStandardTest::class, 'standard_test_id')
        ->select('id', 'testing_method_id', 'standard')
        ->with([
            'method:id,name,unit',
            'method.place',
            'method.parameters.param'
        ])
        ;
    }

    public function value(){
        return $this->hasMany(StationWorkSideTestParam::class,'station_work_side_test_id','id')
        ->with([
            'param',
            'parameter.place'
        ]);
    }

    public function tester(){
        return $this->belongsTo(User::class,'tester_id','id')->select('id', 'name');
    }

    public function images(){
        return $this->hasMany(ProjectImage::class,'station_work_side_test_id','id')
        ->select('id','station_work_side_test_id','image_uri','lat','lng');
    }

    public function test(){
        return $this->hasMany(StationWorkSideTestParam::class,'station_work_side_test_id','id');
    }

    public function testingTrx(){
        return $this->belongsTo(TestingTRX::class,'testing_trx_id', 'id');
    }
    

}
