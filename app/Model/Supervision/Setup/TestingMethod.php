<?php

namespace App\Model\Supervision\Setup;

use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Setup\TestingStandard;
use App\Model\Supervision\Projects\StandardTestReport;
use App\Model\Supervision\Setup\Work;
use App\Model\Supervision\Setup\WorkTestMethod;
use Illuminate\Database\Eloquent\Model;
use App\Model\Supervision\Setup\TestingMethodType;

class TestingMethod extends Model
{
    protected $table = "sup_testing_method";
    protected $primarykey = "id";


    //==========================> Relationship Model <=========================

    public function TestingMethodType(){
        return $this->belongsTo(TestingMethodType::class,'type_id')->select('id','name');
    }

    public function parameters(){
        return $this->hasMany(TestingMethodTypParamater::class, 'testing_method_id');
    }

    public function place(){
        return $this->belongsTo(TestingMethodType::class, 'type_id')
        ->select('id','name');
    }

    public function standardTest()
    {
        return $this->hasMany(ProjectStandardTest::class, 'testing_method_id', 'id');
    }

    public function standardTestReports()
    {
        return $this->hasMany(StandardTestReport::class, 'testing_method_id', 'id');
    }

    public function works()
    {
        return $this->belongsToMany(Work::class, 'sup_work_test_method', 'testing_method_id', 'work_id');
    }

    public function layers()
    {
        return $this->hasMany(WorkTestMethod::class, 'testing_method_id');
    }

    public function works_testingmethod(){
        return $this->hasMany(WorkTestMethod::class,'testing_method_id');
    }

    public function standardGroup(){
        return $this->hasMany(TestingStandard::class,'method_id', 'id');
    }

}
