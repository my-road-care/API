<?php

namespace App\Model\Supervision\Setup;

use Illuminate\Database\Eloquent\Model;

class OrganizationType extends Model
{
    protected $table = 'sup_organizations_type';

    //========================> Relationship Model <===========================
    public function organization()
    {
        return $this->hasMany(Organization::class, 'type_id', 'id');
    }
}
