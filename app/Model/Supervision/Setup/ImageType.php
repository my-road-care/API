<?php

namespace App\Model\Supervision\Setup;

use Illuminate\Database\Eloquent\Model;

class ImageType extends Model
{
    protected $table = "sup_images_type";
    protected $primarykey = "id";
}
