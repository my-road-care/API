<?php

namespace App\Model\Supervision\Setup;

use App\Model\Supervision\Setup\Work;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkType extends Model
{
    use SoftDeletes;

    protected $table = 'sup_works_type';

    public function works()
    {
        return $this->hasMany(Work::class, 'type_id');
    }
}
