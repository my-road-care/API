<?php

namespace App\Model\Supervision\Setup;

use App\Model\Supervision\Setup\Work;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkGroup extends Model
{
    use SoftDeletes;

    protected $table = 'sup_groups_work';

}
