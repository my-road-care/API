<?php

namespace App\Model\Supervision\Setup;

use App\Model\Supervision\Projects\Project;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectType extends Model
{
    use SoftDeletes;

    protected $table = 'sup_type';

    public function projects()
    {
        return $this->hasMany(Project::class, 'type_id');
    }
}
