<?php

namespace App\Model\Supervision\Setup;
use App\Model\Supervision\Setup\TestingStandard;
use App\Model\Supervision\Projects\StationWork;
use App\Model\Supervision\Setup\WorkType;
use Illuminate\Database\Eloquent\Model;
use App\Model\User;
class Work extends Model
{

    protected $table = 'sup_work';

    // ================> Relationship Model <==================
    public function type()
    {
        return $this->belongsTo(WorkType::class, 'type_id')->select('id', 'name');
    }

    public function stationWorks()
    {
        return $this->hasMany(StationWork::class, 'work_id');
    }

    public function group(){
        return $this->belongsTo(WorkGroup::class,'group_id')->select('id', 'name');
    }

    public function user(){
        return $this->belongsTo(User::class, 'creator_id')->select('id', 'name');;
    }

    public function testingMethods(){
        return $this->belongsToMany(TestingMethod::class,'sup_work_test_method','work_id','testing_method_id');
    }
    
    public function testingmethod(){
        return $this->hasMany(WorkTestMethod::class,'work_id');
    }

    public function standard(){
        return $this->hasMany(TestingStandard::class,'layer_id', 'id');
    }

    public function methods(){
        return $this->hasMany(WorkTestMethod::class, 'work_id');
    }


}
