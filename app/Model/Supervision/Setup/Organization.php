<?php

namespace App\Model\Supervision\Setup;

use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProjectMGT;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table = 'sup_organization';

    //========================> Relationship Model <===========================
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'sup_project_mgt_structures', 'organization_id', 'project_id');
    }

    public function type()
    {
        return $this->belongsTo(OrganizationType::class, 'type_id', 'id')->select('id', 'name');
    }

    public function MGT()
    {
        return $this->hasMany(ProjectMGT::class, 'organization_id','id')->whereNull('deleted_at');
    }
}
