<?php

namespace App\Model\Supervision\Setup;

use App\Model\Supervision\Projects\StationWorkSideTestParam;
use App\Model\Supervision\Setup\TestingMethod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestingMethodTypParamater extends Model
{
    use SoftDeletes;

    protected $table = 'sup_testing_method_parameters';

    public function type()
    {
        return $this->belongsTo(TestingMethod::class, 'testing_method_id');
    }

    public function param(){
        return $this->hasMany(StationWorkSideTestParam::class,'parameter_id','id');
    }
}
