<?php

namespace App\Model\Supervision\Setup;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'sup_category';
}
