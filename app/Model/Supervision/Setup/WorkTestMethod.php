<?php

namespace App\Model\Supervision\Setup;

use App\Model\Supervision\Setup\TestingMethod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkTestMethod extends Model
{
    // use SoftDeletes;

    protected $table = 'sup_work_test_method';

    public function layer(){
        return $this->belongsTo(Work::class, 'work_id');
    }

    public function method(){
        return $this->belongsTo(TestingMethod::class, 'testing_method_id');
    }

    public function standards(){
        return $this->hasMany(TestingStandard::class, 'method_id');
    }

}
