<?php

namespace App\Model\Supervision\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Budget extends Model
{
    
    use SoftDeletes;

    protected $table = 'sup_budget';
}