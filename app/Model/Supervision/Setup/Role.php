<?php

namespace App\Model\Supervision\Setup;

use App\Model\User;
use App\Model\Supervision\Projects\ProjectWorkingGroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $table = 'role';

    // ================> Relationship Model <==================

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'sup_role_has_permission', 'role_id', 'permission_id');
    }

    // public function user()
    // {
    //     return $this->hasMany(User::class, 'role_id', 'id');
    // }

    public function projectWorkingGroups()
    {
        return $this->hasMany(ProjectWorkingGroup::class, 'role_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function role(){
        return $this->belongsTo(Role::class,'role_id');
    }

}
