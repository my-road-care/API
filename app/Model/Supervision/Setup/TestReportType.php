<?php

namespace App\Model\Supervision\Setup;

use Illuminate\Database\Eloquent\Model;

class TestReportType extends Model
{

    protected $table = 'sup_test_report_type';

    // ================> Relationship Model <==================
}
