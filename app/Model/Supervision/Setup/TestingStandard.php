<?php

namespace App\Model\Supervision\Setup;

use App\Model\Supervision\Setup\WorkTestMethod;
use Illuminate\Database\Eloquent\Model;

class TestingStandard extends Model
{
    protected $table = "sup_standard";
    protected $primarykey = "id";
    protected $fillable = [
        'id',
        'group_id',
        // 'layer_id',
        'method_id',
        'value'
    ];

    public function method(){
        return $this->belongsTo(WorkTestMethod::class,'method_id', 'id');
    }
}
