<?php

namespace App\Model\Supervision\Setup;

use Illuminate\Database\Eloquent\Model;

class Side extends Model
{
    protected $table = 'sup_side';
}
