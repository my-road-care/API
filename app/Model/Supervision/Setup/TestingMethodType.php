<?php

namespace App\Model\Supervision\Setup;

use App\Model\Supervision\Setup\TestingMethod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestingMethodType extends Model
{
    use SoftDeletes;

    protected $table = 'sup_testing_methods_type';

    public function testingMethod()
    {
        return $this->hasMany(TestingMethod::class, 'type_id');
    }
}
