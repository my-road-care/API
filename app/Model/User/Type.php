<?php

namespace App\Model\User;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
   
    protected $table = 'users_type';

    public function users(){
        return $this->hasMany(User::class, 'type_id');
    }
   
}
