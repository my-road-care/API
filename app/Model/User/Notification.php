<?php

namespace App\Model\User;

use App\Model\Supervision\Projects\Project;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
   
    protected $table = 'user_notifications';
    public function user(){
        return $this->belongsTo('App\Model\User\User', 'user_id');
    }

    public function project(){
        return $this->belongsTo(Project::class,'project_id','id');
    }

    
   
}
