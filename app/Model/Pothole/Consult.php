<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Consult extends Model
{	
	//use SoftDeletes;
    protected $table = 'pothole_consults';

    public function pothole() {
        return $this->belongsTo('App\Model\Pothole\Main', 'pothole_id');
    }

    public function commenter() {
        return $this->belongsTo('App\Model\User\Main', 'creator_id');
    }

   
}
