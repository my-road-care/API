<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class ReportType extends Model
{	
	//use SoftDeletes;
    
    protected $table = 'pothole_report_types';
    
    public function reports() {//M-1
        return $this->hasMany('App\Model\Pothole\Report', 'type_id');
    }

    
   
}
