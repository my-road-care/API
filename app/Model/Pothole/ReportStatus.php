<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class ReportStatus extends Model
{	

    protected $table = 'pothole_report_statuses';

    public function report() { //M-1
        return $this->belongsTo('App\Model\Pothole\Report', 'report_id');
    }

    public function status() {
        return $this->belongsTo('App\Model\Setting\Status', 'status_id');
    }

    public function updater() {
        return $this->belongsTo('App\Model\User\Main', 'updater_id');
    }

    public function mt() {
        return $this->belongsTo('App\Model\Authority\MT\Main', 'mt_id');
    }

    public function files() {
        return $this->hasMany('App\Model\Pothole\StatusFile', 'status_id');
    }

   
}
