<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class ReportLocation extends Model
{	
	//use SoftDeletes;
    use SpatialTrait;
    protected $table = 'pothole_report_locations';
    protected $fillable = [
        'point',
    ];
     protected $spatialFields = [
        'point',
    ];

    public function report() { //M-1
        return $this->belongsTo('App\Model\Pothole\Report', 'report_id');
    }

    public function village() { //M-1
        return $this->belongsTo('App\Model\Location\Village', 'village_id');
    }

    public function commune() { //M-1
        return $this->belongsTo('App\Model\Location\Commune', 'commune_id');
    }

     public function district() { //M-1
        return $this->belongsTo('App\Model\Location\District', 'district_id');
    }

     public function province() { //M-1
        return $this->belongsTo('App\Model\Location\Province', 'province_id');
    }

   
}
