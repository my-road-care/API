<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Main extends Model
{	
	//use SoftDeletes;
    
    protected $table = 'pothole';

    public function location() { //M-1
        return $this->hasOne('App\Model\Pothole\Location', 'pothole_id');
    }

    public function road() { //M-1
        return $this->belongsTo('App\Model\Road\Main', 'road_id');
    }

    public function point() { //M-1
        return $this->belongsTo('App\Model\Road\Point', 'point_id');
    }
    public function ru() { //M-1
        return $this->belongsTo('App\Model\Member\Main', 'member_id');
    }

    public function action() { //M-1
        return $this->belongsTo('App\Model\Authority\MT\Action', 'action_id');
    }

    public function statuses() { //1-M
        return $this->hasMany('App\Model\Pothole\Status', 'pothole_id');
    }
    public function mStatuses(){ //M-M
    	return $this->belongsToMany('App\Model\Setting\Status', 'potholes_statuses', 'pothole_id', 'status_id');
    }

    public function comments() { //1-M
        return $this->hasMany('App\Model\Pothole\Consult', 'pothole_id');
    }
   
    public function maintence() { //M-1
        return $this->belongsTo('App\Model\Setting\Maintence\Main', 'maintence_id');
    }

    public function reports() { //1-M
        return $this->hasMany('App\Model\Pothole\Report', 'pothole_id');
    }

    public function files() { //1-M-M
        return $this->hasManyThrough('App\Model\Pothole\StatusFile', 'App\Model\Pothole\Status', 'pothole_id', 'status_id');
    }


}
