<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatusFile extends Model
{	
	use SoftDeletes;
    protected $table = 'pothole_status_files';

    public function status() {
        return $this->belongsTo('App\Model\Pothole\Status', 'status_id');
    }

   
}
