<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{	
	use SoftDeletes;
    protected $table = 'pothole_report_comments';

    public function report() {
        return $this->belongsTo('App\Model\Pothole\Report', 'report_id');
    }

    public function commenter() {
        return $this->belongsTo('App\Model\User\Main', 'creator_id');
    }

   
}
