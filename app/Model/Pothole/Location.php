<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Location extends Model
{	
	//use SoftDeletes;
    use SpatialTrait;
    protected $table = 'pothole_location';

    protected $fillable = [
        'points',
    ];
    protected $spatialFields = [
        'points',
    ];

    public function pothole() { //1-1
        return $this->belongsTo('App\Model\Pothole\Main', 'pothole_id');
    }

    public function village() { //M-1
        return $this->belongsTo('App\Model\Location\Village', 'village_id');
    }

    public function commune() { //M-1
        return $this->belongsTo('App\Model\Location\Commune', 'commune_id');
    }

     public function district() { //M-1
        return $this->belongsTo('App\Model\Location\District', 'district_id');
    }

     public function province() { //M-1
        return $this->belongsTo('App\Model\Location\Province', 'province_id');
    }

   
}
