<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Report extends Model
{	
	use SoftDeletes;
    
    protected $table = 'pothole_reports';
    public function pothole() {//M-1
        return $this->belongsTo('App\Model\Pothole\Main', 'pothole_id');
    }

    public function type() {//M-1
        return $this->belongsTo('App\Model\Pothole\ReportType', 'type_id');
    }
    
    public function ru() { //Member
        return $this->belongsTo('App\Model\Member\Main', 'member_id');
    }

    public function files() {//1-M
        return $this->hasMany('App\Model\Pothole\File', 'report_id');
    }

    public function comments() {//1-M
        return $this->hasMany('App\Model\Pothole\Comment', 'report_id');
    }
    
    public function commune() {//M-1
        return $this->belongsTo('App\Model\Location\Commune', 'commune_id');
    }

    public function locations() {//1-M
        return $this->hasMany('App\Model\Pothole\ReportLocation', 'report_id');
    }

    public function status() { //M-1
        return $this->belongsTo('App\Model\Setting\Status', 'status_id');
    }

    public function statuses() {//1-M
        return $this->hasMany('App\Model\Pothole\ReportStatus', 'report_id');
    }
   
}
