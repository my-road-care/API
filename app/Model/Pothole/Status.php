<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{	
	use SoftDeletes;
    protected $table = 'potholes_statuses';

    public function pothole() {
        return $this->belongsTo('App\Model\Pothole\Main', 'pothole_id');
    }

    public function status() {
        return $this->belongsTo('App\Model\Setting\Status', 'status_id');
    }

    public function updater() {
        return $this->belongsTo('App\Model\User\Main', 'updater_id');
    }

    public function mt() {
        return $this->belongsTo('App\Model\Authority\MT\Main', 'mt_id');
    }

    public function files() {
        return $this->hasMany('App\Model\Pothole\StatusFile', 'status_id');
    }

}
