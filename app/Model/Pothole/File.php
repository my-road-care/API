<?php

namespace App\Model\Pothole;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{	
	use SoftDeletes;
    protected $table = 'pothole_report_files';

    public function report() {
        return $this->belongsTo('App\Model\Pothole\Report', 'report_id');
    }

   
}
