<?php

namespace App\Model\SupLocation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class District extends Model
{
   	use SoftDeletes;
    use SpatialTrait;

    protected $table = 'sup_districts';

    protected $fillable = [
        'central',
    ];
     protected $spatialFields = [
        'central',
    ];
}
