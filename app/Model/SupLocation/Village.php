<?php

namespace App\Model\SupLocation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Village extends Model
{
   	use SoftDeletes;
    use SpatialTrait;

    protected $table = 'sup_villages';

    protected $fillable = [
        'central',
    ];
     protected $spatialFields = [
        'central',
    ];

    
}
