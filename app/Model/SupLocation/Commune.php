<?php

namespace App\Model\SupLocation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Commune extends Model
{
   	use SoftDeletes;
    use SpatialTrait;

    protected $table = 'sup_communes';

    protected $fillable = [
        'central', 'boundary'
    ];
     protected $spatialFields = [
        'central', 'boundary'
    ];
}
