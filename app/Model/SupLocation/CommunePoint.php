<?php

namespace App\Model\SupLocation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommunePoint extends Model
{
   	use SoftDeletes;

    protected $table = 'communes_points';
}
