<?php

namespace App\Model\SupLocation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use App\Model\LatLngScope; 

class Province extends Model
{
   	use SoftDeletes;
    use SpatialTrait;

    protected $table = 'sup_provinces';
    // protected static function boot()
    // {
    //     parent::boot();
    //     //static::addGlobalScope(new LatLngScope);
    // }

    protected $fillable = [
        'central',
    ];
     protected $spatialFields = [
        'central',
    ];

    
}
