<?php

namespace App\Model\Setting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ManagementType extends Model
{
   	use SoftDeletes;
    protected $table = 'management_type';
    
    public function mos() {//1-M
        return $this->hasMany('App\Model\Authority\MO\Main', 'management_type_id');
    }
}
