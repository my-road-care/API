<?php

namespace App\Model\Setting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
   	use SoftDeletes;
    protected $table = 'status';
    
    public function potholes() {//1-M
        return $this->hasMany('App\Model\Pothole\Main', 'status_id');
    }

    public function children() {//1-M
        return $this->hasMany('App\Model\Setting\Status', 'parent_id');
    }

}
