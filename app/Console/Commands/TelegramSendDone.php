<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use JWTAuth;
use App\Model\Pothole\Report as Report;
use TelegramBot\Api\Types\InputMedia\InputMediaPhoto;
use Illuminate\Support\Facades\Crypt;
use Telegram\Bot\Laravel\Facades\Telegram;
class TelegramSendDone extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:send-report-done';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            //Get general data with status (Fixed, InPlanning, Specialist)
            $data = Report::select('id', 'pothole_id', 'description', 'lat', 'lng', 'member_id', 'commune_id', 'additional_location', 'is_posted', 'created_at', 'is_send', 'is_mt_send');
            $data = $data->whereIn('status_id', [4,5,7]);
            // $data = $data->orderBy('id', 'desc')->whereDate('created_at', Carbon::today())->where('is_done_send', 0)->get();
            $data = $data->orderBy('id', 'desc')->where('is_done_send', 0)->get();
            // To get data with status (Fixed, InPlanning)
            $reports = Report::select('id', 'pothole_id', 'description', 'lat', 'lng', 'member_id', 'commune_id', 'additional_location', 'is_posted', 'created_at', 'is_send', 'is_mt_send');
            $reports = $reports->whereIn('status_id', [4]);
            $reports = $reports->orderBy('id', 'desc')->where('is_report_send', 0)->get();
            
            // Send To MPWT News
            foreach($reports as $report){
                //$this->postMPWTNews($report->id);
            }
            // Send to telegram notification
            foreach($data as $row){
                $this->sendTelegram($row->id);
            }
         
            var_dump('success');die;
    }

    function showReport($id = 0){
        $data = Report::select('id', 'status_id', 'pothole_id', 'description', 'lat', 'lng', 'member_id', 'commune_id', 'additional_location', 'is_posted', 'created_at')
        ->with('status:id,name,kh_name')
        ->with([
            'ru:id,user_id',
            'ru.user:id,name,avatar,email,phone',
            'files'=>function($query){
                $query->select('id', 'report_id', 'uri', 'lat', 'lng', 'is_accepted', 'is_fixed_image')->orderBy('id', 'DESC')->orderBy('is_fixed_image', 'DESC');
            },
            'comments'=>function($query){
                $query->select('id', 'report_id', 'creator_id', 'comment', 'created_at')
                ->with('commenter:id,name,avatar,email,phone')->orderBy('id', 'DESC')->get();
            },
            'pothole'=>function($query) {
                $query->select('id', 'created_at', 'point_id', 'code', 'maintence_id')
                ->with([
                     'comments'=>function($query){
                        $query->select('id', 'pothole_id', 'creator_id', 'comment', 'created_at')
                        ->with('commenter:id,name,avatar,email,phone')->orderBy('id', 'DESC')->get();
                    },

                    'location:id,pothole_id,village_id,commune_id,district_id,province_id,lat,lng',
                    'location.village:id,name,code',
                    'location.commune:id,name,code',
                    'location.district:id,name,code',
                    'location.province:id,name,code',


                    'point:id,pk_id,meter',
                    'point.pk:id,code,road_id',
                    'point.pk.road:id,name,start_point,end_point',

                    'statuses'=>function($query){
                        $query->select('id', 'pothole_id', 'status_id', 'mt_id', 'updater_id', 'comment', 'updated_at')
                        ->with([
                            'status:id,name',
                            'updater:id,name,avatar,email,phone',
                            'mt:id,user_id',
                            'mt.user:id,name,avatar,email,phone'
                        ])
                        ->orderBy('id', 'DESC')->get();
                    },
                    'files:uri,lat,lng'
                   
                ]);
            },

            'statuses'=>function($query) {
                $query->select('id', 'report_id', 'status_id', 'mt_id', 'updater_id', 'comment', 'updated_at')
                ->with([
                    'status'=>function($query) {
                        $query->select('id', 'name');
                        $query->addSelect('mo as permissions');
                    },
                    'updater:id,name,avatar,email,phone',
                    'mt:id,user_id',
                    'mt.user:id,name,avatar,email,phone'
                ])
                ->orderBy('id', 'DESC')->get();
            }
        ])->withCount('comments as num_of_comments');

       
        $data = $data->where('is_posted', 1)
        ->find($id);

        if($data){
           
            return $data;
        }else{
           return response()->json([
                'status_code'   =>  403,
                'errors'        =>  ['message'  =>  ['no data found or invalid access']]
            ], 403);
        }
    }

    public function sendTelegram($report_id){
        $data = $this->showReport($report_id);
        // Telegrame Chat
        $encrypted = Crypt::encryptString($report_id);
        $url = env('WEB_URL', '');
        $baseurl = $url.$encrypted.'?s='.$data->status_id;
        $description    = $data->description ?? '';
        $reporter       = $data->ru->user->name ?? '';
        $report_date    = $data->created_at ?? '';
        //check road
        if($data->pothole){
            if($data->pothole->point){
                if($data->pothole->point->pk){
                    if($data->pothole->point->pk->road){
                        $res_road = 'ផ្លូវ'.$data->pothole->point->pk->road->name ?? '';
                    }else{
                        $res_road = '';
                    }
                }else{
                        $res_road = '';
                    }
            }else{
                    $res_road = '';
                }

        }else{
                $res_road = '';
            }

        if($data->pothole){
            if($data->pothole->point){
                if($data->pothole->point->pk){
                    $res_pk = 'PK'.$data->pothole->point->pk->code .'+'. $data->pothole->point->meter  . '-';
                }else{
                    $res_pk = '';
                }
            }else{
                $res_pk =  '';
            }
            if($data->pothole->location){
                if($data->pothole->location->village){
                    $res_village = $data->pothole->location->village->name .'';
                }else{
                    $res_village = '';
                    $res_commune = '';
                    $res_district = '';
                    $res_province = '';
                }
                if($data->pothole->location->commune){
                    $res_commune = $data->pothole->location->commune->name .' ';
                }else{
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
                }
                if($data->pothole->location->district){
                    $res_district = $data->pothole->location->district->name .' ';
                }else{
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
                }
                if($data->pothole->location->province){
                    $res_province = $data->pothole->location->province->name;
                }else{
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
                }
            }else{
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
            }
        }else{
            $res_village = '';
            $res_commune = '';
            $res_district = '';
            $res_province = '';
        }
       
        $road           = $res_road;
        $pk             = $res_pk ;
        $location = $res_village . $res_commune . $res_district . $res_province;
        $status_name    = $data->status->kh_name ;
        $mt_status         = $data->statuses[0]->updater->name;
        $status_date    = $data->statuses[0]->updated_at;
        $comment        = $data->statuses[0]->comment;
        $mt_id             = $data->statuses[0]->updater->id;
        if($data->status->id == 4){
            $emoji = ' &#9989;';
        }elseif($data->status->id == 5){
            $emoji = '&#128221;';
        }else{
            $emoji = '&#10548;';
        }

        $text = "\n"
        ."$emoji $status_name ដោយ $mt_status នៅថ្ងៃទី: $status_date\n"
        ."$comment \n"
        ."- ទីតាំង:$road $pk $location \n"
        ."\n"
        ."- អ្នករាយការណ៍: $reporter \n"
        . "- នៅថ្ងៃទី: $report_date \n"
        . "- មតិសំណេីរមុនដោះស្រាយ: $description \n"
        ."\n"
        . "$baseurl " ;
        if($mt_id != 13299){
            //To Update TElegrame
            $this->UpdateReport($report_id);
            $data = $this->showReport($report_id);
            // Telegram::sendMessage([
            //     'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
            //     'parse_mode' => 'HTML',
            //     'text' => $text
            // ]);
                $bot = new \TelegramBot\Api\BotApi(env('TELEGRAM_BOT_TOKEN', ''));
                $media = new \TelegramBot\Api\Types\InputMedia\ArrayOfInputMedia();
                $i = 0;
                foreach($data->files as $row){
                    if($data->status_id == 4){
                        if($row->is_fixed_image ==1 ){
                            $media->addItem(new InputMediaPhoto('https://file.mpwt.gov.kh/'.$row->uri,$text, 'HTML', 'HTML'));
                        }else{
                            $media->addItem(new InputMediaPhoto('https://file.mpwt.gov.kh/'.$row->uri));
                        }
                    }else{
                        if($i++ == 0){
                            $media->addItem(new InputMediaPhoto('https://file.mpwt.gov.kh/'.$row->uri,$text, 'HTML', 'HTML'));
                        }else{
                            $media->addItem(new InputMediaPhoto('https://file.mpwt.gov.kh/'.$row->uri));
                        }
                    }
                    
                }
                
                $bot->sendMediaGroup(env('TELEGRAM_CHANNEL_ID', ''), $media);
        }
        
    }

    function UpdateReport($report_id){
        $data               = Report::find($report_id);
        $data->is_done_send   = 1;
        $data->save();
    }

   function UpdateReportSendToApi($report_id){
        $data                   = Report::find($report_id);
        $data->is_report_send     = 1;
        $data->save();
    }

    public function postMPWTNews($report_id){
        $data = $this->showReport($report_id);
        // Telegrame Chat
        $encrypted = Crypt::encryptString($report_id);
        $url = env('WEB_URL', '');
        $baseurl = $url.$encrypted.'?s='.$data->status_id;
        $description    = $data->description ?? '';
        $reporter       = $data->ru->user->name ?? '';
        $report_date    = $data->created_at ?? '';
        

        //check road
        if($data->pothole){
            if($data->pothole->point){
                if($data->pothole->point->pk){
                    if($data->pothole->point->pk->road){
                        $res_road = 'ផ្លូវ'.$data->pothole->point->pk->road->name ?? '';
                    }else{
                        $res_road = '';
                    }
                }else{
                        $res_road = '';
                    }
            }else{
                    $res_road = '';
                }

        }else{
                $res_road = '';
            }

        if($data->pothole){
            if($data->pothole->point){
                if($data->pothole->point->pk){
                    $res_pk = 'PK'.$data->pothole->point->pk->code .'+'. $data->pothole->point->meter  . '-';
                }else{
                    $res_pk = '';
                }
            }else{
                $res_pk =  '';
            }
            if($data->pothole->location){
                if($data->pothole->location->village){
                    $res_village = $data->pothole->location->village->name .'';
                }else{
                    $res_village = '';
                    $res_commune = '';
                    $res_district = '';
                    $res_province = '';
                }
                if($data->pothole->location->commune){
                    $res_commune = $data->pothole->location->commune->name .' ';
                }else{
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
                }
                if($data->pothole->location->district){
                    $res_district = $data->pothole->location->district->name .' ';
                }else{
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
                }
                if($data->pothole->location->province){
                    $res_province = $data->pothole->location->province->name;
                }else{
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
                }
            }else{
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
            }
        }else{
            $res_village = '';
            $res_commune = '';
            $res_district = '';
            $res_province = '';
        }
       
        $road           = $res_road;
        $pk             = $res_pk ;
        $location = $res_village . $res_commune . $res_district . $res_province;
        $status_name    = $data->status->kh_name ;
        $mt_status      = $data->statuses[0]->updater->name;
        $status_date    = $data->statuses[0]->updated_at;
        $comment        = $data->statuses[0]->comment;
        $mt_id             = $data->statuses[0]->updater->id;

        //To set emoji
        if($data->status->id == 4){
            $emoji = ' &#9989;';
        }elseif($data->status->id == 5){
            $emoji = '&#128221;';
        }else{
            $emoji = '&#10548;';
        }

        $title = "$emoji $status_name ដោយ $mt_status ''$comment''"
        ."ករណីសំណេីរលេីទីតាំង: $road $pk $location ''$description'' "
        ;

        $text = "\n"
        ."$status_name ដោយ $mt_status នៅថ្ងៃទី: $status_date\n"
        ."$comment \n"
        ."- ទីតាំង:$road $pk $location \n"
        ."\n"
        ."- អ្នករាយការណ៍: $reporter \n"
        . "- នៅថ្ងៃទី: $report_date \n"
        . "- មតិសំណេីរមុនដោះស្រាយ: $description \n"
        ;
        // To get image fixed
        $image_url_fix = '';
        $image_url_not_fix = '';
        foreach($data->files as $row){
            if($row->is_fixed_image ==1 ){
                $image_url_fix = $row->uri;
            }else{
                $image_url_not_fix = $row->uri;
            }
        }
        // Condition to get image url
        $image_url = '';
        if($image_url_fix != ''){
            $image_url = $image_url_fix;
        }else{
            $image_url = $image_url_not_fix;
        }
        // End of Condition

        $encrypted = Crypt::encryptString($image_url);
        if($mt_id != 13299){
            if($data->status->id == 4){
            $this->UpdateReportSendToApi($report_id);
            // set post fields
            $post = [
                'title'         => $title,
                'news_date'     => \Carbon\Carbon::parse($status_date)->format('Y-m-d'),
                'link'          => $baseurl,
                // 'image'         => 'http://file.mpwt.gov.kh/image?token='.$encrypted,
                'image'         => 'http://file.mpwt.gov.kh/'.$image_url,
                'video'         => '',
                'long_content'  => $text,
            ];

            $ch = curl_init(env('MPWT_NEWS_API', ''));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            // execute!
            $response = curl_exec($ch);
            // close the connection, release resources used
            curl_close($ch);
            // do anything you want with your response
            var_dump($response);
            }
        }
    }
}
