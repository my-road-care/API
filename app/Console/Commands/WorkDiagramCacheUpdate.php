<?php

namespace App\Console\Commands;

use App\Jobs\UpdateWorkDiagramCache;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class WorkDiagramCacheUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'work-diagram-cache:update {--tid= : Thread ID} {pid : Project ID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Work Diagram Cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pid = $this->argument('pid');
        dispatch(new UpdateWorkDiagramCache($pid));
    }
}
