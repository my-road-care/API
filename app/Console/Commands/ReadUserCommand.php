<?php

namespace App\Console\Commands;

use App\Jobs\ReadUserJob;
use Illuminate\Console\Command;

class ReadUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:read {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        sleep(10);
        dispatch(new ReadUserJob($this->argument('id')));
    }
}
