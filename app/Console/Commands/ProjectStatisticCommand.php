<?php

namespace App\Console\Commands;

use App\Jobs\ProjectStatisticJob;
use Illuminate\Console\Command;

class ProjectStatisticCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project-statistic:get {projectId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Project Statistic';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new ProjectStatisticJob($this->argument('projectId')));
        
    }
}
