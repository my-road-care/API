<?php
namespace App\Enum; 

class Status {
    const Pendding      = 1;
    const Repairings    = 2;
    const Decline       = 3;
    const Fixed         = 4;
    const Planning      = 5;
    const Done          = 6;
    const Specialists   = 7;
}