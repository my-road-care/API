<?php
namespace App\Enum; 

class Progress {

    const NotStarted            = 1;
    const UnderConstruciton     = 2;
    const Finished              = 3;
    const Checked               = 4;
    const Tested                = 5;
   
}
