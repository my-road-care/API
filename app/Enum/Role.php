<?php
namespace App\Enum; 

class Role {
    const SuperAdmin             = 1;
    const MO                     = 2;
    const MT                     = 3;
    const PotholeManager         = 8;
    const PotholeUser            = 5;
    const SupervisionManager     = 6;
    const SupervisionUser        = 7;
    const RU                     = 4;
    const SVHeadOfCommittee      = 9;
    const Minister               = 10;
    const SVInfoEntry            = 11;
    const SpecialInspector       = 12;
   
}
