<?php

    function responseSuccess($message , $data=[] , $code=200){
        return response()->json([
            'statusCode'    => $code,
            'message'       => $message,
            'data'          => $data
        ],$code);
    }

    function responseError($message, $code=400){
        return response()->json([
            'statusCode'    => $code,
            'message'       => $message
        ],$code);
    }
