<?php

namespace App\Http\Resources\Report;

use Illuminate\Http\Resources\Json\Resource;
// Encryt
use Illuminate\Support\Facades\Crypt;
class ReportResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $partern = parent::toArray($request);

        $encrypted = Crypt::encryptString($this->id);
        $url = env('WEB_URL', '');
        $baseurl = $url.$encrypted;
        $partern['sharebled_link'] = $baseurl;

        if($this->pothole){
            if($this->pothole->point){
                if($this->pothole->point->pk){
                    if($this->pothole->point->pk->road){
                        $res_road = 'ផ្លូវ'.$this->pothole->point->pk->road->name ?? '';
                    }else{
                        $res_road = '';
                    }
                }else{
                        $res_road = '';
                    }
            }else{
                $res_road = '';
            }

        }else{
            $res_road = '';
        }

        if($this->pothole){
            if($this->pothole->point){
                if($this->pothole->point->pk){
                    $res_pk = 'PK'.$this->pothole->point->pk->code.'+'. $this->pothole->point->meter . '-';
                }else{
                  $res_pk = '';  
                }
                
            }else{
                $res_pk = '';
            }

            if($this->pothole->location){
                if($this->pothole->location->village){
                    $res_village = $this->pothole->location->village->name.'';
                }else{
                   // $res_pk = '';
                    $res_village = '';
                    $res_commune = '';
                    $res_district = '';
                    $res_province = '';
                }
                if($this->pothole->location->commune){
                    $res_commune = $this->pothole->location->commune->name.' ';
                }else{
                // $res_pk = '';
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
                }
                if($this->pothole->location->district){
                    $res_district =  $this->pothole->location->district->name.' ';
                }else{
                    // $res_pk = '';
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
                }
                if($this->pothole->location->province){
                    $res_province =  $this->pothole->location->province->name;
                }else{
                    // $res_pk = '';
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
                }
            }else{
                // $res_pk = '';
                $res_village = '';
                $res_commune = '';
                $res_district = '';
                $res_province = '';
            }
        }else{
            // $res_pk = '';
            $res_village = '';
            $res_commune = '';
            $res_district = '';
            $res_province = '';
        }
        $reporter = $this->ru->user->name ?? "";
        $location =$res_road . $res_pk . $res_village . $res_commune . $res_district . $res_province;
        $partern['sharebled_link_full'] ='របាយការណ៍​ថ្មី/n' 
                                        .$this->description
                                        .'/n'
                                        .'- អ្នករាយការណ៍:'. $reporter
                                        .'/n'
                                        .'- ស្ថានភាព:'.$this->status->kh_name 
                                        .'/n'
                                        .'- កាលបរិច្ឆេទ:'.$this->created_at
                                        .'/n'
                                        .'- ទីតាំង:'.$location
                                        .'/n'
                                        .$baseurl;
        // To get location Empty
        if($this->pothole){
            if($this->pothole->location){
                if(!$this->pothole->location->province_id){
                    $partern['pothole']['location']['province_id'] = '25';
                    $partern['pothole']['location']['province'] = ['id' => 25, 'name' => 'ត្បូងឃ្មំុ', 'code' => 25 ];
                }
            }   
        }
        return $partern;
    }

     public function with($request)
    {
        return [
            'version' => '1.0',
            'success' => true,
        ];
    }
}


