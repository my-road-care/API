<?php

namespace App\Http\Resources\Report;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Http\Resources\Report\ReportResource;

class ReportCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function __construct($resource)
    {
        
        $this->current_page = $resource->currentPage();

        $this->first_page_url = $resource->url($resource->currentPage());

        $this->last_page = $resource->lastPage();
        $this->last_page_url = $resource->url($resource->lastPage());
        $this->next_page_url = $resource->nextPageUrl();
        $this->path = $resource->url('');
        $this->per_page = $resource->perPage();
        $this->total = $resource->total();    

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }
    public function toArray($request)
    {   


        $partern = ReportResource::collection($this->collection);
        

        return [
            'data' => $partern,
            'current_page' => $this->current_page,
            "first_page_url" => $this->first_page_url,
            "last_page" => $this->last_page,
            'last_page_url' => $this->last_page_url,
            'next_page_url' => $this->next_page_url,
            'per_page' => $this->per_page,
            'path' => $this->path,
            'total' => $this->total,
           
        ];
       
    }
}