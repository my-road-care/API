<?php

namespace App\Http\Resources\Report;

use Illuminate\Http\Resources\Json\Resource;
// Encryt
use Illuminate\Support\Facades\Crypt;
use App\Model\Setting\Status as Model;
class MapReportResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $partern = parent::toArray($request);
        $status = Model::find($this->status_id);
        $partern['status_name'] = $status->name;
        
        return $partern;
    }

     public function with($request)
    {
        return [
            'version' => '1.0',
            'success' => true,
        ];
    }
}


