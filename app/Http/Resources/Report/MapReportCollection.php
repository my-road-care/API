<?php

namespace App\Http\Resources\Report;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Http\Resources\Report\MapReportResource;

class MapReportCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function __construct($resource)
    {

        parent::__construct($resource);
    }
    public function toArray($request)
    {   


        $partern = MapReportResource::collection($this->collection);
        

        return [
            'data' => $partern
        ];
       
    }
}