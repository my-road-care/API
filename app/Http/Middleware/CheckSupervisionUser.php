<?php

namespace App\Http\Middleware;

use App\Enum\Role;
use Closure;

class CheckSupervisionUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = auth()->user()->type_id;
        if(in_array($role, [Role::SuperAdmin, Role::SVHeadOfCommittee, Role::Minister, Role::SupervisionManager, Role::SupervisionUser, Role::SVInfoEntry, Role::SpecialInspector])){
            return $next($request);
        }else{
            return response()->json([
                'status_code'   =>  403,
                'message'  =>  'លោកអ្នកមិនមានសិទ្ធជា Supervision ទេ សូមអរគុណ!'
            ], 403);
        }
    }
}
