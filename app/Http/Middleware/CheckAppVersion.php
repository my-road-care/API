<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;

class CheckAppVersion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Check it it is not method get
        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            $user = JWTAuth::parseToken()->authenticate(); 
            if($user){
                if ($user->mo || $user->mt) {
                    return response()->json([
                        'status_code'   =>  403,
                        'errors'        =>  ['message'  =>  ['លោកអ្នកកំពុងប្រើប្រាស់កម្មវិធីជំនាន់ចាស់ សូមទាញយកកម្មវិធីជំនាន់ថ្មី ពីក្នុង App Store ឬ Play Store ដើម្បីបន្តការប្រើប្រាស់ សូមអរគុណ!']]
                    ], 403);
                }
            }
        }

        return $next($request);
    }
}
