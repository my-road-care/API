<?php

namespace App\Http\Middleware;

use App\Enum\Role;
use Closure;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user()->type_id;
        if($user == Role::SuperAdmin){
            return $next($request);            
        }else{
            abort(403, "មិនមានសិទ្ធីក្នុងការប្រើប្រាស់");
        }        
    }
}
