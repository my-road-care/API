<?php

namespace App\Http\Middleware;

use App\Enum\Role;
use Closure;

class AuthorityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $role = auth()->user()->type_id;
        if(in_array($role, [Role::SuperAdmin, Role::PotholeManager])){
            return $next($request);
        }else{
            return response()->json([
                'status_code'   =>  403,
                'message'  =>  'លោកអ្នកមិនមានសិទ្ធទេ!'
            ], 403);
        }
        return $next($request);
    }
}
