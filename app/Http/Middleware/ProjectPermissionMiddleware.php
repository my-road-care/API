<?php

namespace App\Http\Middleware;

use App\Enum\Role;
use App\Model\Supervision\Projects\ProjectWorkingGroup;
use Closure;

class ProjectPermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param = $request->route('projectId');
        $user = auth()->user();
        if($param){
            if(in_array($user->type_id, [Role::SuperAdmin, Role::SVHeadOfCommittee, Role::Minister, Role::SupervisionManager])){
                return $next($request);                
            }else{
                $workingGroup = ProjectWorkingGroup::where([
                    'project_id' => $param,
                    'user_id'    => $user->id
                ])->first();

                if($workingGroup){
                    return $next($request);
                }else{
                    abort(400, "រកគម្រោងមិនឃើញ");
                }
            }
        }else{
            return $next($request);
        }
    }
}
