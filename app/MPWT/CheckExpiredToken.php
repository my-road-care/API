<?php

namespace App\MPWT;
use App\Model\User\Main as User;

use Illuminate\Http\Request;
use JWTAuth;
use Illuminate\Support\Carbon;

class CheckExpiredToken{
    
    /** function forwar normal file */
    public static function check(){
        $token = '';
        $user = JWTAuth::parseToken()->authenticate();
        $data_user = User::find($user->id);
        if($data_user->expired_at){
            
            $now = Carbon::now();
            $lengthDay =  Carbon::parse($user->expired_at)->diffInDays($now);
            if( $lengthDay  > 15){
                return  $token;
                
            }else{
                //Find expired at
                $expired_date = Carbon::now()->addMinutes(env('JWT_TTL', '43200'))->format('Y-m-d H:i:s');
                $existing_user = User::find($user->id);
                $existing_user->expired_at = $expired_date;
                $existing_user->save();
                $token = JWTAuth::refresh();
                return  $token;
            }
        }else{
                $expired_date   = Carbon::now()->addMinutes(env('JWT_TTL', '43200'))->format('Y-m-d H:i:s');
                $existing_user  = User::find($user->id);
                $existing_user->expired_at = $expired_date;
                $existing_user->save();
                $token = JWTAuth::refresh();
                return  $token;
        }
    }


    
  
}
