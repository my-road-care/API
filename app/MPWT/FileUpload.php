<?php

namespace App\MPWT;


use Illuminate\Http\Request;
use Image;

class FileUpload{
    


    public static function forwardFile($file, $incommingFolder = 'unknown'){
        
        $post = [
            'projectKey' => env('PROJECT_KEY'),
            'projectSecret' => env('PROJECT_SECRET'),
            'file' => $file,
            'folder' => 'RoadCare',
            'fileType' => 'Image',
            'documentTypeId' => '2',
            'isMultipleFileSize' => '1'
        ];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('FILE_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => array(
            "Authorization: Basic ".env('FILE_BASIC_AUTH')
            ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response,true);

        
        return $response['options']['L']['uri'];



        
        // $dataFile = explode(',', $file);
        // $ini =substr($dataFile[0], 11);
        // $ext = explode(';', $ini);
        

        // //extract data from the post
        // //set POST variables
        // $folder = 'public/uploads/'.env('FILE_FOLDER').'/'.$incommingFolder.'/'; 
        // $fileName = uniqid().'.'.$ext[0]; 
        // $fields = array(
        //     'file' => urlencode($file), 
        //     'folder'=> $folder,
        //     'fileName' => urlencode($fileName)
        // );
        // //return $folder.$fileName; 
        // //url-ify the data for the POST
        // $fields_string = '';
        // foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        // rtrim($fields_string, '&');

        // //open connection
        // $ch = curl_init();

        // //set the url, number of POST vars, POST data
        // curl_setopt($ch,CURLOPT_URL, env('FILE_URL').'/api/upload/64base');
        // curl_setopt($ch,CURLOPT_POST, count($fields));
        // curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

        // //execute post
        // $result = curl_exec($ch);

        // //close connection
        // curl_close($ch);

        // //Check if file exist 
        // return $folder.$fileName; 

    }


    public static function forwardImage2($file = '', $folder = 'public/roadcare/unknown', $sizes = ""){
        

        $post = [
            'projectKey' => env('PROJECT_KEY'),
            'projectSecret' => env('PROJECT_SECRET'),
            'file' => $file,
            'folder' => 'RoadCare',
            'fileType' => 'Image',
            'documentTypeId' => '2',
            'isMultipleFileSize' => '1'
        ];
        $curl = curl_init();
  
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('FILE_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => array(
            "Authorization: Basic ".env('FILE_BASIC_AUTH')
            ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);

        $response = json_decode($response,true);
        
        return $response['fileURL']['uri'];

        // if($file !="") {
            
        //     $dataFile = explode(',', $file);

        //     $ini =substr($dataFile[0], 11);
        //     $ext = explode(';', $ini);

        //     if(isset($ext[0])){
                
        //         $ext = strtolower($ext[0]);
        //         if($ext == "png" || $ext == "jpg" || $ext == "jpeg" ){
        //              //extract data from the post
        //             //set POST variables

        //             $fileName = uniqid(); 
        //             $fields = array(
        //                 'file' => urlencode($file), 
        //                 'folder'=> $folder,
        //                 'fileName' => urlencode($fileName), 
        //                 'sizes' => $sizes
        //             );
        //             //return $folder.$fileName; 
        //             //url-ify the data for the POST
        //             $fields_string = '';
        //             foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        //             rtrim($fields_string, '&');

        //             //open connection
        //             $ch = curl_init();

        //             //set the url, number of POST vars, POST data
        //             curl_setopt($ch,CURLOPT_URL, env('FILE_URL').'/api/upload/image/64base');
        //             curl_setopt($ch,CURLOPT_POST, count($fields));
        //             curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

        //             //execute post
        //             $result = curl_exec($ch);

        //             //close connection
        //             curl_close($ch);

        //             //Check if file exist 
        //             return $folder.$fileName.'.'.$ext; 

        //         }else{
        //             return ""; 
        //         }
        //     }else{
        //         return "";
        //     }  
        // }else{
        //     return "";
        // }    
           

    }

    
    public static function forwardImageResize($file = '', $folder = 'public/roadcare/unknown', $sizes = ""){
        $post = [
            'projectKey' => 'TEST',
            'projectSecret' => '3e0f5d9e87e057a06c58b73f8e881d8c85665e55',
            'file' => $file,
            'folder' => 'RoadCare',
            'fileType' => 'Image',
            'documentTypeId' => '2',
            'isMultipleFileSize' => '1'
        ];
        $curl = curl_init();
  
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dev-file.mpwt.gov.kh/v3/api/upload/image/64base/resize",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => array(
            "Authorization: Basic ZmlsZXVzZXI6RiFMRVdRMTI="
            ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);

        $response = json_decode($response,true);
    
        return $response['options']['L']['uri'];
        


        // if($file !="") {
            
        //     $dataFile = explode(',', $file);

        //     $ini =substr($dataFile[0], 11);
        //     $ext = explode(';', $ini);

        //     if(isset($ext[0])){
                
        //         $ext = strtolower($ext[0]);
        //         if($ext == "png" || $ext == "jpg" || $ext == "jpeg" ){
        //              //extract data from the post
        //             //set POST variables

        //             $fileName = uniqid(); 
        //             $fields = array(
        //                 'file' => urlencode($file), 
        //                 'folder'=> $folder,
        //                 'fileName' => urlencode($fileName), 
        //                 'sizes' => $sizes
        //             );
                    
        //             //return $folder.$fileName; 
        //             //url-ify the data for the POST
        //             $fields_string = '';
        //             foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        //             rtrim($fields_string, '&');

        //             //open connection
        //             $ch = curl_init();

        //             //set the url, number of POST vars, POST data
        //             curl_setopt($ch,CURLOPT_URL, env('FILE_URL').'/api/upload/image/64base/resize');
        //             curl_setopt($ch,CURLOPT_POST, count($fields));
        //             curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

        //             //execute post
        //             $result = curl_exec($ch);

        //             //close connection
        //             curl_close($ch);

        //             //Check if file exist 
        //             return $folder.$fileName.'.'.$ext; 

        //         }else{
        //             return ""; 
        //         }
        //     }else{
        //         return "";
        //     }  
        // }else{
        //     return "";
        // }    
    }
   

    public static function forwardVoice($project, $file, $folder){

        $payload = [
            'projectKey' => 'TEST',
            'projectSecret' => '3e0f5d9e87e057a06c58b73f8e881d8c85665e55',
            'file' => $file,
            'folder' => $folder
        ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            
                CURLOPT_URL => "https://dev-file.mpwt.gov.kh/v3/api/attach/voice",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $payload,
            )
        );

        $response = curl_exec($curl);
        curl_close($curl);
        return   json_decode( $response, true );
    }


  
}
