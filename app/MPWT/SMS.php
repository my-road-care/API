<?php

namespace App\MPWT;

use App\Http\Controllers\Controller;
use Twilio\Rest\Client;
use Nexmo; 

class SMS extends Controller
{

    public static function sendSMSTwilio($receivNumber = '+855077677599', $message = 'Hello from MPWT'){
        $client = new Client(env('TWILIO_SID'), env('TWILIO_TOKEN'));
        $sms = $client->messages->create($receivNumber, ['from' => 'Road Care', 'body' => $message]);
        return $sms; 
    }

    public static function sendSMSNexmo($receivNumber = '85577677599', $message = 'Hello from MPWT'){
        $sms = $message = Nexmo::message()->send([
            'to'   => $receivNumber,
            'from' => env('APP_NAME'),
            'text' => $message
        ]);
        return $sms; 
    }

    public static function sendSMSPlasgate($receivNumber, $message){
        $remove_0 = "855".(int)$receivNumber;
        $number = intval($remove_0);
        $ch = curl_init('https://api.plasgate.com/send?token=kBOtNoY3ewAmiLZ4XIPXHCan0QiD7f&text='.$message.'&phone='.$number.'&senderID=RoadCare');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

    }

    public static function sendSMS($receivNumber = '077677599', $message = 'Hello from MPWT'){
        if(preg_match("/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/", $receivNumber)){
            if(env('SMS_CHOICE') == 'TWILIO'){
                if(env('TWILIO_SID') !== null && env('TWILIO_TOKEN') !== null ){
                    self::sendSMSTwilio('+855'.substr($receivNumber, 1), $message); 
                    return ['status'=>'success', 'message'=>'Message has been sent']; 
                }else{
                    return ['status'=>'error', 'message'=>'Invalid TWILIO config']; 
                }
            }else if(env('SMS_CHOICE') == 'NEXMO'){
                if(env('NEXMO_KEY') !== null  && env('NEXMO_SECRET') !== null ){
                    self::sendSMSNexmo('855'.substr($receivNumber, 1), $message); 
                    return ['status'=>'success', 'message'=>'Message has been sent']; 
                }else{
                    return ['status'=>'error', 'message'=>'Invalid NEXMO config']; 
                }
            
            }
            else if(env('SMS_CHOICE') == 'PLASGATE'){
                self::sendSMSPlasgate($receivNumber, $message);
            }
            else{
                return ['status'=>'error', 'message'=>'No SMS provider']; 
            }
        }else{
           return ['status'=>'error', 'message'=>'Invalid number']; 
        }

            
    }
 
}
