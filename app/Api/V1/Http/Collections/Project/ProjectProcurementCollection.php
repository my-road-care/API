<?php

namespace App\Api\V1\Http\Collections\Project;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectProcurementCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $paginations;

    public function __construct($resource)
    {
        $this->paginations = [
            'total'         => $resource->total(),
            'count'         => $resource->count(),
            'per_page'      => $resource->perPage(),
            'current_page'  => $resource->currentPage(),
            'total_pages'   => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }
     
    public function toArray($request)
    {
        $projectProcurement = [];
        foreach($this -> collection as $row){
            $projectProcurement [] = [
                "id"                => $row -> id,
                "creator"           => $row -> creator ? $row -> creator -> name : null,
                "title"             => $row -> title,
                "docu_code"         => $row -> docu_code,
                "file"              => $row -> image_uri,
                "description"       => $row -> description,
                "date"              => $row->date ? date('Y-m-d', strtotime($row->date)) : null

            ];
        }
        return [
            'data' => $projectProcurement,
            'pagination'    => $this->paginations
        ];
    }
}
