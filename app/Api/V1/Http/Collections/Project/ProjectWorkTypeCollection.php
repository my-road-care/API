<?php

namespace App\Api\V1\Http\Collections\Project;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectWorkTypeCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

     
    public function toArray($request)
    {
        $road = [];
        $art = [];

        foreach($this -> collection as $row){
            if($row->project_work_id == 1){
                $road [] = [
                    "id"                => $row -> id,
                    "location"          => $row -> provinces ? $row -> provinces -> name : null,
                    "slab_type"         => $row -> buildTypes ? $row -> buildTypes -> name : null,
                    "height"            => $row -> height,
                    "length"            => $row -> length,
                    "width"             => $row -> width,
                    "description"       => "ពី គ.ម ". $row -> from_meter ." ដល់ គ.ម ".$row -> to_meter,
                ];
            }else{
                $art [] = [
                    "id"                => $row -> id,
                    "location"          => $row -> provinces ? $row -> provinces -> name : null,
                    "slab_type"         => $row -> buildTypes ? $row -> buildTypes -> name : null,
                    "height"            => $row -> height,
                    "length"            => $row -> length,
                    "width"             => $row -> width,
                    "description"       => "ពី គ.ម ". $row -> from_meter ." ដល់ គ.ម ".$row -> to_meter,
                ];
            }
            
        }
        return [
            "road" => $road,
            "art"  => $art,
        ];
    }
}
