<?php

namespace App\Api\V1\Http\Collections\Project;

use App\Model\Supervision\Setup\ProgressStatus;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectImageCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    // private $paginations;
    // public function __construct($resource)
    // {
    //     $this->paginations = [
    //         'total' => $resource->total(),
    //         'count' => $resource->count(),
    //         'per_page' => $resource->perPage(),
    //         'current_page' => $resource->currentPage(),
    //         'total_pages' => $resource->lastPage()
    //     ];

    //     $resource = $resource->getCollection();

    //     parent::__construct($resource);
    // }

    public function toArray($request)
    {
        $projectImages = [];
        foreach($this -> collection as $row){
            // $status = '';
            // if($row->progress_status_id == 2){
            //     $status = "មុនពេលសាងសង់";
            // }elseif($row->progress_status_id == 3){
            //     $status = "កំពុងសាងសង់";
            // }else{
            //     $status = "បញ្ចប់ការសាងសង់";
            // }
            $station = $row->side ? $row->side->station ? $row->side->station->station ? $row->side->station->station : null : null : null;
            $pk = $station->point ? $station->point->pk : null;
            
            $from = ProgressStatus::select('id','kh_name as name','icon','color')->where('id', $row->progress->id -1)->first();
            $projectImages [] = [
                "id" => $row->id,
                "pk" => "PK ".$pk."+".$station->station_num,
                "side" =>  $row->side ? $row->side->side->abrv : null,
                "from" => $from,
                "to"   => $row->progress,
                "work" =>  $row->side ? $row->side->station ? $row->side->station->work ? $row->side->station->work->name : null : null : null,
                "comment" => $row->comment,
                "updater" => $row->updater,
                "created_at" => $row->created_at ? date('d-m-Y', strtotime($row->created_at)) : null,
                "images" => $row->progressTrx ? $row->progressTrx->images : null
            ];
        }
        return [
            'pictures'       => $projectImages,
            // 'pagination'     => $this -> paginations
        ];
    }
}
