<?php

namespace App\Api\V1\Http\Collections\Project;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TestingReportCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    // private $paginations;
    // public function __construct($resource)
    // {
    //     $this->paginations = [
    //         'total' => $resource->total(),
    //         'count' => $resource->count(),
    //         'per_page' => $resource->perPage(),
    //         'current_page' => $resource->currentPage(),
    //         'total_pages' => $resource->lastPage()
    //     ];

    //     $resource = $resource->getCollection();

    //     parent::__construct($resource);
    // }

    public function toArray($request)
    {
        $testingReports = [];
        foreach($this -> collection as $key => $row){
            $point = $row->side->station->station;
            $testingReports [] = [
                "id"                   => $row -> id,
                "tester"               => $row -> tester,
                "side"                 => $row -> side -> side,
                "created_at"           => $row -> created_at ? date('d-m-Y', strtotime($row -> created_at)) : null,
                "comment"              => $row -> comment,
                "result"               => $row -> result,
                "location"             => "PK ".$point->point->pk . "+" .$point->station_num,
                "standard_test"        => [
                    "id"                 => $row->standardTest ? $row->standardTest->id : null,
                    "testing_method_id"  => $row->standardTest ? $row->standardTest->testing_method_id : null, 
                    "standard"           => $row->standardTest ? $row->standardTest->standard : null,
                    "method"             => $row->standardTest ? $row->standardTest ? $this->parameterFormat($row->standardTest, $row->id) : null : null

                ],
                "work"                 => $row->side ? $row->side->station ? $row->side->station->work : null : null,              
                "images"               => $row -> testingTrx ? $row -> testingTrx -> images : null,
                "is_pass"              => $row->is_pass

            ];
        }
        return [
            'data' => $testingReports,
            // 'pagination' => $this -> paginations
        ];
    }

    private function parameterFormat($parameters, $id = 0){
        $param = [];
        foreach($parameters->method ? $parameters->method->parameters : null as $row){
            $param[] = [
                'id' => $row->id,
                'name' => $row->name,
                'value' => $row->param ? $this->value($row->param, $id) : null,
                'unit'  => $row->unit
            ];
        }
        $resp = [
            'id'            => $parameters->method ? $parameters->method->id : null,
            'name'          => $parameters->method ? $parameters->method->name : null,
            'unit'          => $parameters->method ? $parameters->method->unit : null,
            'parameters'    => $param,
            "place"         => $parameters->method ? $parameters->method->place : null

        ];
        return $resp;
    }

    private function value($values, $id = 0){
        $res = '';
        foreach($values->where('station_work_side_test_id', $id) as $data){
            $res = $data->value;
        }
        return $res;
    }

    private function reportType($data){
        $res = [
            'id'    => $data->id,
            'testing_method_type_id' => $data->type_id,
            'name'  => $data->name
        ];
        return $res;
    }

    private function testingMethod($data){
        $res = [
            'id'                     => $data->id,
            'name'                   => $data->name
        ];
        return $res;
    }
}
