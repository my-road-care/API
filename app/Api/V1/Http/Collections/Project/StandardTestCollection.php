<?php

namespace App\Api\V1\Http\Collections\Project;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StandardTestCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $paginations;
    public function __construct($resource)
    {
        $this->paginations = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $standardTests = [];
        foreach($this -> collection as $key => $row){
            $standardTests [] = [
                "id"            => $row -> id,
                "test_type"     => $row -> testTypes ? $row -> testTypes -> name : null,
                "soil_layer"    => $row -> soilLayer ? $row -> soilLayer -> name : null,
                "standard"      => $row -> standard,
                "created_date"  => $row -> created_at ? date('d-m-Y', strtotime($row -> created_at)) : null,
                "creator"       => $row -> creator ? $row -> creator -> name : null,
                "location"      => "PK".$row -> pk_start . "-" ."PK". $row -> pk_end,
            ];
        }
        return [
            'projects' => $standardTests,
            'pagination' => $this -> paginations
        ];
    }
}
