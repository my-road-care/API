<?php

namespace App\Api\V1\Http\Collections\Project;

use App\Api\V1\Http\Resources\Project\ProjectResource;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Api\V1\Service\CheckPermission;
use App\Model\Supervision\Projects\ProjectWorkingGroup;

class ProjectCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $paginations;
    
    use CheckPermission;

    public function __construct($resource)
    {
        $this->paginations = [
            'total'         => $resource->total(),
            'count'         => $resource->count(),
            'per_page'      => $resource->perPage(),
            'current_page'  => $resource->currentPage(),
            'total_pages'   => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $data = [];
        foreach($this -> collection as $key => $row){

            $data [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'],
                "code"          => $row['code'] ?? "",
                "category"      => $row->category ?? "",
                "road_id"       => $row['road_id'] ?? "",
                "reviewer"      => $this->getPIU($row->organizations ? $row->organizations->where('type_id',2)->first() : null),
                "entity"        => $this->getPIU($row->organizations ? $row->organizations->where('type_id',6)->first() : null),
                "status"        => $row->status ?? "",
                "budget_plans"  => $this->budget($row->budgets, $row->year),
                "updated_at"    => $row->updated_at ? date('Y-m-d H:i:s', strtotime($row -> updated_at)) : null,
                "permission"    => $this->permission($row['id'] ?? ""),
                "is_approved"   => $row->diagram_approver_id ? 1 : 0
            ];

        }

        return [
            'data'          => $data,
            'pagination'    => $this->paginations
        ];

    }

    private function getPIU($entity){
        $res  = [
            "id"               => $entity['id'] ?? "",
            "name"             => $entity['kh_name'] ?? "",
            "abbre"            => $entity['abbre'] ?? "",
        ];
        return $res;
    }

    private function projectOwner($entities){
            $entities  = [
                "id"               => $entities['id'] ?? "",
                "abbre"            => $entities['abbre'] ?? "",
            ];
        return $entities;
    }

    private function permission($projectId = 0){
        $role = $this->checkPermission("project-info-*", 'R', $projectId); 

        // ==========================> Get User Role Project's
        $projectWorkingGroup = ProjectWorkingGroup::with('sys')->where([
            'user_id' => auth()->id(),
            'project_id' => $projectId])->first();

        if($projectWorkingGroup){
            $userProjectRole = $projectWorkingGroup->sys ? $projectWorkingGroup->sys->name : null;
        }else{
            $userProjectRole = null;
        }
        
        return [
                'role' => $userProjectRole, 
                'can'       => [
                    'access_info'       => $role['check'], 
                    'access_technical'  => $this->checkPermission("project-technical-*", 'R', $projectId)['check'], 
                    'access_report'     => $this->checkPermission("project-report-*", 'R', $projectId)['check'], 
                    'delete'            => $this->checkPermission("project-*", 'D', $projectId)['check']
                ]
            ]; 
    }

    private function budget($budget, $year)
    {
        $name = $budget ? $budget->name : null;
        $budYear =  $year ? " (ឆ្នាំ ".$year->year.")" : null;
        return [
            'id'   => $budget->id,
            'name' => $name.$budYear,
            // 'budget_year_id' => $year ? $year->id : null
            // 'year' => $year ? "(ឆ្នាំ ".$year->year.")" : null
        ];
    }
}
