<?php

namespace App\Api\V1\Http\Collections\Project;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectPlanCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    private $paginations;
    public function __construct($resource)
    {
        $this->paginations = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $projectPlans = [];
        foreach($this -> collection as $row){
            $projectPlans [] = [
                "id"                => $row -> id,
                "creator"           => $row -> creator,
                "title"             => $row -> title,
                "docu_code"         => $row -> docu_code,
                "image_uri"         => $row -> image_uri,
                "description"       => $row -> description,
                "date"              => $row->date ? date('Y-m-d', strtotime($row->date)) : null
            ];
        }
        return [
            'plans'       => $projectPlans,
            'pagination' => $this -> paginations
        ];
    }
}
