<?php

namespace App\Api\V1\Http\Collections\Project;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LetterCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    private $paginations;
    public function __construct($resource)
    {
        $this->paginations = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $letters = [];
        foreach($this -> collection as $row){
            $letters [] = [
                "id"                => $row -> id,
                "creator"           => $row -> user ? $row -> user : null,
                "title"             => $row -> title,
                "image_uri"         => $row -> image_uri,
                "letter_type"       => $row -> letter_type ? $row -> letter_type: null,
                "date"              => $row->date ? date('Y-m-d', strtotime($row->date)) : null
            ];
        }
        return [
            'letters'       => $letters,
            'pagination' => $this -> paginations
        ];
    }
}
