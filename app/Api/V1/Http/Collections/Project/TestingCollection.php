<?php

namespace App\Api\V1\Http\Collections\Project;

use App\Model\Supervision\Setup\ProgressStatus;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TestingCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    private $paginations;
    public function __construct($resource)
    {
        $this->paginations = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $data = [];
        foreach($this->collection as $row){
            $data[] = [
                'id'                => $row->id,
                'tester'            => $row->tester ? $row->tester->name : null,
                'pk'                => $row->pk ? $row->pk->pk : null,
                'station_start'     => $row->station_start,
                'station_end'       => $row->station_end,
                'side'              => $row->side,
                'work'              => $row->work,
                'comment'           => $row->comment,
                'method'            => $row->testingStandard ? $row->testingStandard->method : null,
                'created_at'        => $row->created_at ? date('Y-m-d', strtotime($row->created_at)) : null,
                'images'            => $row->images
            ];
        }
        return [
            'testing'        => $data,
            'pagination'     => $this -> paginations
        ];
    }
}
