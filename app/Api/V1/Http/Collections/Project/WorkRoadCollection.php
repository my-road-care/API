<?php

namespace App\Api\V1\Http\Collections\Project;

use Illuminate\Http\Resources\Json\ResourceCollection;

class WorkRoadCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

     
    public function toArray($request)
    {
        $projectWorkTypes = [];
        foreach($this -> collection as $key => $row){
            $projectWorkTypes [] = [
                "id"                => $row['id'] ?? "",
                "location"          => $row->provinces ? $row->provinces->name : null,
                "slab_type"         => $row -> slabTypes ? $row -> slabTypes -> name : null,
                "height"            => $row -> height,
                "length"            => $row -> length,
                "width"             => $row -> width,
                "description"       => "ពី គ.ម ". $row -> from_meter ." ដល់ គ.ម ".$row -> to_meter,
            ];
        }
        return $projectWorkTypes;
    }
}
