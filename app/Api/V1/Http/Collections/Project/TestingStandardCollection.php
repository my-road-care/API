<?php

namespace App\Api\V1\Http\Collections\Project;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TestingStandardCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $paginations;
    public function __construct($resource)
    {
        $this->paginations = [
            'total'         => $resource->total(),
            'count'         => $resource->count(),
            'per_page'      => $resource->perPage(),
            'current_page'  => $resource->currentPage(),
            'total_pages'   => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $standardTests = [];
        foreach ($this->collection as $row) {
            $standardTests[] = [
                "id"            => $row->id,
                "method"        => $row->method ? $row->method : null,
                "work_type"     => $row->work ? $row->work : null,
                "standard"      => $row->standard,
                "date"          => $row->date ? date('Y-m-d', strtotime($row->date)) : null,
                "creator"       => $row->creator ? $row->creator->name : null,
                "pk_start"      => $row->pk_start,
                "pk_end"        => $row->pk_end,
                "station_start" => $row->station_start,
                "station_end"   => $row->station_end,
            ];
        }
        return [
            'testing_standard'  => $standardTests,
            'pagination'        => $this->paginations
        ];
    }
}
