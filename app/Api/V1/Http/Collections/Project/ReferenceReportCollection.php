<?php

namespace App\Api\V1\Http\Collections\Project;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReferenceReportCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    private $paginations;
    public function __construct($resource)
    {
        $this->paginations = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $references = [];
        foreach($this -> collection as $row){
            $references [] = [
                "id"=> $row->id,
                "title"=> $row->title,
                "image_uri" => $row->image_uri,
                "created_at"=> $row->created_at ? date('Y-m-d', strtotime($row->created_at)) : null,
                "creator"=> $row->creator
            ];
        }
        return [
            'references' => $references,
            'pagination' => $this -> paginations
        ];
    }
}
