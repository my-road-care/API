<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProvinceCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $provinces = [];
        foreach($this -> collection as $key => $row){
            $provinces [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? "",
            ];
        }
        return $provinces;
    }
}