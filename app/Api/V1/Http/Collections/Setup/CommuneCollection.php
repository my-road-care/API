<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CommuneCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $communes = [];
        foreach($this -> collection as $key => $row){
            $communes [] = [
                "id"            => $row['id'] ?? "",
                "district_id"   => $row['district_id'] ?? "",
                "name"          => $row['name'] ?? ""
            ];
        }
        return $communes;
    }
}