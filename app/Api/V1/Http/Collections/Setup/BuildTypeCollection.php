<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BuildTypeCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $buildTypes = [];
        foreach($this -> collection as $key => $row){
            $buildTypes [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? ""
            ];
        }
        return $buildTypes;
    }
}