<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DistrictCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $districts = [];
        foreach($this -> collection as $key => $row){
            $districts [] = [
                "id"            => $row['id'] ?? "",
                "province_id"   => $row['province_id'] ?? "",
                "name"          => $row['name'] ?? ""
            ];
        }
        return $districts;
    }
}