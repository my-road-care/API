<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReviewerCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $reviewers = [];
        foreach($this -> collection as $key => $row){
            $reviewers [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? "",
            ];
        }
        return $reviewers;
    }
}