<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class WorkTypeCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $work_types = [];
        foreach($this -> collection as $key => $row){
            $work_types [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? ""
            ];
        }
        return $work_types;
    }
}