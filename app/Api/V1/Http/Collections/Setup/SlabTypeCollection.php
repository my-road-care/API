<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SlabTypeCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $slabTypes = [];
        foreach($this -> collection as $key => $row){
            $slabTypes [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? ""
            ];
        }
        return $slabTypes;
    }
}