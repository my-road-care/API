<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RoleCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $roles = [];
        foreach($this -> collection as $key => $row){
            $roles [] = [
                "id"            => $row['id'] ?? "",
                "abbrivation"   => $row['abbrivation'] ?? "",
                "name"          => $row['name'] ?? "",
                "en_name"       => $row['en_name']
            ];
        }
        return $roles;
    }
}