<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $users = [];
        foreach($this -> collection as $key => $row){
            $users [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? "",
                "phone"         => $row['phone'] ?? "",
                "organization"  => $row['organization']['kh_name'] ?? ''
            ];
        }
        return $users;
    }
}