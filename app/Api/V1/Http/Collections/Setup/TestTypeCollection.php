<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TestTypeCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $testTypes = [];
        foreach($this -> collection as $key => $row){
            $testTypes [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? ""
            ];
        }
        return $testTypes;
    }
}