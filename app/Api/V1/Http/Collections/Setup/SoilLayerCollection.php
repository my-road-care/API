<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SoilLayerCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $soilLayers = [];
        foreach($this -> collection as $key => $row){
            $soilLayers [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? ""
            ];
        }
        return $soilLayers;
    }
}