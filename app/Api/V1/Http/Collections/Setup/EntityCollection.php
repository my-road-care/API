<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EntityCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $entities = [];
        foreach($this -> collection as $key => $row){
            $entities [] = [
                "id"               => $row['id'] ?? "",
                "name"             => $row['kh_name'] ?? "",
                "en_name"          => $row['en_name'] ?? "",
                "abbre"            => $row['abbre'] ?? "",
            ];
        }
        return $entities;
    }
}