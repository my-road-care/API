<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class FrameWorkCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $frameWorks = [];
        foreach($this -> collection as $key => $row){
            $frameWorks [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? "",
            ];
        }
        return $frameWorks;
    }
}