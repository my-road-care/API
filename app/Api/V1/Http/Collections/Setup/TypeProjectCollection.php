<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TypeProjectCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $typeProjects = [];
        foreach($this -> collection as $key => $row){
            $typeProjects [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? ""
            ];
        }
        return $typeProjects;
    }
}