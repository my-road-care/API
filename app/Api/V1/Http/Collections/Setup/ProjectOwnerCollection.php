<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectOwnerCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $projectOwners = [];
        foreach($this -> collection as $key => $row){
            $projectOwners [] = [
                "id"               => $row['id'] ?? "",
                "kh_name"          => $row['kh_name'] ?? "",
                "en_name"          => $row['en_name'] ?? "",
                "abbre"            => $row['abbre'] ?? "",
            ];
        }
        return $projectOwners;
    }
}