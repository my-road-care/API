<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ExecutiveCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $executives = [];
        foreach($this -> collection as $key => $row){
            $executives [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? "",
            ];
        }
        return $executives;
    }
}