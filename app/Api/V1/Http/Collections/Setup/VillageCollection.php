<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class VillageCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $villages = [];
        foreach($this -> collection as $key => $row){
            $villages [] = [
                "id"            => $row['id'] ?? "",
                "commune_id"    => $row['commune_id'] ?? "",
                "name"          => $row['name'] ?? ""
            ];
        }
        return $villages;
    }
}