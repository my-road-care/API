<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectTypeCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $projectTypes = [];
        foreach($this -> collection as $key => $row){
            $projectTypes [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? "",
            ];
        }
        return $projectTypes;
    }
}