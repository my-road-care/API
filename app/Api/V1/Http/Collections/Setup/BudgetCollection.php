<?php

namespace App\Api\V1\Http\Collections\Setup;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BudgetCollection extends ResourceCollection
{

    public function toArray($request)
    {
        $budgets = [];
        foreach($this -> collection as $key => $row){
            $budgets [] = [
                "id"            => $row['id'] ?? "",
                "name"          => $row['name'] ?? ""
            ];
        }
        return $budgets;
    }
}