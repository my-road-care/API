<?php

namespace App\Api\V1\Http\Collections\CP\Reporter;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Carbon\Carbon;

class ReportsCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $paginations;
    public function __construct($resource)
    {
        $this->paginations = [
            'from'          => $resource->firstItem(),
            'total'         => $resource->total(),
            'per_page'      => $resource->perPage(),
            'current_page'  => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $list = [];
        $rn = $this->paginations['from'];
        foreach ($this->collection as $row) {
            
            $list[] = [
                "rn"               => $rn,
                "id"               => $row->id,
                'description'      => $row->description,
                "created_at"       => Carbon::parse($row->created_at)->format('M d, Y, H:i:s A'),
                "file"             => count($row->files) > 0 ? $row->files[0] : null,
                "location"         => [
                    'village'      => $row->pothole->location->village,
                    'commune'      => $row->pothole->location->commune,
                    'district'     => $row->pothole->location->district,
                    'province'     => $row->pothole->location->province
                ],
                "point"            => $row->pothole->point ? [
                    'pk'           => $row->pothole->point->pk->code . ' + ' . $row->pothole->point->meter,
                    'road'         => $row->pothole->point->pk->road
                ] : null,
            ];
            $rn++;
        }
        return [
            'data'          => $list,
            'pagination'    => $this->paginations
        ];
    }
}
