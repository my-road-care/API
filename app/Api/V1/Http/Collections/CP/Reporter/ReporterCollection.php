<?php

namespace App\Api\V1\Http\Collections\CP\Reporter;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Carbon\Carbon;
use App\Enum\Status;

class ReporterCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $paginations;
    public function __construct($resource)
    {
        $this->paginations = [
            'from'          => $resource->firstItem(),
            'total'         => $resource->total(),
            'per_page'      => $resource->perPage(),
            'current_page'  => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $list = [];
        $rn = $this->paginations['from'];
        foreach ($this->collection as $row) {
            $all         = 0;
            $pendding    = 0;
            $repairings  = 0;
            $decline     = 0;
            $fixed       = 0;
            $planning    = 0;
            $specialists = 0;
            $done        = 0;
            $useds       = 0;
            foreach ($row->reports as $report) {
                $all++;
                if ($report->status_id == Status::Pendding) {
                    $pendding++;
                } elseif ($report->status_id == Status::Repairings) {
                    $repairings++;
                } elseif ($report->status_id == Status::Decline) {
                    $decline++;
                } elseif ($report->status_id == Status::Fixed) {
                    $fixed++;
                } elseif ($report->status_id == Status::Planning) {
                    $planning++;
                } elseif ($report->status_id == Status::Specialists) {
                    $specialists++;
                }
                $done = $fixed + $planning + $specialists;
                $useds = $repairings + $done;
            }
            $list[] = [
                "rn"               => $rn,
                "id"               => $row->id,
                "created_at"       => Carbon::parse($row->created_at)->format('Y/m/d'),
                "user_id"          => $row['user_id'],
                "n_of_reports"     => $all,
                "n_of_paddings"    => $pendding,
                "n_of_repairings"  => $repairings,
                "n_of_declineds"   => $decline,
                "n_of_fixeds"      => $fixed,
                "n_of_plannings"   => $planning,
                "n_of_specialists" => $specialists,
                "n_of_dones"       => $done,
                "n_of_useds"       => $useds,
                "user"             => $row->user
            ];
            $rn++;
        }
        return [
            'data'          => $list,
            'pagination'    => $this->paginations
        ];
    }
}
