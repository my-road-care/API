<?php

namespace App\Api\V1\Http\Resources\Project;

use Illuminate\Http\Resources\Json\Resource;

class StandardTestResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this -> id,
            "test_type" => $this -> testTypes ? $this -> testTypes -> name : null,
            "soil_layer" => $this -> soilLayer ? $this -> soilLayer -> name : null,
            "standard" => $this -> standard,
            "pk_start" => $this -> pk_start,
            "pk_end" => $this -> pk_end,
            "lat_lng_start" => $this -> lat_long_start,
            "lat_lng_end" => $this -> lat_long_end,
        ];
    }
}
