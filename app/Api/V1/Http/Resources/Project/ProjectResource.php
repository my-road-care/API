<?php

namespace App\Api\V1\Http\Resources\Project;

use App\Api\V1\Http\Collections\Project\ProjectWorkTypeCollection;
use App\Api\V1\Http\Collections\Setup\EntityCollection;
use App\Api\V1\Http\Collections\Setup\ExecutiveCollection;
use App\Api\V1\Http\Collections\Setup\FrameWorkCollection;
use App\Api\V1\Http\Collections\Setup\ReviewerCollection;
use Illuminate\Http\Resources\Json\Resource;

class ProjectResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "province" => $this -> provinces,
            "name" => $this -> name,
            "code" => $this -> code,
            "work_type" => $this -> workTypes,
            "status"    => $this -> status,
            "road"    => $this -> road,
            "type_project"    => $this -> typeProject,
            "budget_plan" => $this -> budget($this->budget, $this->year),
            "framework"     => $this->data($this->organizations ? $this->organizations->where('type_id',4) : null),
            "project_owner" => $this->projectOwner($this->organizations ? $this->organizations->where('type_id',1)->first() : null),
            "executive_director" => $this->data($this->organizations ? $this->organizations->where('type_id',3) : null),
            "entities"      => $this->data($this->organizations ? $this->organizations->where('type_id',2) : null),
            "reviewer"      => $this->data($this->organizations ? $this->organizations->where('type_id',5) : null),
            "contract_no" => $this -> contract_no,
            "bud_build_req" => $this -> bud_build_req,
            "bud_review_req" => $this -> bud_review_req,
            "bud_after_build" => $this -> bud_after_build,
            "bud_after_review" => $this -> bud_after_review,
            "bud_build_contract" => $this -> bud_build_contract,
            "bud_review_contract" => $this -> bud_review_contract,
            "total_bud_contract" => $this -> total_bud_contract,
            "project_start_date_contract" => $this -> project_start_date_contract ? date('Y-m-d', strtotime($this -> project_start_date_contract)) : null,
            "project_end_date_contract" => $this -> project_end_date_contract ? date('Y-m-d', strtotime($this -> project_end_date_contract)) : null,
            "process_contract_duration" => $this -> process_contract_duration ? $this -> process_contract_duration : null,
            "delay_date" => $this -> delay_date ? date('Y-m-d', strtotime($this -> delay_date)) : null,
            "warranty_duration" => $this -> warranty_duration ? $this -> warranty_duration : null,
            "close_project_date" => $this -> close_project_date ? date('Y-m-d', strtotime($this -> close_project_date)) : null,
            "project_work_types" => $this -> projectWorkType ? new ProjectWorkTypeCollection($this -> projectWorkType) : null,
        ];
    }

    private function data($entities){
        $arrEntities = [];
        foreach($entities as $entity){
            $arrEntities [] = [
                "id"               => $entity['id'] ?? "",
                "name"             => $entity['kh_name'] ?? "",
                "en_name"          => $entity['en_name'] ?? "",
                "abbre"            => $entity['abbre'] ?? "",
            ];
        }
        return $arrEntities;
    }
    private function projectOwner($entities){
            $entities = [
                "id"               => $entities['id'] ?? "",
                "kh_name"             => $entities['kh_name'] ?? "",
                "en_name"          => $entities['en_name'] ?? "",
                "abbre"            => $entities['abbre'] ?? "",
            ];
        return $entities;
    }
    private function budget($budget, $year)
    {
        $name = $budget->name;
        $budYear =  $year ? " (ឆ្នាំ ".$year->year.")" : null;
        return [
            'id'   => $budget->id,
            'name' => $name.$budYear,
            // 'year' => $year ? "(ឆ្នាំ ".$year->year.")" : null
        ];
    }
}
