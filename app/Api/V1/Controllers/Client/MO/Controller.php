<?php

namespace App\Api\V1\Controllers\Client\MO;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\Model\Pothole\Main as Pothole;
use App\Model\Authority\MO\Main as MO;
use App\Model\Authority\MO\Road as MORoad;
use App\Model\Authority\MT\Main as MT;
use App\Model\Road\Main as Road;
use App\Model\Setting\Maintence\Main as Maintence;

class Controller extends ApiController
{
    use Helpers;
   
    function mts(){
        $user = JWTAuth::parseToken()->authenticate(); 
       

        if($user->mo){
             $data = MT::select('id', 'user_id', 'province_id', 'name')->with(['user:id,name,avatar,phone,email', 'province:id,name,abbre']); 
          
            if($mo->management_type_id == 2){ //Managed by Road
               $moRoadData = MORoad::select('road_id', 'start_pk', 'end_pk')->where('mo_id', $mo->id)->get(); 
                $moRoads = []; 

                foreach($moRoadData as $row){
                   $moRoads[] = $row->road_id; 
                }

                $data = $data->whereHas('roads', function($query) use ($moRoads){
                    $query->whereIn('road_id', $moRoads); 
                });

            }else{ //Managed by Boundary
                
                $data = $data->whereHas('mos', function($query) use ($mo){
                    $query->where('mo_id', $mo->id);
                }); 
            }


            $data= $data->orderBy('id', 'asc')->paginate(intval(isset($_GET['limit'])?$_GET['limit']:100));
            return response()->json($data, 200);
        }else{
            return response()->json(['status'=>'error', 'message'=>'Invalid Access'], 401);
        } 
    }
    
    function maintenceCodes(Request $request){

        $lang = JWTAuth::parseToken()->authenticate()->lang;
        if($request->get('lang')){
            if($request->get('lang') == 'en' || $request->get('lang') == 'kh'){
                $lang = $request->get('lang'); 
            }
        }

        $maintenceCodes = Maintence::select('id', 'group_id', 'type_id', 'subtype_id', 'unit_id', 'code', $lang.'_name as name', 'rate', 'description')
        ->with([
            'group'=>function($query) use ($lang){
                $query->select('id', $lang.'_name as name');
            },
            'type'=>function($query) use ($lang){
                $query->select('id', $lang.'_name as name');
            },
            'subtype'=>function($query) use ($lang){
                $query->select('id', $lang.'_name as name');
            },
            'unit'=>function($query) use ($lang){
                $query->select('id', $lang.'_name as name');
            },
        ])->get(); 

        return response()->json(['data'=>$maintenceCodes, 'status_code'=>200], 200);
    }

    
}
