<?php

namespace App\Api\V1\Controllers\Client\Nearby;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;

use App\MPWT\LatLngUMTConvert;
use App\MPWT\RoadCare; 
use App\Model\Location\Province; 
use App\Model\Road\Main as Road; 


class NearbyController extends ApiController
{
    use Helpers;

    function locations(Request $request){
        
        //Check validation 
        $this->validate($request, [  
            'lat'               => 'required|numeric', 
            'lng'               => 'required|numeric'
        ]);

        $lat = $request->get('lat'); 
        $lng = $request->get('lng'); 

        $data = RoadCare::getLocation($lat, $lng);
        if(!$data){
            $data = ['id'=>1, 'province' => ['id'=>25, 'name'=> 'ត្បូងឃ្មំុ', 'code' => '25']];
        }
        return response()->json(['data'=>$data, 'status_code'=>200], 200);
    }


    
    function nrs(Request $request){
        
        //Check validation 
        $this->validate($request, [  
            'lat'               => 'required|numeric', 
            'lng'               => 'required|numeric'
        ]);

        $data = RoadCare::getRoad($request->get('lat'), $request->get('lng')); 
        return response()->json(['data'=>$data, 'status_code'=>200], 200);
    }

    function potholes(Request $request){
        
        //Check validation 
        $this->validate($request, [  
            'lat'               => 'required|numeric', 
            'lng'               => 'required|numeric',
        ]);

        $data = RoadCare::potholeSearching($request->get('lat'), $request->get('lng')); 
        return response()->json(['data'=>$data, 'status_code'=>200], 200);
    }

    function provinces(Request $request){
        
        $data = Province::select('id', 'name', 'en_name', 'abbre')->get(); 
        return response()->json(['data'=>$data, 'status_code'=>200], 200);
    }

    function roads(){
        $data = Road::select('id', 'name')->orderBy('name', 'ASC')->get(); 
           
        return response()->json(['data'=>$data, 'status_code'=>200], 200);
    }
}
