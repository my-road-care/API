<?php

namespace App\Api\V1\Controllers\Client\Gen;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\Model\Location\Province; 
use App\Model\Pothole\Main as Pothole;

use App\Model\Authority\MT\Main as MT;
use App\Model\Authority\MO\Main as MO;
use App\Model\Authority\MO\Road as MORoad;
use App\Model\Road\Main as Road;
use App\Model\Setting\Maintence\Main as Maintence;
use App\Model\Setting\Status;
use App\Model\Setting\AppInfo;
use App\Model\WhatNews\Main as WhatNews;
class Controller extends ApiController
{
    use Helpers;

   
    function provinces(Request $request){
        $data = Province::select('id', 'name', 'en_name', 'abbre')->get(); 
        return response()->json(['data'=>$data, 'status_code'=>200], 200);
    }

    function roads(Request $request){
        
        $user = JWTAuth::parseToken()->authenticate();

        $mo = MO::select('id', 'management_type_id')->wherehas('user', function($query) use ($user){
            $query->where('user_id', $user->id); 
        })->first(); 

        //return $mo;

        if($mo){
            
            $data = Road::select('id', 'name');
            if($mo->management_type_id == 2){
                $data = $data->whereHas('mos', function($query) use ($mo){
                    $query->where('mo_id', $mo->id); 
                }); 
            }else{
                
                $mts = []; 
                foreach($mo->mts as $row){
                    $mts[] = $row->mt_id; 
                }

                $data = $data->whereHas('mts', function($query) use ($mts){
                    $query->whereIn('mt_id', $mts);
                }); 

            }

            $data = $data->orderBy('name', 'asc')->get(); 
            return response()->json(['data'=>$data, 'status_code'=>200], 200);

        }else{
            $mt = MT::select('id')->where('user_id', $user->id)->first();


            if($mt){
                
                $data = Road::select('id', 'name')->whereHas('mts', function($query) use ($mt){
                    $query->where('mt_id', $mt->id);
                })->orderBy('name', 'asc')->get(); 
                return response()->json(['data'=>$data, 'status_code'=>200], 200);

            }else{

                $data = Road::select('id', 'name')->whereHas('potholes', function($query) use ($user){
                    $query->whereHas('reports', function($query) use ($user){
                        $query->wherehas('ru', function($query) use ($user){
                            $query->where('user_id', $user->id); 
                        });
                    })
                    ->whereNotNull('road_id')
                    ->where('road_id', '<>', 0); 

                })->orderBy('name', 'asc')->get(); 
                return response()->json(['data'=>$data, 'status_code'=>200], 200);
            }
        }      
    }

    function maintenceCodes(Request $request){

        $lang = JWTAuth::parseToken()->authenticate()->lang;
        if($request->get('lang')){
            if($request->get('lang') == 'en' || $request->get('lang') == 'kh'){
                $lang = $request->get('lang'); 
            }
        }

        $maintenceCodes = Maintence::select('id', 'group_id', 'type_id', 'subtype_id', 'unit_id', 'code', $lang.'_name as name', 'rate', 'description')
        ->with([
            'group'=>function($query) use ($lang){
                $query->select('id', $lang.'_name as name');
            },
            'type'=>function($query) use ($lang){
                $query->select('id', $lang.'_name as name');
            },
            'subtype'=>function($query) use ($lang){
                $query->select('id', $lang.'_name as name');
            },
            'unit'=>function($query) use ($lang){
                $query->select('id', $lang.'_name as name');
            },
        ])->orderBy('code', 'ASC')->get(); 

        return response()->json(['data'=>$maintenceCodes, 'status_code'=>200], 200);
    }

    function maintenceCoded(Request $request){

        $lang = JWTAuth::parseToken()->authenticate()->lang;
        if($request->get('lang')){
            if($request->get('lang') == 'en' || $request->get('lang') == 'kh'){
                $lang = $request->get('lang'); 
            }
        }

        $maintenceCodes = Maintence::select('id', 'code')->orderBy('code', 'asc')->get(); 

        return response()->json(['data'=>$maintenceCodes, 'status_code'=>200], 200);
    }

    function mos(){
        $user = JWTAuth::parseToken()->authenticate(); 
        $mt = MT::where('user_id', $user->id)->first(); 

        if($mt){
            $data = MO::select('id', 'user_id', 'name')
            ->with(['user:id,name,avatar,phone,email'])
            ->whereHas('mts', function($query) use ($mt){
                $query->whereHas('mt', function($query) use ($mt){
                    $query->where('id', $mt->id); 
                });
            })
            ;

           
            $from=isset($_GET['from'])?$_GET['from']:"";
            $to=isset($_GET['to'])?$_GET['to']:"";
            if(isValidDate($from)){
                if(isValidDate($to)){

                    $from .=" 00:00:00";
                    $to .=" 23:59:59";
                    $data = $data->whereBetween('created_at', [$from, $to]);
                }
            }

            $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
            $data= $data->orderBy('id', 'desc')->paginate($limit);
            return response()->json($data, 200);
        }else{
            return response()->json(['status'=>'error', 'message'=>'Invalid Access'], 401);
        }
           
    }

    function mts(){
        $user = JWTAuth::parseToken()->authenticate(); 
        if($user->mo){
             $data = MT::select('id', 'user_id', 'province_id', 'name')->with(['user:id,name,avatar,phone,email', 'province:id,name,abbre']); 
          
            if($user->mo->management_type_id == 2){ //Managed by Road
                $moRoadData = MORoad::select('road_id', 'start_pk', 'end_pk')->where('mo_id', $user->mo->id)->get(); 
                $moRoads = []; 

                foreach($moRoadData as $row){
                   $moRoads[] = $row->road_id; 
                }

                $data = $data->whereHas('roads', function($query) use ($moRoads){
                    $query->whereIn('road_id', $moRoads); 
                });

            }else{ //Managed by Boundary
                
                $data = $data->whereHas('mos', function($query) use ($mo){
                    $query->where('mo_id', $user->mo->id);
                }); 
            }


            $data= $data->orderBy('id', 'asc')->paginate(intval(isset($_GET['limit'])?$_GET['limit']:100));
            return response()->json($data, 200);
        }else{
            return response()->json(['status'=>'error', 'message'=>'Invalid Access'], 401);
        } 
    }

    function permissions(){
        $data = Status::select('id', 'kh_name', 'parent_id', 'name', 'mo', 'mt', 'requirements')->with(['children:id,parent_id,kh_name,name,mo,mt,requirements'])->where('parent_id', 0)->get(); 
       return response()->json(['data'=>$data, 'status_code'=>200], 200);
    }

    function appInfo(){
        $data = AppInfo::first(); 
        return response()->json($data, 200);
    }

    function statuses(){
        
        $user = JWTAuth::parseToken()->authenticate();
        $roles = $this->checkUserPosition($user->id); 

        $data = Status::select('id', 'kh_name', 'en_name', 'parent_id', 'name', 'color', 'requirements as fields'); 
        // if(in_array('mo', $roles)){
        //     $data = $data->addSelect('mo as permissions'); 
        // }else if(in_array('mt', $roles)){
        //     $data = $data->addSelect('mt as permissions'); 
        // }

        $data = $data
        // ->with(['children:id,parent_id,kh_name,name,requirements'])
        // ->where('parent_id', 0)->where('id', '<>', 1)
        ->get(); 
        return response()->json(['data'=>$data, 'status_code'=>200], 200);
    }
    function whatNews(){
        $user = JWTAuth::parseToken()->authenticate();
        $data = WhatNews::select('id', 'title', 'content', 'posted_at')->orderBy('id', 'desc'); 
        $data = $data->paginate(25); 
        return response()->json(['data'=>$data, 'status_code'=>200], 200);
    }
}
