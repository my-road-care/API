<?php

namespace App\Api\V1\Controllers\Client\Stats;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Api\V1\Controllers\CP\Tool\Controller as ToolController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\Model\Location\Province; 
use App\Model\Pothole\Main as Pothole;
use App\Model\Pothole\ReportStatus as ReportStatus;

use App\Model\Authority\MT\Main as MT;
use App\Model\Authority\MO\Main as MO;
use App\Model\Authority\MO\Road as MORoad;
use App\Model\Road\Main as Road;
use App\Model\Setting\Maintence\Main as Maintence;
use App\Model\Setting\Status;
use Carbon\Carbon;

class Controller extends ToolController
{
    use Helpers;

   
    function index(Request $request){
        $user = JWTAuth::parseToken()->authenticate(); 
        $mt = MT::where('user_id', $user->id)->first(); 

        if($mt){

            $yesterday = Carbon::yesterday(); 
            $period = [Carbon::yesterday()->format('Y-m-d').' 00:00:00', $yesterday->format('Y-m-d').' 23:59:59']; 

            if($request->get('period')){

                if($request->get('period') == 'daily'){
                    $period = [Carbon::yesterday()->format('Y-m-d').' 00:00:00', $yesterday->format('Y-m-d').' 23:59:59']; 
                }elseif($request->get('period') == 'weekly'){
                    $period = [Carbon::yesterday()->subDay(7)->format('Y-m-d').' 00:00:00', $yesterday->format('Y-m-d').' 23:59:59'];
                }elseif($request->get('period') == 'monthly'){
                    $period = [Carbon::yesterday()->subDay(30)->format('Y-m-d').' 00:00:00', $yesterday->format('Y-m-d').' 23:59:59']; 
                }elseif($request->get('period') == 'quarterly'){
                    $period = [Carbon::yesterday()->subDay(90)->format('Y-m-d').' 00:00:00', $yesterday->format('Y-m-d').' 23:59:59']; 
                }elseif($request->get('period') == 'semester'){
                    $period = [Carbon::yesterday()->subDay(180)->format('Y-m-d').' 00:00:00', $yesterday->format('Y-m-d').' 23:59:59']; 
                }elseif($request->get('period') == 'anually'){
                    $period = [Carbon::yesterday()->subDay(365)->format('Y-m-d').' 00:00:00', $yesterday->format('Y-m-d').' 23:59:59']; 
                }

            }else{

                $from=isset($_GET['from'])?$_GET['from']:"";
                $to=isset($_GET['to'])?$_GET['to']:"";
                if(isValidDate($from)){
                    if(isValidDate($to)){
                        $period = [$from .=" 00:00:00",  $to .=" 23:59:59"]; 
                    }
                }

            }

                
            $data = $this->getMTReport($mt, $period); 
            return response()->json(['data'=>$data, 'period'=>['from'=>$period[0], 'to'=>$period[1]]], 200);
        }

        return $mt;   
    }

    function graph(Request $request){
        $user = JWTAuth::parseToken()->authenticate(); 
        $mt = MT::where('user_id', $user->id)->first(); 

        $partition   = $request->get('partition'); 
        $type   = $request->get('type'); 
        $from   = $request->get('from'); 
        $to     = $request->get('to'); 
        
        $yesterday  = Carbon::yesterday(); 
        $period     = []; 
        $data       = [];
       
        if( $type == 'daily'){
            
            $period     = [
                Carbon::yesterday()->subDay($partition)->format('Y-m-d H:i:s'), 
                Carbon::yesterday()->addHour(24)->subSecond()->format('Y-m-d H:i:s')
            ]; 

            for( $i = 0; $i < $partition; $i++ ){
                
                $label = Carbon::yesterday()->subDay($partition - $i);
                $range = [ 
                    $label.' 00:00:00', 
                    $label.' 23:59:59'
                ]; 

                $data[] = [
                    'label'     =>  $label->format('Y-m-d'),
                    'period'    =>  $range, 
                    'stats'     =>  $this->getMTReport($mt, $range)
                ];
            }

        }elseif($type  == 'weekly'){
            
            $period = [
                Carbon::parse('this week')->subWeeks($partition)->format('Y-m-d  00:00:00'),
                Carbon::parse('this week')->subDay()->format('Y-m-d  23:59:59')
            ];

            for( $i = 0; $i < $partition; $i++ ){

                $label = Carbon::parse('this week')->subWeeks($partition - $i);

                $range = [
                    $label->format('Y-m-d 00:00:00'), 
                    $label->addWeek()->subDay()->format('Y-m-d 23:59:59')
                ];

                //$weekOfMonth = $label->addWeek()->subDay()->weekOfMonth - 1;

                $data[] = [
                    //'label'     =>  $weekOfMonth.' of '.$label->format('Y-m'),
                    'period'    =>  $range, 
                    'stats'     =>  $this->getMTReport($mt, $range)
                ];
            }

        }elseif($type  == 'monthly'){
            
            $startDate = Carbon::parse('last day of last month'); 

            $period = [
                $startDate->subMonths($partition)->format('Y-m-d  00:00:00'),
                $startDate->addMonths($partition)->subDay()->format('Y-m-d  23:59:59')
            ];

            for( $i = 0; $i < $partition; $i++ ){

                //$label = Carbon::parse('last day of last month')->subMonths($partition - $i);
                $range = [
                    $startDate->subMonths($partition - $i)->format('Y-m-d 00:00:00'), 
                    $startDate->addMonth()->subDay()->format('Y-m-d 23:59:59')
                ];

                //$label->addDay(); 
                $startDate->addMonths($partition-1)->addDay(); 

                $data[] = [
                    //'label'     =>  $label->subDay()->format('Y-m'),
                    'period'    =>  $range, 
                    'stats'     =>  $this->getMTReport($mt, $range)
                ];
            }

        }elseif($type  == 'quarterly'){
            
            $startDate  = Carbon::parse('first day of this year')->subMonth(); 
            $month      = Carbon::today()->format('n') ;

 
            if ($month < 4) {
                $startDate  = Carbon::parse('first day of this year')->subMonth(); 
            }elseif ($month > 3 && $month < 7) {
                $startDate = $startDate->modify('last day of march ' . $startDate->format('Y'));
                
            } elseif ($month > 6 && $month < 10) {
                $startDate = $startDate->modify('last day of june ' . $startDate->format('Y'));
                
            } elseif ($month > 9) {
                $startDate = $startDate->modify('last day of september ' . $startDate->format('Y'));
            }


            $period = [
                $startDate->subQuarters($partition)->format('Y-m-d 00:00:00'),
                $startDate->addQuarters($partition)->subDay()->format('Y-m-d  23:59:59')
            ];


            $startDate->subQuarters($partition+1); 

            for( $i = $partition; $i > 0; $i-- ){
                
                $startDate->addQuarter(); 

                $range = [
                    $startDate->format('Y-m-d 00:00:00'), 
                    $startDate->addQuarter()->subDay()->format('Y-m-d 23:59:59')
                ];

                $startDate->subQuarter()->addDay(); 

                $data[] = [
                    'label'     =>  '',
                    'period'    =>  $range, 
                    'stats'     =>  $this->getMTReport($mt, $range)
                ];

            }

        }elseif($type  == 'semester'){
            
            $startDate  = Carbon::parse('first day of this year')->subMonth(); 
            $month      = Carbon::today()->format('n') ;

            if ($month < 6) {
                $startDate  = Carbon::parse('first day of this year')->subMonth(); 
            } else {
                $startDate = $startDate->modify('last day of june ' . $startDate->format('Y'));
            }


            $period = [
                $startDate->subQuarters(2*$partition)->format('Y-m-d 00:00:00'),
                $startDate->addQuarters(2*$partition)->subDay()->format('Y-m-d  23:59:59')
            ];


            $startDate->addDay(); 

            for( $i = $partition; $i > 0; $i-- ){
                
                $startDate->subQuarters($i*2); 

                $range = [
                    $startDate->format('Y-m-d 00:00:00'), 
                    $startDate->addQuarters(2)->subDay()->format('Y-m-d 23:59:59')
                ];

                $startDate->addQuarters($i*2-2)->addDay(); 

                $data[] = [
                    'label'     =>  '',
                    'period'    =>  $range, 
                    'stats'     =>  $this->getMTReport($mt, $range)
                ];

            }

        }elseif($type  == 'anually'){
            $lastYear = Date('Y')-1; 
            $period = [
                Carbon::parse('31-12-'.$lastYear)->subYears($partition)->addDay()->format('Y-m-d  00:00:00'),
                Carbon::parse('31-12-'.$lastYear)->format('Y-m-d  23:59:59')
            ];

            for( $i = 0; $i < $partition; $i++ ){

                $label = Carbon::parse('31-12-'.$lastYear)->subYears($partition - $i);
                $range = [
                    $label->addDay()->format('Y-m-d 00:00:00'), 
                    $label->addYear()->subDay()->format('Y-m-d 23:59:59')
                ];

                $data[] = [
                    'label'     => $label->format('Y'),
                    'period'    =>  $range, 
                    'stats'     =>  $this->getMTReport($mt, $range)
                ];
            }

        }else{

            $from   =   isset($_GET['from'])?$_GET['from']:"";
            $to     =   isset($_GET['to'])?$_GET['to']:"";
            if(isValidDate($from)){
                if(isValidDate($to)){
                    $period = [$from .=" 00:00:00",  $to .=" 23:59:59"]; 
                    $data[] = [
                        'stats'     =>  $this->getMTReport($mt, $period)
                    ];
                }
            }
        }
        
        return [
            'partition'     => $partition, 
            'type'          => $type, 
            'period'        => $period, 
            'data'          => $data
        ];
        
    }


}
