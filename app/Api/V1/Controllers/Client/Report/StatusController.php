<?php

namespace App\Api\V1\Controllers\Client\Report;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\MPWT\FileUpload;

use App\Model\Pothole\Main as Pothole;
use App\Model\Pothole\Report as Report;
use App\Model\Pothole\ReportStatus as Status;

class StatusController extends ApiController
{
    use Helpers;

    function create(Request $request, $reportId){
        $user = JWTAuth::parseToken()->authenticate();

        $this->validate($request, [ 
            'status_id'             => 'required|numeric|exists:status,id'

        ], [
            'status_id.required'         =>  'Please select any statuses.',
            'status_id.numeric'          =>  'Status must be a numberic value',
            'status_id.exists'           =>  'Please choose a valid status',

        ]);

        $report = Report::find($reportId);  
        if($report){
            
            //Create Staus
            $status             = new Status; 
            $status->report_id = $reportId; 
            $status->status_id  = $request->input('status_id'); 
            $status->comment    = $request->input('comment'); 
            $status->creator_id = $user->id; 
            $status->updater_id = $user->id; 
            $status->save();

            //Update Report Status
            $report->status_id = $request->input('status_id'); 
            $report->save();

            // $files =  json_decode($request->input('files')); 
            // $this->upload($status->id, $files);

            return response()->json([
                'status'          =>  $status, 
                'status_code'   =>  200,
                'message'=>'stauts updated'
            ], 200);

        }else{
            return response()->json([
                'status_code'   =>  403,
                'errors'        =>  ['message'  =>  ['invalid report']]
            ], 403);
        }
    }


    
}
