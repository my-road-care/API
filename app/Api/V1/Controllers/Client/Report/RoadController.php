<?php

namespace App\Api\V1\Controllers\Client\Report;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

use App\Model\Pothole\Main as Pothole;


class RoadController extends ApiController
{
    use Helpers;

    function roads(){
        $user = JWTAuth::parseToken()->authenticate();
        $data = Pothole::select('road_id')->distinct()->with(['road:id,name'])
        ->whereHas('reports', function($query) use ($user){
            $query->wherehas('ru', function($query) use ($user){
                $query->where('user_id', $user->id); 
            });
        })
        ->whereNotNull('road_id')
        ->where('road_id', '<>', 0)
        ->get(); 

        return response()->json(['data'=>$data, 'status_code'=>200], 200);
    }
}
