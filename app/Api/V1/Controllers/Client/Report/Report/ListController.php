<?php

namespace App\Api\V1\Controllers\Client\Report\Report;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

use App\MPWT\FileUpload;
use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use TelegramBot; 

use App\Model\Member\Main as RU;
use App\Model\User\Main as User;
use App\Model\Pothole\Main as Pothole;
use App\Model\Pothole\Status as PotholeStatus;
use App\Model\Pothole\Location as PotholeLocation;
use App\Model\Pothole\Report as Report;
use App\Model\Pothole\ReportLocation as ReportLocation;
use App\Model\Pothole\ReportStatus as ReportStatus;
use App\Model\Pothole\File as ReportFile;
use App\Model\Pothole\Comment as ReportComment;

use App\Model\Road\Point as PKPoint;
use App\Model\Authority\MO\Road as MORoad;

use App\Model\Authority\MO\Main as MO;
use App\Model\Authority\MT\Main as MT;

use App\Model\Location\Commune;
use App\Model\Location\Village;

use App\MPWT\LatLngUMTConvert;
use App\MPWT\RoadCare; 

use Grimzy\LaravelMysqlSpatial\Types\MultiPoint;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\MPWT\Notification as Notify;
use App\Http\Resources\Report\ReportCollection;
use Illuminate\Support\Carbon;
class ListController extends ApiController
{
    use Helpers;

    function list(){

        //Fetch authenticated user information
        $user = JWTAuth::parseToken()->authenticate();
        $roles = $this->checkUserPosition($user->id); 
        //return $roles;
        //=======================================================================>> Selection
        $data = $this->dataSelection($user, $roles); 
        // return response()->json($data->limit(10)->orderBy('id','DESC')->get(), 200);
        

        // return 'hi';

        //=================================================>> Check Report by Roles
        $roles = $this->checkUserPosition($user->id); 


//        if(in_array('mo', $roles)){
//            $mo = MO::select('id', 'management_type_id')->with(['mts:id,mt_id,mo_id', 'mts.mt:id,province_id'])->where('user_id', $user->id)->first();
//            if( $mo->management_type_id == 2 ){ //Managed by Road
//                //Select all roads that manage by this MO
//                $moRoadData = MORoad::select('road_id', 'start_pk', 'end_pk')->where('mo_id', $mo->id)->get();
//                $moRoads = [];
//
//                foreach($moRoadData as $row){
//                   $moRoads[] = $row->road_id;
//                }
//
//                $data = $data->where(function($query) use ($moRoads, $mo){
//                    $query->whereHas('pothole', function($query) use ($moRoads){
//                        $query->whereIn('road_id', $moRoads);
//                        //need to query more on PKs
//
//                    })->orWhereHas('ru', function($query) use ($mo) {
//                        $query->whereHas('user', function($query) use ($mo) {
//                            $query->where('id', $mo->user_id);
//                        });
//                    });
//                });
//                // dd('hi1');
//            }else{ //Managed by Boundary
//                $provinces = [];
//                foreach($mo->mts as $row){
//                    if($row->mt->province_id != null){
//                        $provinces[] = $row->mt->province_id;
//                    }
//                }
//                $data = $data->where(function($query) use ($provinces, $mo){
//                    $query->whereHas('pothole', function($query) use ($provinces){
//                        $query->whereHas('location', function($query) use ($provinces){
//                           $query->whereIn('province_id', $provinces);
//                        });
//                    })->orWhereHas('ru', function($query) use ($mo) {
//                        $query->whereHas('user', function($query) use ($mo) {
//                            $query->where('id', $mo->user_id);
//                        });
//                    });
//                });
//
//            }
//        }else if(in_array('mt', $roles)){
//            $mt = MT::where('user_id', $user->id)->first();
//            $data = $data->where(function($query) use ($mt){
//                $query->whereHas('pothole', function($query) use ($mt){
//                    $query->whereHas('action', function($query) use ($mt){
//                        $query->where('mt_id', $mt->id);
//                    });
//                })->orWhereHas('ru', function($query) use ($mt) {
//                    $query->whereHas('user', function($query) use ($mt) {
//                        $query->where('id', $mt->user_id);
//                    });
//                });
//            });
//        }else if(in_array('ru', $roles)){
//
//            //=================================================>> Check for Public Report
//            $privacy = isset($_GET['privacy'])?$_GET['privacy']:"";
//            if($privacy == ''){
//
//                $data = $data->whereHas('ru', function($query) use ($user) {
//                    $query->whereHas('user', function($query) use ($user) {
//                        $query->where('id', $user->id);
//                    });
//                });
//
//                $data = $data->orWhere(function($query){
//                    $query->whereIn('status_id', [4, 5, 7]);
//                });
//
//
//            }else if($privacy == 'me-only'){
//                $data = $data->whereHas('ru', function($query) use ($user) {
//                    $query->whereHas('user', function($query) use ($user) {
//                        $query->where('id', $user->id);
//                    });
//                });
//
//            }else if($privacy == 'done-only'){
//
//                $data = $this->dataSelection($user, $roles);
//                $data = $data->where(function($query){
//                    $query->whereIn('status_id', [4, 5, 7]);
//                });
//            }else{
//                $data = $this->dataSelection($user, $roles);
//            }
//
//
//        }else{
//            return [];
//        }
        
        // if(in_array('mt', $roles)){
        //     $mt = MT::where('user_id', $user->id)->first();
        //     $data = $data->where(function($query) use ($mt){
        //                          $query->whereHas('pothole', function($query) use ($mt){
        //                                           $query->whereHas('action', function($query) use ($mt){
        //                                                            $query->where('mt_id', $mt->id);
        //                                                            });
        //                                           })->orWhereHas('ru', function($query) use ($mt) {
        //                                                          $query->whereHas('user', function($query) use ($mt) {
        //                                                                           $query->where('id', $mt->user_id);
        //                                                                           });
        //                                                          });
        //                          });
        // }

//         if(in_array('mt', $roles)){
//             $mt = MT::where('user_id', $user->id)->with(['parent'])->first();
//             if ($mt->id == 26 || $mt->id == 27) {
//                 $data = $data->where(function($query) use ($mt){
// //                     $query->whereHas('pothole', function($query) use ($mt){
// //                          $query->whereHas('action', function($query) use ($mt){
// ////                               $query->whereHas('mt', function($query) use ($mt){
// //////                                    $query->whereHas('children', function($query) use ($mt){
// ////////                                             $query->whereBetween('province_id', [1,27]);
// //////                                         });
// //////                                    });
// ////                               
// ////                               });
// //                          })->orWhereHas('ru', function($query) use ($mt) {
// //                                 $query->whereHas('user', function($query) use ($mt) {
// //                                      $query->where('id', $mt->user_id);
// //                                      });
// //                                 });;
//                      });
//             } else {
//                 $data = $data->where(function($query) use ($mt){
//                      $query->whereHas('pothole', function($query) use ($mt){
//                           $query->whereHas('action', function($query) use ($mt){
//                                $query->whereHas('mt', function($query) use ($mt){
//                                     $query->whereHas('children', function($query) use ($mt){
//                                          $query->where('province_id', $mt->province_id);
//                                          })->orWhere('id', $mt->id);
                                    
//                                     });
                               
//                                });
//                           })->orWhereHas('ru', function($query) use ($mt) {
//                              $query->whereHas('user', function($query) use ($mt) {
//                                   $query->where('id', $mt->user_id);
//                                   });
//                              });
//                      });
//             }
//             // return $mt;
            
//         }
        if(in_array('mt', $roles)){
            $mt = MT::where('user_id', $user->id)->with(['parent'])
                   ->with(array('roads'=>function($query){
                       $query->select('id', 'mt_id', 'road_id', 'start_pk', 'end_pk')->with('road:id,name');
                   }))
                   ->first();
           
           /** To find MT control PK */
           $roads = $mt->roads;
           if($mt->id == 26 || $mt->id == 27 || $mt->id == 56){
               $data = $data->whereIn('status_id', [2,3,4,5,6]);
           }else{
           	//Check if report status not in 1
            $data = $data->whereNotIn('status_id', [1])->whereHas('statuses', function($query) use ($mt) {
                $query->whereNotNull('mt_id');
            });  
            $data = $data->where(function($query) use ($mt,$roads){
                /** To see report from RU under his provincail */
                $query->whereHas('pothole', function($query) use ($mt){
                                    $query->whereHas('action', function($query) use ($mt){
                                    $query->whereHas('mt', function($query) use ($mt){
                                        $query->whereHas('children', function($query) use ($mt){
                                            $query->where('province_id', $mt->province_id);
                                        })->orWhere('id', $mt->id);
                                    });
                                    });
                                    })
                        /** To see his own report */
                        ->orWhereHas('ru', function($query) use ($mt) {
                                    $query->whereHas('user', function($query) use ($mt) {
                                        $query->where('id', $mt->user_id);
                                    });
                        })
                        /** To see report query by pk */
                        ->whereHas('pothole', function($query) use ($roads) {
                            $query->whereHas('point', function($query) use ($roads) {
                                $query->whereHas('pk', function($query) use ($roads){
                                    foreach($roads as $road){
                                        $query->where('road_id', $road->road_id)
                                        ->whereBetween('code',[$road->start_pk, $road->end_pk]);
                                    }
                                    
                                });
                            });
                        });
                });
            }
               
        }
        elseif(in_array('mo', $roles)){

            $data = $data;
        }else{
            $data = $data->whereIn('status_id', [2,4,5,6,7])->orWhereHas('ru', function($query) use ($user) {
                $query->whereHas('user', function($query) use ($user) {
                    $query->where('id', $user->id);
                });
            });
            
            $statusId      =   intval(isset($_GET['status_id'])?$_GET['status_id']:0); 
            if($statusId != 0){
                $data = $data->where('status_id', $statusId); 
            }

            // $data = $data;
            //$data = $data->whereIn('status_id', [2,3,4]);

        }
        
        // Searcch by Type 
        $type = isset($_GET['type'])?$_GET['type']:"";
        if($type){
            $data = $data->where([ 'type_id' => $type ]);
        }

        $data = $data->orderBy('updated_at', 'desc')->paginate(intval(isset($_GET['limit'])?$_GET['limit']:10));
        // return response()->json($data, 200);
        
        $data = new ReportCollection( $data );
        
        return $data;    
    }

    function dataSelection($user, $roles){
        /** To Get MT */
        $mt = MT::where('user_id', $user->id)->with(['parent'])->first();
	
        /** End Get MT */
        $data = Report::select('id','type_id', 'pothole_id', 'description', 'lat', 'lng', 'member_id', 'commune_id', 'additional_location', 'is_posted', 'status_id', 'created_at', 'updated_at')
        ->with([
            'type:id,kh_name,en_name',
            'ru:id,user_id',
            'ru.user:id,name,avatar,email,phone',
            'files'=>function($query){
                $query->select('id', 'report_id', 'uri', 'is_resize', 'lat', 'lng', 'is_accepted', 'is_fixed_image')->orderBy('is_fixed_image', 'DESC');
            }, 
            'comments'=>function($query){
                $query->select('id', 'report_id', 'creator_id', 'comment', 'created_at', 'voice')
                ->with('commenter:id,name,avatar,email,phone')->orderBy('id', 'DESC')->get();
            }, 
            'pothole'=>function($query) use ($user){
                $query->select('id', 'created_at', 'point_id', 'code', 'maintence_id')
                ->with([
                     'comments'=>function($query){
                        $query->select('id', 'pothole_id', 'creator_id', 'comment', 'created_at')
                        ->with('commenter:id,name,avatar,email,phone')->orderBy('id', 'DESC')->get();
                    }, 

                    'location:id,pothole_id,village_id,commune_id,district_id,province_id,lat,lng', 
                    'location.village:id,name,code', 
                    'location.commune:id,name,code', 
                    'location.district:id,name,code', 
                    'location.province:id,name,code', 


                    'point:id,pk_id,meter', 
                    'point.pk:id,code,road_id',
                    'point.pk.road:id,name,start_point,end_point',

                    'statuses'=>function($query){
                        $query->select('id', 'pothole_id', 'status_id', 'mt_id', 'updater_id', 'comment', 'updated_at')
                        ->with([
                            'status:id,name', 
                            'updater:id,name,avatar,email,phone', 
                            'mt:id,user_id', 
                            'mt.user:id,name,avatar,email,phone'
                        ])
                        ->orderBy('id', 'DESC')->get(); 
                    }, 
                    'files:uri,lat,lng'
                   
                ]); 
            }, 

            'locations'=>function($query){
                $query->select('id', 'report_id', 'village_id', 'distance')
                ->with([
                    'village:id,name,code,commune_id', 
                    'village.commune:id,name,code,district_id',
                    'village.commune.district:id,name,code,province_id', 
                    'village.commune.district.province:id,name,code'
                ]); 
            }, 

            'statuses'=>function($query) use ($roles, $mt){
                $query->select('id', 'report_id', 'status_id', 'mt_id', 'updater_id', 'comment', 'updated_at')
                ->with([
                    'status'=>function($query) use ($roles, $mt){
                        $query->select('id', 'name', 'requirements as fields')->with(['children:id,parent_id,kh_name,name,requirements']); 
                        if(in_array('mo', $roles)){
                            $query->addSelect('mo as permissions'); 
                        }else if(in_array('mt', $roles)){
                            $query->addSelect('mt as permissions');
       //                      $query->whereHas('potholes', function($q) use ($mt){
							// 	$q->whereHas('location', function($q) use ($mt){
							// 		$q->where('province_id', $mt->province_id);
							// 	});
							// }); 
                        }

                    }, 
                    'updater:id,name,avatar,email,phone', 
                    'mt:id,user_id', 
                    'mt.user:id,name,avatar,email,phone'
                ])
                ->orderBy('id', 'DESC')->get(); 
            }
        ])->withCount('comments as num_of_comments'); 

         //=======================================================================>> Conditional checking
        $from=isset($_GET['from'])?$_GET['from']:"";
        $to=isset($_GET['to'])?$_GET['to']:"";
        if(isValidDate($from)){
            if(isValidDate($to)){
               
                $from .=" 00:00:00";
                $to .=" 23:59:59";
                $data = $data->whereBetween('created_at', [$from, $to]);
            }else{
                $to = Carbon::today()->toDateString();
                $from .=" 00:00:00";
                $to .=" 23:59:59";
                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }

        //=======================================================================>>Conditional on status checking
        $statusId      =   intval(isset($_GET['status_id'])?$_GET['status_id']:0); 

        if($statusId != 0){
            $data = $data->where('status_id', $statusId); 
        }

        $key = isset($_GET['key'])?$_GET['key']:"";
        $data = $data->where('is_posted', 1)->where(function($query) use ($key) {
            if(is_numeric($key)){
                $query->where('pothole_id', 'LIKE', '%'.$key.'%'); 
                // dd('hi1');
            }else{

                $potholeData = explode('-', $key); 
                

                if(count($potholeData) == 2){
                    $potholeId = intval($potholeData[1]); 
                    if($potholeId != 0 ){
                        $query->where('pothole_id', $potholeId); 
                    }
                }else{
                    $query->where('description', 'LIKE', '%'.$key.'%');
                }
            }
        }); 
        
        $reporterId      =   intval(isset($_GET['reporter_id'])?$_GET['reporter_id']:0); 
        if($reporterId != 0){
            $data = $data->whereHas('ru', function($query) use ($reporterId){
                $query->where('user_id', $reporterId);
            }); 
        }

        $potholeId      =   intval(isset($_GET['pothole_id'])?$_GET['pothole_id']:0); 
        if($potholeId != 0){
            $data = $data->whereHas('pothole', function($query) use ($potholeId){
                $query->where('id', $potholeId); 
            }); 
        }

        $roadId      =   intval(isset($_GET['road_id'])?$_GET['road_id']:0); 
        if($roadId != 0){
            $data = $data->whereHas('pothole', function($query) use ($roadId){
                $query->where('road_id', $roadId); 
            }); 
        }


        $provinceId      =   intval(isset($_GET['province_id'])?$_GET['province_id']:0); 
        // dd($provinceId);
        if($provinceId != 0){
            $data = $data->whereHas('pothole', function($query) use ($provinceId){
                $query->whereHas('location', function($query) use ($provinceId){
                   // dd(typeof($provinceId));
                    $query->where('province_id', $provinceId); 
                }); 
            }); 
        }

        // Searcch by Type 
        $type = isset($_GET['type'])?$_GET['type']:"";
        if($type){
            $data = $data->where([ 'type_id' => $type ]);
        }

        // if(in_array('ru', $roles)){
        	
        //     $data = $data->whereIn('status_id', [2,4,5,6,7])->orWhereHas('ru', function($query) use ($user) {
        //         $query->whereHas('user', function($query) use ($user) {
        //             $query->where('id', $user->id);
        //         });
        //     });
        //     $data = $data;
        // }


        return $data; 

    }
 
   
}
