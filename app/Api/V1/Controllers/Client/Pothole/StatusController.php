<?php

namespace App\Api\V1\Controllers\Client\Pothole;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\MPWT\Notification as Notify;
use App\MPWT\FileUpload;

use App\Model\Pothole\Main as Pothole;
use App\Model\Pothole\Report as Report;
use App\Model\Pothole\ReportStatus as ReportStatus;
use App\Model\Pothole\Status as PotholeStatus;
use App\Model\Pothole\StatusFile as File;
use App\Model\Pothole\Comment as Comment;
use App\Model\Setting\Status as StatusData;
use App\Model\Pothole\File as ReportFile;
use App\Model\Authority\MT\Main as MT;
use App\Model\Authority\MT\Action as Action;
use App\Model\Authority\MT\ActionDetail as ActionDetail;
use App\Model\Authority\MO\Road as MORoad;

class StatusController extends ApiController
{
    use Helpers;

    function pending(Request $request, $potholeId){
               
        $this->validate($request, [ 
            'comment'                 => 'required'
        ], 
            [
                'comment.required'    => 'Please add your comment'
            ]
        );

        $user       = JWTAuth::parseToken()->authenticate();
        $pothole    = Pothole::find($potholeId);
        if($pothole){
            
            //Create Staus
            $potholeStatus             = new PotholeStatus; 
            $potholeStatus->pothole_id = $potholeId; 
            $potholeStatus->status_id  = 1; //Pending
            $potholeStatus->comment    = $request->input('comment'); 
            $potholeStatus->creator_id = $user->id; 
            $potholeStatus->updater_id = $user->id; 
            $potholeStatus->save();

        
            $pothole->status_id  = 1; //Pending 
            $pothole->save();

            //Update Reports' Status and Notification
            $reports =  explode('-', $request->input('reports'));
            if(count($reports) > 0){
                foreach($reports as $report_id){
                    $report = Report::select('member_id', 'id')->distinct()->with(['ru:id,user_id', 'files:id,report_id,uri'])->where('pothole_id', $potholeId)->find($report_id);
                    
                    if($report){
                        $report->status_id = 1;  //Pending
                        $report->save(); 

                        //Report Status Records
                        $status             = new ReportStatus; 
                        $status->report_id  = $report_id; 
                        $status->status_id  = 1; //Pending 
                        $status->comment    = $request->input('comment'); 
                        $status->creator_id = $user->id; 
                        $status->updater_id = $user->id; 
                        $status->save();     

                        $reportFile = ReportFile::find($report->id);
                        $metaDataSubMt = [
                            'way'           =>  'firebase', 
                            'title'         =>  'New Work!', 
                            'description'   =>  'របាយការណ៍ថ្មី ត្រូវបានបញ្ជួលមកាន់លោកអ្នក។', 
                            'type'          =>  'Pending', 
                            'image'         =>  $reportFile->uri, 
                            'action'       =>   'feeddetail', 
                            'action_id'     =>  $report->id
                        ];
                        Notify::send($report->member_id, $metaDataSubMt);
                    }
                }
            }

           return $this->statusResponse($potholeId, $reports);

        }else{
            return response()->json([
                'status_code'   =>  403,
                'errors'        =>  ['message'  =>  ['invalid pothole']]
            ], 403);
        }

    }
   
    function repairing(Request $request, $potholeId){
        $this->validate($request, [ 
            'mt_id'                 => 'required|exists:mt,id'
        ], 
            [
                'mt_id.required' => 'Please choose an MT', 
                'mt_id.exists' => 'Invalid mt' 
            ]
        );

        $user = JWTAuth::parseToken()->authenticate(); 
        if($user->mo){
            $mt = MT::select('id', 'user_id', 'province_id')->with(['user:id,app_token,name,avatar,phone,email']); 
            if($user->mo->management_type_id == 2){ //Managed by Road
                //Select all roads that manage by this MO
                $moRoadData = MORoad::select('road_id', 'start_pk', 'end_pk')->where('mo_id', $user->mo->id)->get(); 
                $moRoads = []; 

                foreach($moRoadData as $row){
                   $moRoads[] = $row->road_id; 
                }

                $mt = $mt->where(function($query) use ($moRoads){
                    $query->whereHas('roads', function($query) use ($moRoads){
                        $query->whereHas('road', function($query) use ($moRoads){
                            $query->whereIn('road_id', $moRoads); 
                        });
                    }); 
                }); 

               
            }else{//Managed by Boundary
                $mt = $mt->whereHas('mos', function($query) use ($user){
                    $query->whereHas('mo', function($query) use ($user){
                        $query->where('user_id', $user->id); 
                    });
                }); 
            }

            $mt = MT::select('id', 'user_id', 'province_id')->with(['user:id,app_token,name,avatar,phone,email'])->where('id',$request->mt_id)->get();
            if($mt){
                $pothole = Pothole::find($potholeId);
                if($pothole){
                    //if(is_null($pothole->action_id)){

                        //Create an action
                        $action = new Action; 
                        $action->assigner_id = $user->mo->id; 
                        $action->mt_id      = $request->input('mt_id');
                        $action->start_date = date('Y-m-d'); 
                        $action->save(); 

                        //Add Action Detail

                        //Update pothole
                        $pothole->action_id     = $action->id; 
                        $pothole->status_id     = 2; //Reparing
                        $pothole->save();

                        //Update pothole Status
                        $potholeStatus             = new PotholeStatus; 
                        $potholeStatus->pothole_id = $pothole->id; 
                        $potholeStatus->status_id  = 2; //Repairing
                        $potholeStatus->mt_id      = $request->input('mt_id');
                        $potholeStatus->comment    = $request->input('comment');
                        $potholeStatus->creator_id = $user->id; 
                        $potholeStatus->updater_id = $user->id;
                        
                        $potholeStatus->save();
                        //Send notification to RU
                        $reports = explode('-', $request->input('reports')); 
                        $sentList = []; 
                        $reporters = Report::select('member_id', 'id')->distinct()->with(['ru:id,user_id', 'ru.user:id,app_token'])->where('pothole_id', $potholeId)->whereIn('id', $reports)->get(); 
                        $i = 0; 
                        

                        foreach($reporters as $reporter){
                            if(in_array($reporter->id, $reports)){
                                //Update each report status; 
                                $reporter->status_id = 2; //Repairing
                                $reporter->updated_at     = now();
                                $reporter->save();

                                //Update each report status history
                                $status             = new ReportStatus; 
                                $status->report_id = $reporter->id; 
                                $status->status_id  = 2; //Repairing
                                $status->mt_id      = $request->input('mt_id');
                                $status->comment    = $request->input('comment');
                                $status->creator_id = $user->id; 
                                $status->updater_id = $user->id; 
                                $status->save();
                                //Create comment
                                
                                $comment                = New Comment; 
                                $comment->report_id     = $reporter->id;
                                $comment->comment       = 'យើងខ្ញុំបានបញ្ជូន សំណើនេះទៅ '. $mt[0]['user']['name'].' ដើម្បីពិនិត្យលទ្ធភាពដោះស្រាយ. ';
                                $comment->creator_id    = 30; 
                                $comment->save();

                                //Send notification
                                if(!in_array($reporter->id, $sentList)){
                                    $file = ReportFile::select('id', 'uri', 'report_id')->where('report_id', $reporter->id)->first();
                                    $image = ''; 

                                    if($file){
                                        $image = $file->uri; 
                                    }

                                    // $metaData = [
                                    //     'way'           =>  'firebase',
                                    //     'title'         =>  'សំណេីរត្រូវបានពិចារណា',
                                    //     'description'   =>  'យើងខ្ញុំបានបញ្ជូន សំណើនេះទៅ '.$mt[0]['user']['name'].' ដើម្បីពិនិត្យលទ្ធភាពដោះស្រាយ. ', 
                                    //     'type'          =>  'Repairing', 
                                    //     'image'         =>   $image, 
                                    //     'action'       =>   'feeddetail', 
                                    //     'action_id'     =>    $reporter->id
                                    // ];
                                    // // return $reporter->ru->user->id;
                                    // Notify::send($reporter->ru->user->id, $metaData);
                                    $sentList[] = $reporter->id;
                                    
                                    //Send notification to MT
                                    // if($i == 0){
                                       
                                    //     if($mt){
                                    //         $metaData = [
                                    //             'way'           =>  'firebase', 
                                    //             'title'         =>  'New Work!', 
                                    //             'description'   =>  'របាយការណ៍ថ្មី ត្រូវបានបញ្ជួលមកាន់លោកអ្នក។', 
                                    //             'type'          =>  'Repairing', 
                                    //             'image'         =>   $image, 
                                    //             'action'       =>   'feeddetail', 
                                    //             'action_id'     =>    $reporter->id
                                    //         ];

                                    //         Notify::send($mt->user_id, $metaData);

                                    //         $i = 1; 
                                    //     }
                                    // }

                                    //Send notification to Main MT
                                    if($i == 0){
                                        if($mt){
                                            $metaData = [
                                                'way'           =>  'firebase', 
                                                'title'         =>  'New Work!', 
                                                'description'   =>  'របាយការណ៍ថ្មី ត្រូវបានបញ្ជួលមកាន់លោកអ្នក។', 
                                                'type'          =>  'Repairing', 
                                                'image'         =>   $image, 
                                                'action'       =>   'feeddetail', 
                                                'action_id'     =>    $reporter->id
                                            ];

                                            Notify::send($mt[0]->user_id, $metaData);
                                            /** Send Notification to Sub Mt */
                                            $sub_mts =  MT::select('id', 'parent_id', 'user_id','province_id')
                                                            ->with(['user:id,app_token,name,avatar,phone,email'])
                                                            ->where('province_id', $mt[0]['province_id'])->get();

                                            /** loop all sub mt and send notification */
                                            foreach($sub_mts as $sub_mt){
                                                $metaDataSubMt = [
                                                    'way'           =>  'firebase', 
                                                    'title'         =>  'New Work!', 
                                                    'description'   =>  'របាយការណ៍ថ្មី ត្រូវបានបញ្ជួលមកាន់លោកអ្នក។', 
                                                    'type'          =>  'Repairing', 
                                                    'image'         =>   $image, 
                                                    'action'       =>   'feeddetail', 
                                                    'action_id'     =>    $reporter->id
                                                ];
                                                Notify::send($sub_mt->user_id, $metaDataSubMt);
                                            }
                                            $i = 1; 
                                        }
                                    }
                                }  
                            }
                        }

                        return $this->statusResponse($potholeId, $reports);
                        
                    // }else{
                    //     return response()->json(['status'=>'error', 'message'=>'Pothole has already been assigned'], 403);
                    // }
                }else{
                    return response()->json(['status'=>'error', 'message'=>'invalid pothole'], 403);
                }
            }else{
                return response()->json(['status'=>'error', 'message'=>'invalid mt'], 403);
            }
        }else{
            return response()->json(['status'=>'error', 'message'=>'invalid mo'], 403);
        }
    }

    function fixed(Request $request, $potholeId){
        $this->validate($request, [ 
            'files'                 => 'required|json', 
            'reports'               => 'required', 
            'maintenance_code_id'     => 'sometimes|exists:maintence,id',
            'quantity'              => 'sometimes|numeric'
        ], 
            [
                'files.json'    => 'Please provide json format.',
                'maintenance_code_id.required' => 'Please choose a maintence code.', 
                
            ]
        );

        $user       = JWTAuth::parseToken()->authenticate();
        $mt         = MT::with('user:id,name')->where('user_id', $user->id)->first(); 
        $pothole    = Pothole::find($potholeId);  
        if($pothole){
            
            //Start Validate on report duplicate
            $reports =  explode('-', $request->input('reports'));
            if(count($reports) > 0){
                foreach($reports as $report_id){
                    // To query report
                    $report = Report::select('member_id', 'id', 'status_id')
                                ->distinct()
                                ->with([
                                        'ru:id,user_id', 
                                        'files:id,report_id,uri',
                                        'statuses'=>function($query){
                                            $query->select('id', 'report_id', 'status_id', 'mt_id', 'updater_id', 'comment', 'updated_at')
                                            ->with([
                                                'status'=>function($query){
                                                    $query->select('id', 'name', 'requirements as fields')->with(['children:id,parent_id,kh_name,name,requirements']); 
                                                }, 
                                                'updater:id,name,avatar,email,phone', 
                                                'mt:id,user_id', 
                                                'mt.user:id,name,avatar,email,phone'
                                            ])
                                            ->orderBy('id', 'DESC')->get(); 
                                        }
                                        ])
                                ->where('pothole_id', $potholeId)->find($report_id);
                    // TO check condition if the same status
                    $report_status  = $report->statuses[0]->status_id;
                    $status_name    = $report->status->kh_name ;
                    $mt_status_name = $report->statuses[0]->updater->name; 
                    $status_date    = $report->statuses[0]->updated_at;
//                    if($report_status == 4){
//                        return response()->json([
//                            'status_code'   =>  403,
//                            'errors'        =>  ['message'  =>  ['របាយការណ៏នេះត្រូវបាន '.$mt_status_name.' ប្តូទៅ '.$status_name.'រួចហើយ']]
//                        ], 403);
//                    }
                }
            }
            //End of Validate on report duplicate

            //Create Staus
            $potholeStatus             = new PotholeStatus; 
            $potholeStatus->pothole_id = $potholeId; 
            $potholeStatus->status_id  = 4; //Fixed
            $potholeStatus->comment    = $request->input('comment'); 
            $potholeStatus->creator_id = $user->id; 
            $potholeStatus->updater_id = $user->id; 
            $potholeStatus->save();

            //Update pothole status
            if($request->input('maintenance_code_id')){
                $pothole->maintence_id = $request->input('maintenance_code_id');
                if($request->input('quantity')){
                    $pothole->quantity    = $request->input('quantity'); 
                }
            }

            $pothole->status_id  = 4; 
            $pothole->save();

            //Uploading file
            $fixedImage = $this->upload($potholeStatus->id, json_decode($request->input('files')));


            //Send Notification To MO
            if($pothole->action){
                //Find image
                $file = ReportFile::select('id', 'uri', 'report_id')->whereHas('report', function($query) use ($pothole){
                    $query->where('pothole_id', $pothole->id); 
                })->first();

            
                $metaData = [
                    'way'           =>  'firebase', 
                    'title'         =>  'បច្ចុប្បន្ន​ភាព​ របាយការណ៍', 
                    'description'   =>  'ដោយ '.$user->name, 
                    'type'          =>   'Fixed', 
                    'image'         =>   $fixedImage, 
                    'action'       =>   'feeddetail', 
                    'action_id'    =>    $file->report_id
                ];

                Notify::send($pothole->action->assigner->user_id, $metaData);
            }

            //Update Reports' Status and Notification
            $reports =  explode('-', $request->input('reports'));

            if(count($reports) > 0){
                foreach($reports as $report_id){
                    $report = Report::select('member_id', 'id')->distinct()->with(['ru:id,user_id', 'files:id,report_id,uri'])->where('pothole_id', $potholeId)->find($report_id); 
                    if($report){

                        $report->status_id = 4; 
                        $report->updated_at     = now();
                        $report->save(); 

                        //Report Status Records
                        $status             = new ReportStatus; 
                        $status->report_id  = $report_id; 
                        $status->status_id  = 4; 
                        $status->creator_id = $user->id; 
                        $status->updater_id = $user->id; 
                        $status->comment    = $request->input('comment');
                        $status->save();

                        //Adding a fixed file to report. 
//                        $file                   = New ReportFile;
//                        $file->is_fixed_image   = 1;
//                        $file->uri              = $fixedImage;
//                        $file->report_id        = $report_id;
//                        $file->save();
                        $this->uploadFile($report_id,json_decode($request->input('files'),true));

                        if($mt){
                            //Create comment
                            $comment                = New Comment; 
                            $comment->report_id     = $report_id; 
                            $comment->comment       = $mt->user->name.' '.$request->input('comment'); 
                            $comment->creator_id    = $user->id; 
                            $comment->save();

                            //Send Notification to Reporters
                            $metaData = [
                                'way'           =>  'firebase', 
                                'title'         =>  'Report Fixed', 
                                'description'   =>  'របាយការណ៏​របស់អ្នកត្រូវបានដោះស្រាយរួចដោយ '.$mt->user->name.'. សូមអគុណចំពោះការសហការ។',  
                                'type'          =>  'Fixed', 
                                'image'         =>   $fixedImage, 
                                'action'       =>   'feeddetail', 
                                'action_id'     =>    $report->id
                            ];
                            Notify::send($report->ru->user_id, $metaData);
                        }
                           
                    }
                }
            }

           return $this->statusResponse($potholeId, $reports);

        }else{
            return response()->json([
                'status_code'   =>  403,
                'errors'        =>  ['message'  =>  ['invalid pothole']]
            ], 403);
        }

    }

    function declined(Request $request, $potholeId){
               
        $this->validate($request, [ 
            'comment'                 => 'required'
        ], 
            [
                'comment.required'    => 'Please add your comment'
            ]
        );

        $user       = JWTAuth::parseToken()->authenticate();
        $mt         = MT::with('user:id,name')->where('user_id', $user->id)->first(); 
        $pothole    = Pothole::find($potholeId);  
        if($pothole){
            
            //Create Staus
            $potholeStatus             = new PotholeStatus; 
            $potholeStatus->pothole_id = $potholeId; 
            $potholeStatus->status_id  = 3; //Declined
            $potholeStatus->comment    = $request->input('comment'); 
            $potholeStatus->creator_id = $user->id; 
            $potholeStatus->updater_id = $user->id; 
            $potholeStatus->save();

        
            $pothole->status_id  = 3; 
            $pothole->save();

            
            //Send Notification To MO
            if($pothole->action){
                //Find image
                $file = ReportFile::select('id', 'uri', 'report_id')->whereHas('report', function($query) use ($pothole){
                    $query->where('pothole_id', $pothole->id); 
                })->first();
                
                $image = ''; 
                if($file){
                    $image = $file->uri; 
                }

                $metaData = [
                    'way'           =>  'firebase', 
                    'title'         =>  'Update from MT!', 
                    'description'   =>  'From '.$user->name, 
                    'type'          =>   'Declined', 
                    'image'         =>   $image, 
                    'action'       =>   'feeddetail', 
                    'action_id'    =>    $file->report_id
                ];

                Notify::send($pothole->action->assigner->user_id, $metaData);
            }

            //Update Reports' Status and Notification
            $reports =  explode('-', $request->input('reports'));

            if(count($reports) > 0){
                foreach($reports as $report_id){
                    $report = Report::select('member_id', 'id')->distinct()->with(['ru:id,user_id', 'files:id,report_id,uri'])->where('pothole_id', $potholeId)->find($report_id); 
                    if($report){

                        $report->status_id = 3; 
                        $report->updated_at     = now();
                        $report->save(); 

                        //Report Status Records
                        $status             = new ReportStatus; 
                        $status->report_id  = $report_id; 
                        $status->status_id  = 3; 
                        $status->comment    = $request->input('comment'); 
                        $status->creator_id = $user->id; 
                        $status->updater_id = $user->id; 
                        $status->comment    = $request->input('comment');
                        $status->save();      

                        //Create comment
                        $comment                = New Comment; 
                        $comment->report_id     = $report_id; 
                        $comment->comment       = $request->input('comment'); 
                        $comment->creator_id    = $user->id; 
                        $comment->save();

                    }
                }
            }

           return $this->statusResponse($potholeId, $reports);

        }else{
            return response()->json([
                'status_code'   =>  403,
                'errors'        =>  ['message'  =>  ['invalid pothole']]
            ], 403);
        }

    }

    function inplanning(Request $request, $potholeId){
               
        $this->validate($request, [ 
            'comment'                 => 'required'
        ], 
            [
                'comment.required'    => 'Please add your comment'
            ]
        );

        $user       = JWTAuth::parseToken()->authenticate();
        $pothole    = Pothole::find($potholeId);  
        if($pothole){
            
            //Create Staus
            $potholeStatus             = new PotholeStatus; 
            $potholeStatus->pothole_id = $potholeId; 
            $potholeStatus->status_id  = 5; //In Planning
            $potholeStatus->comment    = $request->input('comment'); 
            $potholeStatus->creator_id = $user->id; 
            $potholeStatus->updater_id = $user->id; 
            $potholeStatus->save();

            //Update Pothole Status
            $pothole->status_id  = 5; 
            $pothole->save();


            //Update Reports' Status and Notification
            $reports =  explode('-', $request->input('reports'));

            if(count($reports) > 0){
                foreach($reports as $report_id){
                    $report = Report::select('member_id', 'id')->distinct()->with(['ru:id,user_id', 'files:id,report_id,uri'])->where('pothole_id', $potholeId)->find($report_id); 
                    if($report){

                        $report->status_id = 5; 
                        $report->updated_at     = now();
                        $report->save(); 

                        //Report Status Records
                        $status             = new ReportStatus; 
                        $status->report_id  = $report_id; 
                        $status->status_id  = 5; 
                        $status->creator_id = $user->id; 
                        $status->updater_id = $user->id; 
                        $status->comment    = $request->input('comment');
                        $status->save();

                        //Create comment
                        $comment                = New Comment; 
                        $comment->report_id     = $report_id; 
                        $comment->comment       = $request->input('comment'); 
                        $comment->creator_id    = $user->id; 
                        $comment->save();


                        //Send Notification to Reporters
                        $metaData = [
                            'way'           =>  'firebase', 
                            'title'         =>  'បច្ចុប្បន្នភាពរបាយការណ៍d', 
                            'description'   =>  'សំណើត្រូវបាន “ដាក់ចូលផែនការឆ្នាំ”', 
                            'type'          =>  'Fixed', 
                            'image'         =>   $report->files[0]->uri, 
                            'action'       =>   'feeddetail', 
                            'action_id'     =>   $report->id
                        ];
                        Notify::send($report->ru->user_id, $metaData);
                    }
                }
            }


           return $this->statusResponse($potholeId, $reports);

        }else{
            return response()->json([
                'status_code'   =>  403,
                'errors'        =>  ['message'  =>  ['invalid pothole']]
            ], 403);
        }

    }

    function specialist(Request $request, $potholeId){
               
        $this->validate($request, [ 
            'comment'                 => 'required'
        ], 
            [
                'comment.required'    => 'Please add your comment'
            ]
        );

        $user       = JWTAuth::parseToken()->authenticate();
        $pothole    = Pothole::find($potholeId);  
        if($pothole){
            
            //Create Staus
            $potholeStatus             = new PotholeStatus; 
            $potholeStatus->pothole_id = $potholeId; 
            $potholeStatus->status_id  = 7; //Specialist
            $potholeStatus->comment    = $request->input('comment'); 
            $potholeStatus->creator_id = $user->id; 
            $potholeStatus->updater_id = $user->id; 
            $potholeStatus->save();

            //Update Pothole Status
            $pothole->status_id  = 7; //Specialist
            $pothole->save();


            //Update Reports' Status and Notification
            $reports =  explode('-', $request->input('reports'));

            if(count($reports) > 0){
                foreach($reports as $report_id){
                    $report = Report::select('member_id', 'id')->distinct()->with(['ru:id,user_id', 'files:id,report_id,uri'])->where('pothole_id', $potholeId)->find($report_id); 
                    if($report){

                        $report->status_id = 7;
                        $report->updated_at     = now(); 
                        $report->save(); 

                        //Report Status Records
                        $status             = new ReportStatus; 
                        $status->report_id  = $report_id; 
                        $status->status_id  = 7; 
                        $status->creator_id = $user->id; 
                        $status->updater_id = $user->id; 
                        $status->comment    = $request->input('comment');
                        $status->save();

                        //Create comment
                        $comment                = New Comment; 
                        $comment->report_id     = $report_id; 
                        $comment->comment       = $request->input('comment');
                        $comment->creator_id    = $user->id; 
                        $comment->save();


                        //Send Notification to Reporters
                        $metaData = [
                            'way'           =>  'firebase', 
                            'title'         =>  'បច្ចុប្បន្នភាពរបាយការណ៍', 
                            'description'   =>  'សំណើត្រូវបាន ជូនជំនាញចាតចែង', 
                            'type'          =>  'Fixed', 
                            'image'         =>   $report->files[0]->uri, 
                            'action'       =>   'feeddetail', 
                            'action_id'     =>   $report->id
                        ];
                        Notify::send($report->ru->user_id, $metaData);
                    }
                }
            }


           return $this->statusResponse($potholeId, $reports);

        }else{
            return response()->json([
                'status_code'   =>  403,
                'errors'        =>  ['message'  =>  ['invalid pothole']]
            ], 403);
        }

    }


    function statusResponse($potholeId = 0, $reports = []){
        
        //Fetch authenticated user information
        $user = JWTAuth::parseToken()->authenticate();
        $roles = $this->checkUserPosition($user->id); 

        $reports = Report::select('id', 'pothole_id')->with([
            'statuses'=>function($query) use ($roles){
                $query->select('id', 'report_id', 'status_id', 'mt_id', 'updater_id', 'comment', 'updated_at')
                ->with([
                    'status'=>function($query) use ($roles){
                        $query->select('id', 'name'); 
                        if(in_array('mo', $roles)){
                            $query->addSelect('mo as permissions'); 
                        }else if(in_array('mt', $roles)){
                            $query->addSelect('mt as permissions'); 
                        }
                    }, 
                    'updater:id,name,avatar,email,phone', 
                    'mt:id,user_id', 
                    'mt.user:id,name,avatar,email,phone'
                ])
                ->orderBy('id', 'DESC')->get(); 
            }
        ])->whereIn('id', $reports)->where('pothole_id', $potholeId)->get();

        $statuses = []; 
        foreach($reports as $row){
            if($row->statuses[0]){
                $statuses[] = $row->statuses[0]; 
            } 
        }

        return response()->json([
            'status_code'   => 200, 
            'message'       => 'status updated', 
            'statuses'        => $statuses
        ], 200);
    }



    function upload($statusId = 0, $files){
        $uri = '';
//        if(count($files)>0){
            foreach($files as $file){
                // $uri = FileUpload::forwardFile($file->img, "pothole/status"); 
                $uri = FileUpload::forwardImageResize($file->img, "public/uploads/roadcare/pothole/status/", '500x500');
                $statusFile = new File(); 
                $statusFile->status_id = $statusId; 
                $statusFile->lat = $file->lat; 
                $statusFile->lng = $file->lng; 
                $statusFile->uri = $uri; 
                $statusFile->save(); 
            }
//        }

        return $uri; 
    }
    
    function uploadFile($report_id, $files){
        if(count($files)>0){
            foreach($files as $item){
                // $uri = FileUpload::forwardImageResize($file->img, "public/uploads/roadcare/pothole/status", '500x500');
                // $statusFile = new File();
                // $statusFile->status_id = $statusId;
                // $statusFile->lat = $file->lat;
                // $statusFile->lng = $file->lng;
                // $statusFile->uri = $uri;
                // $statusFile->save();
                        $uri = FileUpload::forwardImageResize($item['img'], "public/uploads/roadcare/pothole/status/", '500x500');
                        $file                   = New ReportFile;
                        $file->is_fixed_image   = 1;
                        $file->uri              = $uri;
                        $file->report_id        = $report_id;
                        $file->save();
            }
        }
    }
  
}
