<?php

namespace App\Api\V1\Controllers\Client\Pothole;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\Model\Pothole\Main as Pothole;
use App\Model\Authority\MT\Main as MT;
use App\Model\Authority\MO\Main as MO;
use App\Model\Authority\MO\Road as MORoad;

class PotholeController extends ApiController
{
    use Helpers;
    function list(){

        $data = Pothole::select('id', 'action_id', 'point_id', 'created_at', 'updated_at')
                ->withCount('reports as n_of_reports')
                        ->with([
                                'comments'=>function($query){
                                    $query->select('id', 'pothole_id', 'creator_id', 'comment', 'created_at')
                                    ->with('commenter:id,name,avatar')->orderBy('id', 'DESC')->get();
                                    }, 

                                'location:id,pothole_id,village_id,commune_id,district_id,province_id', 
                                'location.village:id,name,code', 
                                'location.commune:id,name,code', 
                                'location.district:id,name,code', 
                                'location.province:id,name,code', 

                                'point:id,pk_id,meter', 
                                'point.pk:id,code,road_id', 
                                'point.pk.road:id,name,start_point,end_point',

                                'statuses'=>function($query){
                                    $query->select('id', 'pothole_id', 'status_id', 'updater_id', 'comment', 'updated_at')
                                    ->with([
                                        'status:id,name', 
                                        'updater:id,name,avatar', 
                                        'files:id,uri,status_id'
                                    ])
                                    ->orderBy('id', 'DESC')->get(); 
                                }
                               
                            ]);
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $appends=array('limit'=>$limit);
       
        $from=isset($_GET['from'])?$_GET['from']:"";
        $to=isset($_GET['to'])?$_GET['to']:"";
        if(isValidDate($from)){
            if(isValidDate($to)){
                $appends['from'] = $from;
                $appends['to'] = $to;
                $from .=" 00:00:00";
                $to .=" 23:59:59";
                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }

        $data= $data->orderBy('id', 'desc')->paginate($limit);
        return response()->json($data, 200);
    }

    
    function relatedMTs($potholeId){
        $potholeData = Pothole::select('id', 'point_id')
                    ->with([
                        'point:id,pk_id,meter', 
                        'point.pk:id,code,road_id', 

                        'location:id,pothole_id,village_id,commune_id,district_id,province_id', 
                        'location.village:id,name,code', 
                        'location.commune:id,name,code', 
                        'location.district:id,name,code', 
                        'location.province:id,name,code', 
                       
                    ])->find($potholeId);

        if($potholeData){
            if($potholeData->point){
                $mts = MT::select('id', 'user_id', 'province_id', 'name')
                ->with(['user:id,name,avatar,phone,email', 'province:id,name,abbre'])

                ->whereHas('roads', function($query) use ($potholeData){
                    $query->where('road_id', $potholeData->point->pk->road_id)
                    ->where('start_pk', '<=', $potholeData->point->pk->code)
                    ->where('end_pk', '>=', $potholeData->point->pk->code)
                    ;
                })
                
                ->orderBy('id', 'asc')->get();

                return response()->json(['data'=>$mts, 'potholeData'=>$potholeData, 'status_code'=>200], 200);

            }elseif($potholeData->location){

                $mts = MT::select('id', 'user_id', 'province_id', 'name')
                ->with(['user:id,name,avatar,phone,email', 'province:id,name,abbre'])
                ->where('province_id', $potholeData->location->province_id)->orWhere('id', '>=', 26)->orderBy('id', 'asc')->get();
                return response()->json(['data'=>$mts, 'potholeData'=>$potholeData, 'status_code'=>200], 200);

            }else{
                return response()->json([
                    'status_code'   =>  403,
                    'errors'        =>  ['message'  =>  ['Pothole does not have point.']]
                ], 403);
            }
        }else{
            
            return response()->json([
                'status_code'   =>  403,
                'errors'        =>  ['message'  =>  ['Invalid pothole']]
            ], 403);
        }
    }


}
