<?php

namespace App\Api\V1\Controllers\Seeder\create\project;
use App\Api\V1\Controllers\ApiController;
use App\Model\FieldTesting;
use App\Model\Supervision\Projects\Project;
use Illuminate\Support\Carbon;
use App\Model\Supervision\Projects\ProjectStandardTest;
use Illuminate\Http\Request;


class ProjectController extends ApiController{

    public function project(){

        $name = ["នាយកដ្ធានផ្លូវថ្នល់","នាយកដ្ធានផ្លូវ","នាយកដ្ធានគយ"];
        $data_all = [];
        $res = [];

        //insert data 
        for($i = 0; $i<10; $i++){
            $data = new Project();
            $data->project_type_id = rand(1,5);
            $data->project_owner = $name[rand(0,2)];
            $data->code = rand(1000,3000)."KT-NC"."00".rand(1,10);
            $data->type_id = rand(1,2);
            $data->provinces_id = rand(1,25);
            $data->budget_id = rand(1,2);
            $data->save();
            $data_all[] = $data;
        }


        $data_all = Project::with([
            'workTypes',
            'projectTypes',
            'provinces',
            'budgets',
        ])
        ->get();

        //prepare data to return back
        foreach($data_all as $key => $row){
            $res [] = [
                "id"            => $row['id'] ?? "",
                "code"          => $row['code'] ?? "",
                "entities"      => $row['project_owner'] ?? "",
                "work_type"     => $row->workTypes['name'] ?? "",
                "location"      => $row->provinces['name'] ?? "",
                "budgets"       => $row->budgets['name'] ?? "",
            ];
        }

        return response()->json($res);

    }
    
    public function projectStandardTest(Request $req){
        $count_type_test = FieldTesting::count("*");
        $layer           = ["Subgrate-Embankment","Middle-Subgrade","Upper Subgrade","Sub Base","Base Course","Surfacing AC"];
        $name            = ["យ៉ាន់ ភារក្ស","ខួច គឿន","វង្ស សុខសំណាង","គីម សុនេន","យឹម ឃ្លោក"];
        $data_all        = [];


        //insert data 
        for($i = 0; $i<10; $i++){
            $data = new ProjectStandardTest();
            $data->project_id     = $req->project_id;
            $data->field_testing_id   = rand(1,$count_type_test);
            $data->soil_layer_id     =rand(1,6);
            $data->standard       = rand(10,200)."MPa";
            $data->creator_id     = 1;
            $data->pk_start       = "PK".rand(0,10);
            $data->pk_end         = "PK".rand(50,100);
            $data->date           = "2022-04-25";
            $data->lat_long_start = 0;
            $data->lat_long_end   = 0;
            $data->save();
        }

        $data_all = ProjectStandardTest::with('soilLayer')->get();
        $res = [];


        //prepare format to return back
        foreach($data_all as $key => $row){
            $test_type = FieldTesting::select('name')->where("id",$row['field_testing_id'])->first();
            $res [] = [
                "id"           => $row['id'],
                "project_id"   => $row['project_id'],
                "test_type"    => $test_type['name'],
                "soil_layer"   => $row -> soilLayer ? $row -> soilLayer -> name : null,
                "standard"     => $row['standard'],
                "created_at"   => Carbon::now()->format('Y-m-d'),
                "creator_id"   => $name[rand(1,count($name))-1],
                "location"     => $row['pk_start']."-".$row['pk_end'],
            ];
        }

        return response()->json($res);
    }
    
}