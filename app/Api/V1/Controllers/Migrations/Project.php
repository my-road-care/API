<?php

namespace App\Api\V1\Controllers\Migrations;
use Exception;
use Illuminate\Http\Request;
use App\Model\Supervision\Setup\Budget;
use App\Model\Supervision\Setup\SoilLayer;
use Illuminate\Support\Carbon;
use App\Model\Supervision\Setup\ProjectType;
use App\Model\Road\Main as Road;
use App\Model\User\Main as User;
use App\Model\Provinces\Province;
use App\Model\Supervision\Setup\ProjectOwner;
use App\Model\Supervision\Projects\ProjectMGT;
use App\Model\SupLocation\Commune;
use App\Model\SupLocation\Village;
use Illuminate\Support\Facades\DB;
use App\Api\V1\Service\FileServers;
use App\Model\Supervision\Projects\ProjectPlan;
use App\Model\Supervision\Projects\ProjectTest;
use App\Model\Supervision\Setup\Category;
use App\Model\SupLocation\District;
use App\Model\Supervision\Projects\ProjectImage;
use App\Model\Supervision\Projects\ProjectWorkType;
use App\Api\V1\Controllers\ApiController;
use App\Model\Supervision\Projects\ProjectProcurement;
use App\Model\Supervision\Projects\Project as Projects;
use App\Api\V1\Controllers\Migrations\ProjectInfo;
use App\Model\Supervision\Setup\Organization;
use App\Model\Supervision\Setup\Reviewer;
use App\Model\Supervision\Setup\Executive;
use App\Model\Supervision\Setup\FrameWork;
class Project extends ApiController{

    use FileServers;
    
    // ====================== step 1
    public function createProject(Request $request) 
    {
        $road_id     = $request->road;
        $start_date  = $request->startDate;
        $endDate     = $request->endDate;
        $road    = Road::find($road_id);
        
        $total_type      = ProjectCategory::count('*');
        $total_work_type = WorkType::count('*');
        $budget          = Budget::count('*');
        $entities        = Organization::count('*');
        $reviewer        = Reviewer::count('*');
        $framework       = FrameWork::count('*');
        $executive       = Executive::count('*');
        $user =          count(ProjectOwner::all());

        $data    = new Projects();
        $data->project_type_id              = 1;
        $data->name                         = $road['name'];
        $data->code                         = rand(1,200)."PK-".rand(1,2000);
        $data->provinces_id                 = rand(1,25);
        $data->type_id                 = rand(1,$total_work_type);
        $data->budget_id                    = rand(1,$budget);
        $data->project_owner_id             = rand(1,$user);
        $data->budget_plan                  = "";
        $data->contract_no                  = rand(2000,5000).'សក អល';
        $data->bud_build_req                = number_format(rand(1000000,2000000));
        $data->bud_review_req               = number_format(rand(2000000,3000000));
        $data->bud_after_build              = number_format(rand(4000000,5000000));
        $data->bud_after_review             = number_format(rand(6000000,7000000));
        $data->bud_build_contract           = number_format(rand(7000000,8000000));
        $data->bud_review_contract          = number_format(rand(8000000,9000000));
        $data->total_bud_contract           = number_format(rand(9000000,10000000));
        $data->project_start_date_contract  = $start_date;
        $data->project_end_date_contract    = $endDate; 
        $data->process_contract_duration    = rand(12,50); 
        $data->delay_date                   = Carbon::now()->format('Y-m-d');
        $data->warranty_duration            = rand(12,50); 
        $data->close_project_date           = "2025-01-16";

        $data->save();
        ProjectMGT::insert([
            'project_id'  => $data->id,
            'entity_id'   => rand(1,$entities),
            'reviewer_id'   => rand(1,$reviewer),
            'framework_id'   => rand(1,$framework),
            'executive_director_id'   => rand(1,$executive)
        ]);

        
    }


    public function ProjectFullProcess(Request $request){
        $roadId = $request->id;
        try{
            DB::beginTransaction();

            //  -> step 1
            $projectId = $this->project($roadId);

            //  -> step 2
            ProjectInfo::createWork($projectId);

            //  -> step 3
            ProjectInfo::createProjectPlan($projectId);
            
            //  -> step 4
            ProjectInfo::createProjectProcurement($projectId);

            //  -> step 5
            ProjectInfo::CreateStandartTest($projectId);

            DB::commit();
            return "Success Creating Project";

        }catch (Exception $exception) {
            DB::rollBack();
            return responseError($exception->getTrace());
        }
    }

    
        private function project($roadId){
            $road    = Road::find($roadId);
            $total_type      = ProjectCategory::count('*');
            $total_work_type = WorkType::count('*');
            $budget          = Budget::count('*');
            $entities        = Organization::count('*');
            $reviewer        = Reviewer::count('*');
            $framework       = FrameWork::count('*');
            $executive       = Executive::count('*');
            $user =          count(ProjectOwner::all());
            
            $data    = new Projects();
            $data->project_type_id              = 1;
            $data->name                         = $road['name'];
            $data->code                         = rand(1,200)."PK-".rand(1,2000);
            $data->provinces_id                 = rand(1,25);
            $data->type_id                 = rand(1,$total_work_type);
            $data->budget_id                    = rand(1,$budget);
            $data->project_owner_id             = rand(1,$user);
            $data->budget_plan                  = "";
            $data->contract_no                  = rand(2000,5000).'សក អល';
            $data->bud_build_req                = rand(1000000,2000000);
            $data->bud_review_req               = rand(2000000,3000000);
            $data->bud_after_build              = rand(4000000,5000000);
            $data->bud_after_review             = rand(6000000,7000000);
            $data->bud_build_contract           = rand(7000000,8000000);
            $data->bud_review_contract          = rand(8000000,9000000);
            $data->total_bud_contract           = rand(9000000,10000000);
            $data->project_start_date_contract  = Carbon::now()->format('Y-m-d');
            $data->project_end_date_contract    = Carbon::now()->format('Y-m-d'); 
            $data->process_contract_duration    = "2023-01-16"; 
            $data->delay_date                   = Carbon::now()->format('Y-m-d');
            $data->warranty_duration            = "2023-01-16"; 
            $data->close_project_date           = "2023-01-16";
            $data->save();
            return $data->id;
    }

    public function delete(){
        Projects::getQuery()->delete();
    }
}