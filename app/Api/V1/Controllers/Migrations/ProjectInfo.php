<?php

namespace App\Api\V1\Controllers\Migrations;
use Illuminate\Http\Request;
use App\Model\Supervision\Setup\Budget;
use App\Model\Supervision\Setup\SoilLayer;
use Illuminate\Support\Carbon;
use App\Model\Supervision\Setup\ProjectType;
use App\Model\User\Main as User;
use App\Model\Provinces\Province;
use App\Model\Road\Main as Roads;
use App\Model\Supervision\Setup\ProjectOwner;
use App\Model\SupLocation\Commune;
use App\Model\SupLocation\Village;
use App\Api\V1\Service\FileServers;
use App\Model\Supervision\Projects\ProjectPlan;
use App\Model\Supervision\Setup\Category;
use App\Model\Supervision\Projects\ProjectWork;
use App\Model\SupLocation\District;
use App\Model\Supervision\Projects\ProjectImage;
use App\Model\Supervision\Projects\ProjectWorkType;
use App\Api\V1\Controllers\ApiController;
use App\Model\FieldTesting;
use App\Model\Supervision\Projects\ProjectProcurement;
use App\Model\Supervision\Projects\Project as Projects;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Setup\SlabType;

class ProjectInfo extends ApiController{
    // ====================== step 2
    static function createWork($id){
        $work          = WorkType::count('*');
        $numberOfWorks  = rand(10,20);
        for($i = 0; $i<$numberOfWorks; $i++){

            $district = rand(1,count(District::all()));
            $commune  = rand(1,count(Commune::all()));
            $village  = rand(1,count(Village::all()));
            $start_lo = District::find($district);
            $end_lo   = District::find(rand(1,count(District::all())));

            $data = new ProjectWorkType();
            $data->project_id       = $id;
            $data->project_work_id  = rand(1,$work);
            $data->provinces_id     = rand(1,25);
            $data->districts_id     = $district ?? 1;
            $data->communes_id      = $commune ?? 2;
            $data->villages_id      = $village ?? 3;
            $data->length           = rand(100000,200000);
            $data->width            = rand(1000,2000);
            $data->height           = rand(1000,2000);
            $data->pk_start         = rand(1000,2000);
            $data->pk_end           = rand(1000,2000);
            $data->from_meter       = rand(1000,2000);
            $data->to_meter         = rand(5000,20000);
            $data->start_cor        = rand(5000,20000);
            $data->end_cor          = rand(5000,20000);
            $data->start_location   = $start_lo['name'] ?? "Prey Chor";
            $data->end_location     = $end_lo['name'] ?? "Prey Chor";
            $data->save();
        }

    }

    // ====================== step 3
    static function createProjectPlan($id){
        $title          = ["ប្លង់គំនូសកាត់ទទឹងផ្លូវ","ប្លង់លូរកាត់ទទឹងផ្លូវ","ប្លង់ពំនុះកាត់ទទឹងផ្លូវ"];
        $numberOfPlans  = rand(2,3);

        for($i = 0; $i < $numberOfPlans; $i++){

            $data = new ProjectPlan();
            $data->project_id = $id;
            $data->title      =  $title[rand(1,count($title)-1)];
            $data->docu_code  =  rand(1000,50000);
            
            $data->creator_id =  rand(1,User::count("*"));
            
            $data->image_uri  =  self::image();
            $data->description=  str_random(15);
            $data->save();
        }

    }

    // ====================== step 4
    static function createProjectProcurement($id){
        $title  = ["ប្លង់គំនូសកាត់ទទឹងផ្លូវ","ប្លង់លូរកាត់ទទឹងផ្លូវ","ប្លង់ពំនុះកាត់ទទឹងផ្លូវ"];
        $numberOfProjectProcurement  = rand(2,3);
        for($i = 0; $i < $numberOfProjectProcurement; $i++){

            $data = new ProjectProcurement();
            $data->project_id = $id;
            $data->title      =  $title[rand(1,count($title)-1)];
            $data->docu_code  =  rand(1000,50000);
            $data->creator_id =  rand(1,count(User::all()));
            $data->image_uri  =  self::image();
            $data->description=  str_random(15);
            $data->save();

        }

    }

    static function image(){
        $images = [
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5124018c69.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c512533da75.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c512533da75.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5127072dc4.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5127d580bd.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5129a9c84c.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c512a61883e.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c512b307ee0.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c512bd617f2.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c512c96e037.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c512d44890a.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c512dec70db.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c512efe7fec.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c512fb18b02.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c51305d60ac.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5130f23a70.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c51318d3898.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5132583013.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5132fec415.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5133ce513e.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5134a60970.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5137ecf372.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c5138a175d2.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c513a033200.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c513ab9f328.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c513b79358d.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c513c2ae81f.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c513cbedb0b.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c513d642c69.jpg",
            "uploads/TEST/M-1000000px/RoadCare/20230116/09/63c513e230c59.jpg"
        ];

        return $images[rand(1,count($images))-1];
    }


    static function CreateStandartTest($id){

        dd("Hi");
        $data  = new ProjectStandardTest();
        $data -> project_id         = $id;
        $data -> project_type_id    = 1;
        $data -> field_testing_id       = rand(1,FieldTesting::count("*"));
        $data -> soil_layer_id      = rand(1,SoilLayer::count("*"));
        $data -> creator_id         = rand(1,User::count("*"));
        $data -> standard           = str_random(10);;
        $data -> date               = Carbon::now();
        $data -> pk_start           = "PK".rand(0,100);
        $data -> pk_end             = "PK".rand(100,200);
        $data -> lat_long_start     = rand(-100,100);
        $data -> lat_long_end       = rand(100,4000);
        $data->save();

    }
}