<?php

namespace App\Api\V1\Controllers\Migrations;
use App\Api\V1\Controllers\ApiController;
use Illuminate\Support\Carbon;
use App\Model\Supervision\Projects\ProjectStandardTest;
use Illuminate\Http\Request;
use App\Model\Road\Main as Roads;
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProjectImage;
use App\Model\Supervision\Setup\Category;
use App\Model\Provinces\Province;
use App\Model\Supervision\Setup\ProjectType;
use App\Model\Supervision\Setup\Budget;
use App\Model\Supervision\Setup\Organization;

class Road extends ApiController{

    public function CreateProjectInfo(){
        
        $total_type      = ProjectCategory::count('*');
        $road            = Roads::all();
        $total_work_type = WorkType::count('*');
        $budget          = Budget::count('*');
        $entities    = Organization::count('*');
        $ministry = ["ក្រសួងសាធារណការ និងដឹកជញ្ជូន","ក្រសួងបរិស្ថាន","ក្រសួងសេដ្ឋកិច្ច និង ហិរញ្ញវត្ថុ"];

        foreach($road as $row){
            $data = new Project();
            $data->project_type_id              = rand(1,$total_type);
            $data->name                         = $row['name'];
            $data->code                         = rand(1,200)."PK-".rand(1,2000);
            $data->provinces_id                 = rand(1,25);
            $data->type_id                 = rand(1,$total_work_type);
            $data->budget_id                    = rand(1,$budget);
            $data->entity_id              = rand(1,$entities);
            $data->budget_plan                  = "";
            $data->framework                    = "នាយកដ្ធាន".rand(1,100);
            $data->executive_director           = $ministry[rand(1,count($ministry))-1];
            $data->project_owner                = "នាយកដ្ធាន".rand(1,100);
            $data->reviewer                     = "នាយកដ្ធាន".rand(1,100);
            $data->contract_no                  = rand(2000,5000).'សក អល';
            $data->bud_build_req                = 1000000;
            $data->bud_review_req               = 2000000;
            $data->bud_after_build              = 2000000;
            $data->bud_after_review             = 2000000;
            $data->bud_build_contract           = 2000000;
            $data->bud_review_contract          = 2000000;
            $data->total_bud_contract           = 2000000;
            $data->project_start_date_contract  = Carbon::now()->format('Y-m-d');
            $data->project_end_date_contract    = Carbon::now()->format('Y-m-d'); 
            $data->process_contract_duration    = rand(10,50); 
            $data->delay_date                   = Carbon::now()->format('Y-m-d');
            $data->warranty_duration            = rand(10,50); 
            $data->close_project_date           = Carbon::now()->format('Y-m-d');
            $data->save();
        }

        $project = Project::all();
        foreach($project as $row){
            $data = new ProjectImage();
            $data->project_id             = $row['id'];
            $data->project_images_type_id = rand(1,3);
            $data->creator_id             = 1;
            $data->title                  = str_random(10);
            $data->image_uri              = rand(1,20).".jpg";
            $data->save();
        }

        return response()->json([
            "message" => "success",
            "data"    => $res
        ]);
    }
}