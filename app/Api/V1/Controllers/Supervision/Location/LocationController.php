<?php

namespace App\Api\V1\Controllers\Supervision\Location;

// ===============>> Service <<=============
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Http\Collections\Setup\CommuneCollection;
use App\Api\V1\Http\Collections\Setup\DistrictCollection;
use App\Api\V1\Http\Collections\Setup\ProvinceCollection;
use App\Api\V1\Http\Collections\Setup\VillageCollection;


// ==============>> Model <<=================
use App\Model\Location\Commune;
use App\Model\Location\District;
use App\Model\Location\Province;
use App\Model\Location\Village;

class LocationController extends ApiController
{
    public function getProvince(){
        $provinces = Province::get();
        return new ProvinceCollection($provinces);
    }

    public function getDistrict($id){
        $districts = District::where('province_id', $id)->get();
        return new DistrictCollection($districts);
    }

    public function getCommune($id){
        $communes = Commune::where('district_id', $id)->get();
        return new CommuneCollection($communes);
    }

    public function getVillage($id){
        $villages = Village::where('commune_id', $id)->get();
        return new VillageCollection($villages);
    }


}