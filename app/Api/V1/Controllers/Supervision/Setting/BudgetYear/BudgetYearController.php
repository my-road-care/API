<?php

namespace App\Api\V1\Controllers\Supervision\Setting\BudgetYear;

// =================>> Service <<================
use App\Api\V1\Controllers\ApiController;
use Illuminate\Http\Request;

// ================>> Model <<=================
use App\Model\Supervision\Projects\BudgetYear;

class BudgetYearController extends ApiController
{
    public function listing (){

        $data =  BudgetYear::with([
            'project:id,name,code,budget_year_id,project_status_id,budget_id',
            'project.status',
            'project.budgets'])
        ->withCount(['project'])
        ->orderBy('year','DESC')
        ->get();
        return response()->json($data,200);
        
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'year' => 'required|unique:sup_year,year'
        ],[
            'year.unique' => 'ឆ្នាំបានបង្កើតរួចហើយ។', 
        ]
    );
        $data = new BudgetYear();
        $data->year = $request->year;
        $data->save();

        return response()->json($data, 200);
    }

    public function delete ($id)
    {       
        $data = BudgetYear::where('id',$id)->first();

        if($data){
            $data->delete();
            return response()->json([
                "message" => "លុបទិន្នន័យជោគជ័យ!"
            ]);
        }
        return response() ->json ([
                "message" => "ទិន្នន័យរកមិនឃើញ"
        ]);
    }

    public function update(Request $request)
    {
        $id = $request->id;

        $data = BudgetYear::where('id', $id)->first();

        if ($data) {
            $data->year   = $request->year;
            $data->save();

            return response()->json($data,200);
                return response()->json([
                    "data"    => $data,
                    "message" => "កែប្រែបានជោគជ័យ!"
                ]);
        }
    }
}