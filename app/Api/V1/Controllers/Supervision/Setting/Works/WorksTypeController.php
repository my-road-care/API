<?php
namespace App\Api\V1\Controllers\Supervision\Setting\Works;

// =============>> Service <<===============
use App\Api\V1\Controllers\ApiController;

// ==============>> Model <<=================
use App\Model\Supervision\Setup\WorkType;

Class WorksTypeController extends ApiController{

    public function listing(){
        $data = WorkType::get();
        return response()->json($data,200);
    }

    
}