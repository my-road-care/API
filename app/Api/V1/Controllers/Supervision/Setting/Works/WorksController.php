<?php

namespace App\Api\V1\Controllers\Supervision\Setting\Works;

// ==============>> Service <<===============
use App\Api\V1\Controllers\ApiController;
use Illuminate\Http\Request;
use App\MPWT\FileUpload;

// ================>> Model <<=================
use App\Model\Supervision\Setup\WorkTestMethod;
use App\Model\Supervision\Setup\TestingMethod;
use App\Model\Supervision\Setup\Work;
use Tymon\JWTAuth\JWTAuth;

class WorksController extends ApiController
{
    public function listing(Request $request){
        
        if($request->type){
            
            $data = Work::with(['type','group','user','testingmethods'])
            ->where('type_id', 1)
            ->orderBy('group_id','DESC')
            ->get();
            return response()->json($data, 200);

        }

        if($request->testing_method_id){
            $check =  WorkTestMethod::where(['testing_method_id' => $request->testing_method_id])->get();

            $workID = [];
            foreach($check as $row){
                $workID[] = $row['work_id'];
            }

            $data = Work::whereNotIn('id',$workID)
            ->where('type_id', 1)
            ->with(['type','group','user','testingmethod'])
            ->orderBy('group_id','DESC')->get();

            return response()->json($data, 200);
        }
    }

    public function create(Request $request){

        $this->validate($request, [
            'name'      => 'required',
            'type_id'   => 'required',
            'image_uri' => 'required',
            'group_id'  => 'required',
        ]);

        $data = new Work();
        $data->en_name    = $request->name;
        $data->kh_name    = $request->name;
        $data->group_id   = $request->group_id;
        $data->type_id    = $request->type_id;
        $data->creator_id = JWTAuth::parseToken()->authenticate()->id;

        if($request->image_uri){
            $file = $request->image_uri;
            $image = FileUpload::forwardFile($file);
            
            $data->image = $image;
        }

        $data->save();
        return response()->json($data,200);
    }

    public function show(Request $request){

        $id = $request->work_id;
        $data = WorkTestMethod::where('work_id',$id)->get();
        $datas = [];

        foreach($data as $row){
            $datas[] = TestingMethod::with(['parameters'])
            ->where('id',$row['testing_method_id'])->first();
        }

        return response()->json($datas);

    }

    public function delete(Request $request) {

        $id = $request->id;
        $data = Work::where('id', $id)->first();
        if ($data) {
            $data->delete();
            return response()->json([
                "message" => "លុបប្រភេទការងារជោគជ៏យ"
            ]);
        }
    }

    public function update(Request $request){

        $id = $request->id;
        $data = Work::where('id', $id)->first();

        if ($data) {
            $data->en_name   = $request->name;
            $data->kh_name   = $request->name;
            $data->type_id   = $request->type_id;
            $data->group_id   = $request->group_id;
            $data->creator_id = JWTAuth::parseToken()->authenticate()->id;
            
            if($request->image_uri){
                $file = $request->image_uri;
                $image = FileUpload::forwardFile($file);
                
                $data->image = $image;
            }

            $data->save();

            return response()->json($data,200);
                return response()->json([
                    "data"    => $data,
                    "message" => "កែប្រែជោគជ៏យ"
                ]);
            }
    }

    public function TesthingMethod(Request $request){

        $data = WorkTestMethod::where(['testing_method_id'=>$request->testing_method_id,'work_id'=>$request->work_id])
        ->first();
        if($data){
            WorkTestMethod::destroy($data['id']);

            return response()->json(['message'=>'delete success'],200);
        }
        
        $data = new WorkTestMethod();
        $data->work_id           = $request->work_id;
        $data->testing_method_id = $request->testing_method_id;
        $data->save();
        return response()->json($data,200);
    }

}
