<?php
namespace App\Api\V1\Controllers\Supervision\Setting\Works;


// =============>> Service <<===============
use App\Api\V1\Controllers\ApiController;

// ============>> Model <<=================
use App\Model\Supervision\Setup\WorkGroup;

Class WorksGroupController extends ApiController{

    public function listing(){
        $data = WorkGroup::get();
        return response()->json($data,200);
    }
    
}