<?php

namespace App\Api\V1\Controllers\Supervision\Setting\TestingStandard;

use App\Api\V1\Controllers\ApiController;
use App\Model\Supervision\Projects\Group;
use App\Model\Supervision\Projects\Standard;
use App\Model\Supervision\Setup\TestingMethod;
use Illuminate\Http\Request;
use App\MPWT\FileUpload;
use App\Model\Supervision\Setup\WorkTestMethod;
use App\Model\Supervision\Setup\Work;
use App\Model\Supervision\Setup\TestingStandard;

class TestingStandardController extends ApiController
{
    public function getGroup(){
        
        $groups = Group::select('id', 'name')->withCount('roads')->get();
        return $groups;
    }
      
    public function getStandard($groupId = 0){

        $layerData     = Work::select('id', 'kh_name as name')
        ->where('type_id', 1)
        // ->with([
        //     'methods', 
        //     'methods.standards'
        // ])
        ->get(); 
        $methodData    = TestingMethod::select('id', 'name')
        ->get();

        $methods = []; 
        $layers  = []; 

        foreach($methodData as $row){

            $standards = []; 
            $layers  = [];

            foreach($layerData as $layerRow){
                $layers[]   = $layerRow->name;
                $value      = ''; 
                $des        = '';

                $testingMethod = WorkTestMethod::where([
                    'work_id'           => $layerRow->id, 
                    'testing_method_id' => $row->id
                ])
                ->whereHas('standards', function($query) use ($groupId, $layerRow){
                    $query->where([
                        'group_id' => $groupId,
                        'layer_id' => $layerRow->id
                    ]); 
                })
                ->with([
                    'standards' => function ($query) use ($groupId, $layerRow){
                        $query->where([
                            'group_id' => $groupId,
                            'layer_id' => $layerRow->id
                        ]); 
                    }
                ])
                ->first(); 
                $method_id=null;
                if($testingMethod && $testingMethod->standards){
                    if(isset($testingMethod->standards[0])){
                        $value = $testingMethod->standards[0]->value; 
                        $des = $testingMethod->method->less_than_pass == 1 ? 'X <= '.$value. ' ' .$testingMethod->method->unit : 'X >= '.$value. '' .$testingMethod->method->unit; 
                        $method_id=$testingMethod->standards[0]->method_id;
                    }
                    
                }

            
                $standards[] = [
                    'layer_id'      => $layerRow->id, 
                    'method_id'     => $method_id,
                    'value'         => $value, 
                    'des'           => $des
                ]; 
            }
            

            $methods[] = [
                'id'        => $row->id, 
                'name'      => $row->name, 
                'standards' => $standards
            ]; 
        }

        return [
            'layers' => $layers, 
            'methods' => $methods
        ];
    }    
    
    public function createStandard(Request $request){

        $methodData = WorkTestMethod::where([
            'work_id'           => $request->layer_id, 
            'testing_method_id' => $request->method_id
        ])
        ->whereHas('method')
        ->with(['method'=>function($q){
            $q->select('id','less_than_pass','unit')->get();
        }])
        ->first();

        if($methodData){
            $method = TestingStandard::where([
                'layer_id'=>$request->layer_id,
                'group_id'=> $request->group_id,
                'method_id'=> $methodData->id])->first();
            if($method){
                return responseError("ទិន្ន័យមានរួចរាល់");
            }else{
                $data=new  TestingStandard();
                $data->layer_id  = $request->layer_id;
                $data->group_id  = $request->group_id;
                $data->method_id = $methodData->id;
                $data->value     = $request->value;
                $data->save();

                if($methodData && $methodData->method){

                    if(isset($methodData->method)){
                        $value = $data->value;
                        $data->des = $methodData->method->less_than_pass == 1 ? 'X <= '.$value. ' ' .$methodData->method->unit : 'X >= '.$value. '' .$methodData->method->unit; 
                    }
                }
                return [
                    'data'    => $data,
                    'message' => "ទិន្ន័យបានបង្កើត"
                ]; 
            }
        }else{
            $workTestMethod=new WorkTestMethod();
            $workTestMethod->work_id            =  $request->layer_id;
            $workTestMethod->testing_method_id  =  $request->method_id;
            $workTestMethod->save();

            $data=new  TestingStandard();
            $data->layer_id  = $request->layer_id;
            $data->group_id  = $request->group_id;
            $data->method_id = $workTestMethod->id;
            $data->value     = $request->value;
            $data->save();
            
            if($workTestMethod){        
                if(isset($workTestMethod)){
                    $value = $data->value;
                    $data->des = $workTestMethod->less_than_pass == 1 ? 'X <= '.$value. ' ' .$workTestMethod->unit : 'X >= '.$value. '' .$workTestMethod->unit; 
                }
            }
            return [
                'data'    => $data,
                'message' => "ទិន្ន័យបានបង្កើត"
            ]; 
           
        }      
       
    } 

    public function delete(Request $request){
        $method = TestingStandard::where([
            'layer_id'  => $request->layer_id,
            'group_id'  => $request->group_id,
            'method_id' => $request->method_id
            ])
            ->first();
        if($method){
            $method->delete();
            return response()->json([
                "data"    => $method,
                "message" => "លុបស្តងដាតេស្តជោគជ៏យ"
            ]);
        }else{
            return responseError("មិនមានទិន្ន័យ"); 
        }
    }

    public function updateStandard(Request $request){
        $method = TestingStandard::where([
            'layer_id'=>$request->layer_id,
            'group_id'=> $request->group_id,
            'method_id'=> $request->method_id])->first();
           
        if($method){
            $method->update([
                "value" => $request->value
            ]);
            // get less_then_pass
            $methodData = WorkTestMethod::whereHas('method')->with(['method'=>function($q){$q->select('id','less_than_pass','unit')->get();}])->find($request->method_id);
            if($methodData){
                $value = $method->value;
                $method->des = $methodData->method->less_than_pass == 1 ? 'X <= '.$value. ' ' .$methodData->method->unit : 'X >= '.$value. '' .$methodData->method->unit;  
            }
            return [
                'data'    => $method,
                'message' => "ទិន្ន័យបានកែប្រែ"
            ];
        }else{
            return responseError("មិនមានទិន្ន័យ");
        }
    }

    private function getMethod($methods = null){
        $method = [];
        foreach($methods as $row){
            if($row->method){
                if($row->method->less_than_pass == 1){
                    $lessThanPass = 1;
                }else{
                    $lessThanPass = 0;
                }
                $method[] = [
                    'id'           => $row->method->id,
                    'standard_id'  => $row->id,
                    'name'         => $row->method->name,
                    'unit'         => $row->method->unit,
                    'value'        => $this->getValue($row, $lessThanPass)
                ];
            }
        }
        return $method;
    }

    private function getValue($standard = null, $lessThanPass = 0){

        $value = 0;
        if($standard){
            if($lessThanPass == 1){
                $value = "X <= ".$standard->value;
            }else{
                $value = "X >= ".$standard->value;
            }
        }
        return [
            'des' => $value,
            'value' => $standard->value
        ];
    }
    public function listing(Request $request){
        
        if($request->type){
            $type = $request->type;
            $data = TestingMethod::with(['TestingMethodType','parameters','works_testingmethod'])
            ->where('type_id',$type)
            ->orderBy('id','DESC')
            ->get();

            foreach($data as $row){
                $works = [];
                foreach($row['works_testingmethod'] as $work){
                    $works[] = Work::where('id',$work['work_id'])->first();
                }
                $row['works'] = $works;
            }
            return response()->json($data,200);
        }

        if($request->work_id){
            $workId = $request->work_id;
            $data = WorkTestMethod::where('work_id',$workId)->get();
            $methodID = [];

            foreach($data as $row){
                $methodID[] = $row['testing_method_id'];
            }

            $data = TestingMethod::whereNotIn('id',$methodID)
            ->select('id','type_id','description','name','unit','image')->with(['TestingMethodType','parameters','works_testingmethod'])
            ->orderBy('id','DESC')
            ->get();

            return response()->json($data, 200);
        }

        $data = TestingMethod::select('id','type_id','description','name','unit','image')
        ->with(['TestingMethodType','parameters','works_testingmethod'])
        ->orderBy('id','DESC')
        ->get();
        
        return response()->json($data, 200);
    }
}
