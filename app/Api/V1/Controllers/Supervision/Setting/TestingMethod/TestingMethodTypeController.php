<?php
namespace App\Api\V1\Controllers\Supervision\Setting\TestingMethod;

// =================>> Service <<==============
use App\Api\V1\Controllers\ApiController;

// =================>> Model <<================
use App\Model\Supervision\Setup\TestingMethodType;

class TestingMethodTypeController extends ApiController
{   

    public function listing()
    {
        $data = TestingMethodType::select('id','name')->orderBy('id','DESC')->get();
        return response()->json($data, 200);
    }
}
