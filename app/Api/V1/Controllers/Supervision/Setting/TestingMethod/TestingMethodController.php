<?php

namespace App\Api\V1\Controllers\Supervision\Setting\TestingMethod;


// ================>> Service <<=================
use App\Api\V1\Controllers\ApiController;
use Illuminate\Http\Request;
use App\MPWT\FileUpload;

// =================>> Model <<===================
use App\Model\Supervision\Setup\TestingMethod;
use App\Model\Supervision\Setup\TestingMethodTypParamater;
use App\Model\Supervision\Setup\WorkTestMethod;
use App\Model\Supervision\Setup\Work;

class TestingMethodController extends ApiController
{
    private function getMethod($methods = null){
        $method = [];
        foreach($methods as $row){
            if($row->method){
                if($row->method->less_than_pass == 1){
                    $lessThanPass = 1;
                }else{
                    $lessThanPass = 0;
                }
                $method[] = [
                    'id'           => $row->method->id,
                    'standard_id'  => $row->id,
                    'name'         => $row->method->name,
                    'unit'         => $row->method->unit,
                    'value'        => $this->getValue($row, $lessThanPass)
                ];
            }
        }
        return $method;
    }

    private function getValue($standard = null, $lessThanPass = 0){

        $value = 0;
        if($standard){
            if($lessThanPass == 1){
                $value = "X <= ".$standard->value;
            }else{
                $value = "X >= ".$standard->value;
            }
        }
        return [
            'des' => $value,
            'value' => $standard->value
        ];
    }
    public function listing(Request $request){
        
        if($request->type){
            $type = $request->type;
            $data = TestingMethod::with(['TestingMethodType','parameters','works_testingmethod'])
            ->where('type_id',$type)
            ->orderBy('id','DESC')
            ->get();

            foreach($data as $row){
                $works = [];
                foreach($row['works_testingmethod'] as $work){
                    $works[] = Work::where('id',$work['work_id'])->first();
                }
                $row['works'] = $works;
            }

            return response()->json($data,200);
        }

        if($request->work_id){

            $workId = $request->work_id;
            $data = WorkTestMethod::where('work_id',$workId)->get();
            $methodID = [];

            foreach($data as $row){
                $methodID[] = $row['testing_method_id'];
            }

            $data = TestingMethod::whereNotIn('id',$methodID)
            ->select('id','type_id','description','name','unit','image')->with(['TestingMethodType','parameters','works_testingmethod'])
            ->orderBy('id','DESC')
            ->get();

            return response()->json($data, 200);
        }

        $data = TestingMethod::select('id','type_id','description','name','unit','image')->with(['TestingMethodType','parameters','works_testingmethod'])->orderBy('id','DESC')->get();
        return response()->json($data, 200);
    }

    public function create(Request $request){

        $this->validate($request, [
            'name'        => 'required',
            'unit'        => '',
            'image_uri'   => 'required',
            'type_id'     => 'required',
        ]);

        $data = new TestingMethod();
        $data->name        = $request->name;
        $data->unit        = $request->unit;
        $data->type_id     = $request->type_id;
        $data->description = $request->description;

        if($request->image_uri){
            $file = $request->image_uri;
            $image = FileUpload::forwardFile($file);
            
            $data->image = $image;
        }
        
        $data->save();
        return response()->json($data,200);
    }

    public function show(Request $request){

        $id = $request->testing_method_id;
        $data = WorkTestMethod::where('testing_method_id',$id)->get();
        $datas = [];

        foreach($data as $row){
            $datas[] = Work::where('id',$row['work_id'])->first();
        }
        return response()->json($datas);

    }

    public function delete(Request $request){

        $id = $request->id;
        $data = TestingMethod::where('id', $id)->first();
        if ($data) {

            $data->delete();

            $check = WorkTestMethod::where('testing_method_id',$request->id)->first();

            if($check){
                $check->delete();
            }
            
            return response()->json([
                "message" => "លុបប្រភេទតេស្តជោគជ៏យ"
            ]);
        }
    }

    public function update(Request $request){

        $id = $request->id;
        $data = TestingMethod::where('id', $id)->with(['parameters'])->first();

        if ($data) {
            $data->name      = $request->name;
            $data->unit      = $request->unit;
            $data->type_id   = $request->type_id;
            $data->description = $request->description;
            
            if($request->image_uri){
                $file = $request->image_uri;
                $image = FileUpload::forwardFile($file);                
                $data->image = $image;
            }

            $data->save();

            if($request->paramData){
                $paramData = [];                
                foreach($request->paramData as $row){                    
                    $paramData[] = ["testing_method_id"=>$data['id'],"name"=>$row];
                }                
                TestingMethodTypParamater::insert($paramData);
            }

            return response()->json([
                "data"    => $data,
                "message" => "កែប្រែជោគជ៏យ"
            ]);
        }
    }

    public function addParam(Request $request){

        $this->validate($request, [
            'name'      => 'required',
            'method_id' => 'required',
        ]);

        $name     = $request->name;
        $methodID = $request->method_id;
        $unit     = $request->unit;

        $data                    = new TestingMethodTypParamater();
        $data->testing_method_id = $methodID;
        $data->unit              = $unit;
        $data->name              = $name;

        $data->save();

        return response()->json($data,200);

    }
    
    public function deleteParam(Request $request){
        
        $this->validate($request, [
            'id' => 'required',
        ]);

        $data = TestingMethodTypParamater::where('id',$request->id)->first();
        
        if($data){
            $data->delete();
        }
        
        return response()->json($data);
    }  
}
