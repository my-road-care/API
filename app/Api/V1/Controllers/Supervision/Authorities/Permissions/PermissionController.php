<?php

namespace App\Api\V1\Controllers\Supervision\Authorities\Permissions;


// =================>> Service <<=================
use Illuminate\Http\Request;
use App\Api\V1\Controllers\ApiController;
use Illuminate\Support\Facades\DB;

// ==================>> Model <<===================
use App\Model\Supervision\Permission\PerPart;
use App\Model\Supervision\Permission\Action;
use App\Model\Supervision\Permission\PerPermission;

class PermissionController extends ApiController
{
    static public function ListingPerPart()
    {
        $parts   = PerPart::all();
        $roles   = DB::table('sup_per_system_possition')->get();
        $actions = Action::all();
        $body    = [];
        $header  = $roles;

        foreach ($parts as $part) {
            $row['title'] = $part->slug;
            $row['part_id'] = $part->id;
            $columns = [];
            foreach ($roles as $role) {

                $checks = [];                
                foreach ($actions as $action) {

                    $value = PerPermission::where([
                        "role_id" => $role->id,
                        "part_id" => $part->id,
                        "action_id" => $action->id,
                    ])->first();

                    $checks[] = [
                        "action_id" => $action->id,
                        "per_id"    => $value ? $value->id : 0,
                        "value" => $value ? 1 : 0
                    ];
                }

                $columns[] = [
                    'role'    => $role->name,
                    'role_id' => $role->id,
                    'checks' => $checks
                ];
            }
            $row['columns'] = $columns;
            $body[] = $row;
        }

        return [
            "headers" => $header,
            "actions" => $actions,
            "body"  => $body
        ];
    }

    public function update(Request $req)
    {
        $roleId   = $req->roleId;
        $partId   = $req->partId;
        $actionId = $req->actionId;
        $check    = PerPermission::where([
            "role_id" => $roleId,
            "part_id" => $partId,
            "action_id" => $actionId,
        ])->first();
        
        if ($check) {
            $check->delete();
            return $check;
        } else {
            $data = new PerPermission();
            $data->role_id = $roleId;
            $data->part_id = $partId;
            $data->action_id = $actionId;
            $data->save();
            return $data;
        }
    }

    public function create(Request $req)
    {
        $data = new  PerPart();
        $data->section = $req->section;
        $data->part    = $req->part;
        $data->name    = $req->name;
        $data->slug    = $req->slug;
        $data->save();        
        $data = $this->ListingPerPart();
        return response()->json($data);
    }

    
}
