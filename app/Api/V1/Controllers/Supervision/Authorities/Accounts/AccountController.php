<?php

namespace App\Api\V1\Controllers\Supervision\Authorities\Accounts;

// =================>> Service <<=================
use Illuminate\Http\Request;
use App\Api\V1\Controllers\ApiController;
use App\Enum\Role as EnumRole;
use App\MPWT\FileUpload;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

// ================>> Model <<==============
use App\Model\User\Type;
use App\Model\Supervision\Setup\OrganizationUser;
use App\Model\User;
use App\Model\Supervision\Setup\Role;
use App\Model\Supervision\Setup\ProjectPossition;
use App\Model\Supervision\Projects\ProjectWorkingGroup;
use App\Model\Supervision\Setup\SystemPossition;
use App\Model\Authority\MO\Main;
use App\Model\Authority\MT\Main as MTMain;
use App\Model\Member\Main as MemberMain;

class AccountController extends ApiController
{

    public function getProjectRole(){
        
        
        $data = ProjectPossition::get();

        $total = 0;
        foreach($data as $row){
            $row['qty'] = ProjectWorkingGroup::where('role_id',$row['id'])
            ->count('*');
            $total += $row['qty'];
        }

        return response()->json([
            "data"=>$data,
            "total"=>$total
        ]);

    }

    public function getSystemRole(){

        $data = SystemPossition::get();

        $total = 0;
        foreach($data as $row){
            $row['qty'] = ProjectWorkingGroup::where('sys_id',$row['id'])
            ->count('*');
            $total += $row['qty'];
        }

        return response()->json([
            "data"=>$data,
            "total"=>$total
        ]);
    }

    public function getUser(Request $request)
    {
        $limit      =   intval(isset($_GET['limit']) ? $_GET['limit'] : 10);
        
        $data = User::query();
        $userType = auth()->user()->type_id;

        if($userType == EnumRole::SuperAdmin){

            $data->select('id', 'name', 'type_id', 'phone', 'email', 'avatar' , 'role_id', 'organization_id', 'is_active')
            ->with(['oranizationRole:id,name', 'organization','type'])
            ->where('id','!=',auth()->id())
            ->where('is_phone_verified', 1)
            ->whereNotIn('type_id', [
                EnumRole::PotholeUser,
                EnumRole::RU,
                EnumRole::MO,
                EnumRole::MT])
            ->orderBy('id', 'DESC');

        }elseif($userType == EnumRole::PotholeManager){

            $data->select('id', 'name', 'type_id', 'phone', 'email', 'avatar' , 'role_id', 'organization_id', 'is_active')
            ->with(['oranizationRole:id,name', 'organization','type'])
            ->where('id','!=',auth()->id())
            ->where('is_phone_verified', 1)
            ->where('type_id', EnumRole::PotholeManager)
            ->orderBy('id', 'DESC');

        }elseif($userType == EnumRole::SupervisionManager){

            $data->select('id', 'name', 'type_id', 'phone', 'email', 'avatar' , 'role_id', 'organization_id', 'is_active')
            ->with(['oranizationRole:id,name', 'organization','type'])
            ->where('id','!=',auth()->id())
            ->where('is_phone_verified', 1)
            ->whereIn('type_id', [
                EnumRole::SupervisionManager,
                EnumRole::SupervisionUser,
                EnumRole::SpecialInspector,
                EnumRole::SVInfoEntry,
            ])
            ->orderBy('id', 'DESC');

        }        

        $data->when($request->key, function($query) use($request){
            $query->where('name', 'LIKE','%'.$request->key.'%')
                  ->orWhere('phone', $request->key);
        });

        return response()->json($data->paginate($limit),200);

    }

    public function getSVUser(Request $request){

        $limit = intval(isset($_GET['limit']) ? $_GET['limit'] : 10);

        $types = [
            EnumRole::SupervisionUser,
            EnumRole::SVInfoEntry,
            EnumRole::SpecialInspector
        ];

        $data = User::query();

        $data->select('id', 'name', 'type_id', 'phone', 'email', 'avatar' , 'role_id', 'organization_id', 'is_active')
        ->orderBy('id','DESC')
        ->with(['oranizationRole','organization','type'])
        ->with(['projectWorkingGroups.project:id,name,code,project_status_id',
                'projectWorkingGroups.project.status:id,kh_name,en_name',
                'projectWorkingGroups.sys'])
        ->whereIn('type_id', $types)
        ->withCount('projectWorkingGroups');

        $data->when($request->key, function($query) use($request){
            $query->where('name', 'LIKE','%'.$request->key.'%')
                  ->orWhere('phone', $request->key);
        });

        return response()->json($data->paginate($limit),200);
    }

    public function getAuthorithySetup(Request $request){

        $roles = Role::select('id', 'name')->get();

        $organizations = OrganizationUser::select('id','name')->get();


        $userType = auth()->user()->type_id;
        $data = Type::query();

        if($userType == EnumRole::SuperAdmin){ //sv admin

            if($request->slug == "authority"){
                $types = $data->whereIn('id',[
                    EnumRole::SVHeadOfCommittee,
                    EnumRole::SupervisionManager,
                    EnumRole::PotholeManager,
                    EnumRole::SupervisionUser,
                    EnumRole::SVInfoEntry,
                    EnumRole::SpecialInspector
                ])->select('id','name')->get();
            }else{
                $types = $data->whereIn('id',[
                    EnumRole::SupervisionManager,
                    EnumRole::SupervisionUser,
                    EnumRole::SVInfoEntry,
                    EnumRole::SpecialInspector
                ])->select('id','name')->get();
            }
            

        }elseif($userType == EnumRole::SupervisionManager){

            $types = $data->whereIn('id',[
                EnumRole::SupervisionUser,
                EnumRole::SVInfoEntry,
                EnumRole::SpecialInspector
            ])->get();

            
        }else{
            $types = null;
        }

        return [
            'roles'         => $roles,
            'organizations' => $organizations,
            'types'         => $types
        ];
    }

    public function getOrganization()
    {
        $data = OrganizationUser::get();
        $total = 0;

        foreach($data as $row){
            $row['qty'] = User::where('organization_id',$row['id'])
            ->count('*');
            $total += $row['qty'];
        }

        return response()->json([
            "data"=>$data,
            "total"=>$total
        ]);
    }

    public function ProjectGetRole()
    {
        $data = Role::all()->whereNotIn('id', 1);
        return response()->json($data, 200);
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name'              => 'required|min:4|max:50',
            'organization_id'   => 'required|exists:organization,id',
            'role_id'           => 'required|exists:role,id',
            'email'             => ['required', Rule::unique('user', 'email')],
            'phone'             => ['required', Rule::unique('user', 'phone'),'regex:/^(\+855|0)[1-9]\d{7,8}$/'],
            'password'          => 'required|min:6|max:255',
            'type_id'           => 'required|exists:users_type,id'
        ],[
            'phone.unique'               =>'លេខទូរស័ព្ទនេះត្រូវបានយករួចហើយ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
            'phone.regex'                =>'លេខទូរស័ព្ទមិនត្រឹមត្រូវ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
            'phone.required'             =>'លេខទូរស័ព្ទត្រូវបានទាមទារ។', 
            'email.required'             =>'បំពេញអ៊ីមែល',
            'email.unique'               =>'អ៊ីមែលត្រូវបានយករួចហើយ។',
            'password.required'          =>'បំពេញពាក្យសម្ងាត់',
            'organization_id.required'   =>'សូមជ្រើសរើសអង្គភាព',
            'organization_id.exists'     =>'មិនមាន Organization Id',
            'role_id.required'           =>'សូមជ្រើសរើសតួនាទីក្នុងអង្គភាព',
            'role_id.exists'             =>'មិនមាន Organization Role',
            'type_id.required'           =>'សូមជ្រើសរើសតួនាទី',
            'type_id.exists'             =>'មិនមាន Type ID',
        ]);

        if ($validator->fails()) {
            return $this->invalidJson(new ValidationException($validator));
        } else {

            $data = new User();

            $data->name                = $request->name;
            $data->organization_id     = $request->organization_id;
            $data->role_id             = $request->role_id;
            $data->email               = $request->email;
            $data->phone               = $request->phone;
            $data->password            = bcrypt($request->password);
            $data->type_id             = $request->type_id;   // 7 = Supervision User


            //verify 
            if($request->image_uri){
                $file = $request->image_uri;
                $image = FileUpload::forwardFile($file);   
                $data->avatar = $image;
            }
            else{
                $data->avatar = "uploads/TEST/L-1500000px/RoadCare/20230327/01/6420f2b993b62.jpeg";
            }
            
            $data->is_phone_verified = 1;
            $data->is_email_verified = 1;
            $data->is_notified_when_login = 1;
            $data->is_notified_when_login_with_unknown_device = 1;
            $data->is_active = 1;
            $data->social_type_id = 1;
            $data->access_supervision_feature = 1;
            $data->save();  
            
            //==========> Add User to MO/MT table <============

            if($request->type == EnumRole::MO){
                Main::insert([
                    'user_id' => $data->id,
                    'name'    => $data->name
                ]);
            }

            if($request->type == EnumRole::MT){
                MTMain::insert([
                    'user_id' => $data->id,
                    'name'    => $data->name
                ]);
            }

            if($request->type == EnumRole::RU){
                MemberMain::insert([
                    'user_id' => $data->id,
                    'name'    => $data->name
                ]);
            }
        }

        return response()->json([
            "message" => "បង្កើតគណនីបានដោយជោគជ័យ",
            "status"  => 200,
            "data"    => $data
        ]);
    }

    public function update(Request $request){
        $id = $request->id;
        $data = User::where('id',$id)->first();

        if(!$data){
            return response()->json([
                "status" => 400,
                "message" => "មិនមានគណនី !",
            ], 200);
        }else{
            $validator = Validator::make($request->all(),[
                'name'              => 'required|min:4|max:50',
                'organization_id'   => 'required|exists:organization,id',
                'role_id'           => 'required|exists:role,id',
                'email'             => 'required|unique:user,email,'.$request->id,
                'phone'             => 'required|unique:user,phone,'.$request->id,'|regex:/^(\+855|0)[1-9]\d{7,8}$/',
                'type_id'           => 'required|exists:users_type,id'
            ],[
                'phone.unique'               =>'លេខទូរស័ព្ទនេះត្រូវបានយករួចហើយ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
                'phone.regex'                =>'លេខទូរស័ព្ទមិនត្រឹមត្រូវ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
                'phone.required'             =>'លេខទូរស័ព្ទត្រូវបានទាមទារ។', 
                'email.required'             =>'បំពេញអ៊ីមែល',
                'email.unique'               =>'អ៊ីមែលត្រូវបានយករួចហើយ។',
                'organization_id.required'   =>'សូមជ្រើសរើសអង្គភាព',
                'organization_id.exists'     =>'មិនមាន Organization Id',
                'role_id.required'           =>'សូមជ្រើសរើសតួនាទីក្នុងអង្គភាព',
                'role_id.exists'             =>'មិនមាន Organization Role',
                'type_id.required'           =>'សូមជ្រើសរើសតួនាទី',
                'type_id.exists'             =>'មិនមាន Type ID',
            ]);
    
            if ($validator->fails()) {
                return $this->invalidJson(new ValidationException($validator));
            } else {
                $data->name                = $request->name;
                $data->organization_id     = $request->organization_id;
                $data->role_id             = $request->role_id;
                $data->email               = $request->email;
                $data->phone               = $request->phone;
                $data->type_id             = $request->type_id;
        
                if($request->image_uri){
                    $file = $request->image_uri;
                    $image = FileUpload::forwardFile($file);   
                    $data->avatar = $image;
                }
        
                $data->save();
        
                return response()->json([
                    "status" => 200,
                    "message" => "គណនីកែប្រែជោគជ័យ !",
                ], 200);
            }
        }
        
    }

    public function updatePassword(Request $request){

        $validator = Validator::make($request->all(),[
            'password' => 'required|min:6|max:255',
        ],[
            'password.required' =>'បំពេញពាក្យសម្ងាត់',
        ]);

        if ($validator->fails()) {
            return $this->invalidJson(new ValidationException($validator));
        } else {
            
            $id = $request->id;
            $user = User::find($id);

            if($user){
                $user->update([
                    'password' => bcrypt($request->password)
                ]);

                return response()->json([
                    "status" => 200,
                    "message" => "គណនីកែប្រែជោគជ័យ !",
                ], 200);

            }else{
                return response()->json([
                    "status" => 400,
                    "message" => "មិនមានគណនី !",
                ], 400);
            }
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $data = User::where('id', $id)->first();

        if ($data) {
            $data->delete();
            return response()->json([
                "message" => "លុបគណនីបានជោគជ័យ"
            ]);
        }
    }

    public function view($id)
    {
        $data = User::with(['roles', 'organization'])
            ->where('id', $id)
            ->first();

        return $data;
    }

    public function updateStatus(Request $req){

        $this->validate($req, [
            'user_id'   =>  'required|numeric',
            'is_active' =>  'required|numeric|max:1|min:0'
       ]);

        $user=User::select('id','is_active')->find($req->user_id);
        
        if($user){
            if($user->is_active == $req->is_active){
                return response()->json([
                    'httpStatus' => 200,
                    'message'    => "ទិន្នន័យបានកែប្រែ"],200);
            }else{
                $user->is_active=!$user->is_active;
                $user->update();
                return response()->json([
                    'httpStatus' => 200,
                    'message'    => "ទិន្នន័យបានកែប្រែ"],200);
            }
        }else{
            return response()->json([
                'httpStatus' => 400,
                'message'    => "រកមិនឃើញទិន្នន័យ"],400);
        }
        return $user;
    }
}
