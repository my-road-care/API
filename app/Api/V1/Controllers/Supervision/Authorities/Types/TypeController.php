<?php

namespace App\Api\V1\Controllers\Supervision\Authorities\Types;

// ==============>> Service <<===============
use App\Api\V1\Controllers\ApiController;

// ==============>> Model <<=================
use App\Model\User;
use App\Model\Supervision\Setup\Role;

class TypeController extends ApiController
{

    public function listing()
    {


        $data = Role::orderBy('id','DESC')->get();

        $total = 0;
        foreach($data as $row){
            $row['qty'] = User::where('role_id',$row['id'])
            ->count('*');
            $total += $row['qty'];
        }

        return response()->json([
            "data"=>$data,
            "total"=>$total
        ]);

    }
}
