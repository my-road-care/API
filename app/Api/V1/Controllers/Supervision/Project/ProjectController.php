<?php

namespace App\Api\V1\Controllers\Supervision\Project;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;


// ==============>> Service <<===============
use App\Api\V1\Http\Collections\Project\ProjectCollection;
use App\Api\V1\Http\Collections\Setup\BudgetCollection;
use App\Api\V1\Http\Collections\Setup\RoleCollection;
use App\Api\V1\Http\Collections\Setup\TypeProjectCollection;
use App\Api\V1\Http\Collections\Setup\WorkTypeCollection;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Service\CheckPermission;
use App\Api\V1\Service\Firebase;
use App\Enum\Role as EnumRole;
use App\Jobs\Thread;
use Carbon\Carbon;

// ==================>> Model <<================
use App\Model\Road\Main;
use App\Model\User;
use App\Model\Supervision\Setup\Budget;
use App\Model\Supervision\Setup\Category;
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProjectMGT;
use App\Model\Supervision\Setup\Status;
use App\Model\Supervision\Projects\ProjectWorkingGroup;
use App\Model\Supervision\Setup\ProjectType;
use App\Model\Supervision\Setup\Organization;
use App\Model\Supervision\Setup\Role;
use App\Model\Location\Province;
use App\Model\Supervision\Projects\BudgetYear;
use App\Model\Supervision\Projects\ProjectProvince;
use App\Model\Supervision\Projects\ProjectSurface;
use App\Model\Supervision\Setup\ProgressStatus;
use App\Model\Supervision\Setup\TestingMethod;
use App\Model\Supervision\Setup\TestingMethodType;
use App\Model\Supervision\Setup\TestingMethodTypParamater;
use App\Model\Supervision\Setup\Work;
use App\Model\Supervision\Setup\WorkTestMethod;
use App\Model\User\Notification;

class ProjectController extends ApiController
{
    use CheckPermission, Firebase;

    public function dataSetup()
    {
        $types          = ProjectType::get();
        $categories     = Category::get();
        $budgets        = Budget::get();

        $status         = Status::select('id', 'kh_name', 'en_name', 'color')->get();

        $roles          = Role::whereNotIn('id', [1, 2, 3])->orderBy('id', 'desc')->get();

        $roads          = Main::orderByRaw('CONVERT(name, SIGNED) ASC')->select('id','name')->get();
        $organizations  = Organization::select('id', 'type_id', 'kh_name as name', 'abbre')->get();
        $years          = BudgetYear::select('id', 'year')->get();
        $surfaces       = Work::whereBetween('id',[1,4])->select('id','kh_name as name')->get();
        $progressStatus = ProgressStatus::select('id', 'kh_name', 'en_name','color')->get();

        $projectOwners  = [];
        $entities       = [];
        $frameworks     = [];
        $executives     = [];
        $reviewers      = [];

        foreach ($organizations as $data) {
            if ($data->type_id == 1) {
                $projectOwners[] = [
                    'id'        => $data->id,
                    'type_id'   => $data->type_id,
                    'name'      => $data->name,
                    'abbre'     => $data->abbre
                ];
            } elseif ($data->type_id == 2) {
                $reviewers[] = [
                    'id'        => $data->id,
                    'type_id'   => $data->type_id,
                    'name'      => $data->name,
                    'abbre'     => $data->abbre
                ];
            } elseif ($data->type_id == 3) {
                $executives[] = [
                    'id'        => $data->id,
                    'type_id'   => $data->type_id,
                    'name'      => $data->name,
                    'abbre'     => $data->abbre
                ];
            } elseif ($data->type_id == 4) {
                $frameworks[] = [
                    'id'        => $data->id,
                    'type_id'   => $data->type_id,
                    'name'      => $data->name,
                    'abbre'     => $data->abbre
                ];
            } elseif ($data->type_id == 5) {
                $specialInsp[] = [
                    'id'        => $data->id,
                    'type_id'   => $data->type_id,
                    'name'      => $data->name,
                    'abbre'     => $data->abbre
                ];
            } else {
                $entities[] = [
                    'id'        => $data->id,
                    'type_id'   => $data->type_id,
                    'name'      => $data->name,
                    'abbre'     => $data->abbre
                ];
            }
        }

        return [
            "types"          => new WorkTypeCollection($types),
            "categories"     => new TypeProjectCollection($categories),
            "budgets"        => new BudgetCollection($budgets),
            "project_owners" => $projectOwners,
            "entities"       => $entities,
            "frameworks"     => $frameworks,
            "executives"     => $executives,
            "reviewers"      => $reviewers,
            "roles"          => new RoleCollection($roles),
            "years"          => $years,
            "status"         => $status,
            "roads"          => $roads,
            "surfaces"       => $surfaces,
            "progress_status" => $progressStatus
        ];

    }

    public function setupOffline(){

        $works = Work::select('id', 'group_id','kh_name','en_name')->where('type_id',1)->get();
        $status = Status::select('id','kh_name','en_name','color')->get();
        $testingMethods = TestingMethod::select('id','type_id','name','less_than_pass','unit','description')->with([
            'TestingMethodType'
        ])->get();
        $testingMethodoTypes = TestingMethodType::select('id','name')->get();
        $parameters = TestingMethodTypParamater::select('id','testing_method_id','name','unit')->get();
        $progressStatus = ProgressStatus::select('id','kh_name','en_name','color')->get();
        $workTestMethod = WorkTestMethod::select('id','work_id','testing_method_id')->get();

        // ============> Format Testing Place <================
        $testingPlace = [];
        foreach($testingMethods as $row){
            $testingPlace[] = [
                'id' => $row->id,
                'name' => $row->name,
                'less_than_pass' => $row->less_than_pass,
                'unit' => $row->unit,
                'description' => $row->description,
                'place' => $row->TestingMethodType
            ];
        };

        return [
            "works"  => $works,
            "status" => $status,
            "progress_status" => $progressStatus,
            "testing_method_types" => $testingMethodoTypes,
            "testing_methods" => $testingPlace,
            "work_test_methods" => $workTestMethod,
            "parameters" => $parameters
        ];
    }

    public function getProvince($roadId = 0){
        $provinces  = Province::whereHas(
            'roads',function($q) use($roadId){
                $q->where('road_id', $roadId);
            })->select('id','name','en_name','abbre')->get();

        return [
            'provinces' => $provinces
        ];

    }

    public function setup()
    {
        $categories = Category::select('id','name')->get();
        $status = Status::select('id', 'kh_name', 'en_name', 'color')->get();
        $province       = Province::get();
        $years          = BudgetYear::select('id', 'year')->get();
        $data = [
            'categories' => $categories,
            'status'     => $status,
            'provinces'  => $province,
            'budget_years' => $years
        ];
        return responseSuccess('success',$data,200);
    }

    public function getNotification(){
        $notification = Notification::where('user_id', auth()->id())->select('id','title','description','project_id','is_seen')->get();
        return responseSuccess('success', $notification);
    }
    
    public function listProject(Request $req)
    {
        $query      = Project::query();
        $userRole   = auth()->user()->type_id;

        //==================>>> Check User Project's
        $query
        ->with(['organizations', 'organizations.type', 'category', 'status', 'projectProvinces','budgets', 'year']);

        if($userRole == EnumRole::SuperAdmin || $userRole == EnumRole::SupervisionManager ){
        }else{

            if($userRole == EnumRole::SVInfoEntry){

                $query->where('created_by', auth()->id());

            }else{

                $query->whereHas('projectWorkingGroups', function($query) {
                    $query->where('user_id', auth()->id());
                });

            }            
        }        

        // ==================>> Get Trashed Project
        if($req->type == "mark-deleted"){
            $query->onlyTrashed();
        }

        $query->when($req->key, function ($query) use ($req) {
            $query->where('name', 'LIKE', '%' . $req->key . '%')
                ->orWhere('code', 'LIKE', '%' . $req->key . '%');
        });

        $query->when($req->province_id, function ($query) use ($req) {
            $query->whereHas('projectProvinces', function($q) use($req){
                $q->where('province_id', $req->province_id);
            });
        });

        if($req->category_id){
            $query->when($req->category_id, function ($query) use ($req) {
                $query->where('category_id', $req->category_id);
            });
        }
        if($req->status_id){
            $query->when($req->status_id, function ($query) use ($req) {
                $query->where('project_status_id', $req->status_id);
            });
        }
        if($req->budget_year_id){
            $query->when($req->budget_year_id, function ($query) use ($req) {
                $query->where('budget_year_id', $req->budget_year_id);
            });
        }

        if($req->start_date && $req->end_date){
            $query->where('created_at', $req->budget_year_id);
        }
        $projects = $query->orderBy('id', 'DESC')->paginate($req->per_page ?? 10);
        return new ProjectCollection($projects);
    }

    public function checkCode(Request $req)
    {
        $code = Project::where('code', $req->code)->first();
        if ($code) {
            return "false";
        } else {
            return "true";
        }
    }

    public function getCode($id = 0)
    {
        $code = Project::select('code')->where('id', $id)->first();
        if ($code) {
            return $code;
        } else {
            return responseError('មិនមានទិន្ន័យ');
        }
    }

    public function createProject(Request $req)
    {

        //==================================================>> Data Validation
        $validator = Validator::make($req->all(), [
            "name"            => 'required',
            "code"            => 'required|unique:sup_project,code',
        ], [
            'name.required' => 'បំពេញឈ្មោះគម្រោង',
            'code.required'   => 'បំពេញលេខគម្រោង',
            'code.unique'   => 'លេខកូដគម្រោងមានរួចហើយ'
        ]);

        if ($validator->fails()) {
            return $this->invalidJson(new ValidationException($validator));
        } else {
            try {
                DB::beginTransaction();

                // Insert into Project Table
                $project = new Project();
                $project->created_by                    = auth()->id();
                $project->name                          = $req->name;
                $project->code                          = $req->code;
                $project->type_id                       = $req->type_id;
                $project->category_id                   = $req->category_id;
                $project->project_status_id             = 1;
                $project->road_id                       = $req->road_id;
                $project->budget_id                     = $req->budget_plan_id;
                $project->budget_year_id                = $req->budget_year_id;

                $project->project_start_date_contract   = $req->project_start_date_contract;
                $project->project_end_date_contract     = $req->project_end_date_contract;
                $project->process_contract_duration     = $req->process_contract_duration;
                $project->delay_date                    = $req->delay_date;
                $project->warranty_duration             = $req->warranty_duration;
                $project->close_project_date            = $req->close_project_date;

                // check if numeric has commas (Budget Request)
                if (preg_match("/^[0-9,]+$/", $req->bud_request_construction)) {
                    $budRequestCon = str_replace(',', '', $req->bud_request_construction);
                    $project->bud_request_construction = $budRequestCon;
                } else {
                    $budRequestCon = $req->bud_request_construction;
                    $project->bud_request_construction = $budRequestCon;
                }

                if (preg_match("/^[0-9,]+$/", $req->bud_request_check)) {
                    $budRequestCheck = str_replace(',', '', $req->bud_request_check);
                    $project->bud_request_check = $budRequestCheck;
                } else {
                    $budRequestCheck = $req->bud_request_check;
                    $project->bud_request_check = $budRequestCheck;
                }

                // ============>  Sum Budget Request <================
                $project->total_bud_request = (int)$budRequestCon + (int)$budRequestCheck;


                // check if numeric has commas (Budget After Warranty)
                if (preg_match("/^[0-9,]+$/", $req->bud_after_warranty_con)) {
                    $budAfterWarrantyCon = str_replace(',', '', $req->bud_after_warranty_con);
                    $project->bud_after_warranty_construction = $budAfterWarrantyCon;
                } else {
                    $budAfterWarrantyCon = $req->bud_after_warranty_con;
                    $project->bud_after_warranty_construction = $budAfterWarrantyCon;
                }

                if (preg_match("/^[0-9,]+$/", $req->bud_after_warranty_check)) {
                    $budAfterWrrantyCheck = str_replace(',', '', $req->bud_after_warranty_check);
                    $project->bud_after_warranty_check = $budAfterWrrantyCheck;
                } else {
                    $budAfterWrrantyCheck = $req->bud_after_warranty_check;
                    $project->bud_after_warranty_check = $budAfterWrrantyCheck;
                }

                // =============> Sum Budget After Warranty <=============
                $project->total_bud_after_warranty = (int)$budAfterWarrantyCon + (int)$budAfterWrrantyCheck;


                // check if numeric has commas (Budget Contract)
                if (preg_match("/^[0-9,]+$/", $req->bud_contract_construciton)) {
                    $budContractCon = str_replace(',', '', $req->bud_contract_construciton);
                    $project->bud_contract_construciton = $budContractCon;
                } else {
                    $budContractCon = $req->bud_contract_construciton;
                    $project->bud_contract_construciton = $budContractCon;
                }

                if (preg_match("/^[0-9,]+$/", $req->bud_contract_check)) {
                    $budContractCheck = str_replace(',', '', $req->bud_contract_check);
                    $project->bud_contract_check = $budContractCheck;
                } else {
                    $budContractCheck = $req->bud_contract_check;
                    $project->bud_contract_check = $budContractCheck;
                }

                // =============> Sum Budget Contract <==============
                $project->total_bud_contract = (int)$budContractCon + (int)$budContractCheck;



                $project->save();

                // Insert into Project_MGT_Structures
                if ($req->entity_id) {
                    ProjectMGT::insert([
                        "project_id"       => $project->id,
                        "organization_id"  => $req->entity_id
                    ]);
                }

                if ($req->project_owner_id) {
                    ProjectMGT::insert([
                        "project_id"       => $project->id,
                        "organization_id"     => $req->project_owner_id
                    ]);
                }

                if ($req->reviewer_id) {
                    ProjectMGT::insert([
                        "project_id"       => $project->id,
                        "organization_id"  => $req->reviewer_id
                    ]);
                }
 
                if ($req->framework_id) {
                    ProjectMGT::insert([
                        "project_id"       => $project->id,
                        "organization_id"     => $req->framework_id
                    ]);
                }

                if ($req->executive_director_id) {
                    ProjectMGT::insert([
                        "project_id"       => $project->id,
                        "organization_id"     => $req->executive_director_id
                    ]);
                }

                // Insert into Province
                if ($req->province_id) {
                    $provinceId = $req->province_id;
                    $provinceIdData = [];
                    foreach ($provinceId as $item) {
                        $provinceIdData[] = [
                            "project_id"       => $project->id,
                            "province_id"     => $item
                        ];
                    }
                    ProjectProvince::insert($provinceIdData);
                }

                // Insert Project Surfaces
                if($req->surface_id){
                    $projectSurface = [];
                    foreach($req->surface_id as $row){
                        $projectSurface[] = [
                            "project_id" => $project->id,
                            "surface_id" => $row
                        ];
                    }
                    ProjectSurface::insert($projectSurface);
                }



                // automatic add mintry, Head Communitee, Special Inspector, SV Info Entry
                $autoRole = [
                    EnumRole::Minister,
                    EnumRole::SVHeadOfCommittee
                ];
                $committeeAndMinister = User::whereIn('type_id',$autoRole)->get();

                $headOfCommittee = [];
                $minister = [];


                $data = [
                    [
                        'project_id' => $project->id,
                        'user_id'    => auth()->id(), // SV Info Entry 
                        'sys_id'     => 1,
                        'creator_id'  => auth()->id(),
                        'created_at' => Carbon::now()
                    ]
                ];

                if($committeeAndMinister){
                    foreach($committeeAndMinister as $row){
                        if($row->type_id == 9){
                            $data[] = [
                                'project_id' => $project->id,
                                'user_id'    => $row->id, // Head Of Communitee 
                                'sys_id'     => 6,
                                'creator_id' => auth()->id(),
                                'created_at' => Carbon::now()
                            ];
                        }else{
                            $data[] = [
                                'project_id' => $project->id,
                                'user_id'    => $row->id, // Minitry User
                                'sys_id'     => 7,
                                'creator_id' => auth()->id(),
                                'created_at' => Carbon::now()
                            ];
                        }
                        
                    }
                }
                $specialInspector = User::select('id')
                ->where('type_id',12)  // special inspector;
                ->get();
                
                if($specialInspector){
                    foreach($specialInspector as $row){
                        $data[] = [
                            'project_id' => $project->id,
                            'user_id'    => $row['id'], 
                            'sys_id'     => 4,  // special inspector
                            'creator_id' => auth()->id(),
                            'created_at' => Carbon::now()
                        ];
                    }
                }                
                
                ProjectWorkingGroup::insert($data);
            
                
                DB::commit();

                return [
                    "statusCode" => 200,
                    "message"    => "គម្រោងបានបង្កើត",
                    "id"         => $project->id
                ];
                
            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function deleteProject($projectId)
    {
        $project = Project::find($projectId); 
        
        if ($project) {

            $project->delete();
            ProjectMGT::where('project_id', $projectId)->delete();
            return responseSuccess('បានលុបទិន្ន័យ');

        } else {
            return responseError('មិនមានទិន្ន័យ');
        }
    }

    public function checkProjectPermission($projectId = 0){

        $project = Project::select('code','diagram_approver_id', 'technical_approved_datetime')->where('id', $projectId)->first();
        if($project){

            // ==========================> Get User Role Project's
            $projectWorkingGroup = ProjectWorkingGroup::with('sys')->where([
                'user_id' => auth()->id(),
                'project_id' => $projectId])->first();

            $specialRole = ProjectWorkingGroup::with('sys')->where([
                'user_id' => auth()->id(),
                'project_id' => $projectId])->whereIn('sys_id',[6,7])->first();
            if($specialRole){
                $userProjectRole = $specialRole -> sys ? $specialRole -> sys -> name : null;
            }elseif($projectWorkingGroup){
                $userProjectRole = $projectWorkingGroup->sys ? $projectWorkingGroup->sys->name : null;
            }else{
                $userProjectRole = null;
            }

            // ==============================================================================>> Access Project Info
            $info               = []; 
            if($this->checkPermission("project-info-*", 'R', $projectId)['check']){

                $info['can_access']             = 1; 

                // =======================>> Stastistic
                $stastistic = [];
                if($this->checkPermission("project-info-statistic", 'R', $projectId)['check']){

                    $stastistic['can_access']       = 1;
                    $stastistic['give_approval']    = $this->checkPermission("project-info-approval", 'R', $projectId)['check']; 

                }else{
                    $stastistic['can_access']       = 0;
                }
                $info['stastistic']                 = $stastistic; 

                // =======================>> General Info
                $genInfo = [];
                if($this->checkPermission("project-info-general", 'R', $projectId)['check']){

                    $genInfo['can_access']      = 1;
                    $genInfo['update']          = $this->checkPermission("project-info-approval", 'R', $projectId)['check']; 

                }else{
                    $genInfo['can_access']       = 0;
                }
                $info['general_info']                 = $genInfo; 

                // =======================>> Procurement
                $procurement = [];
                if($this->checkPermission("project-info-procurement", 'R', $projectId)['check']){

                    $procurement['can_access']      = 1;
                    $procurement['create']          = $this->checkPermission("project-info-procurement", 'C', $projectId)['check']; 
                    $procurement['update']          = $this->checkPermission("project-info-procurement", 'U', $projectId)['check']; 
                    $procurement['delete']          = $this->checkPermission("project-info-procurement", 'D', $projectId)['check']; 
                
                }else{
                    $procurement['can_access']       = 0;
                }
                $info['procurement']                = $procurement; 

                // =======================>> User
                $user = [];
                if($this->checkPermission("project-info-user", 'R', $projectId)['check']){

                    $user['can_access']      = 1;
                    $user['create']          = $this->checkPermission("project-info-user", 'C', $projectId)['check']; 
                    $user['update']          = $this->checkPermission("project-info-user", 'U', $projectId)['check']; 
                    $user['delete']          = $this->checkPermission("project-info-user", 'D', $projectId)['check']; 
                
                }else{
                    $user['can_access']       = 0;
                }
                $info['user']                = $user; 

            }else{
                $info['can_access'] = 0; 
            }

            // ==============================================================================>> Access Technical 
            $technical          = []; 
            if($this->checkPermission("project-technical-*", 'R', $projectId)['check']){

                $technical['can_access']            = 1; 

                // =======================>> Diagram
                $diagram            = []; 
                if($this->checkPermission("project-technical-diagram", 'R', $projectId)['check']){

                    $diagram['can_access']              = 1; 

                    // ================= >> Check Project has approved yet
                    if($project->diagram_approver_id && $project->technical_approved_datetime){
                        $diagram['add_update_work'] = 0;
                    }else{
                        $diagram['add_update_work']     = $this->checkPermission("project-technical-diagram", 'C', $projectId)['check'];
                    }
                    $diagram['udpate_progress']         = $this->checkPermission("project-technical-diagram-progress-update", 'R', $projectId)['check']; 
                    $diagram['quality_check']           = $this->checkPermission("project-technical-diagram-quality-check", 'R', $projectId)['check']; 
                    
                }else{
                    $diagram['can_access']              = 0; 
                }

                $technical['diagram']                   = $diagram; 

                // =======================>> Standard Test
                $standardTest                       = []; 
                if($this->checkPermission("project-technical-diagram-standard-test", 'R', $projectId)['check']){

                    $standardTest['can_access']     = 1; 
                    $standardTest['create']         = $this->checkPermission("project-technical-diagram-standard-test", 'C', $projectId)['check']; 
                    $standardTest['update']         = $this->checkPermission("project-technical-diagram-standard-test", 'U', $projectId)['check']; 
                    $standardTest['delete']         = $this->checkPermission("project-technical-diagram-standard-test", 'D', $projectId)['check']; 

                }else{
                    $standardTest['can_access']     = 0; 
                }

                $technical['standard_test']         = $standardTest; 


                // =======================>> Plan
                $plan                       = []; 
                if($this->checkPermission("project-technical-diagram-plan", 'R', $projectId)['check']){

                    $plan['can_access']     = 1; 
                    $plan['create']         = $this->checkPermission("project-technical-diagram-plan", 'C', $projectId)['check']; 
                    $plan['update']         = $this->checkPermission("project-technical-diagram-plan", 'U', $projectId)['check']; 
                    $plan['delete']         = $this->checkPermission("project-technical-diagram-plan", 'D', $projectId)['check']; 

                }else{
                    $plan['can_access']     = 0; 
                }

                $technical['plan']         = $plan; 
                
            }else{
                $technical['can_access'] = 0; 
            }

            // ==============================================================================>> Access Report
            $report             = []; 
            if($this->checkPermission("project-report-*", 'R', $projectId)['check']){

                $report['can_access']            = 1; 

                // =======================>> Progress Report
                $report['progress_report']['can_access']      = $this->checkPermission("project-report-progress", 'R', $projectId)['check'] == 1 ? 1 : 0; 
                // =======================>> Testing Report
                $report['testing_report']['can_access']       = $this->checkPermission("project-report-testing-report", 'R', $projectId)['check'] == 1 ? 1 : 0; 

                // =======================>> Letter
                $letter     = [];
                if($this->checkPermission("project-report-letter", 'R', $projectId)['check']){
                    $letter['can_access']           = 1;
                    $letter['create']               = $this->checkPermission("project-report-letter", 'C', $projectId)['check']; 
                    $letter['update']               = $this->checkPermission("project-report-letter", 'U', $projectId)['check']; 
                    $letter['delete']               = $this->checkPermission("project-report-letter", 'D', $projectId)['check']; 
                }else{
                    $letter['can_access']           = 0;
                }
                $report['letter']                 = $letter; 

                // =======================>> System Report
                $report['system_report']['can_access']     = $this->checkPermission("project-report-system-report", 'R', $projectId)['check'] == 1 ? 1 : 0; 
                // =======================>> Reference Report
                $report['reference_report']['can_access']  = $this->checkPermission("project-report-reference-report", 'R', $projectId)['check'] == 1 ? 1 : 0; 

            }else{
                $report['can_access'] = 0; 
            }

            return [
                'code'  => $project->code, 
                'role'  => $userProjectRole,
                'permission' => [
                    'info'              => $info, 
                    'technical'         => $technical, 
                    'report'            => $report
                ]
            ]; 
        }else{
            return responseError('មិនមានទិន្ន័យ');
        }
        
    }

    public function approveTechnicalInfo($projectId)
    {
        $project = Project::find($projectId); 
        if ($project) {

            if($project->diagram_creator_id == null) return responseError("មិនទាន់មានតារាងការងារ");

            // TODO: Update Project Status
            $project->update([
                'project_status_id' => 2
            ]);

            // TODO: Check Permission if the approver is Head Of Committee            
            $project->technical_approved_datetime   = Date('Y-m-d H:i:s'); 
            $project->diagram_approver_id           =  auth()->user()->id; 
            $project->save(); 

            // TODO: Send Notification via Firebase
            $data = [
                'date' => $project->technical_approved_datetime,
                'by'   => $project->diagramApprover ? $project->diagramApprover->name : null,
            ];

            $workingGroups = ProjectWorkingGroup::with('user')->where('project_id', $projectId)->get();

            $message = "ការងារបច្ចេកទេសត្រូវបានផ្តល់គោលការណ៏ដាក់ឲ្យប្រើប្រាស់ដោយៈ ". auth()->user()->name . " កាលបរិច្ឆេទៈ " . Date('Y-m-d');
            foreach($workingGroups as $row){

                $this->sendNotification(null, $row->user->id, $message);

            }

            // TODO: Update Cache
            dispatch(new Thread("project-statistic:get $projectId"));
            dispatch(new Thread("work-diagram-cache:update $projectId"));


            return responseSuccess('ការងារបច្ចេកទេស ត្រូវបានអនុម័ត', $data);
        } else {
            return responseError('មិនមានទិន្ន័យ');
        }
    }


}
