<?php

namespace App\Api\V1\Controllers\Supervision\Project\Info;

// =============>> Service <<=================
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Controllers\Supervision\Project\Info\ProjectProcurementController as ProjectProcurement;
use App\Api\V1\Service\CheckPermission;

// ================>> Model <<=================
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProjectProvince;
use App\Model\Supervision\Projects\ProjectSurface;
use App\Model\Supervision\Projects\ProjectMGT;
use App\Model\Supervision\Projects\ProjectWorkingGroup;

class ProjectInfoController extends ApiController
{
    use CheckPermission;
    public function getGeneral(Request $request, $projectId)
    {
        $project = Project::with(['budgets', 'organizations', 'road', 'year','surfaces'])
        ->where('id', $projectId)
        ->first();

        if ($project) {
            return $this->getGeneralInfo($project, $request->type);
        } else {
            return responseError('មិនមានទិន្ន័យ');
        }       
    }

    public function getProcurement(Request $request, $projectId)
    {
        //check permission
        // Get Project by ID
        return [
            'procurement'   => ProjectProcurement::index($request, $projectId),
        ];
    }

    public function getWorkingStructure(Request $request, $projectId)
    {
        //check permission
        // Get Project by ID
        $projectWorkingGroups   = ProjectWorkingGroup::with(['role', 'user'])->where('project_id', $projectId)->get();
        return [
            'project_working_groups' => $this->roleHasUser($projectWorkingGroups, $request)
        ];
    }




    private function getGeneralInfo($project = null, $type)
    {


        if ($type == "mobile") {
            return [
                'data' => [
                    [
                        'block' => 'ព័ត៌៌មានទូទៅ',
                        'child' => [
                            [
                                'key'   => 'ឈ្មោះគម្រោង',
                                'value' => $project->name
                            ],[
                                'key'   => 'ប្រភេទកម្រាល',
                                'value' => $this->surfaces($project->surfaces)
                            ], [
                                'key'   => 'លេខកូដគម្រោង',
                                'value' => $project->code
                            ], [
                                'key'   => 'ផ្លុវជាតិលេខ',
                                'value' => $project->road
                            ], [
                                'key'   => 'ប្រតិបត្តិករ',
                                'value' => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 3)->first() : null),
                            ], [
                                'key'   => 'ជំពូកថវិកា',
                                'value' => $this->budget($project->budgets, $project->year),
                            ], [
                                'key'   => 'ក្របខណ្ឌ',
                                'value' => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 4)->first() : null),
                            ], [
                                'key'   => 'ម្ចាស់គម្រោង',
                                'value' => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 1)->first() : null),
                            ], [
                                'key'   => 'អង្គភាព/ស្ថាប័ន អនុវត្តគម្រោង',
                                'value' => $this->data($project->organizations ? $project->organizations->where('type_id', 2) : null),
                            ], [
                                'key'   => 'អ្នកត្រួតពិនិត្យ',
                                'value' => $this->data($project->organizations ? $project->organizations->where('type_id', 5) : null),
                            ]
                        ]
                    ], [
                        'block' => 'ព័ត៌៌មានលទ្ធកម្ម',
                        'child' => [
                            [
                                'key'   => 'តម្លៃកិច្ចសន្យា សាងសង់ស្នើសុំ',
                                'value' => $project->bud_request_construction." រៀល"
                            ], [
                                'key'   => 'តម្លៃកិច្ចសន្យា ត្រួតពិនិត្យ ស្នើសុំ',
                                'value' => $project->bud_request_check." រៀល"
                            ], [
                                'key'   => 'តម្លៃកិច្ចសន្យា សាងសង់ក្រោយធានាចំណាយ',
                                'value' => $project->bud_after_warranty_construction." រៀល"
                            ], [
                                'key'   => 'តម្លៃកិច្ចសន្យាត្រួតពិនិត្យ ក្រោយធានាចំណាយ',
                                'value' => $project->bud_after_warranty_check." រៀល"
                            ], [
                                'key'   => 'តម្លៃកិច្ចសន្យា សាងសង់',
                                'value' => $project->bud_contract_construciton." រៀល"
                            ], [
                                'key'   => 'តម្លៃកិច្ចសន្យា ត្រួតពិនិត្យ',
                                'value' => $project->bud_contract_check." រៀល"
                            ]
                        ]
                    ], [
                        'block' => 'ព័ត៌៌មានកាលបរិច្ចេទ',
                        'child' => [
                            [
                                'key'   => 'កាលបរិច្ឆេទ ចាប់ផ្តើមអនុវត្ត កិច្ចសន្យាគម្រោង',
                                'value' => $project->project_start_date_contract ? date('Y-m-d', strtotime($project->project_start_date_contract)) : null,
                            ], [
                                'key'   => 'រយៈពេលអនុវត្ត កិច្ចសន្យាគម្រោង',
                                'value' => $project->process_contract_duration ? $project->process_contract_duration." ខែ" : null,
                            ], [
                                'key'   => 'កាលបរិច្ឆេទបញ្ចប់​ កិច្ចសន្យាគម្រោង',
                                'value' => $project->project_end_date_contract ? date('Y-m-d', strtotime($project->project_end_date_contract)) : null,
                            ], [
                                'key'   => 'កាលបរិច្ឆេទបញ្ចប់​ កិច្ចសន្យាគម្រោង(ករណីពន្យា)',
                                'value' => $project->delay_date ? date('Y-m-d', strtotime($project->delay_date)) : null,
                            ], [
                                'key'   => 'រយៈពេលធានា សុពលភាពគម្រោង',
                                'value' => $project->warranty_duration ? $project->warranty_duration." ខែ" : null,
                            ], [
                                'key'   => 'កាលបរិច្ឆេទបញ្ចប់សុពលភាពគម្រោង',
                                'value' => $project->close_project_date ? date('Y-m-d', strtotime($project->close_project_date)) : null
                            ]
                        ]
                    ],
                ]
            ];
        } else {
            return [
                'data' => [
                    'main' => [
                        'id'                    => $project->id,
                        'name'                  => $project->name,
                        "code"                  => $project->code,
                        "executive_director"    => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 3)->first() : null),
                        "type"                  => $project->types,
                        "budget_plan"           => $this->budget($project->budgets, $project->year),
                        "framework"             => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 4)->first() : null),
                        "project_owner"         => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 1)->first() : null),
                        "entity"                => $this->data($project->organizations ? $project->organizations->where('type_id', 6)->first() : null),
                        "reviewer"              => $this->data($project->organizations ? $project->organizations->where('type_id', 2)->first() : null),
                        "category"              => $project->category ?? "",
                        "road"                  => $project->road,
                        "provinces"             => ProjectProvince::with(['provinces'])->where('project_id',$project['id'])->get(),
                        "status"                => $project->status ?? "",
                        "surfaces"              => $this->surfaces($project->surfaces)
                    ],
                    'procurement' => [
                        "bud_request_construction" => $project->bud_request_construction,
                        "bud_request_check" => $project->bud_request_check,
                        "bud_after_warranty_con" => $project->bud_after_warranty_construction,
                        "bud_after_warranty_check" => $project->bud_after_warranty_check,
                        "bud_contract_construciton" => $project->bud_contract_construciton,
                        "bud_contract_check" => $project->bud_contract_check,
                    ],
                    'date' => [
                        "project_start_date_contract" => $project->project_start_date_contract ? date('Y-m-d', strtotime($project->project_start_date_contract)) : null,
                        "project_end_date_contract" => $project->project_end_date_contract ? date('Y-m-d', strtotime($project->project_end_date_contract)) : null,
                        "process_contract_duration" => $project->process_contract_duration ? $project->process_contract_duration : null,
                        "delay_date" => $project->delay_date ? date('Y-m-d', strtotime($project->delay_date)) : null,
                        "warranty_duration" => $project->warranty_duration ? $project->warranty_duration : null,
                        "close_project_date" => $project->close_project_date ? date('Y-m-d', strtotime($project->close_project_date)) : null
                    ]
                ]
            ];
        }
    }

    private function data($data = null)
    {
        if($data){
            $res = [
                "id"               => $data['id'] ?? "",
                "name"             => $data['kh_name'] ?? "",
                "en_name"          => $data['en_name'] ?? "",
                "abbre"            => $data['abbre'] ?? "",
            ];
        }else{
            $res = null;
        }
        
        return $res;
    }

    private function surfaces($surfaces)
    {
        $projectSurfaces = [];
        foreach ($surfaces as $row) {
            $projectSurfaces[] = [
                "id"               => $row['id'] ?? "",
                "name"             => $row['kh_name'] ?? ""
            ];
        }
        return $projectSurfaces;
    }
    private function projectOwner($entities)
    {
        $entities = [
            "id"               => $entities['id'] ?? "",
            "name"             => $entities['kh_name'] ?? "",
            "en_name"          => $entities['en_name'] ?? "",
            "abbre"            => $entities['abbre'] ?? "",
        ];
        return $entities;
    }

    private function budget($budget, $year)
    {
        $name = $budget ? $budget->name : null;
        $budYear =  $year ? " (ឆ្នាំ ".$year->year.")" : null;
        return [
            'id'   => $budget->id,
            'name' => $name.$budYear,
            'budget_year_id' => $year ? $year->id : null
        ];
    }

    private function roleHasUser($projectWorkingGroups, $request)
    {

        $management         = [];
        $approver    = [];
        $pmu    = [];
        $piu       = [];
        $specialInspector = [];

        foreach ($projectWorkingGroups as $row) {

            if ($row->role_id == 1) {

                $management[] = [
                    "id"        => $row->id,
                    "user_id" => $row->user->id,
                    "name" => $row->user->name,
                    "phone" => $row->user->phone
                ];
            } elseif ($row->role_id == 2) {

                $approver[] = [
                    "id"        => $row->id,
                    "user_id" => $row->user->id,
                    "name" => $row->user->name,
                    "phone" => $row->user->phone
                ];
            } elseif ($row->role_id == 3) {

                $pmu[] = [
                    "id"        => $row->id,
                    "user_id" => $row->user->id,
                    "name" => $row->user->name,
                    "phone" => $row->user->phone
                ];
            } elseif ($row->role_id == 4) {

                $piu[] = [
                    "id"        => $row->id,
                    "user_id" => $row->user->id,
                    "name" => $row->user->name,
                    "phone" => $row->user->phone
                ];
            } else {

                $specialInspector[] = [
                    "id"        => $row->id,
                    "user_id" => $row->user->id,
                    "name" => $row->user->name,
                    "phone" => $row->user->phone
                ];
            }
        }
        if ($request->type == "mobile") {
            return [
                "permission"    => [],
                "data" => [
                    [
                        'block' => 'ថ្នាក់ដឹកនាំ. Management',
                        'value' => $management
                    ], [
                        'block' => 'អ្នកអនុម័ត. Approver',
                        'value' => $approver
                    ], [
                        'block' => 'អង្គភាពគ្រប់គ្រងគម្រោង. PMU',
                        'value' => $pmu
                    ], [
                        'block' => 'អង្គភាពអនុវត្តគម្រោង. PIU',
                        'value' => $piu
                    ], [
                        'block' => 'ក្រុមត្រួតពិនិត្យពិសេស. Special Inspector',
                        'value' => $specialInspector
                    ]
                ]
            ];
        } else {
            return [
                "permission"                => [],
                "management"                => $management,
                "approver"                  => $approver,
                "pmu"                       => $pmu,
                "piu"                       => $piu,
                "specialInspector"          => $specialInspector
            ];
        }
    }

    public function updateMainInfo(Request $request, $projectId = 0)
    {
        $project = Project::with(['budgets', 'road', 'types', 'organizations', 'organizations.type', 'category', 'status', 'projectProvinces.provinces', 'surfaces'])
            ->where('id', $projectId)
            ->first();

        $validator = Validator::make($request->all(), [
            "name"            => 'required',
            "code"            => 'sometimes|required|unique:sup_project,code,' . $project->id,
        ], [
            'name.required' => 'បំពេញឈ្មោះគម្រោង',
            'code.required'   => 'បំពេញលេខគម្រោង',
            'code.unique'   => 'លេខកូដគម្រោងមានរួចហើយ'
        ]);

        if ($validator->fails()) {
            return $this->invalidJson(new ValidationException($validator));
        } else {
            if (!$project == null) {
                try {

                    // Delete Project_MGT_Structures
                    ProjectMGT::where('project_id', $projectId)->delete();

                    DB::beginTransaction();

                    // Update Main
                    $project->name                          = $request->name;
                    $project->code                          = $request->code;
                    $project->road_id                       = $request->road_id;
                    $project->type_id                       = $request->type_id;
                    $project->category_id                   = $request->category_id;
                    $project->budget_id                     = $request->budget_plan_id;
                    $project->budget_year_id                = $request->budget_year_id;

                    
                    $project->save();

                    if ($request->entity_id) {
                        ProjectMGT::insert([
                            "project_id"       => $project->id,
                            "organization_id"  => $request->entity_id
                        ]);
                    }
                    if ($request->project_owner_id) {
                        ProjectMGT::insert([
                            "project_id"       => $project->id,
                            "organization_id"     => $request->project_owner_id
                        ]);
                    }
    
                    if ($request->reviewer_id) {
                        ProjectMGT::insert([
                            "project_id"       => $project->id,
                            "organization_id"  => $request->reviewer_id
                        ]);
                    }

                    if ($request->framework_id) {
                        ProjectMGT::insert([
                            "project_id"       => $project->id,
                            "organization_id"     => $request->framework_id
                        ]);
                    }

                    if ($request->executive_director_id) {
                        ProjectMGT::insert([
                            "project_id"       => $project->id,
                            "organization_id"     => $request->executive_director_id
                        ]);
                    }
    
                    // Project Structure Management
    
                    if ($request->working_group) {
                        $workingGroups = json_decode($request->working_group);
                        $workingGroupData = [];
                        foreach ($workingGroups as $row) {
                            $workingGroupData[] = [
                                "project_id"    => $project->id,
                                "role_id"       => $row->role_id,
                                "user_id"       => $row->user_id
                            ];
                        }
                        ProjectWorkingGroup::insert($workingGroupData);
                    }

                    // Project Surface
                    ProjectSurface::where('project_id', $projectId)->delete();
                    if($request->surface_id){
                        $projectSurface = [];
                        foreach($request->surface_id as $row){
                            $projectSurface[] = [
                                "project_id" => $project->id,
                                "surface_id" => $row
                            ];
                        }
                    ProjectSurface::insert($projectSurface);
                    }

                    // Insert into Province

                    ProjectProvince::where('project_id', $projectId)->delete();

                    if ($request->province_id) {
                        $provinceId = $request->province_id;
                        $provinceIdData = [];
                        foreach ($provinceId as $item) {
                            $provinceIdData[] = [
                                "project_id"      => $project->id,
                                "province_id"     => $item
                            ];
                        }
                        ProjectProvince::insert($provinceIdData);
                    }

                    DB::commit();

                    // need to select it again to get latest data
                    $project = Project::with(['projectProvinces.provinces', 'budgets', 'year', 'road', 'types', 'organizations', 'organizations.type', 'category', 'status'])->where('id', $projectId)->first();
                    $data = [
                        'id'                    => $project->id,
                        'name'                  => $project->name,
                        "code"                  => $project->code,
                        "executive_director"    => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 3)->first() : null),
                        "type"                  => $project->types,
                        "budget_plan"           => $this->budget($project->budgets, $project->year),
                        "framework"             => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 4)->first() : null),
                        "project_owner"         => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 1)->first() : null),
                        "entity"                => $this->data($project->organizations ? $project->organizations->where('type_id', 6)->first() : null),
                        "reviewer"              => $this->data($project->organizations ? $project->organizations->where('type_id', 2)->first() : null),
                        "provinces"             => $project->projectProvinces,
                        "category"              => $project->category ?? "",
                        "road"                  => $project->road,
                        "status"                => $project->status ?? "",
                        "surfaces"              => $this->surfaces($project->surfaces)
                    ];
                    return responseSuccess(
                        'គម្រោងបានកែប្រែ',
                        $data,
                        200
                    );
                    return [
                        'message' => 'គម្រោងបានកែប្រែ',

                    ];
                } catch (\Exception $exception) {
                    DB::rollBack();
                    return responseError($exception);
                }
            } else {
                return responseError('មិនមានទិន្ន័យ');
            }
        }
    }

    public function updateProcedureInfo(Request $request, $projectId = 0)
    {
        $validator = Validator::make($request->all(), [
            // "budget_plan_id"     => 'required'
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            $project = Project::where('id', $projectId)->first();
            if ($project) {
                try {
                    DB::beginTransaction();

                    // check if numeric has commas (Budget Request)
                    if (preg_match("/^[0-9,]+$/", $request->bud_request_construction)) {
                        $budRequestCon = str_replace(',', '', $request->bud_request_construction);
                        $project->bud_request_construction = $budRequestCon;
                    } else {
                        $budRequestCon = $request->bud_request_construction;
                        $project->bud_request_construction = $budRequestCon;
                    }

                    if (preg_match("/^[0-9,]+$/", $request->bud_request_check)) {
                        $budRequestCheck = str_replace(',', '', $request->bud_request_check);
                        $project->bud_request_check = $budRequestCheck;
                    } else {
                        $budRequestCheck = $request->bud_request_check;
                        $project->bud_request_check = $budRequestCheck;
                    }

                    // ============>  Sum Budget Request <================
                    $project->total_bud_request = (int)$budRequestCon + (int)$budRequestCheck;


                    // check if numeric has commas (Budget After Warranty)
                    if (preg_match("/^[0-9,]+$/", $request->bud_after_warranty_con)) {
                        $budAfterWarrantyCon = str_replace(',', '', $request->bud_after_warranty_con);
                        $project->bud_after_warranty_construction = $budAfterWarrantyCon;
                    } else {
                        $budAfterWarrantyCon = $request->bud_after_warranty_con;
                        $project->bud_after_warranty_construction = $budAfterWarrantyCon;
                    }

                    if (preg_match("/^[0-9,]+$/", $request->bud_after_warranty_check)) {
                        $budAfterWrrantyCheck = str_replace(',', '', $request->bud_after_warranty_check);
                        $project->bud_after_warranty_check = $budAfterWrrantyCheck;
                    } else {
                        $budAfterWrrantyCheck = $request->bud_after_warranty_check;
                        $project->bud_after_warranty_check = $budAfterWrrantyCheck;
                    }

                    // =============> Sum Budget After Warranty <=============
                    $project->total_bud_after_warranty = (int)$budAfterWarrantyCon + (int)$budAfterWrrantyCheck;


                    // check if numeric has commas (Budget Contract)
                    if (preg_match("/^[0-9,]+$/", $request->bud_contract_construciton)) {
                        $budContractCon = str_replace(',', '', $request->bud_contract_construciton);
                        $project->bud_contract_construciton = $budContractCon;
                    } else {
                        $budContractCon = $request->bud_contract_construciton;
                        $project->bud_contract_construciton = $budContractCon;
                    }

                    if (preg_match("/^[0-9,]+$/", $request->bud_contract_check)) {
                        $budContractCheck = str_replace(',', '', $request->bud_contract_check);
                        $project->bud_contract_check = $budContractCheck;
                    } else {
                        $budContractCheck = $request->bud_contract_check;
                        $project->bud_contract_check = $budContractCheck;
                    }

                    // =============> Sum Budget Contract <==============

                    $project->total_bud_contract = (int)$budContractCon + (int)$budContractCheck;
                    $project->save();

                    $data = [
                        "bud_request_construction" => $project->bud_request_construction,
                        "bud_request_check" => $project->bud_request_check,
                        "bud_after_warranty_con" => $project->bud_after_warranty_construction,
                        "bud_after_warranty_check" => $project->bud_after_warranty_check,
                        "bud_contract_construciton" => $project->bud_contract_construciton,
                        "bud_contract_check" => $project->bud_contract_check,
                    ];

                    DB::commit();
                    return responseSuccess("គម្រោងបានកែប្រែ", $data);
                } catch (\Exception $exception) {
                    DB::rollBack();
                    return responseError($exception->getTrace());
                }
            } else {
                return responseError('មិនមានទិន្ន័យ');
            }
        }
    }

    public function updateDateInfo(Request $request, $projectId = 0)
    {
        $validator = Validator::make($request->all(), [
            // "project_start_date_contract"            => 'required'
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            $project = Project::where('id', $projectId)->first();
            if (!$project == null) {
                try {
                    DB::beginTransaction();

                    $project->project_start_date_contract   = $request->project_start_date_contract;
                    $project->project_end_date_contract     = $request->project_end_date_contract;
                    $project->process_contract_duration     = $request->process_contract_duration;
                    $project->delay_date                    = $request->delay_date;
                    $project->warranty_duration             = $request->warranty_duration;
                    $project->close_project_date            = $request->close_project_date;
                    $project->save();

                    $data = [
                        "project_start_date_contract" => $project->project_start_date_contract ? date('Y-m-d', strtotime($project->project_start_date_contract)) : null,
                        "project_end_date_contract" => $project->project_end_date_contract ? date('Y-m-d', strtotime($project->project_end_date_contract)) : null,
                        "process_contract_duration" => $project->process_contract_duration ? $project->process_contract_duration : null,
                        "delay_date" => $project->delay_date ? date('Y-m-d', strtotime($project->delay_date)) : null,
                        "warranty_duration" => $project->warranty_duration ? $project->warranty_duration : null,
                        "close_project_date" => $project->close_project_date ? date('Y-m-d', strtotime($project->close_project_date)) : null
                    ];

                    DB::commit();
                    return responseSuccess("គម្រោងបានកែប្រែ", $data);
                } catch (\Exception $exception) {
                    DB::rollBack();
                    return responseError($exception->getTrace());
                }
            } else {
                return responseError('មិនមានទិន្ន័យ');
            }
        }
    }
}
