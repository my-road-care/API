<?php

namespace App\Api\V1\Controllers\Supervision\Project\Info;

// ================>> Service <<================
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Http\Collections\Project\ProjectProcurementCollection;
use App\Api\V1\Service\FileServers;
use Carbon\Carbon;

// ==============>> Model <<==================
use App\Model\Supervision\Projects\ProjectProcurement;
class ProjectProcurementController extends ApiController
{

    use FileServers;
    public static function index($req, $projectId = null)
    {
        // Get Picture
        $projectProcurement = ProjectProcurement::with('creator')->where('project_id', $projectId)->orderBy('id', 'DESC')->paginate($req->per_page ?? 10);

        return new ProjectProcurementCollection($projectProcurement);
    }

    public function create(Request $request, $projectId = 0)
    {
        $validator = Validator::make($request->all(), [
            "title" => "required"
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {
                DB::beginTransaction();

                $data = new ProjectProcurement();

                $data->project_id     = $projectId;
                $data->title          = $request->title;
                $data->docu_code      = $request->docu_code;
                $data->date           = Carbon::now();
                $data->description    = $request->description;
                $data->creator_id     = auth()->id();

                // Store Image to File Server
                $image = $this->fileServer($request->file, $request->image_uri);
                if ($request->file) {
                    $uri = $image['uuid'];
                } else {
                    $uri = $image['fileURL']['uri'];
                }

                $data->image_uri = $uri;
                $data->save();
                $result = [
                    'id' => $data->id,
                    'title' => $data->title,
                    'docu_code' => $data->docu_code,
                    'description' => $data->description,
                    'date' => $data->date ? date('Y-m-d', strtotime($data->date)) : null,
                    'creator' => $data->creator ? $data->creator->name : null,
                    'file' => $uri

                ];
                DB::commit();
                return responseSuccess("គម្រោងបានបង្កើត", $result);
            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function update(Request $request, $projectId = 0, $procedure_id = 0)
    {

        $validator = Validator::make($request->all(), [
            "title" => "required"
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {

                $data = ProjectProcurement::where([
                    "project_id" => $projectId,
                    "id"         => $procedure_id
                ])->first();

                if ($data) {

                    DB::beginTransaction();

                    $data->project_id     = $projectId;
                    $data->title          = $request->title;
                    $data->docu_code      = $request->docu_code;
                    $data->description    = $request->description;
                    $data->creator_id     = auth()->id();

                    // Store Image to File Server
                    if ($request->file != null || $request->image_uri != null) {
                        $image = $this->fileServer($request->file, $request->image_uri);
                        if ($request->file) {
                            $uri = $image['uuid'];
                        } else {
                            $uri = $image['fileURL']['uri'];
                        }
                        $data->image_uri = $uri;
                    }


                    $data->save();
                    DB::commit();
                    $result = [
                        'id' => $data->id,
                        'title' => $data->title,
                        'docu_code' => $data->docu_code,
                        'description' => $data->description,
                        'date' => $data->date ? date('Y-m-d', strtotime($data->date)) : null,
                        'creator' => $data->creator ? $data->creator->name : null,
                        'file' => $data->image_uri
                    ];
                    return responseSuccess("គម្រោងបានកែប្រែ", $result);
                } else {
                    return responseSuccess('មិនមានទិន្ន័យ');
                }
            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function delete($projectId = 0, $procedure_id = 0)
    {

        $data = ProjectProcurement::where([
            "project_id" => $projectId,
            "id"         => $procedure_id
        ])->first();

        if ($data) {
            $data->delete();
            return responseSuccess('បានលុបទិន្ន័យ');
        } else {
            return responseError('មិនមានទិន្ន័យ');
        }
    }
}
