<?php

namespace App\Api\V1\Controllers\Supervision\Project\Info;


// ===============>> Service <<==============
use Illuminate\Http\Request;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Service\CheckPermission;
use App\Api\V1\Service\Firebase;
use App\Enum\Role as EnumRole;
use App\MPWT\FileUpload;
use App\MPWT\SMS;

// =================>> Model <<================
use App\Model\Supervision\Projects\ProjectWorkingGroup;
use App\Model\Supervision\Projects\ProjectRole;
use App\Model\Supervision\Projects\SystemRole;
use App\Model\Supervision\Setup\Role;
use App\Model\User\Notification;
use App\Model\User;

class ProjectWorkingStructureController extends ApiController
{

    use Firebase, CheckPermission;

    public function listing(Request $request,$projectId){

        $roles = [
            EnumRole::Minister,
            EnumRole::SVHeadOfCommittee,
            EnumRole::SpecialInspector
        ];
        $users = User::whereIn('type_id', $roles)->get();

        $userRoles = [];
        foreach($users as $user){
            $userRoles[] = $user['id'];
        }

        if($request->type == "mobile"){
            
            $roles = SystemRole::select('id','name')
            ->get();
            
            foreach($roles as $role){
                $role['value'] = ProjectWorkingGroup::where(['project_id'=>$projectId,'sys_id'=>$role['id']])
                ->whereNotIn('user_id',$userRoles) // minister , special inspector , head of commitee
                ->with(['user.organization','role','sys'])
                ->select('id','user_id','role_id','sys_id','project_id')
                ->get();
            }

            return response()->json(["data"=>$roles],200);

        }

        $data = ProjectWorkingGroup::where('project_id',$projectId)
        ->whereNotIn('user_id',$userRoles) // minister , special inspector , head of commitee
        ->whereNotIn('sys_id',[EnumRole::SuperAdmin, EnumRole::RU]) // info entry
        ->with([
            'user.organization',
            'user.role',
            'sys',
            'role',
            'creator'
        ]);

        $data   = $data->orderBy('id', "DESC")->paginate($request->pageSize ?? 10);

        return response()->json($data,200);
    }

    public function getRole(){
        $roles = Role::whereNotIn('id', [1, 2, 3])->orderBy('id', 'desc')->get();
        return response()->json($roles,200);
    }

    public function getUser(Request $request){
        
        $projectId = $request->projectId;

        // ================>> get the exit user in project <<=================
        $select = ProjectWorkingGroup::where(['project_id'=>$projectId])->get();
        $users = [];

        foreach($select as $row){
            $users[] = $row['user_id'];
        }


        // =================>> select user <<=================
        $data = User::with([
            'organization',
            'role'
        ])
        ->select('id','role_id','organization_id','name','avatar','phone')
        ->orderBy('id','desc')
        ->whereNotIn('id',$users)
        ->where('type_id', 7)->get();
        
        return response()->json($data,200);
    }

    public function getSysRole(){

        $systemRole  = SystemRole::select('id','name','abbre')
        ->whereNotIn('id',[1,4,6,7])
        ->get();
        
        $projectRole = ProjectRole::select('id','name')
        ->get(); 

        return response()->json([
            "systemRole"  => $systemRole,
            "projectRole" => $projectRole
        ],200);

    }

    public function create(Request $request, $projectId)
    {
        $this->validate($request,[
            'user_id'       => 'required',
            'sys_id'        => 'required'
        ]);

        $data = new ProjectWorkingGroup();
        $data->project_id  = $projectId;
        $data->user_id     = $request->user_id;
        $data->role_id     = $request->role_id;
        $data->sys_id      = $request->sys_id;
        $data->creator_id  = auth()->id();
        $data->responsible = $request->responsible;

        $data->save();

        $workingGroup = ProjectWorkingGroup::with('project')->where([
            'project_id' => $projectId,
            'user_id'    => $request->user_id
        ])->first();


        $user = User::with('type')->where('id', $request->user_id)->first();

        // ===============>> Get Role
        $role = SystemRole::where('id', $request->sys_id)->first();

        // ===============>> Message to Firebase
        $message      = "គម្រោងលេខ ៖ ". $workingGroup->project->code . "\nតួនាទីរបស់អ្នកជា ៖ " . $role->name . "\nសូមអរគុណ";

        // ===============>> Store User Notification
        $notification = new Notification();

        $notification->user_id      = $request->user_id;
        $notification->project_id   = $projectId;
        $notification->title        = $user->type->name;
        $notification->type         = "Supervision";
        $notification->description  = $message;
        $notification->way          = "firebase";
        $notification->is_seen      = 0;
        $notification->save();      
        
        $project =  $this->formatProject($workingGroup->project, $request->user_id, $notification);

        if($user->app_token){
            $this->sendNotification($project, $request->user_id, $message);
        }else{
            SMS::sendSMS($user->phone, $message);
        }

        return response()->json($data,200);            

    }

    private function formatProject($project, $user_id, $notification = null)
    {
        $data = [
            "id"                => $project['id'] ?? "",
            "notification_id"   => $notification->id,
            "notification_type" => $notification->type,
            "name"              => $project['name'],
            "code"              => $project['code'] ?? "",
            "category"          => $project->category ?? "",
            "piu"               => $this->getPIU($project->organizations ? $project->organizations->where('type_id',2) : null),
            "status"            => $project->status ?? "",
            "budget_plans"      => $this->budget($project->budgets, $project->year),
            "permission"        => $this->permission($project['id'] ?? "", $user_id)

        ];
        return $data;
    }

    private function getPIU($entities){
        $arrEntities = [];
        foreach($entities as $entity){
            $arrEntities [] = [
                "id"               => $entity['id'] ?? "",
                "abbre"            => $entity['abbre'] ?? "",
            ];
        }
        return $arrEntities;
    }

    private function projectOwner($entities){
            $entities  = [
                "id"               => $entities['id'] ?? "",
                "abbre"            => $entities['abbre'] ?? "",
            ];
        return $entities;
    }

    private function budget($budget, $year){
        $name = $budget ? $budget->name : null;
        $budYear =  $year ? " (ឆ្នាំ ".$year->year.")" : null;
        return [
            'id'   => $budget->id,
            'name' => $name.$budYear
        ];
    }

    private function permission($projectId = 0, $userId = 0){

        $role = $this->checkPermission("project-info-*", 'R', $projectId, $userId);

        // ==========================> Get User Role Project's
        $projectWorkingGroup = ProjectWorkingGroup::with('sys')->where([
            'user_id' => $userId,
            'project_id' => $projectId])->first();

        if($projectWorkingGroup){
            $userProjectRole = $projectWorkingGroup->sys ? $projectWorkingGroup->sys->name : null;
        }else{
            $userProjectRole = null;
        }
        return [
                'role' => $userProjectRole, 
                'can'       => [
                    'access_info'       => $role['check'], 
                    'access_technical'  => $this->checkPermission("project-technical-*", 'R', $projectId, $userId)['check'], 
                    'access_report'     => $this->checkPermission("project-report-*", 'R', $projectId, $userId)['check'], 
                    'delete'            => $this->checkPermission("project-*", 'D', $projectId, $userId)['check']
                ]
            ]; 
    }

    public function createUserProject(Request $request, $projectId){

        
        $data = new User();
        $data->type_id = 7;


        $check = User::where('email', $request->email)->first();
        $phone = User::where('phone', $request->phone)->first();

        if ($phone) {
            return response()->json([
                "status" => 400,
                "message" => "លេខទូរស័ព្ទត្រូវបានប្រើប្រាស់រួចរាល់ហើយ !",
            ], 200);
        }

        if ($check) {
            return response()->json([
                "status" => 400,
                "message" => "អុីម៉ែលត្រូវបានប្រើប្រាស់រួចរាល់ហើយ !",
            ], 200);
        }

        $data->name                = $request->name;
        $data->organization_id     = $request->organization_id;
        $data->role_id             = $request->organization_role;
        $data->email               = $request->email;
        $data->phone               = $request->phone;
        $data->password            = bcrypt($request->password);
        $data->type_id             = EnumRole::SupervisionUser;

        if($request->image_uri){
            $file = $request->image_uri;
            $image = FileUpload::forwardFile($file);   
            $data->avatar = $image;
        }
        else{
            $data->avatar  = "uploads/TEST/L-1500000px/RoadCare/20230327/01/6420f2b993b62.jpeg";
        }
        
        $data->is_phone_verified = 1;
        $data->is_email_verified = 1;
        $data->is_notified_when_login = 1;
        $data->is_notified_when_login_with_unknown_device = 1;
        $data->is_active = 1;
        $data->social_type_id = 1;
        $data->access_supervision_feature = 1;
        $data->save();


        $project = new ProjectWorkingGroup();

        $project->project_id  = $projectId;
        $project->user_id     = $data['id'];
        $project->role_id     = $request->role_id;
        $project->sys_id      = $request->sys_id;
        $project->responsible = $request->responsible;
        $project->creator_id  = auth()->id();

        $project->save();

    }

    public function show(Request $request, $projectId)
    {
        return $projectId;
    }

    public function delete(Request $request)
    {
        $this->validate($request,[
            'userId'    => 'required',
            'projectId' => 'required'
        ]);

        $UserId    = $request->userId;
        $projectId = $request->projectId;

        $data = ProjectWorkingGroup::where(['user_id'=>$UserId,'project_id'=>$projectId])->first();

        if($data){
            $data->delete();
            return response()->json(["message"=>"ទិន្នន័យត្រូវបានលុប"]);
        } else {
            return response()->json(["message"=>"មិនមានទិន្នន័យ"]);
        }
    }

    public function update(Request $request){

        $data = ProjectWorkingGroup::where('id',$request->id)->first();
        if($data){
            $data->responsible = $request->responsible;
            $data->sys_id   = $request->system_id;
            $data->role_id  = $request->project_id;
            $data->save();
            return response()->json($data,200);
        }
        else{
            return response()->json(["message"=>"Not Found ! "],404);
        }
    }
    
}
