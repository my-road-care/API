<?php

namespace App\Api\V1\Controllers\Supervision\Project\Info;

use App\Api\V1\Controllers\ApiController;
use App\Jobs\ProjectStatisticJob;
use App\Jobs\UpdateWorkDiagramCache;
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Projects\Station;
use App\Model\Supervision\Projects\StationWork;
use App\Model\Supervision\Projects\Transaction;
use App\Model\Supervision\Projects\StationWorkSideTest;
use App\Model\Supervision\Setup\Work;
use Illuminate\Support\Facades\Cache;

class ProjectStatisticController extends ApiController
{

    public function approve($projectId = 0)
    {

        $project = Project::where('id', $projectId)->first();

        if ($project->diagramApprover && $project->technical_approved_datetime) {
            $approval = [
                'date' => $project->technical_approved_datetime,
                'by'   => $project->diagramApprover ? $project->diagramApprover->name : null
            ];
        } else {
            $approval = null;
        }

        return responseSuccess('success', $approval);
    }

    public function getStatistic($projectId = 0)
    {

        $cache_key = 'project_statistic_' . $projectId;

        $response = Cache::get($cache_key, function () use ($projectId, $cache_key) {
            dispatch(new ProjectStatisticJob($projectId));
            return Cache::get($cache_key);
        });

        return json_decode($response, true);
        
        
    }

    private function station($stations)
    {
        $arrStation = [];

        foreach ($stations as $row) {
            $arrStation[] = [
                'id' => $row->id,
                'transaction_id' => $row->transaction_id,
                'number'         => $row->number,
                'trx'            => $row->trx
            ];
        }

        return $arrStation;
    }
}
