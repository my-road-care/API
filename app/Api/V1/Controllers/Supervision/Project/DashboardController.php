<?php

namespace App\Api\V1\Controllers\Supervision\Project;
use App\Api\V1\Controllers\ApiController;
use App\Model\Supervision\Projects\Project;

class DashboardController extends ApiController
{

    public function dashboard(){
        if(auth()->user()->type_id == 7){
            $projects      = Project::whereHas('projectWorkingGroups', function($query) {
                $query->where('user_id', auth()->id());
            })->with([
                'status:id,kh_name,en_name,color',
                'road:id,name,length,start_point,end_point',
                'road.provinces:id,road_id,province_id',
                'road.provinces.province:id,name,abbre,en_name'
            ])->get();
        }elseif(auth()->user()->type_id == 11){
            $projects = Project::where('created_by', auth()->id())->get();
        }else{
            $projects      = Project::with([
                'status:id,kh_name,en_name,color',
                'road:id,name,length,start_point,end_point',
                'road.provinces:id,road_id,province_id',
                'road.provinces.province:id,name,abbre,en_name'
            ])->get();            
        }

        if($projects){
            $complete = [];
            $pro = [];
            foreach($projects as $row){
                $roads = [];

                if($row->project_status_id == 3){
                    $complete[] = $row;
                }

                // =========> Get Road and Province <============
                $roads[] = $row->road;

                $pro[] = [
                    'id'     => $row->id,
                    'name'   => $row->name,
                    'code'   => $row->code,
                    'status' => $row->status,
                    'road'   => $roads
                ];
            }

            $res = [
                'all' => count($projects),
                'complete' => count($complete),
                'projects' => $pro
            ];


        }else{
            $res = [
                'all' => 0,
                'complete' => 0,
                'projects' => []
            ];
        }

        //===================> Potholes

        $potholes = [
            'all' => 0,
            'complete' => 0
        ];

        return [
            'project' => $res,
            'pothole' => $potholes
        ];

    }
}
