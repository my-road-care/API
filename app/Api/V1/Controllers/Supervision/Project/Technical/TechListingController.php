<?php

namespace App\Api\V1\Controllers\Supervision\Project\Technical;

// ===============>> Service <<================
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Controllers\Supervision\Project\Technical\TechTestingStandardController;
use Illuminate\Http\Request;

// ==============>> Model <<=============
use App\Model\Supervision\Setup\TestingMethod;
use App\Model\Supervision\Projects\ProjectPlan;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Setup\Work;

class TechListingController extends ApiController
{

    public function index(Request $request, $projectId = 0)
    {
        
        $tests                  = ProjectStandardTest::with(['testTypes', 'work', 'users'])->where('project_id', $projectId)->orderBy('id', 'DESC')->get();
        $projectPlans           = ProjectPlan::where('project_id', $projectId)->orderBy('id', 'DESC')->get();

        if ($request->type == "mobile") {
            return [

                "work_diagram" => [],
                "testing_standard" => [
                    "permission" => [],
                    "data"       => $this->tests($tests)
                ],
                "test_policies" => [
                    "permission" => [],
                    "data"       => []
                ],
                "project_plan" => [
                    "permission" => [],
                    "data"       => $this->plans($projectPlans)
                ]
            ];
        } else {
            $workTypes              = Work::get();
            $tesing                 = TestingMethod::get();

            return [

                "data_setup" => [
                    "work_types"    => $this->workTypes($workTypes),
                    "testing"       =>   $this->testing($tesing),
                ],
                "work_diagram" => [],
                "testing_standard" => [
                    "permission" => [],
                    "data"       => $this->tests($tests)
                ],
                "test_policies" => [
                    "permission" => [],
                    "data"       => []
                ]

            ];
        }
    }

    private function tests($projectTests)
    {

        $tests = [];

        foreach ($projectTests as $row) {
            $tests[] = [
                "id"                => $row->id,
                "test_type"         => TechTestingStandardController::testType($row->testTypes),
                "work_type"         => TechTestingStandardController::workType($row->work),
                "creator"           => $row->creator ? $row->users->name : null,
                "standard"          => $row->standard,
                'date'              => $row->date ? date('Y-m-d', strtotime($row->date)) : null,
                "pk_start"          => $row->pk_start,
                "pk_end"            => $row->pk_end,
            ];
        }
        return $tests;
    }

    private function workTypes($workTypes)
    {

        $workTypesId = [];

        foreach ($workTypes as $row) {
            $workTypesId[] = [
                "id"         => $row->id,
                "name"       => $row->en_name,
            ];
        }
        return $workTypesId;
    }

    private function testing($tesings)
    {

        $testingId = [];

        foreach ($tesings as $row) {
            $testingId[] = [
                "id"         => $row->id,
                "name"       => $row->name,
            ];
        }
        return $testingId;
    }

    private function testPrinciples($Principles)
    {

        $testPrinciples = [];

        foreach ($Principles as $row) {
            $testPrinciples[] = [
                "id"         => $row->id,
                "title"      => $row->title,
                "image_uri"  => $row->file_uri,
                "date"       => $row->date ? date('Y-m-d', strtotime($row->date)) : null,
            ];
        }
        return $testPrinciples;
    }
}
