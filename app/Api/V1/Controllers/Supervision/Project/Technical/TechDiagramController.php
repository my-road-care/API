<?php

namespace App\Api\V1\Controllers\Supervision\Project\Technical;


// ==================>> Service <<=================
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use App\Api\V1\Controllers\ApiController;
use Carbon\Carbon;
use App\Jobs\Thread;
use App\Enum\Roadgroup;
use App\Jobs\UpdateWorkDiagramCache;

// =============>> Model <<===============
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\Station;
use App\Model\Supervision\Projects\StationWork;
use App\Model\Supervision\Projects\StationWorkSide;
use App\Model\Supervision\Projects\StationWorkSideHistory;
use App\Model\Supervision\Projects\StationWorkSideTest;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Projects\StationWorkSideTestParam;
use App\Model\Supervision\Projects\Transaction;
use App\Model\Supervision\Setup\Work;

class TechDiagramController extends ApiController
{

    public function dataSetup(){
        $workTypes = Work::with('testingMethods')->where('type_id', 1)->get();
        $subGrade = [];
        $parvement = [];

        foreach ($workTypes as $row) {
            if ($row->group_id == 1) {
                $subGrade[] = [
                    "id" => $row->id,
                    "en_name" => $row->en_name,
                    "kh_name" => $row->kh_name,
                    'testing_amount' => count($row->testingmethod)
                ];
            } else {
                $parvement[] = [
                    "id" => $row->id,
                    "en_name" => $row->en_name,
                    "kh_name" => $row->kh_name,
                    'testing_amount' => count($row->testingmethod)
                ];
            }
        }

        return [
            "subgrades"  => $subGrade,
            "parvements" => $parvement
        ];
    }

    public function dataSetupArt()
    {
        $workTypes = Work::where('type_id', 2)->get();

        $works = [];

        foreach ($workTypes as $row) {
            $works[] = [
                "id" => $row->id,
                "en_name" => $row->en_name,
                "kh_name" => $row->kh_name
            ];
        }

        return $works;
    }

    public function updateTest($projectId = 0)
    {

        // ===============> Sub Base <==================
        $stations   = Station::whereHas('point', function ($query) use ($projectId) {
            $query->where('project_id', $projectId)->whereBetween('pk', [18, 28]);
        })->get();

        $staionIds = [];
        foreach ($stations as $stationId) {
            $staionIds[] = $stationId->id;
        }

        $works = Work::where('id', 6)->get();
        $data = [];

        foreach ($works as $work) {
            $l = [];
            $r = [];
            foreach ($stations as $station) {
                $stationWork = StationWork::where([
                    'station_id' => $station->id,
                    'work_id' => $work->id
                ])
                    ->with([
                        'sides',
                        'sides.status',
                        'sides.testingRecords.standardTest'
                    ])->first();
                if ($stationWork) {
                    foreach ($stationWork->sides as $side) {

                        $sideId = StationWorkSide::where('id', $side->id)->first();
                        $sideId->update([
                            'progress_status_id' => 3
                        ]);
                    }
                }
            }
        }

        // ===============> Base Course <==================
        $stations   = Station::whereHas('point', function ($query) use ($projectId) {
            $query->where('project_id', $projectId)->whereBetween('pk', [22, 28]);
        })->get();

        $staionIds = [];
        foreach ($stations as $stationId) {
            $staionIds[] = $stationId->id;
        }

        $works = Work::where('id', 5)->get();
        $data = [];

        foreach ($works as $work) {
            $l = [];
            $r = [];
            foreach ($stations as $station) {
                $stationWork = StationWork::where([
                    'station_id' => $station->id,
                    'work_id' => $work->id
                ])
                    ->with([
                        'sides',
                        'sides.status',
                        'sides.testingRecords.standardTest'
                    ])->first();
                if ($stationWork) {
                    foreach ($stationWork->sides as $side) {

                        $sideId = StationWorkSide::where('id', $side->id)->first();
                        $sideId->update([
                            'progress_status_id' => 3
                        ]);
                    }
                }
            }
        }

        // ===============> DBST <==================
        $stations   = Station::whereHas('point', function ($query) use ($projectId) {
            $query->where('project_id', $projectId)->whereBetween('pk', [25, 28]);
        })->get();

        $staionIds = [];
        foreach ($stations as $stationId) {
            $staionIds[] = $stationId->id;
        }

        $works = Work::where('id', 4)->get();
        $data = [];

        foreach ($works as $work) {
            $l = [];
            $r = [];
            foreach ($stations as $station) {
                $stationWork = StationWork::where([
                    'station_id' => $station->id,
                    'work_id' => $work->id
                ])
                    ->with([
                        'sides',
                        'sides.status',
                        'sides.testingRecords.standardTest'
                    ])->first();
                if ($stationWork) {
                    foreach ($stationWork->sides as $side) {

                        $sideId = StationWorkSide::where('id', $side->id)->first();
                        $sideId->update([
                            'progress_status_id' => 3
                        ]);
                    }
                }
            }
        }
    }


    public function getDiagram(Request $request, $projectId = 0)
    {

        $cache_key = 'work_diagram_' . $projectId;

        $response = Cache::get($cache_key, function () use ($projectId, $cache_key) {
            dispatch(new UpdateWorkDiagramCache($projectId));
            return Cache::get($cache_key);
        });

        return json_decode($response, true);
    }

    public function create(Request $request, $projectId = 0)
    {
        $validator = Validator::make($request->all(), [
            "pk_start"  => "required",
            "pk_end"    => "required",
            "work_type" => "required"
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {

                DB::beginTransaction();
                $project = Project::where('id', $projectId)->with(['road'=>function($q){
                    $q->select('id','name')->get();
                }])->first();
                if ($project->diagram_creator_id == null) {
                    $project->update([
                        'diagram_creator_id'  => auth()->id(),
                        'diagram_create_date' => Carbon::now(),
                        'interval'            => $request->stationScale
                    ]);
                } else {
                    $project->update([
                        'diagram_updater_id'  => auth()->id(),
                        'diagram_update_date' => Carbon::now(),
                        'interval'            => $request->stationScale
                    ]);
                }

                //===================>> Delete Data
                $this->deleteRelatedData($projectId);

                // =========================>> Generate PK Range
                if($request->station_end > 0){
                    $nOfPKs = $request->pk_end - $request->pk_start + 1;
                }else{
                    $nOfPKs = $request->pk_end - $request->pk_start;
                }
                $pks    = [];

                for ($i = 0; $i <= $nOfPKs; $i++) {
                    $pks[] = [
                        'project_id' => $projectId,
                        'pk'         => $request->pk_start + $i,
                    ];
                }

                Transaction::insert($pks);

                // ========================>> Generate Station
                $transactions   = Transaction::where('project_id', $projectId)->get();
                $stations       = [];
                $arr            = [];
                $num            = 0;

                $scale = $request->stationScale;
                $numStation = 1000 / $scale;

                foreach ($transactions as $transaction) {
                    if ($transaction->pk == $request->pk_end + 1) {
                        $arr[] = [
                            'transaction_id' => $transaction->id,
                            'number'         => number_format($num * $scale, 0, '.', ','),
                            'station_num'    => '0',
                            'trx'            => $request->pk_end + 1 . '+000'
                        ];
                    } else {
                        for ($i = 0; $i <= $numStation - 1; $i++) {
                            $num++;
                            $number = $i * $scale;
                            $st = $num * $scale - $scale;
                            $trx = $transaction->pk . '+' . sprintf("%03s", $number);
                            $stations[] = [
                                'transaction_id' => $transaction->id,
                                'number'         => number_format($st, 0, '.', ','),
                                'station_num'    => $number,
                                'trx'            => $trx
                            ];
                        }
                    }
                }

                $arrayMerger = array_merge($stations, $arr);

                Station::insert($arrayMerger);

                $deleteStart = Station::whereHas('point', function($query) use($projectId, $request){
                    $query->where([
                        'project_id' => $projectId,
                        'pk'         => $request->pk_start
                    ]);
                })->where('station_num', '<', $request->station_start);

                if($deleteStart->get()){
                    $deleteStart->delete();
                }

                $deleteEnd = Station::whereHas('point', function($query) use($projectId, $request){
                    $query->where([
                        'project_id' => $projectId,
                        'pk'         => $request->pk_end
                    ]);
                })->where('station_num', '>', $request->station_end);

                if($deleteEnd->get()){
                    $deleteEnd->delete();
                }

                if($request->station_end > 0){
                    Transaction::where([
                        'project_id' => $projectId,
                        'pk'         => $request->pk_end + 1
                    ])->delete();
                }

                // ================>> Add Construction Work <<==============
                $collection = collect($request->works_id);
                $works = $collection->sort();

                $stations = Station::whereHas('point', function ($query) use ($projectId) {
                    $query->where('project_id', $projectId);
                })->get();

                foreach ($stations as $station) {
                    foreach ($works as $work) {
                        $stationWorkId = StationWork::insertGetId([
                            'station_id' => $station->id,
                            'work_id'    => $work
                        ]);

                        // ===============>> Left <<================
                        $sideId = StationWorkSide::insertGetId([
                            'station_work_id'    => $stationWorkId,
                            'side_id'            => 1,
                            'progress_status_id' => 1,
                            'can_update'         => 0
                        ]);

                        StationWorkSideHistory::insert([
                            'station_work_side_id'  => $sideId,
                            'progress_status_id'    => 1,
                            'updater_id'           => null,
                            'created_at'            => Carbon::now(),
                            'comment'               => null,
                        ]);

                        // =================>> Right <<==============
                        $sideId = StationWorkSide::insertGetId([
                            'station_work_id'    => $stationWorkId,
                            'side_id'            => 2,
                            'progress_status_id' => 1,
                            'can_update'         => 0

                        ]);

                        StationWorkSideHistory::insert([
                            'station_work_side_id'  => $sideId,
                            'progress_status_id'    => 1,
                            'updater_id'           => null,
                            'created_at'            => Carbon::now(),
                            'comment'               => null,
                        ]);
                    }
                }

                // =============> Add Standard Test <<============
                $transactions = Transaction::with([
                    'stations'
                ])->where('project_id', $projectId)->get();
        
                $pkStart = null;
                $pkEnd   = null;
                $stationStart = null;
                $stationEnd   = null;
                $i       = 0;
                $len     = count($transactions);
                $j = 0;
                $k = 0;

                foreach($transactions as $transaction){
                    if($i == 0){

                        $pkStart = $transaction->pk;
                        foreach($transaction->stations as $newstation){
                            if($j == 0){
                                $stationStart = $newstation->number;
                            }
                            $j ++;
                        }
                    }elseif($i == $len - 1){

                        $pkEnd = $transaction->pk;
                        $stationEndCount = count($transaction->stations);
                        foreach($transaction->stations as $newstation){
                            if($k == $stationEndCount-1){
                                $stationEnd = $newstation->number;
                            }
                            $k ++;
                        }
                    }
                    $i++;            
                }

                // TODO:: Get Work
                $stations   = Station::whereHas('point', function ($query) use ($projectId) {
                    $query->where('project_id', $projectId);
                })->get();
        
                $staionIds = [];
                foreach ($stations as $stationId) {
                    $staionIds[] = $stationId->id;
                }

                if(Str::length($project->road->name) == 1){
                    $groupRoad=RoadGroup::RoadNum1;
                }elseif(Str::length($project->road->name) == 2){
                    $groupRoad=RoadGroup::RoadNum2;
                }else{
                    $groupRoad=RoadGroup::RoadNum3;
                } 

                $newWorks=Work::with(['standard'=>function($q) use ($groupRoad){
                    $q->select('id','layer_id','group_id','method_id','value')->where('group_id',$groupRoad)->with(['method'=>function($q){
                        $q->select('id','testing_method_id')->with('method')->get();
                    }])->get();
                }])->select('id', 'kh_name as name')->whereHas('stationWorks', function ($query) use ($staionIds) {
                        $query->whereIn('station_id', $staionIds);
                })->orderBy('group_id', 'DESC')->get();

                foreach($newWorks as $newWork){
                    foreach($newWork->standard as $newTestingMethod){
                        $projectStandard = new ProjectStandardTest();
                        $projectStandard->project_id          = $projectId;
                        $projectStandard->testing_method_id   = $newTestingMethod->method->testing_method_id;
                        $projectStandard->work_id             = $newWork->id;
                        $projectStandard->standard            = $newTestingMethod->value;
                        $projectStandard->creator_id          = auth()->id();
                        $projectStandard->date                = Carbon::now();
                        $projectStandard->pk_start            = $pkStart;
                        $projectStandard->pk_end              = $pkEnd;
                        $projectStandard->station_start       = $stationStart;
                        $projectStandard->station_end         = $stationEnd;
                        $projectStandard->save();
                    }                    
                }
                    
                DB::commit();
                
                dispatch(new Thread("work-diagram-cache:update $projectId"));

                return responseSuccess("Diagram has been created.");
            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    private function deleteRelatedData($projectId = 0)
    {

        // ============>> Standard Test <<============
        ProjectStandardTest::where('project_id', $projectId)->delete();

        // ============>> Testing Param <<============
        StationWorkSideTestParam::whereHas('test', function ($query) use ($projectId) {
            $query->whereHas('standardTest', function ($query) use ($projectId) {
                $query->where('project_id', $projectId);
            });
        })
        ->delete();

        // ===============>> Testing Record <<==================
        StationWorkSideTest::whereHas('standardTest', function ($query) use ($projectId) {
            $query->where('project_id', $projectId);
        })
        ->delete();

        // =================>> Standard Test <<====================
        ProjectStandardTest::where('project_id', $projectId)->delete();

        // =================>> Status Histrory <<==================
        StationWorkSideHistory::whereHas('side', function ($query) use ($projectId) {
            $query->whereHas('station', function ($query) use ($projectId) {
                $query->whereHas('station', function ($query) use ($projectId) {
                    $query->whereHas('point', function ($query) use ($projectId) {
                        $query->where('project_id', $projectId);
                    });
                });
            });
        })
        ->delete();

        // ====================>> Status Histrory <<===================
        StationWorkSideHistory::whereHas('side', function ($query) use ($projectId) {
            $query->whereHas('station', function ($query) use ($projectId) {
                $query->whereHas('station', function ($query) use ($projectId) {
                    $query->whereHas('point', function ($query) use ($projectId) {
                        $query->where('project_id', $projectId);
                    });
                });
            });
        })
            ->delete();

        // ===================>> PK, Station, Work, Side <<===================
        Transaction::where('project_id', $projectId)->delete();
    }

    public function createArtistic(Request $request, $projectId = 0)
    {

        // =========================>> Add Construction Work
        $work = $request->work_id;

        $transaction = Transaction::where('project_id', $projectId)->get();
        $stations = Station::whereHas('transaction', function ($query) use ($projectId) {
            $query->where('project_id', $projectId);
        })->whereBetween('id', [$request->start, $request->end])->get();

        foreach ($stations as $station) {
            $stationWorkId = StationWork::insertGetId([
                'station_id' => $station->id,
                'work_id'    => $work
            ]);

            // ==================>> Left <<=================
            $sideId = StationWorkSide::insertGetId([
                'station_work_id'    => $stationWorkId,
                'side_id'            => 1,
                'progress_status_id' => 1
            ]);

            StationWorkSideHistory::insert([
                'station_work_side_id'  => $sideId,
                'progress_status_id'    => 1,
                'updater_id'           => null,
                'created_at'            => Carbon::now(),
                'comment'               => null,
            ]);

            // =================>> Right <<====================
            $sideId = StationWorkSide::insertGetId([
                'station_work_id'    => $stationWorkId,
                'side_id'            => 2,
                'progress_status_id' => 1
            ]);

            StationWorkSideHistory::insert([
                'station_work_side_id'  => $sideId,
                'progress_status_id'    => 1,
                'updater_id'           => null,
                'created_at'            => Carbon::now(),
                'comment'               => null,
            ]);
        }
    }
}
