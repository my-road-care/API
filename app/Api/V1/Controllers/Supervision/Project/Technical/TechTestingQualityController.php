<?php

namespace App\Api\V1\Controllers\Supervision\Project\Technical;


// ===============>> Service <<===============
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Service\FileServers;
use App\Jobs\Thread;
use Carbon\Carbon;


// =============>>  Model <<===============
use App\Model\Supervision\Projects\ProgressTRX;
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProjectImage;
use App\Model\Supervision\Projects\Station;
use App\Model\Supervision\Projects\StationWork;
use App\Model\Supervision\Projects\StationWorkSide;
use App\Model\Supervision\Projects\Transaction;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Projects\StationWorkSideHistory;
use App\Model\Supervision\Projects\StationWorkSideTest;
use App\Model\Supervision\Projects\StationWorkSideTestParam;
use App\Model\Supervision\Projects\TestingTRX;
use App\Model\Supervision\Setup\Work;
use App\Model\Supervision\Setup\Side;
use App\Model\Supervision\Setup\TestingMethodTypParamater;

class TechTestingQualityController extends ApiController
{
    use FileServers;

    public function testingInfo($projectId = 0, $pkId = 0){

        // =========> Gets Station <==========
        $stations   = Station::select('id','station_num')->whereHas('point', function ($query) use ($projectId, $pkId) {
            $query->where([
                'project_id'     => $projectId,
                'transaction_id' => $pkId
            ]);
        })->get();

        // ========> Get Station ID <=========
        $staionIds = [];
        foreach ($stations as $stationId) {
            $staionIds[] = $stationId->id;
        }

        // =========> Find Works that related to station <=========
        $works = Work::select('id', 'kh_name', 'en_name')        
        ->whereHas('stationWorks', function ($query) use ($staionIds) {
            $query->whereIn('station_id', $staionIds);
        })
        ->with([
            'testingMethods.standardTest',
            'testingMethods.standardTest' => function($query) use($projectId){
                $query->where('project_id', $projectId);
        }])->orderBy('group_id', 'DESC')->get();

        // =========> Get Side <=========
        $sides = Side::select('id', 'name', 'abrv')->get();

        // =========> Get PK <============
        $pk = Transaction::where('id', $pkId)->first();
        $pkName = $pk->pk;

        $arrWork = [];
        foreach($works as $work){
            $arrWork[] = [
                'id'     => $work->id,
                'name'   => $work->kh_name,
                'method' => $this->getTestingMethod($work->testingMethods, $work->id, $pkName),
            ];
        }
        return [
            "sides"   => $sides,
            "works"   => $arrWork,
            "station" => $stations
        ];
    }

    public function testingCheck(Request $request ,$projectId = 0){        

        $standardTests = ProjectStandardTest::with('method')->where([
            'project_id'            => $projectId,
            'testing_method_id'     => $request->method_id,
            'work_id'               => $request->work_id
        ])->where('pk_start','<=', $request->pk)->where('pk_end', '>=', $request->pk)->get();

        $project = Project::where('id',$projectId)->first();
        $interval = $project->interval;
        $arrStandard = [];

        foreach($standardTests as $standardTest){

            $stationNum = $standardTest->pk_start;
            $length = $standardTest->pk_end - $standardTest->pk_start;
            
            for($k = 0; $k <= $length ; $k++){
                for($i = 0; $i < 1000/$interval ; $i++){
                    if($i==0) $stationNum = (int) $standardTest->station_start;
                    else
                    $stationNum += $interval;
                    if($request->pk == $standardTest->pk_end){
                        if($request->station_start <= $stationNum && 
                        $request->station_end >= $stationNum &&
                        $standardTest->station_end >= $stationNum){
                            if(($standardTest->pk_start + $k) == $request->pk){
                                $arrStandard[] = [
                                    'id'        => $standardTest->id,
                                    'pk'        => $standardTest->pk_start + $k,
                                    'station'   => "PK ". ($standardTest->pk_start + $k) . "-" . sprintf("%03s", $stationNum),
                                    'require'   => $standardTest->standard,
                                    'unit'      => $standardTest->method ? $standardTest->method->unit : null
                                ];
                            }
                            
                        }                        
                    }else{
                        if(($standardTest->pk_start + $k) == $request->pk &&
                        $request->station_start <= $stationNum && 
                        $request->station_end >= $stationNum){
                            $arrStandard[] = [
                                'id'        => $standardTest->id,
                                'pk'        => $standardTest->pk_start + $k,
                                'station'   => "PK ". ($standardTest->pk_start + $k) . "-" . sprintf("%03s", $stationNum),
                                'require'   => $standardTest->standard,
                                'unit'      => $standardTest->method ? $standardTest->method->unit : null
                            ];
                        }
                    }
                }
            }
        }

        // =========> Get Method Parameters <============
        $params = TestingMethodTypParamater::select('id','name','unit')->where('testing_method_id', $request->method_id)->get();

        $collectStation = collect($arrStandard);
        $sort = $collectStation->sortBy('station');

        $res = [
            'pk' => $request->pk,
            'testing_standards' => array_values($sort->toArray()),
            'params'    => $params
        ];
        return $res;            
    }

    public function updateTest(Request $request, $projectId = 0){

        $data = json_decode($request->data);

        if($data){

            $pk = Transaction::where([
                'project_id'=> $projectId,
                'pk'        => $request->pk
            ])->first();

            $testingTrx                 = new TestingTRX();
            $testingTrx->tester_id      = auth()->id();
            $testingTrx->project_id     = $projectId;
            $testingTrx->pk_id          = $pk->id;
            $testingTrx->comment        = $request->message;
            $testingTrx->created_at     = Carbon::now();
            $testingTrx->save(); 

            $i = 0;
            $len = count($data);
            $progressTrxId = 0;
            $testingID = 0;

            foreach($data as $row){
                try {

                    DB::beginTransaction();

                    if ($i == 0) {
                        // first
                        $testing = TestingTRX::where('id', $testingTrx->id)->first();
                        $testing->update([
                            'work_id'       => $row->work_id,
                            'station_start' => $row->station,
                            'side_id'       => $row->side_id,
                            'testing_standard_id' => $row->testing_standard_id
                        ]);
                    } elseif ($i == $len - 1) {
                        // last
                        $testing = TestingTRX::where('id', $testingTrx->id)->first();
                        $testing->update([
                            'station_end' => $row->station
                        ]);
                    }
                    $i++;

                    // ============> Get Station <=============
                    $station = Station::where([
                        'transaction_id' => $pk->id,
                        'station_num'    => $row->station
                    ])->first();
                    
                    // =============> Get Station Work <=============
                    $stationWork = StationWork::where([
                        'work_id' => $row->work_id,
                        'station_id'=> $station->id])->first();

                    // ============> Get Station Work Side <================
                    $stationWorkSide = StationWorkSide::where([
                        'side_id'=> $row->side_id,
                        'station_work_id'=> $stationWork->id])->first();

                    // =============> Add Test Record <==============

                    $testStandard = ProjectStandardTest::with('method')->where('id', $row->testing_standard_id)->first();
                    if($testStandard->method->less_than_pass == 0){
                        if($row->result >= $testStandard->standard){
                            $isPass = 1;
                        }else{
                            $isPass = 0;
                        }
                    }else{
                        if($row->result <= $testStandard->standard){
                            $isPass = 1;
                        }else{
                            $isPass = 0;
                        }
                    }                   

                    $test                           = new StationWorkSideTest(); 
                    $test->station_work_side_id     = $stationWorkSide->id; 
                    $test->testing_trx_id           = $testingTrx->id; 
                    $test->standard_test_id         = $row->testing_standard_id; 
                    $test->result                   = $row->result; 
                    $test->comment                  = $request->message; 
                    $test->tester_id                = auth()->id();
                    $test->is_pass                  = $isPass;
                    $test->save();

                    if($request->parameters){
                        $parameters = json_decode($row->parameters);
                        $paramData = []; 

                        foreach($parameters as $parameter){
                            $paramData[] = [
                                'station_work_side_test_id' => $test->id, 
                                'standard_test_id'          => $row->standard_test_id, 
                                'parameter_id'              => $parameter->parameter_id, 
                                'value'                     => $parameter->value
                            ]; 
                        }
                        
                        if(count($paramData) > 0){
                            StationWorkSideTestParam::insert($paramData);
                        }
                    }
                    
                    // ===> Store Image to File Server
                    if($request->images && $testingID == 0){
                        $images = json_decode($request->images);
                        $imgs = [];
                        foreach($images as $img){
                            $size_in_bytes = (int) (strlen(rtrim($img->img, '=')) * 3 / 4);
                            $size_in_kb    = $size_in_bytes / 1024;
                            if($size_in_kb <= 4000){
                                $file = $this->fileServer('', $img->img);
                                $imgs[] = [
                                    'project_id'                   => $projectId,
                                    'station_work_side_test_id'    => $test->id,
                                    'testing_trx_id'               => $testing->id,
                                    'image_uri'                    => $file['fileURL']['uri'],
                                    'lat'                          => $img->lat,
                                    'lng'                          => $img->lng
                                ];
                            }                             
                        }
                        // ===> Store Image
                        ProjectImage::insert($imgs);                                            
                    }

                    // ===> Store PDF to File Server
                    if($request->file){
                        $file = $this->fileServer($request->file, '');
                        if ($request->file) {
                            $uri = $file['uuid'];
                        } else {
                            $uri = $file['fileURL']['uri'];
                        }
                        $files = [
                            'project_id'                   => $projectId,
                            'station_work_side_test_id'    => $test->id,
                            'image_uri'                    => $uri
                        ];     
                        
                        // ===> Store Image
                        ProjectImage::insert($files); 
                    }
                    
                    // ========> Update Progress <========
                    if($stationWorkSide->progress_status_id == 3 && $isPass == 1){

                        $stationWorkSide->update([
                            'progress_status_id' => $stationWorkSide->progress_status_id + 1
                        ]);

                        if($progressTrxId == 0){

                            $progressTrx                = new ProgressTRX();
                            $progressTrx->tester_id     = auth()->id();
                            $progressTrx->project_id    = $projectId;
                            $progressTrx->pk_id         = $pk->id; 
                            $progressTrx->created_at    = Carbon::now();
                            $progressTrx->station_start = $row->station;
                            $progressTrx->station_end   = $row->station;
                            $progressTrx->comment       = $request->message;
                            $progressTrx->side_id       = $row->side_id;
                            $progressTrx->work_id       = $row->work_id;
                            $progressTrx->save();

                            $progressTrxId = $progressTrx->id;

                            // ========> Create Progress update record <========
                            $history                        = new StationWorkSideHistory(); 
                            $history->station_work_side_id  = $stationWorkSide->id; 
                            $history->progress_trx_id       = $progressTrx->id;
                            $history->progress_status_id    = $stationWorkSide->progress_status_id; 
                            $history->comment               = $request->message; 
                            $history->created_at            = Date('Y-m-d H:i:s');
                            $history->updater_id            = auth()->id();
                            $history->save();


                            // ========> Add Image <=========
                            $images = ProjectImage::where([
                                'project_id' => $projectId,
                                'testing_trx_id' => $testing->id
                            ])->get();

                            foreach($images as $image){
                                $image->update([
                                    'progress_trx_id' => $progressTrxId
                                ]);
                            }

                        }elseif($progressTrxId != 0){

                            $progressTrx = ProgressTRX::where('id', $progressTrxId)->first();
                            $progressTrx->station_end   = $row->station;
                            $progressTrx->save();

                            // ========> Create Progress update record <========
                            $history                        = new StationWorkSideHistory(); 
                            $history->station_work_side_id  = $stationWorkSide->id; 
                            $history->progress_trx_id       = $progressTrx->id;
                            $history->progress_status_id    = $stationWorkSide->progress_status_id; 
                            $history->comment               = $request->message; 
                            $history->created_at            = Date('Y-m-d H:i:s');
                            $history->updater_id            = auth()->id();
                            $history->save();

                            // ========> Add Image <=========
                            $images = ProjectImage::where([
                                'project_id' => $projectId,
                                'testing_trx_id' => $testing->id
                            ])->get();

                            foreach($images as $image){
                                $image->update([
                                    'progress_trx_id' => $progressTrxId
                                ]);
                            }
                        }
                    }
                    $testingID = $testingTrx->id;
                        
                    DB::commit();

                    dispatch(new Thread("work-diagram-cache:update $projectId"));
                    dispatch(new Thread("project-statistic:get $projectId"));

                }catch (\Exception $exception) {
                    DB::rollBack();
                    return responseError($exception);
                }
            }
            return responseSuccess('បានធ្វើតេស្តជោគជ័យ');
        }else{
            return responseError("មិនមានទិន្ន័យគ្រប់គ្រាន់");
        }
        
    }

    private function getTestingMethod($methods = null, $workId = null, $pk = null){
        $arrMethod = [];        
        foreach($methods as $method){
            $arrMethod[] = [
                'id'             => $method->id,
                'name'           => $method->name,
                'less_than_pass' => $method->less_than_pass,
                'unit'           => $method->unit,
                'have_standard_test' => $this->haveStandardTest($method->standardTest, $workId, $pk)
            ];
        }
        return $arrMethod;
    }

    private function haveStandardTest($standardTests = null, $workId = null, $pk = null){
        $have = 0;
        $test = [];
        foreach($standardTests as $row){
            $test[] = $row->pk_start;
            if(str_replace(' ', '', $row->work_id) == str_replace(' ', '', $workId)){
                
                if(str_replace(' ', '', $row->pk_start) == str_replace(' ', '', $pk)){
                    $have = 1;
                    break;
                }
                    
            }else{
                $have = 0;
            }
        }
        return $have;
    }
}
