<?php

namespace App\Api\V1\Controllers\Supervision\Project\Technical;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


// ==============>> Service <<============
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Http\Collections\Project\TestingStandardCollection;
use Carbon\Carbon;
use App\Enum\Roadgroup;
use Illuminate\Support\Str;

// ==============>> Model <<===============
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\Transaction;
use App\Model\Supervision\Setup\Work;
use App\Model\Supervision\Setup\TestingMethodType;
use App\Model\Supervision\Setup\TestingMethod;
use App\Model\Supervision\Projects\ProjectStandardTest;

class TechTestingStandardController extends ApiController
{


    

    public function dataSetupTestingStandards($projectId = 0)
    {
        $project= Project::select('id','road_id')->with(['road'=>function ($q) { $q->select('id','name')->get();
        }])->find($projectId);
        if(Str::length($project->road->name) == 1){
          $groupRoad=RoadGroup::RoadNum1;
        }elseif(Str::length($project->road->name) == 2){
          $groupRoad=RoadGroup::RoadNum2;
        }else{
          $groupRoad=RoadGroup::RoadNum3;
        } 
        $pks = Transaction::select('id', 'project_id', 'pk')->where('project_id', $projectId)
        ->with([
            'stations'
        ])
        ->get();
        $layers      = Work::select('id', 'kh_name as name')
        ->whereHas('stationWorks', function($query) use ($projectId){
            $query->whereHas('station', function($query) use ($projectId){
                $query->whereHas('point', function($query) use ($projectId){
                    $query->where('project_id', $projectId);
                });
            });
        })
        ->with([
          'standard:layer_id,group_id,method_id,value',
          'standard.method:id,testing_method_id',
          'standard.method.method:id,name,less_than_pass,unit,type_id',
          'standard.method.method.TestingMethodType'
          ])->get();
       
        //  get Method
        $methods = TestingMethod::select('id', 'name','type_id','unit')
        ->whereHas('works',function($query) use ($projectId){
          $query->whereHas('stationWorks', function($query) use ($projectId){
            $query->whereHas('station', function($query) use ($projectId){
                $query->whereHas('point', function($query) use ($projectId){
                    $query->where('project_id', $projectId);
                });
              });
          });
        })
      ->get();
     
        return [
            'pks'       => $pks,
            'layers'    => $layers,
            'methods'   => $methods
        ];
    }

    public function listing(Request $req, $projectId = 0)
    {
        $data  = ProjectStandardTest::select('id', 'project_id', 'testing_method_id', 'work_id', 'creator_id', 'standard', 'date', 'pk_start', 'pk_end', 'station_start', 'station_end')
            ->with([
                'method' => function ($q) {
                    $q->select('id', 'name','type_id','unit')->with(['TestingMethodType'])->get();
                },
                'work' => function ($q) {
                    $q->select('id', 'kh_name as name');
                },
                'creator' => function ($q) {
                    $q->select('id', 'name');
                }
            ])->where('project_id', $projectId)->orderBy('id', 'DESC');
        return [
            "permission"    => [],
            "data"          => new TestingStandardCollection($data->paginate($req->pageSize ?? 10))
        ];
    }

    public function get($projectId = 0, $standardTestId = 0)
    {
        $projectStandardTest = ProjectStandardTest::where([
            'project_id' => $projectId,
            'id'         => $standardTestId
        ])->first();

        if ($projectStandardTest) {
            $data = [
                'id'        => $projectStandardTest->id,
                'test_type' => $projectStandardTest->testTypes ? $projectStandardTest->testTypes->name : null,
                'work_type' => $projectStandardTest->work ? $projectStandardTest->work->kh_name : null,
                'pk_start'  => $projectStandardTest->pk_start,
                'pk_end'    => $projectStandardTest->pk_end,
                'standard'  => $projectStandardTest->standard,
                'creator'   => $projectStandardTest->creator ? $projectStandardTest->creator->name : null,
                'date'      => $projectStandardTest->date ? date('Y-m-d', strtotime($projectStandardTest->date)) : null,

            ];
        } else {
            return responseError('មិនមានទិន្ន័យ');
        }
        return [
            'permission' => [],
            'data'       => $data
        ];
    }

    public static function create(Request $request, $projectId)
    {

        $validator = Validator::make($request->all(), [
            "testing_id"  => "required",
            "type_id" => "required",

        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {
                $standardTest = ProjectStandardTest::where([
                    'project_id'         => $projectId,
                    'pk_start'           => $request->pk_start,
                    'pk_end'             => $request->pk_end,
                    'station_start'      => $request->station_start,
                    'station_end'        => $request->station_end,
                    'work_id'            => $request->type_id,
                    'testing_method_id'  => $request->testing_id,
                ])->first();

                if(! $standardTest){

                    $projectStandard = new ProjectStandardTest();
                    $projectStandard->project_id          = $projectId;
                    $projectStandard->testing_method_id   = $request->testing_id;
                    $projectStandard->work_id             = $request->type_id;
                    $projectStandard->standard            = $request->standard;
                    $projectStandard->creator_id          = auth()->id();
                    $projectStandard->date                = Carbon::now();
                    $projectStandard->pk_start            = $request->pk_start;
                    $projectStandard->pk_end              = $request->pk_end;
                    $projectStandard->station_start       = $request->station_start;
                    $projectStandard->station_end         = $request->station_end;
                    $projectStandard->save();

                  DB::commit();
                $data = [
                    'id'            => $projectStandard->id,
                    'method'     => [
                        'id'                  => $projectStandard->method ? $projectStandard->method->id : null,
                        'name'                => $projectStandard->method ? $projectStandard->method->name : null,
                        'unit'                => $projectStandard->method ? $projectStandard->method->unit : null,
                        'testing_method_type' => TestingMethodType::select('id','name')->find($projectStandard->method->type_id)
                    ],
                    'work_type'     => [
                        'id'        => $projectStandard->work ? $projectStandard->work->id : null,
                        'name'      => $projectStandard->work ? $projectStandard->work->en_name : null
                    ],
                    'pk_start'      => $projectStandard->pk_start,
                    'pk_end'        => $projectStandard->pk_end,
                    'station_start' => $projectStandard->station_start,
                    'station_end'   => $projectStandard->station_end,
                    'standard'      => $projectStandard->standard,
                    'creator'       => $projectStandard->creator ? $projectStandard->creator->name : null,
                    'date'          => $projectStandard->date ? date('Y-m-d', strtotime($projectStandard->date)) : null,
                    ];
                    return responseSuccess("បានបង្កើតស្តង់ដាតេស្ត", $data);

                }else{
                    return responseError('ស្តង់ដាតេស្តមានរួចហើយ',208);
                }
            } catch (\Exception $exception) {

                return responseError($exception);
            }
        }
    }

    public function update(Request $request, $projectId, $standardId = 0)
    {

        $validator = Validator::make($request->all(), [
            "testing_id"  => "required",
            "type_id" => "required"
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {
                $messageStart='';
                $messageEnd='';
                $betweenStartEnd='';
                $canCreate=true;
                $message='';

                $standardTest = ProjectStandardTest::where([
                  'project_id'         => $projectId,
                  'pk_start'           => $request->pk_start,
                  'pk_end'             => $request->pk_end,
                  'station_start'      => $request->station_start,
                  'station_end'        => $request->station_end,
                  'work_id'            => $request->type_id,
                  'testing_method_id'  => $request->testing_id,
                  'standard'           => $request->standard
              ])->first();
               if(!$standardTest){
                $projectStandard = ProjectStandardTest::where([
                  'project_id' => $projectId,
                  'id'         => $standardId
              ])->first();
              //
              if( $projectStandard->work_id === $request->type_id &&  $projectStandard->testing_method_id === $request->testing_id){

                  if($request->station_start < $projectStandard->station_start && $request->station_end == $projectStandard->station_end){
                    $Tests = ProjectStandardTest::select('station_start','station_end')->where([
                      'project_id'         => $projectId,
                      'work_id'            => $request->type_id,
                      'testing_method_id'  => $request->testing_id,
                      ])
                      ->where('station_end','<=',$projectStandard->station_start)
                      ->take(2)->get();
                      foreach( $Tests as $t){

                          if(intval($t->station_end) > intval($request->station_start) ){

                            $messageStart.=' '.$t->station_start.'-'. $t->station_end;

                            $canCreate=false;
                            break;
                          }
                        }
                  }
                  if($request->station_start == $projectStandard->station_start && $request->station_end > $projectStandard->station_end){

                    $Tests = ProjectStandardTest::select('station_start','station_end')->where([
                      'project_id'         => $projectId,
                      'work_id'            => $request->type_id,
                      'testing_method_id'  => $request->testing_id,
                      ])
                      ->where('station_start' , '>=' , intval($projectStandard->station_end))
                      ->take(2)->get();
                      foreach( $Tests as $t){

                        if(intval($request->station_end) > intval($t->station_start)  ){

                          $messageStart.=' '.$t->station_start.'-'. $t->station_end;

                          $canCreate=false;
                          break;
                        }
                      }
                  }
                  if($request->station_start < $projectStandard->station_start && $request->station_end > $projectStandard->station_end){
                    // start
                    $Teststart = ProjectStandardTest::select('station_start','station_end')->where([
                      'project_id'         => $projectId,
                      'work_id'            => $request->type_id,
                      'testing_method_id'  => $request->testing_id,
                      ])
                      ->where('station_end','<=',$projectStandard->station_start)
                      ->take(2)->get();
                      foreach( $Teststart as $t){

                          if(intval($t->station_end) > intval($request->station_start) ){

                            $messageStart.=' '.$t->station_start.'-'. $t->station_end;

                            $canCreate=false;
                            break;
                          }
                        }
                    // end
                      $TestEnd = ProjectStandardTest::select('station_start','station_end')->where([
                          'project_id'         => $projectId,
                          'work_id'            => $request->type_id,
                          'testing_method_id'  => $request->testing_id,
                          ])
                          ->where('station_start' , '>=' , intval($projectStandard->station_end))
                          ->take(2)->get();
                          foreach( $TestEnd as $t){

                            if(intval($request->station_end) > intval($t->station_start)  ){

                              $messageEnd.=' '.$t->station_start.'-'. $t->station_end;

                              $canCreate=false;
                              break;
                            }
                          }
                  }
              }else{
                $check=self::checkStation($request,$projectId);
                if(! $check[0]){
                  return responseError([$check[1]],208);
                }
              }

              if ($projectStandard) {

                $projectStandard->testing_method_id   = $request->testing_id;
                $projectStandard->work_id             = $request->type_id;
                $projectStandard->standard            = $request->standard;
                $projectStandard->creator_id          = auth()->id();
                $projectStandard->date                = Carbon::now();
                $projectStandard->pk_start            = $request->pk_start;
                $projectStandard->pk_end              = $request->pk_end;
                $projectStandard->station_start       = $request->station_start;
                $projectStandard->station_end         = $request->station_end;

                $projectStandard->save();
                DB::commit();
                $data = [
                    'id' => $projectStandard->id,
                    'method'     => $this->testType($projectStandard->method),
                    'work_type'     => $this->workType($projectStandard->work),
                    'pk_start'      => $projectStandard->pk_start,
                    'pk_end'        => $projectStandard->pk_end,
                    'station_start' => $projectStandard->station_start,
                    'station_end'   => $projectStandard->station_end,
                    'standard'      => $projectStandard->standard,
                    'creator'       => $projectStandard->creator ? $projectStandard->creator->name : null,
                    'date'          => $projectStandard->date ? date('Y-m-d', strtotime($projectStandard->date)) : null,
                ];
                return responseSuccess("បានកែប្រែស្តង់ដាតេស្ត", $data);
            } else {
                return responseError("មិនមានទិន្ន័យ",200);
            }
               }else{
                if( $standardTest->standard != $request->standard ){
                  return responseError('ស្តង់ដាតេស្តមានរួចហើយ',208);
                }else{
                  return responseError("ទិន្ន័យមិនបានផ្លាស់ប្តូរ",200);
                }
               }


            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function delete($projectId = 0, $standardId = 0)
    {

        $projectStandard = ProjectStandardTest::where([
            "project_id" => $projectId,
            "id"         => $standardId
        ])->first();

        if ($projectStandard) {
            $projectStandard->delete();
            return responseSuccess('ទិន្ន័យបានលុប');
        } else {
            return responseError("មិនមានទិន្ន័យ");
        }
    }

    static public function workType($workType)
    {
        $worksType = [
            'id' => $workType->id,
            'name' => $workType->en_name
        ];
        return $worksType;
    }

    static public function testType($testType)
    {
        $testType = [
            'id'    => $testType->id,
            'name'  => $testType->name,
            'unit'  => $testType->unit,
            'testing_method_type' => TestingMethodType::select('id','name')->find($testType->type_id)
        ];
        return $testType;
    }

    public function  sortDataSetupTestingStandards(Request $req,$projectId = 0,$sortBy = ''){

      $data  = ProjectStandardTest::select('id', 'project_id', 'testing_method_id', 'work_id', 'creator_id', 'standard', 'date', 'pk_start', 'pk_end', 'station_start', 'station_end')
      ->with([
          'method' => function ($q) {
              $q->select('id', 'name','type_id','unit')->with(['TestingMethodType']);
          },
          'work' => function ($q) {
              $q->select('id', 'kh_name as name');
          },
          'creator' => function ($q) {
              $q->select('id', 'name');
          }
      ])
      ->where('project_id',$projectId);

      if(($req->workid != null || $req->workid != '') && ($req->methodid == null || $req->methodid == '')){

        $data = $data->where('work_id',$req->workid)->orderBy('station_end', 'DESC');

      }else if( ($req->workid == null || $req->workid == '') && ($req->methodid != null || $req->methodid != '')){

        $data = $data->where('testing_method_id',$req->methodid)->orderBy('station_end', 'DESC');

      }else if(($req->workid != null || $req->workid != '') && ($req->methodid != null || $req->methodid != '')){

        $data = $data->where('testing_method_id',$req->methodid)->where('work_id',$req->workid)->orderBy('station_end', 'DESC');

      }else{

        $data = $data->orderBy('station_end', 'DESC');

      }
      return [
          "permission"    => [],
          "data"          => new TestingStandardCollection($data->paginate($req->pageSize ?? 10))
      ];
    }

    public static function checkStation($request,$projectId){

        $messageStart='';
        $messageEnd='';
        $betweenStartEnd='';
        $canCreate=true;
        $message='';

        DB::beginTransaction();
        $Tests = ProjectStandardTest::select('station_start','station_end')->where([
          'project_id'         => $projectId,
          'work_id'            => $request->type_id,
          'testing_method_id'  => $request->testing_id,
          ])
          ->get();

          foreach( $Tests as $t){

            if($t->station_start <= $request->station_start &&  $request->station_start < $t->station_end){

              $messageStart.=' '.$t->station_start.'-'. $t->station_end;

              $canCreate=false;

            }
            if($t->station_start < $request->station_end &&  $request->station_end <= $t->station_end){

              $messageEnd.=' '.$t->station_start.'-'. $t->station_end;

              $canCreate=false;

            }
            if($t->station_start>$request->station_start && $t->station_end < $request->station_end ){

              $betweenStartEnd.=' '.$t->station_start.'-'. $t->station_end;

              $canCreate=false;

            }
          }
          if($messageStart !='' || $messageEnd != ''){

              if($messageStart === $messageEnd ){

                  $message="ចន្លោះ ".$messageStart;
              }else{

                  if($messageStart !='' && $messageEnd == ''){

                      $message="ចន្លោះ ".$messageStart;

                  }elseif($messageStart == '' && $messageEnd != ''){

                      $message="ចន្លោះ ".$messageEnd;

                  }else{

                      $message="ចន្លោះ ".$messageStart.' ហើយ '.$messageEnd;
                  }
              }
          }
          if(! $canCreate){
            $messages=self::generateMessage( $message,$betweenStartEnd,$request);
        }else{
          $messages='';
        }
        return [$canCreate,$messages];
    }

    public static function generateMessage($message,$betweenStartEnd,$request){
      $work=Work::select('kh_name')->where('id' ,$request->type_id)->first();

      $method=TestingMethod::select('name')->where('id' , $request->testing_id)->first();

      if($betweenStartEnd != ''){

        $messages="ចន្លោះ ".$betweenStartEnd.' ស្រទាប់ '.$work->kh_name.' វិធីសាស្ត្រតេស្ត '.$method->name.' '.' មានរួចហើយ!';

      }else{

        $messages=$message.' ស្រទាប់ '.$work->kh_name.' វិធីសាស្ត្រតេស្ត '.$method->name.' '.' មានរួចហើយ!';

      }
      return $messages;
    }
}
