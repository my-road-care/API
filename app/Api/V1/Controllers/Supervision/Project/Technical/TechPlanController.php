<?php

namespace App\Api\V1\Controllers\Supervision\Project\Technical;


// ===============>> Service <<===============
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Http\Collections\Project\ProjectPlanCollection;
use App\Api\V1\Service\FileServers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

// ===========>> Model <<============
use App\Model\Supervision\Projects\ProjectPlan;

class TechPlanController extends ApiController
{ 
    use FileServers;

    public function listing(Request $request ,$projectId = null){

        $projectPlans           = ProjectPlan::where('project_id', $projectId)->orderBy('id','DESC');
        return [
            "permission" => [],
            "data"       => new ProjectPlanCollection($projectPlans->paginate($request->per_page ?? 10))
        ]; 
    }

    public static function index($projectId = null){

        // ===========>> Get Plan <<============
        $projectPlans = ProjectPlan::with('creator')->where('project_id', $projectId)->get();
        
        return [
            'permission' => [
                'edit' => 1, 
                'add' => 0,
                'delete' => 1
            ], 
            'data' => new ProjectPlanCollection($projectPlans)
        ];
    }    

    public static function get($projectId = null, $id = 0){

        // =============>> Get Plan <<================
        $projectPlan = ProjectPlan::with('creator')->where([
            'project_id' => $projectId,
            'id'         => $id
        ])->first();

        if($projectPlan){
            return [
                'permission' => [
                    'edit' => 1, 
                    'add' => 0,
                    'delete' => 1
                ], 
                'data' => [
                    "id"                => $projectPlan -> id,
                    "creator"           => $projectPlan -> creator ? $projectPlan -> creator -> name : null,
                    "title"             => $projectPlan -> title,
                    "docu_code"         => $projectPlan -> docu_code,
                    "image_uri"         => $projectPlan -> file_uri,
                    "description"       => $projectPlan -> description
                ]
            ];
        }else{
            return responseError("មិនមានទិន្ន័យ");
        }        
    }   

    public function create(Request $request, $projectId = 0){
        
        $data = new ProjectPlan();

        $validator = Validator::make($request->all(),[
            "title" => "required"
        ]);
        
        if ($validator->fails()) {
            return responseError($validator -> errors(),422);
        }else{
            try{
                DB::beginTransaction();

                $data = new ProjectPlan();

                $data -> project_id  = $projectId;
                $data -> title       = $request -> title;
                $data -> docu_code   = $request -> docu_code;
                $data -> description = $request -> description;
                $data -> creator_id  = auth()->id();
                $data -> date        = Carbon::now() ;

                // =============>> Store Image to File Server <<==============
                $file = $this->fileServer($request->file, $request->image_uri);
                if($request->file){
                    $uri = $file['uuid'];
                }else{
                    $uri = $file['fileURL']['uri'];
                }
                $data -> image_uri = $uri;
                $data -> save();
                DB::commit();

                $projectPlan = [
                    "id"                => $data -> id,
                    "creator"           => $data -> creator,
                    "title"             => $data -> title,
                    "docu_code"         => $data -> docu_code,
                    "image_uri"         => $data -> image_uri,
                    "date"              => $data -> date ? date('Y-m-d', strtotime($data->date)) : null,
                    "description"       => $data -> description
                ];
                return responseSuccess("គម្រោងបានបង្កើត", $projectPlan);                
                
            }catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function update(Request $request, $projectId = 0, $id = 0){
        

        $validator = Validator::make($request->all(),[
            "title" => "required"
        ]);
        
        if ($validator->fails()) {
            return responseError($validator -> errors(),422);
        }else{
            try{
                DB::beginTransaction();

                $data = ProjectPlan::where([
                    'project_id' => $projectId,
                    'id'         => $id
                ])->first();

                if($data){
                    $data -> title      = $request -> title;
                    $data -> docu_code  = $request -> docu_code;
                    $data -> description = $request -> description;
                    $data -> creator_id = auth()->id();
    
                    // Store Image to File Server
                    if($request->file != null || $request->image_uri != null){

                    $file = $this->fileServer($request->file, $request->image_uri);
                        if($request->file){
                            $uri = $file['uuid'];
                        }else{
                            $uri = $file['fileURL']['uri'];
                        }
                        $data -> image_uri = $uri;
                    }
    
                    $data -> save();
                    DB::commit();
    
                    $projectPlan = [
                        "id"                => $data -> id,
                        "creator"           => $data -> creator,
                        "title"             => $data -> title,
                        "docu_code"         => $data -> docu_code,
                        "image_uri"         => $data -> image_uri,
                        "description"       => $data -> description,
                        "date"              => $data -> date ? date('Y-m-d', strtotime($data->date)) : null,

                    ];
                    return responseSuccess("ទិន្ន័យបានកែប្រែ", $projectPlan);
                }else{
                    return responseSuccess("មិនមានទិន្ន័យ");
                }
                
                
                
            }catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function delete($projectId = 0, $planId = 0){
        
        $data = ProjectPlan::where([
            "project_id" => $projectId,
            "id"         => $planId
        ])->first();

        if($data){
            $data -> delete();
            return responseSuccess('បានលុបទិន្ន័យ');
        }else{
            return responseError('មិនមានទិន្ន័យ');
        }
    }
}