<?php

namespace App\Api\V1\Controllers\Supervision\Project\Technical;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

// =================>> Service <<=================
use Carbon\Carbon;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Service\FileServers;
use App\Enum\Progress;
use App\Jobs\Thread;

// =================>> Model <<===============
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProjectImage;
use App\Model\Supervision\Projects\StationWork;
use App\Model\Supervision\Projects\StationWorkSide;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Projects\StationWorkSideHistory;
use App\Model\Supervision\Projects\StationWorkSideTest;
use App\Model\Supervision\Projects\StationWorkSideTestParam;
use App\Model\Supervision\Setup\ProgressStatus;
use App\Model\Supervision\Setup\Side;
use App\Model\Supervision\Setup\TestingMethod;

class TechSideController extends ApiController
{
    use FileServers;

    public function getSide(){
        $data = Side::select('id', 'name', 'abrv')->get();
        return $data;
    }

    public function getStatus() {
        $data = ProgressStatus::select('id', 'kh_name', 'en_name', 'color')->get();
        return $data;
    }

    public function preview($projectId = 0, $sideId = 0){

        $side = StationWorkSide::where('id', $sideId)->first();
        $station = StationWork::where('id', $side->station_work_id)->first();
        $stationId = $station->station_id;

        $query = StationWorkSide::WhereHas('station', function($query) use($stationId){
            $query->whereHas('station',  function($query) use($stationId){
                $query->where('id', $stationId);
            });
        });
        $data = StationWorkSide::with([

            'side',
            'station', 
            'station.work',
            'station.work.type',
            'station.station',
            'station.station.point',
            'station.station.point.project',
            'status', 
            'histories',
            'histories.progress',
            'histories.updater',
            'histories.images',
            'testingRecords',
            'testingRecords.tester',
            'testingRecords.images',
            'testingRecords.standardTest.method.place'

        ])
        ->find($sideId);

        

        $currentProgress = $data->status; 
        $nextProgress    = ProgressStatus::select('id', 'kh_name as name', 'color')->find($data->status->id + 1);
        return [

            'project_code'  => $data->station->station->point->project->code,
            'point'         => 'PK'.$data->station->station->point->pk.' + '.$data->station->station->station_num . " (" . $data->station->station->station_num. ")", 
            'work'          => $data->station->work->name, 
            'type'          => $data->station->work->type->name, 
            'side'          => $data->side,

            'setup' => [
                'sides'         => $this->getSide(), 
                'statues'       => $this->getStatus()
            ],

            'progress' => [
                'current_progress'           => $currentProgress, 
                'next_progress'              => $nextProgress, 
                'histories'                  => $data->histories,
            ], 

            'quality_check' => [
                'can_update'            =>  $data->status->id == Progress::Finished || $data->status->id == Progress::Finished ? 'YES' : 'NO', 
                'standards'             =>  $this->getTestingStandard($data->station->station->point->project->id, $data->station->station->point->pk, $data->station->station->station_num, $data->station->work->id),
                'testing_records'       =>  $data->testingRecords ? $this->testRecord($data->testingRecords) : null,
            ],

        ];
    }    

    private function testRecord($testRecords){
        $response = [];
        foreach($testRecords as $row){
            if($row->standardTest->method->less_than_pass == 1){
                $result = $row->result <= $row->standardTest->standard ? 1 : 0;
            }else{
                $result = $row->result >= $row->standardTest->standard ? 1 : 0;
            }
            $response[] =  [
                "id"                    => $row->id,
                "station_work_side_id"  => $row->station_work_side_id,
                "standard_test_id"      => $row->standard_test_id,
                "comment"               => $row->comment,
                "created_at"            => $row->created_at ? date('Y-m-d', strtotime($row->created_at)) : null,
                "tester"                => $row->tester,
                "result"                => $row->result,
                "standard_test"         => [
                    "id"                 => $row->standardTest ? $row->standardTest->id : null,
                    "testing_method_id"  => $row->standardTest ? $row->standardTest->testing_method_id : null, 
                    "standard"           => $row->standardTest ? $row->standardTest->standard : null,
                    "method"             => $row->standardTest ? $row->standardTest ? $this->parameterFormat($row->standardTest, $row->id) : null : null
                ],
                "images"                => $row->images,
                "is_pass"               => $result
            ];
        }
        return $response;
    }

    public function getTestingStandard($projectId = 0, $pk = 0, $stationNum = 0, $workId = 0){
        $methods = TestingMethod::select('id', 'name', 'type_id')
        ->with([
            'place',
            'parameters' => function ($q){
                $q->select('id','testing_method_id','name','unit');
            }
        ])
        ->get();
        
        $testingStandards = ProjectStandardTest::with([
            'work',
            'method', 
            
        ])
        ->where([
            'work_id' => $workId,
            'project_id' => $projectId
        ])
        ->get(); 

        $data = []; 
        
        foreach($methods as $method){
            foreach($testingStandards as $testingStandard){
                if($method->id == $testingStandard->method->id){
                    if(intval($testingStandard->pk_start) <= intval($pk) 
                       && intval($pk) <= intval($testingStandard->pk_end)
                       && intval($stationNum) >= intval($testingStandard->station_start)
                       && intval($stationNum) <= intval($testingStandard->station_end)){
                        $data[] = [
                            'standard_test_id'        => $testingStandard->id,
                            'method'                  => [
                                'id'   => $method->id,
                                'name' => $method->name
                            ], 
                            'parameters'              => $method->parameters,
                            'place'                   => $method->place,
                            'requirment'              => $testingStandard->standard,
                            'unit'                    => $testingStandard->method ? $testingStandard->method->unit : null,
                            'less_than_pass'          => $testingStandard->method ? $testingStandard->method->less_than_pass : null

                        ];
                    }elseif(intval($testingStandard->pk_start) <= intval($pk) 
                       && intval($pk) <= intval($testingStandard->pk_end)
                       && intval($testingStandard->station_start) == 0
                       && intval($testingStandard->station_end) == 0){
                        $data[] = [
                            'standard_test_id'        => $testingStandard->id,
                            'method'                  => [
                                'id'   => $method->id,
                                'name' => $method->name
                            ], 
                            'parameters'              => $method->parameters,
                            'place'                   => $method->place,
                            'requirment'              => $testingStandard->standard,
                            'unit'                    => $testingStandard->method ? $testingStandard->method->unit : null,
                            'less_than_pass'          => $testingStandard->method ? $testingStandard->method->less_than_pass : null                            
                        ];
                    }
                }
            }
        }

        return $data; 
    }

    public function updateProgress(Request $request, $projectId = 0, $sideId = 0){

        $validator = Validator::make($request->all(),[
            "status_id" => "required"
        ]);
        
        if ($validator->fails()) {
            return responseError($validator -> errors(),422);
        }else{
            try{
                DB::beginTransaction();

                $data = StationWorkSide::where('id',$sideId)->first();

                if($data){

                    if($data->progress_status_id != $request -> status_id){

                        //=========>> Update Updated_at for Offline Mode <<=========
                        if($request->created_at){
                            $createdAt = $request->created_at;
                        }else{
                            $createdAt = Carbon::now()->format('Y-m-d H:i:s');
                        }
                        $project = Project::where('id', $projectId)->first();
                        $project->update([
                            'updated_at' => $createdAt
                        ]);
                        $data -> progress_status_id         = $request -> status_id;

                        $data -> save();

                        // ===========>> Add Status History <<=============
                        $workSideHistories = StationWorkSideHistory::where([
                            'station_work_side_id' => $sideId,
                            'progress_status_id'   => $request -> status_id
                        ])->first();

                        if(!$workSideHistories){

                            $history                        = new StationWorkSideHistory(); 
                            $history->station_work_side_id  = $data->id; 
                            $history->progress_status_id    = $data->progress_status_id; 
                            $history->comment               = $request->message; 
                            $history->created_at            = Date('Y-m-d H:i:s');
                            $history->updater_id            = auth()->id();                            
                            $history->save();

                            // =========>> Store Image to File Server <<==============
                            $images = json_decode($request->images);
                            $imgs = [];
                            if($images){
                                foreach($images as $row){
                                    $size_in_bytes = (int) (strlen(rtrim($row->img, '=')) * 3 / 4);
                                    $size_in_kb    = $size_in_bytes / 1024;
                                        $file = $this->fileServer('', $row->img);
                                        $imgs[] = [
                                            'project_id'                   => $projectId,
                                            'station_work_side_history_id' => $history->id,
                                            'image_uri'                    => $file['fileURL']['uri'],
                                            'lat'                          => $row->lat,
                                            'lng'                          => $row->lng
                                        ];                           
                                }                                                 
                            }
                            
                            // ==========>> Store Image <<=========
                            ProjectImage::insert($imgs);

                            DB::commit();

                            $history = StationWorkSideHistory::with([
                                'progress', 
                                'updater',
                                'images'
                            ])
                            ->find($history->id);

                        }else{
                            return responseError("ទិន្ន័យមានរួចហើយ");
                        }                           

                        dispatch(new Thread("work-diagram-cache:update $projectId"));

                        return responseSuccess("ទិន្ន័យបានកែប្រែ", $history);

                    }else{
                        return responseError("ទិន្ន័យមានរួចហើយ");
                    }
                }else{
                    return responseSuccess("មិនមានទិន្ន័យ");
                }
            }catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }

    }

    public function listTestingStandard($projectId = 0){
        $data  = ProjectStandardTest::select('id', 'project_id', 'testing_method_id', 'work_id', 'creator_id', 'standard', 'date', 'pk_start', 'pk_end', 'station_start', 'station_end')
            ->with([
                'method' => function ($q) {
                    $q->select('id', 'name','type_id','unit')->with(['TestingMethodType','parameters' => function($q){
                        $q->select('id','testing_method_id','name','unit');
                    }])->get();
                },
                'work' => function ($q) {
                    $q->select('id', 'kh_name as name');
                },
                'creator' => function ($q) {
                    $q->select('id', 'name');
                }
            ])->where('project_id', $projectId)->orderBy('id', 'DESC')->get();

        $standardTests = [];

        foreach ($data as $row) {
            $standardTests[] = [
                "id"            => $row->id,
                "method"        => $row->method ? $row->method : null,
                "work_type"     => $row->work ? $row->work : null,
                "standard"      => $row->standard,
                "date"          => $row->date ? date('Y-m-d', strtotime($row->date)) : null,
                "creator"       => $row->creator ? $row->creator->name : null,
                "pk_start"      => $row->pk_start,
                "pk_end"        => $row->pk_end,
                "station_start" => $row->station_start,
                "station_end"   => $row->station_end,
            ];
        }

        return responseSuccess("success",$standardTests);
    }

    public function addQualityTest(Request $request, $projectId = 0, $sideId = 0){
       
        $validator = Validator::make($request->all(),[
            "standard_test_id"      => "required", 
            "result"                => "required"
        ]);
        
        if ($validator->fails()) {
            return responseError($validator -> errors(),422);
        }else{
            try{
                DB::beginTransaction();

                $data = StationWorkSide::find($sideId);
                if($data){

                    // Update Updated_at for Offline Mode
                    if($request->created_at){
                        $createdAt = $request->created_at;
                    }else{
                        $createdAt = Carbon::now()->format('Y-m-d H:i:s');
                    }
                    $project = Project::where('id', $projectId)->first();
                    $project->update([
                        'updated_at' => $createdAt
                    ]);

                    // ===> Add Test

                    $test                           = new StationWorkSideTest(); 
                    $test->station_work_side_id     = $data->id; 
                    $test->standard_test_id         = $request->standard_test_id; 
                    $test->result                   = $request->result; 
                    $test->comment                  = $request->message; 
                    $test->tester_id                = auth()->id();
                    $test->save();

                    if($request->parameters){
                        $parameters = json_decode($request->parameters);
                        $paramData = []; 

                        foreach($parameters as $parameter){
                            $paramData[] = [
                                'station_work_side_test_id' => $test->id, 
                                'standard_test_id'          => $request->standard_test_id, 
                                'parameter_id'              => $parameter->parameter_id, 
                                'value'                     => $parameter->value
                            ]; 
                        }
                        
                        if(count($paramData) > 0){
                            StationWorkSideTestParam::insert($paramData);
                        }
                    }
                    
                    // ===> Store Image to File Server
                    if($request->images){
                        $images = json_decode($request->images);
                        $imgs = [];
                        foreach($images as $row){
                            $size_in_bytes = (int) (strlen(rtrim($row->img, '=')) * 3 / 4);
                            $size_in_kb    = $size_in_bytes / 1024;
                            if($size_in_kb <= 4000){
                                $file = $this->fileServer('', $row->img);
                                $imgs[] = [
                                    'project_id'                   => $projectId,
                                    'station_work_side_test_id'    => $test->id,
                                    'image_uri'                    => $file['fileURL']['uri'],
                                    'lat'                          => $row->lat,
                                    'lng'                          => $row->lng
                                ];
                            }                             
                        }     
                        // ===> Store Image
                        ProjectImage::insert($imgs);                                            
                    }

                    // ===> Store PDF to File Server
                    if($request->file){
                        $file = $this->fileServer($request->file, '');
                        if ($request->file) {
                            $uri = $file['uuid'];
                        } else {
                            $uri = $file['fileURL']['uri'];
                        }
                        $files = [
                            'project_id'                   => $projectId,
                            'station_work_side_test_id'    => $test->id,
                            'image_uri'                    => $uri
                        ];     
                        
                        // ===> Store Image
                        ProjectImage::insert($files); 
                    }
                    
                    
                    
                    DB::commit();

                    $data = StationWorkSideTest::with([
                        'standardTest.method.place', 
                        'tester',
                        'images'
                    ])
                    ->find($test->id);
                    if($data->standardTest->method->less_than_pass == 1){
                        $result = $data->result <= $data->standardTest->standard ? 1 : 0;
                    }else{
                        $result = $data->result >= $data->standardTest->standard ? 1 : 0;
                    }

                    $res = [
                        "id"                    => $data->id,
                        "station_work_side_id"  => $data->station_work_side_id,
                        "standard_test_id"      => $data->standard_test_id,
                        "comment"               => $data->comment,
                        "created_at"            => $data->created_at ? date('Y-m-d', strtotime($data->created_at)) : null,
                        "tester"                => $data->tester,
                        "result"                => $data->result,
                        "standard_test"         => [
                            "id"                 => $data->standardTest->id,
                            "testing_method_id"  => $data->standardTest->testing_method_id,
                            "standard"           => $data->standardTest->standard,
                            "method"             => $data->standardTest ? $this->parameterFormat($data->standardTest, $data->id) : null,

                        ],
                        "images"                => $data->images,
                        "is_pass"               => $result
                    ];

                    return responseSuccess("Test has been added", $res);

                }else{
                    return responseSuccess("មិនមានទិន្ន័យ");
                }
            }catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }

    }

    private function parameterFormat($parameters, $id = 0){
        $param = [];
        foreach($parameters->method ? $parameters->method->parameters : null as $row){
            $param[] = [
                'id' => $row->id,
                'name' => $row->name,
                'value' => $row->param ? $this->value($row->param, $id) : null
            ];
        }
        $resp = [
            'id'            => $parameters->method ? $parameters->method->id : null,
            'name'          => $parameters->method ? $parameters->method->name : null,
            'parameters'    => $param,
            "place"         => $parameters->method ? $parameters->method->place  : null

        ];
        return $resp;
    }

    private function value($values, $id = 0){
        $res = '';
        foreach($values->where('station_work_side_test_id', $id) as $data){
            $res = $data->value;
        }
        return $res;
    }

    public function deleteHistory($projectId = 0, $id = 0){

        $history = StationWorkSideHistory::where('id', $id)->first();
        $testRecord  = StationWorkSideTest::where('id', $id)->first();

        if($history){
            $history->delete();
            return responseSuccess("ទិន្ន័យបានលុប");
        }elseif($testRecord){
            $testRecord->delete();  
            return responseSuccess("ទិន្ន័យបានលុប");          
        }else{
            return responseError("មិនមានទិន្ន័យ");
        }

    }

    
}
