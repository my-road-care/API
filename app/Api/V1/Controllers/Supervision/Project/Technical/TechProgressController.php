<?php

namespace App\Api\V1\Controllers\Supervision\Project\Technical;

// ================>> Service <<================
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Service\FileServers;
use App\Jobs\Thread;
use Carbon\Carbon;

// =================>> Model <<==================
use App\Model\Supervision\Projects\ProjectImage;
use App\Model\Supervision\Projects\ProgressTRX;
use App\Model\Supervision\Projects\Station;
use App\Model\Supervision\Projects\StationWork;
use App\Model\Supervision\Projects\StationWorkSide;
use App\Model\Supervision\Projects\Transaction;
use App\Model\Supervision\Projects\StationWorkSideHistory;
use App\Model\Supervision\Setup\Work;
use App\Model\Supervision\Setup\ProgressStatus;
use App\Model\Supervision\Setup\Side;

class TechProgressController extends ApiController
{
    use FileServers;

    public function getPks($projectId = 0){

        $pks = Transaction::where('project_id', $projectId)->select('id','pk')->get();

        return [
            'pks' => $pks
        ];

    }
    
    public function progressInfo($projectId = 0, $pkId = 0){

        // =========> Gets Station <==========
        $stations   = Station::select('id','station_num')->whereHas('point', function ($query) use ($projectId, $pkId) {
            $query->where([
                'project_id'     => $projectId,
                'transaction_id' => $pkId
            ]);
        })->get();

        // ========> Get Station ID <=========
        $staionIds = [];
        foreach ($stations as $stationId) {
            $staionIds[] = $stationId->id;
        }

        // =========> Find Works that related to station <=========
        $works = Work::select('id', 'kh_name', 'en_name')->whereHas('stationWorks', function ($query) use ($staionIds) {
            $query->whereIn('station_id', $staionIds);
        })->orderBy('group_id', 'DESC')->get();

        // =========> Get Side <=========
        $sides = Side::select('id', 'name', 'abrv')->get();

        return [
            "sides"   => $sides,
            "works"   => $works,
            "station" => $stations
        ];
    }

    public function progressCheck(Request $request, $projectId = 0){

        // =======> Get Station Work <==========
        $stationWorks = StationWork::whereHas('station', function($query) use($request){
            $query->whereBetween('id',[$request->station_start, $request->station_end]);
        })->whereHas('sides', function($query) use($request){
            $query->where('side_id', $request->side_id)->where('progress_status_id','<',4);
        })
        ->with([
            'station.point',
            'sides.status',
            'sides' => function($query) use($request){
                $query->where('side_id', $request->side_id);
            }
        ])->where('work_id', $request->work_id)->get();

        $data = null;
        $stations = [];
        foreach($stationWorks as $stationWork){
            $pk = $stationWork->station->point->pk;
            $stationNum = $stationWork->station ? $stationWork->station->station_num : null;
            $stations[] = [
                'id'      => $stationWork->id,
                'station' => "PK ".$pk."+".sprintf("%03s", $stationNum),
                'now'     => $this->now($stationWork->sides),
                'next'    => $this->next($stationWork->sides)
            ];

            $data = [
                'pk' => $stationWork->station ? $stationWork->station->point ? $stationWork->station->point->pk : null : null,
                'stations' => $stations
            ];
        }

        return $data;
    }

    public function progressUpdate(Request $request ,$projectId = 0){

        $data = json_decode($request->data);
        if($data){

            try {

                DB::beginTransaction();
                $progressID = 0;

                $progressTrx                = new ProgressTRX();
                $progressTrx->tester_id     = auth()->id();
                $progressTrx->project_id    = $projectId;
                $progressTrx->pk_id         = $request->pk_id; 
                $progressTrx->created_at    = Carbon::now();
                $progressTrx->station_start = $request->station_start;
                $progressTrx->station_end   = $request->station_end;
                $progressTrx->comment       = $request->message;
                $progressTrx->side_id       = $request->side_id;
                $progressTrx->work_id       = $request->work_id;
                $progressTrx->save();

                foreach($data as $row){

                    // ========> Query Station Work Side <===========
                    $stationWorkSide = StationWorkSide::where([
                        'station_work_id' => $row->station_id,
                        'side_id'         => $request->side_id
                    ])->first();

                    // =========> Check If Prgress Status can update
                    if($stationWorkSide->progress_status_id <= 3){

                        // =========> Update Progress Status <===========
                        $stationWorkSide->update([
                            'progress_status_id' => $stationWorkSide->progress_status_id + 1
                        ]);

                        // ========> Create Progress update record <========
                        $history                        = new StationWorkSideHistory(); 
                        $history->station_work_side_id  = $stationWorkSide->id; 
                        $history->progress_trx_id       = $progressTrx->id;
                        $history->progress_status_id    = $stationWorkSide->progress_status_id; 
                        $history->comment               = $request->message; 
                        $history->created_at            = Date('Y-m-d H:i:s');
                        $history->updater_id            = auth()->id();
                        
                        $history->save();

                        // ========> Store Image to File Server <==========
                        $images = json_decode($request->images);
                        $imgs = [];
                        if($images && $progressID == 0){
                            foreach($images as $row){
                                $size_in_bytes = (int) (strlen(rtrim($row->img, '=')) * 3 / 4);
                                $size_in_kb    = $size_in_bytes / 1024;
                                // if($size_in_kb <= 4000){
                                    $file = $this->fileServer('', $row->img);
                                    $imgs[] = [
                                        'project_id'                   => $projectId,
                                        'station_work_side_history_id' => $history->id,
                                        'progress_trx_id'              => $progressTrx->id,
                                        'image_uri'                    => $file['fileURL']['uri'],
                                        'lat'                          => $row->lat,
                                        'lng'                          => $row->lng
                                    ];
                                // }                             
                            }                                                 
                        }    
                        $progressID = $progressTrx->id;

                        // ======> Store Image <=======
                        ProjectImage::insert($imgs);

                    }             
                }

                DB::commit();

                dispatch(new Thread("work-diagram-cache:update $projectId"));
                dispatch(new Thread("project-statistic:get $projectId"));

                
                return responseSuccess('បានធ្វើវឌ្ឍនៈភាព');

            }catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }else{
            return responseError('មិនមានទិន្ន័យ');
        }
    }

    private function now($sides){
        $now = null;
        foreach($sides as $side){
            $now = $side->status;
            $id[] = $side->id;
        }
        return $now;
    }

    private function next($sides){

        $now = null;
        foreach($sides as $side){
            $id = $side->status ? $side->status->id : null;
            $now = $side->status;
        }

        if($id <= 3){
            $next = ProgressStatus::select('id','kh_name as name','color')->where('id', $id + 1)->first();
        }else{
            $next = $now;
        }

        return $next;
    }
}
