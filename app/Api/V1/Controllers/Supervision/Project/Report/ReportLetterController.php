<?php

namespace App\Api\V1\Controllers\Supervision\Project\Report;


// ===============>> Service <<=================
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Http\Collections\Project\LetterCollection;
use App\Api\V1\Service\FileServers;
use Carbon\Carbon;

// =============>> Model <<===============
use App\Model\Supervision\Projects\LetterType;
use App\Model\Supervision\Projects\ProjectLetter;

class ReportLetterController extends ApiController
{

  use FileServers;

  public function dataSetup(){
    $data = LetterType::select('id','name')->get();
    return $data;
  }
    public function listing(Request $req, $projectId = 0)
    {
         $data=ProjectLetter::with([

          'user'=>function($q){
            $q->select('id','name','avatar')->get();
          },

          'letter_type'=>function($q){

            $q->select('id','name')->get();

          }]);
        if($req->letter_type !=null){
          $data=$data->where([
            'project_id'                    => $projectId,
            'project_intro_letters_type_id' => $req->letter_type
            ])->orderBy('id', 'DESC');
        }else{
          $data=$data->where('project_id', $projectId)->orderBy('id', 'DESC');
        }
          return [
              "permission"    => [],
              "data"         =>new LetterCollection($data->paginate($req->per_page ?? 10))
          ];
    }
    public function create(Request $req,$ProjectId=0){

      $validator = Validator::make($req->all(), [
        "title" => "required",
        "letter_type"   => "required",
      ]);

      if ($validator->fails()) {
          return responseError($validator->errors(), 422);
      } else {
          try{

            DB::beginTransaction();
            $letter=new ProjectLetter();
            $letter->project_id                    = $ProjectId;
            $letter->title                         = $req->title;
            $letter->date                          = Carbon::now();
            $letter->project_intro_letters_type_id = $req->letter_type;
            $letter->creator_id                    = auth()->id();

            // Store Image to File Server
            $image = $this->fileServer($req->file, $req->image);
            if ($req->file) {
                $uri = $image['uuid'];
            } else {
                $uri = $image['fileURL']['uri'];
            }

            $letter->image_uri = $uri;
            $letter->save();

            $res [] = [
              "id"                => $letter -> id,
              "creator"           => $letter -> user ? $letter -> user : null,
              "title"             => $letter -> title,
              "image_uri"         => $letter -> image_uri,
              "letter_type"       => $letter -> letter_type ? $letter -> letter_type: null,
              "date"              => $letter->date ? date('Y-m-d', strtotime($letter->date)) : null
            ];

            DB::commit();
            return responseSuccess("លិខិតបានបង្កើត",$res);
          }catch (\Exception $exception) {
            DB::rollBack();
            return responseError($exception);
        }
      }

    }

    public function preview($projectId = 0, $letterId = 0){
      $letter = ProjectLetter::where([
        'project_id'  => $projectId,
        'id'          => $letterId
      ])->first();

      if($letter){
        $res [] = [
          "id"                => $letter -> id,
          "creator"           => $letter -> user ? $letter -> user : null,
          "title"             => $letter -> title,
          "image_uri"         => $letter -> image_uri,
          "letter_type"       => $letter -> letter_type ? $letter -> letter_type: null,
          "date"              => $letter->date ? date('Y-m-d', strtotime($letter->date)) : null
        ];
        return responseSuccess("មានទិន្ន័យ",$res);
      }else{
        return responseError('មិនមានទិន្ន័យ');
      }
    }

    public function update(Request $req, $projectId = 0, $letterId = 0){
      $validator = Validator::make($req->all(), [
        "title" => "required",
        "letter_type"   => "required"
      ]);

      if ($validator->fails()) {
          return responseError($validator->errors(), 422);
      } else {
          try{
            $letter = ProjectLetter::where([
              'project_id'  => $projectId,
              'id'          => $letterId
            ])->first();

            if($letter){
              DB::beginTransaction();

              $letter->project_id                    = $projectId;
              $letter->title                         = $req->title;
              $letter->date                          = Carbon::now();
              $letter->project_intro_letters_type_id = $req->letter_type;
              $letter->creator_id                    = auth()->id();
  
              // Store Image to File Server
              if ($req->file != null || $req->image != null) {
                $image = $this->fileServer($req->file, $req->image);
                if ($req->file) {
                    $uri = $image['uuid'];
                } else {
                    $uri = $image['fileURL']['uri'];
                }
                $letter->image_uri = $uri;

              }
  
              $letter->save();
  
              $res [] = [
                "id"                => $letter -> id,
                "creator"           => $letter -> user ? $letter -> user : null,
                "title"             => $letter -> title,
                "image_uri"         => $letter -> image_uri,
                "letter_type"       => $letter -> letter_type ? $letter -> letter_type: null,
                "date"              => $letter->date ? date('Y-m-d', strtotime($letter->date)) : null
              ];
  
              DB::commit();
              return responseSuccess("លិខិតបានបង្កើត",$res);
            }else{
              return responseError('មិនមានទិន្ន័យ');
            }            
          }catch (\Exception $exception) {
            DB::rollBack();
            return responseError($exception);
        }
      }
    }

    public function delete($projectId = 0, $letterId = 0){
      $letter = ProjectLetter::where([
        'project_id'  => $projectId,
        'id'          => $letterId
      ])->first();

      if($letter){
        $letter->delete();
        return responseSuccess("លិខិតបានលុប");
      }else{
        return responseError('មិនមានទិន្ន័យ');
      }
    }
}
