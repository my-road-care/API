<?php

namespace App\Api\V1\Controllers\Supervision\Project\Report;


// =================>> Service <<=================
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Http\Collections\Project\ProgressCollection;
use App\Api\V1\Http\Collections\Project\ProjectImageCollection;
use App\Api\V1\Service\FileServers;
use App\Api\V1\Service\Picture;

// =================>> Model <<===============
use App\Model\Supervision\Projects\ProgressTRX;
use App\Model\Supervision\Projects\ProjectProcurement;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Projects\StationWorkSideTest;
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProjectImage;
use App\Model\Supervision\Projects\ProjectPicture;
use App\Model\Supervision\Projects\StationWorkSideHistory;
use App\Model\Supervision\Projects\StationWork;
use App\Model\Supervision\Projects\ProjectProvince;
use App\Model\Supervision\Setup\Work;
use App\Model\Supervision\Projects\Station;


class ReportProgressController extends ApiController
{
    use FileServers, Picture;   

    public function dataSetup(){
        $data = [
            [
                'id'    => 2,
                'name'  => 'មុនពេលសាងសង់'
            ],[
                'id'    => 3,
                'name'  => 'កំពុងសាងសង់'
            ],[
                'id'    => 4,
                'name'  => 'បញ្ចប់ការសាងសង់'
            ],
        ];
        return $data;
    }

    public function listing(Request $request, $projectId = 0)
    {

        if($request->type == "mobile"){
            $query = StationWorkSideHistory::whereHas('side', function($query) use($projectId, $request){
                $query->whereHas('station', function($query) use($projectId, $request){
                    $query->whereHas('station', function($query) use($projectId, $request){
                        $query->whereHas('point', function($query) use($projectId, $request){
                            $query->where('project_id', $projectId);
                            $query->when($request->pk_start || $request->pk_end, function($query) use($request){
                                $query->where('pk', '>=', $request->pk_start)->where('pk', '<=', $request->pk_end);
                            });
                        });
                    });
                });
            })->with([
                'progressTrx.images',
                'progress',
                'side.side',
                'side.station.station.point',
                'side.station.work'
            ])->whereIn('progress_status_id', [2,3,4])->orderBy('id', 'DESC');
    
            $query->when($request->picture_type, function ($query) use ($request) {
                $query->where('progress_status_id',  $request->picture_type);
            });
    
            $data = new ProjectImageCollection($query->paginate($request->per_page ?? 10));

            return $data;

        }else{

            $progressTrx = ProgressTRX::with([
                'tester',
                'pk',
                'images',
                'work',
                'side'
            ])->where('project_id', $projectId)->orderBy('id','DESC');

            $progressTrx->when($request->pk_start && $request->pk_end, function($query) use($request){
                $query->where('pk_id','>=',$request->pk_start)->where('pk_id', '<=', $request->pk_end);
            });

            $progressTrx->when($request->work_id , function($query) use($request){
                $query->where('work_id',$request->work_id);
            });
    
            return new ProgressCollection($progressTrx->paginate($request->per_page ?? 10));
        }        
    }

    public function viewProgress(Request $request, $projectId = 0, $progressTrxID = 0)
    {
        $query = StationWorkSideHistory::whereHas('side', function($query) use($projectId, $request){
            $query->whereHas('station', function($query) use($projectId, $request){
                $query->whereHas('station', function($query) use($projectId, $request){
                    $query->whereHas('point', function($query) use($projectId, $request){
                        $query->where('project_id', $projectId);
                        $query->when($request->pk_start || $request->pk_end, function($query) use($request){
                            $query->where('pk', '>=', $request->pk_start)->where('pk', '<=', $request->pk_end);
                        });
                    });
                });
            });
        })->with([
            'images',
            'progress',
            'side.side',
            'side.station.station.point',
            'side.station.work'
        ])->where('progress_trx_id', $progressTrxID)->whereIn('progress_status_id', [2,3,4])->orderBy('id', 'DESC');

        $query->when($request->picture_type, function ($query) use ($request) {
            $query->where('progress_status_id',  $request->picture_type);
        });

        $data = new ProjectImageCollection($query->get());

        return $data;
    }

    public function imageFormat($data)
    {
    
        $all                = [];
        $preConstruction    = [];
        $inConstruction     = [];
        $afterConstruction  = [];
        return $data;
        foreach($data as $row){
            if($row->progress_status_id == 2){
                $preConstruction[] = [
                    "id" => $row->id,
                    "comment" => $row->comment,
                    "updater" => $row->updater,
                    "created_at" => $row->created_at ? date('d-m-Y', strtotime($row->created_at)) : null,
                    "images" => $row->images
                ];
            }elseif($row->progress_status_id == 3){
                $inConstruction[] = [
                    "id" => $row->id,
                    "comment" => $row->comment,
                    "updater" => $row->updater,
                    "created_at" => $row->created_at ? date('d-m-Y', strtotime($row->created_at)) : null,
                    "images" => $row->images
                ];
            }else{
                $afterConstruction[] = [
                    "id" => $row->id,
                    "comment" => $row->comment,
                    "updater" => $row->updater,
                    "created_at" => $row->created_at ? date('d-m-Y', strtotime($row->created_at)) : null,
                    "images" => $row->images
                ];
            }
        }
        $all = array_merge($preConstruction, $inConstruction, $afterConstruction);

        return [
            'all'                => $all,
            'pre_construction'   => $preConstruction,
            'in_construction'    => $inConstruction,
            'after_construction' => $afterConstruction
        ];
    }

    public function create(Request $request, $projectId)
    {
        $validator = Validator::make($request->all(), [
            "image_type_id" => "required",
            "title"         => "required"
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {

                DB::beginTransaction();

                $data = new ProjectPicture();

                $data->project_id                 = $projectId;
                $data->image_type_id              = $request->image_type_id;
                $data->title                      = $request->title;
                $data->creator_id                 = auth()->id();
                $data->save();

                // ==========>> Store Image to File Server <<===========

                $images = json_decode($request->images);
                $imgs = [];
                if($images){
                    foreach($images as $row){
                        $file = $this->fileServer('', $row->img);
                        $imgs[] = [
                            'project_picture_id'           => $data->id,
                            'image_uri'                    => $file['fileURL']['uri'],
                            'lat'                          => $row->lat,
                            'lng'                          => $row->lng
                        ];                          
                    }                                                 
                }              
                // ======> Store Image <<======
                ProjectImage::insert($imgs);

                DB::commit();
                return responseSuccess("គម្រោងបានបង្កើត");
            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function update(Request $request, $projectId = 0, $imageId = 0)
    {
       
        $validator = Validator::make($request->all(), [
            "image_type_id" => "required",
            "title"         => "required"
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {

                $data = ProjectPicture::where([
                    'project_id' => $projectId,
                    'id'         => $imageId
                ])->first();

                if ($data) {

                    DB::beginTransaction();

                    $data->project_id        = $projectId;
                    $data->image_type_id     = $request->image_type_id;
                    $data->title             = $request->title;
                    $data->creator_id        = auth()->id();
                    $data->save();

                    // ==============>> Store Image to File Server <<==============
                    $images = json_decode($request->images);
                    $imgs = [];
                    if($images){
                        foreach($images as $row){
                            $size_in_bytes = (int) (strlen(rtrim($row->img, '=')) * 3 / 4);
                            $size_in_kb    = $size_in_bytes / 1024;
                            if($size_in_kb <= 4000){
                                $file = $this->fileServer('', $row->img);
                                $imgs[] = [
                                    'project_picture_id'           => $data->id,
                                    'image_uri'                    => $file['fileURL']['uri'],
                                    'lat'                          => $row->lat,
                                    'lng'                          => $row->lng
                                ];
                            }                             
                        }                                                 
                    }              
                    // ==============>> Store Image <<================
                    ProjectImage::insert($imgs);
                    DB::commit();

                    return responseSuccess("ទិន្ន័យបានកែប្រែ");
                } else {
                    return responseError("មិនមានទិន្ន័យ");
                }
            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function delete($projectId, $imageId)
    {
        $data = ProjectImage::where([
            'project_id' => $projectId,
            'id'         => $imageId
        ])->first();

        if ($data) {
            $data->delete();
            return responseSuccess('បានលុបទិន្ន័យ');
        } else {
            return responseError('មិនមានទិន្ន័យ');
        }
    }

    // ======================> Get Data JS Report <========================

    public function getdataJsreport(Request $request, $projectId){
        
        $project = Project::with(['budgets', 'organizations', 'road','year'])->where('id', $projectId)->first();
        $general = $this->getGeneralInfo($project);
        $statistic = self::getStatistic($projectId);
        $standardTest = $this->standardTest($projectId, $project);
        $projectProcurements = $this->getprojectProcurement($projectId);

        return $this -> jsReport($general , $statistic, $projectProcurements, $standardTest);


    }

    //==============> Private Function for JS Report <===============
    public function standardTest($projectId = 0, $project = null)
    {
        $works      = Work::select('id', 'kh_name')
        ->whereHas('stationWorks', function($query) use ($projectId){
            $query->whereHas('station', function($query) use ($projectId){
                $query->whereHas('point', function($query) use ($projectId){
                    $query->where('project_id', $projectId);
                });
            });
        })->get();

        $data = [];
        foreach($works as $work){
            $data[] = [
                "name" => $work->kh_name,
                "data" => $this->testingStandard($work->id, $projectId)
            ];
        }
        $newData['testing'] = $data;

        return $data;
    }
    private function testingStandard($workId = null, $projectId = null)
    {

        $standardTests = ProjectStandardTest::select('id', 'project_id', 'testing_method_id', 'work_id', 'creator_id', 'standard', 'date', 'pk_start', 'pk_end', 'station_start', 'station_end')
        ->with([
            'method' => function ($q) {
                $q->select('id', 'name','type_id','unit')->with(['TestingMethodType'])->get();
            },
            'work' => function ($q) {
                $q->select('id', 'kh_name');
            },
            'creator' => function ($q) {
                $q->select('id', 'name');
            }
        ])->where([
            'project_id' => $projectId,
            'work_id'    => $workId
        ])->orderBy('id', 'DESC')->get();

        $methodID = [];

        foreach($standardTests as $standardTest){
            $methodID[] = $standardTest->method;
        }

        $arr =  collect($methodID)->unique();
        $ids  = array_values($arr->toArray());
        $newData = [];
        
        foreach($ids as $row){
            $standardArr = [];
            foreach($standardTests as $data){
                if($row['id'] == $data->method->id){
                    $standardArr[] = $data;                    
                }
                
            }
            $newData[] = [
                'method_name' => $row['name'],
                'standards'   => $standardArr
            ];
        }
        return $newData;

    }
    private function getGeneralInfo($project = null)
    {
        return [
            'main' => [
                'id'                    => $project->id,
                'name'                  => $project->name,
                "code"                  => $project->code,
                "executive_director"    => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 3)->first() : null),
                "type"                  => $project->types,
                "budget_plan"           => $this->budget($project->budgets, $project->year),
                "framework"             => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 4)->first() : null),
                "project_owner"         => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 1)->first() : null),
                "entities"              => $this->data($project->organizations ? $project->organizations->where('type_id', 2) : null),
                "reviewer"              => $this->data($project->organizations ? $project->organizations->where('type_id', 5) : null),
                "category"              => $project->category ?? "",
                "road"                  => $project->road,
                "provinces"             => ProjectProvince::with(['provinces'])->where('project_id',$project['id'])->get(),
                "status"                => $project->status ?? "",
                "surfaces"              => $this->surfaces($project->surfaces)
            ],
            'procurement' => [
                "bud_request_construction" => $project->bud_request_construction,
                "bud_request_check" => $project->bud_request_check,
                "bud_after_warranty_con" => $project->bud_after_warranty_construction,
                "bud_after_warranty_check" => $project->bud_after_warranty_check,
                "bud_contract_construciton" => $project->bud_contract_construciton,
                "bud_contract_check" => $project->bud_contract_check,
            ],
            'date' => [
                "project_start_date_contract" => $project->project_start_date_contract ? date('Y-m-d', strtotime($project->project_start_date_contract)) : null,
                "project_end_date_contract" => $project->project_end_date_contract ? date('Y-m-d', strtotime($project->project_end_date_contract)) : null,
                "process_contract_duration" => $project->process_contract_duration ? $project->process_contract_duration : null,
                "delay_date" => $project->delay_date ? date('Y-m-d', strtotime($project->delay_date)) : null,
                "warranty_duration" => $project->warranty_duration ? $project->warranty_duration : null,
                "close_project_date" => $project->close_project_date ? date('Y-m-d', strtotime($project->close_project_date)) : null
            ]
        ];            
    }
    
    private function data($entities){
        $arrEntities = [];
        foreach ($entities as $entity) {
            $arrEntities[] = [
                "id"               => $entity['id'] ?? "",
                "name"             => $entity['kh_name'] ?? "",
                "en_name"          => $entity['en_name'] ?? "",
                "abbre"            => $entity['abbre'] ?? "",
            ];
        }
        return $arrEntities;
    }
    private function surfaces($surfaces)
    {
        $projectSurfaces = [];
        foreach ($surfaces as $row) {
            $projectSurfaces[] = [
                "id"               => $row['id'] ?? "",
                "name"             => $row['kh_name'] ?? ""
            ];
        }
        return $projectSurfaces;
    }
    private function projectOwner($entities)
    {
        $entities = [
            "id"               => $entities['id'] ?? "",
            "kh_name"          => $entities['kh_name'] ?? "",
            "en_name"          => $entities['en_name'] ?? "",
            "abbre"            => $entities['abbre'] ?? "",
        ];
        return $entities;
    }

    private function budget($budget, $year)
    {
        $name = $budget ? $budget->name : null;
        $budYear =  $year ? " (ឆ្នាំ ".$year->year.")" : null;
        return [
            'id'   => $budget->id,
            'name' => $name.$budYear,
            'budget_year_id' => $year ? $year->id : null
            // 'year' => $year ? "(ឆ្នាំ ".$year->year.")" : null
        ];
    }

    public function getStatistic($projectId = 0)
    {
        $project = Project::find($projectId); 
        $test = [];
        if($project){
            $stations = Station::whereHas('point', function ($query) use ($projectId) {
                $query->where('project_id', $projectId);
            })->get();

            if(!$stations->isEmpty()){        

                // dd('hello');
                $staionIds = [];
                foreach ($stations as $stationId) {
                    $staionIds[] = $stationId->id;
                }
                $works = Work::whereHas('stationWorks', function ($query) use ($staionIds) {
                    $query->whereIn('station_id', $staionIds);
                })
                ->with([
                    'stationWorks.sides.testingRecords'
                ])
                ->get();

                $allProcess         = 0;
                $finishProcess      = 0;
                $startProcess       = 0;
                $notStartProcess    = 0;

                // =========> Check On Road Need to test

                $testings = ProjectStandardTest::where('project_id', $projectId)->get();
                $allTest            = 0;

                // return $testings;
                foreach($testings as $testing){
                    $start = $testing->pk_start * 1000 + $testing->station_start;
                    $end   = $testing->pk_end * 1000 + $testing->station_end;

                    $allTest = ($allTest + ($end - $start)*2);
                }
                // return $allTest;

                // ========> Get Stations Have Test <=============
                $testStations = StationWorkSideTest::whereHas('side', function($query) use($projectId){
                    $query->whereHas('station', function($query) use($projectId){
                        $query->whereHas('station', function($query) use($projectId){
                            $query->whereHas('point', function($query) use($projectId){
                                $query->where('project_id', $projectId);
                            });
                        });
                    });
                })->get();

                $haveTest = [];
                // $newData = collect($haveTest)->unique();
                // return $testStations;
                if(!$testStations->isEmpty()){
                    foreach($testStations as $testStation){
                        $haveTest[] = [
                            "standard_id" =>$testStation->standard_test_id,
                            "test_id"    =>$testStation->station_work_side_id
                        ];
                    }
                    $newData = collect($haveTest)->unique();

                    // =================> Get Interval <================
                    $project = Project::where('id', $projectId)->first();
                    $interval = $project->interval;

                    $testingBlock = $allTest == 0 ? 0 : $allTest/$interval * 2;
                    $haveTesting = count(collect($haveTest)->unique());

                    $result = $testingBlock == 0 ? 0 : round( (float) $haveTesting / $testingBlock * 100, 2);
                }else{
                    $result = 0;
                }

                // return $result;
                foreach ($works as $work) {

                
                    foreach ($stations as $station) {
                        $stationWork = StationWork::where([
                            'station_id' => $station->id,
                            'work_id' => $work->id
                        ])
                            ->with([
                                'sides',
                                'sides.status'
                            ])->first();
                        if ($stationWork) {
                            foreach ($stationWork->sides as $side) {

                                if(!$side->testingRecords->isEmpty()){
                                    // $haveTest++;
                                }
                                // Process
                                $allProcess++;
                                if ($side->progress_status_id == 1) {
                                    $notStartProcess++;                                    
                                }elseif($side->progress_status_id == 2) {
                                    $startProcess++;
                                }else{
                                    $finishProcess++;
                                }
                                
                            }
                        }
                    }

                    $road[] = [
                        'method' => [
                            'id'    => $work->id,
                            'name'  => $work->en_name                       
                        ],
                    ];

                }

                $series = [ 
                            "finishProcess" => number_format( (float)$finishProcess * 100 / $allProcess, 2),
                            "startProcess" => number_format( (float)$startProcess * 100 / $allProcess, 2),
                            "notStartProcess" => number_format( (float)$notStartProcess * 100 / $allProcess, 2)];

                if($project->diagramApprover && $project->technical_approved_datetime){
                    $approval = [
                        'date' => $project->technical_approved_datetime,
                        'by'   => $project->diagramApprover ? $project->diagramApprover->name : null
                    ];
                }else{
                    $approval = null;
                }
                
                $testingQuality = [
                    "testingQuality" => $result,
                    "notestingQuality" => 100 - $result
                ];
                return  [
                    "technical_approved" => $approval,
                    "data"    => [
                        "test_quality" => [
                            'series' => $testingQuality,
                            'labels' => [
                                'បានធ្វើតេស្តរួច',
                                'មិនទាន់បានតេស្ត'
                            ],
                            'colors' => [
                                '#FFB10D',
                                '#FFFFFF'
                            ]
                        ],
                        'process' => [
                            'series' => $series,
                            'labels' => [
                                'បញ្ចប់ការងារ',
                                'កំពុងអនុវត្ត',
                                'មិនទាន់អនុវត្ត',
                            ],
                            'colors' => [
                                '#00FF00',
                                '#90CAF9',
                                '#EEEEEE'
                            ]
                        ],
                    ]
                ];
                
            }else{
                if($project->diagramApprover && $project->technical_approved_datetime){
                    $approval = [
                        'date' => $project->technical_approved_datetime,
                        'by'   => $project->diagramApprover ? $project->diagramApprover->name : null
                    ];
                }else{
                    $approval = null;
                }
                
                return  [
                    "technical_approved" => $approval,
                    "data"    => [
                        'process' => [
                            'series' => [
                                "haveTest" => 0 ,
                                "finishProcess" =>0,
                                "startProcess" =>0, 
                                "notStartProcess"=>100
                            ],
                            'labels' => [
                                'បានធ្វើតេស្តរួច',
                                'បញ្ចប់ការងារ',
                                'កំពុងអនុវត្ត',
                                'មិនទាន់អនុវត្ត',
                            ],
                            'colors' => [
                                '#FFB10D',
                                '#00FF00',
                                '#90CAF9',
                                '#EEEEEE'
                            ]
                        ],
                    ]
                ];
            }
        }else{
            return responseError('មិនមានទិន្ន័យ');
        }
        
        
        
    }

    public function listingPicture(Request $request, $projectId = 0)
    {
        $query = StationWorkSideHistory::whereHas('side', function($query) use($projectId){
            $query->whereHas('station', function($query) use($projectId){
                $query->whereHas('station', function($query) use($projectId){
                    $query->whereHas('point', function($query) use($projectId){
                        $query->where('project_id', $projectId);
                    });
                });
            });
        })->with([
            'images'
        ])->whereIn('progress_status_id', [2,3,4])->orderBy('id', 'DESC')->get();

        // $query->when($request->picture_type, function ($query) use ($request) {
        //     $query->where('progress_status_id',  $request->picture_type);
        // });

        $projectImages = [];
        foreach($query as $row){
            $projectImages [] = [
                "id" => $row->id,
                "comment" => $row->comment,
                "updater" => $row->updater,
                "created_at" => $row->created_at ? date('d-m-Y', strtotime($row->created_at)) : null,
                "images" => $row->images->first() ? $row->images->first()->image_uri : null ,
                "status"    => $row->progress
            ];
        }

        return $projectImages;

    }

    public static function getprojectProcurement($projectId = null)
    {

        // Get Picture
        $projectProcurements = ProjectProcurement::with('creator')->where('project_id', $projectId)->orderBy('id', 'DESC')->get();

        $projectProcurement = [];
        foreach( $projectProcurements as $row){
            $projectProcurement [] = [
                "id"                => $row -> id,
                "creator"           => $row -> creator ? $row -> creator -> name : null,
                "title"             => $row -> title,
                "docu_code"         => $row -> docu_code,
                "file"              => $row -> image_uri,
                "description"       => $row -> description,
                "date"              => $row->date ? date('Y-m-d', strtotime($row->date)) : null

            ];
        }
        return $projectProcurement;
    }

    private function imagesFormat($images){
        $imgs = $images->image_uri;
        return $imgs;

    }
}

