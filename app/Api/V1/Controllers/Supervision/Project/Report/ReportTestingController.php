<?php

namespace App\Api\V1\Controllers\Supervision\Project\Report;

// ===================>> Service <<====================
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Http\Collections\Project\TestingCollection;
use App\Api\V1\Http\Collections\Project\TestingReportCollection;
use App\Api\V1\Service\FileServers;
use Carbon\Carbon;

// =================>> Model <<===============
use App\Model\Supervision\Projects\StandardTestReport;
use App\Model\Supervision\Projects\Station;
use App\Model\Supervision\Projects\StationWorkSideTest;
use App\Model\Supervision\Projects\TestingTRX;
use App\Model\Supervision\Projects\Transaction;
use App\Model\Supervision\Setup\TestingMethod;
use App\Model\Supervision\Setup\Work;

class ReportTestingController extends ApiController
{
    use FileServers;

    public function dataSetup(){
        $method = TestingMethod::select('id','name')->get();
        return $method;
    }

    public function listing(Request $request, $projectId = 0){

        if($request->type == "mobile"){
            $query = StationWorkSideTest::whereHas('side', function($query) use($projectId){
                $query->whereHas('station', function($query) use($projectId){
                    $query->whereHas('station', function($query) use($projectId){
                        $query->whereHas('point', function($query) use($projectId){
                            $query->where('project_id', $projectId);
                        });
                    });
                });
            })->with([
                'testingTrx.images',
                'standardTest.method.place',
                'side.station.work',
                'side.station.station',
                'side.side'
            ])->orderBy('id','DESC');
    
            $query->when($request->key, function ($query) use ($request) {
                $query->where('comment', 'LIKE', '%' . $request->key . '%');
            });
    
            $query->when($request->test_method, function ($query) use ($request) {
                $query->whereHas('standardTest', function($query) use($request){
                    $query->where('testing_method_id', $request->test_method);
                });
            });
    
            $query->when($request->work_id, function ($query) use ($request) {
                $query->whereHas('side', function($query) use($request){
                    $query->whereHas('station', function($query) use($request){
                        $query->where('work_id', $request->work_id);
                    });
                });
            });
    
            $data = new TestingReportCollection($query->paginate($request->per_page ?? 10));
    
            return $data;
        }else{
            $testingTrx = TestingTRX::with([
                'tester',
                'work',
                'images',
                'side',
                'testingStandard'
            ])->where('project_id', $projectId)->orderBy('id','DESC');

            $testingTrx->when($request->pk_start && $request->pk_end, function($query) use($request){
                $query->where('pk_id','>=',$request->pk_start)->where('pk_id', '<=', $request->pk_end);
            });

            $testingTrx->when($request->work_id , function($query) use($request){
                $query->where('work_id', $request->work_id);
            });
    
            return new TestingCollection($testingTrx->paginate($request->per_page ?? 10));
        }        
    }

    public function getTestingChart($projectId = 0){

        // =============> Get PK with Station <===========
        $transactions = Transaction::with([
            'stations.stationWorks.sides.testingRecords',
        ])->where('project_id', $projectId)->select('id', 'pk')->get();

        $stations   = Station::whereHas('point', function ($query) use ($projectId) {
            $query->where('project_id', $projectId);
        })->get();
        $categories = [];
        foreach($transactions as $transaction){
            $categories[] = $transaction->pk;
        }

        // ==============> Get Work <=================

        $staionIds = [];
        foreach ($stations as $stationId) {
            $staionIds[] = $stationId->id;
        }

        $works = Work::select('id', 'kh_name as name', 'color')->whereHas('stationWorks', function ($query) use ($staionIds) {
            $query->whereIn('station_id', $staionIds);
        })->orderBy('group_id', 'DESC')->get();

        // ============> Get Testing <==============
        $series = [];
        foreach($transactions as $transaction){
            $series[] = [
                'pk' => $transaction->pk,
                'data' => $this->getTesting($projectId, $transaction, $works)
            ];
        }
        
        return [
            'data' => $series
        ];
    }

    private function getTesting($projectId, $transaction, $works){

        $testing = [];
        foreach($works as $work){
            $query = StationWorkSideTest::whereHas('side', function($query) use($projectId, $work, $transaction){
                $query->whereHas('station', function($query) use($projectId, $work, $transaction){
                    $query->whereHas('work', function($query) use($work){
                        $query->where('id', $work->id);
                    });
                    $query->whereHas('station', function($query) use($projectId, $transaction){
                        $query->whereHas('point', function($query) use($projectId, $transaction){
                            $query->where([
                                'id' => $transaction->id,
                                'project_id' => $projectId
                            ]);
                        });
                    });
                });
            })->get();

            $isPass = 0;
            foreach($query as $row){
                if($row->is_pass == 1){
                    $isPass ++;
                }
            }

            $testing[] = [
                'name'      => $work->name,
                'color'     => $work->color,
                'data'      => count($query),
                'is_pass'   => $isPass
            ];
        }
        return $testing;
    }

    public function viewTesting(Request $request, $projectId = 0, $testingTrxID = 0)
    {
        $query = StationWorkSideTest::whereHas('side', function($query) use($projectId){
            $query->whereHas('station', function($query) use($projectId){
                $query->whereHas('station', function($query) use($projectId){
                    $query->whereHas('point', function($query) use($projectId){
                        $query->where('project_id', $projectId);
                    });
                });
            });
        })->with([
            'standardTest.method.place',
            'side.station.work',
            'side.station.station',
            'side.side'
        ])->where('testing_trx_id', $testingTrxID)->orderBy('id','DESC');

        $query->when($request->key, function ($query) use ($request) {
            $query->where('comment', 'LIKE', '%' . $request->key . '%');
        });

        $query->when($request->test_method, function ($query) use ($request) {
            $query->whereHas('standardTest', function($query) use($request){
                $query->where('testing_method_id', $request->test_method);
            });
        });

        $query->when($request->work_id, function ($query) use ($request) {
            $query->whereHas('side', function($query) use($request){
                $query->whereHas('station', function($query) use($request){
                    $query->where('work_id', $request->work_id);
                });
            });
        });

        $data = new TestingReportCollection($query->get());

        return $data;
    }

    private function reportTypeSetup($data){
        $res = [];
        foreach($data as $row){
            $res[] = [
                'id' => $row->id,
                'name' => $row->name
            ];
        }
        return $res;
    }

    private function testingMethodSetup($data){
        $res = [];
        foreach($data as $row){
            $res[] = [
                'id' => $row->id,
                'testing_method_type_id' => $row->type_id,
                'name' => $row->name
            ];
        }
        return $res;
    }

    public function create(Request $request, $projectId)
    {
        $validator = Validator::make($request->all(), [
            "test_report_type_id" => "required",
            "testing_method_id"   => "required"

        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {

                $data = new StandardTestReport();

                $data->project_id         = $projectId;
                $data->report_type_id     = $request->test_report_type_id;
                $data->field_testing_id   = $request->testing_method_id;
                $data->title              = $request->title;
                $data->creator_id         = auth()->id();
                $data->date               = Carbon::now();

                // Store Image to File Server
                $image = $this->fileServer($request->file, $request->image);
                if ($request->file) {
                    $uri = $image['uuid'];
                } else {
                    $uri = $image['fileURL']['uri'];
                }
                $data->image_uri = $uri;
                $data->save();

                DB::commit();
                return responseSuccess("គម្រោងបានបង្កើត");
            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function preview($projectId = 0, $testReportId = 0){
        
        $data = StandardTestReport::where([
            'project_id' => $projectId,
            'id'         => $testReportId
        ])->first();

        if($data){
            $res = [
                "id"                   => $data -> id,
                "testing_method_types" => $data -> reportTypes ? $this->testingMethod($data -> reportTypes)  : null,
                "testing_methods"      => $data -> testingMethod ? $this->reportType($data -> testingMethod) : null,
                "creator_id"           => $data -> creator ? $data -> creator->name : null,
                "created_date"         => $data -> created_at ? date('d-m-Y', strtotime($data -> created_at)) : null,
                "title"                => $data -> title,
                "image_uri"            => $data -> image_uri
            ];
            return responseSuccess("មានទិន្ន័យ", $res);

        }else{
            return responseError('មិនមានទិន្ន័យ');
        }
    }

    public function update(Request $request, $projectId = 0, $testReportId = 0)
    {
        $validator = Validator::make($request->all(), [
            "test_report_type_id" => "required",
            "testing_method_id"   => "required"
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {

                $data = StandardTestReport::where([
                    'project_id' => $projectId,
                    'id'         => $testReportId
                ])->first();

                if ($data) {
                    DB::beginTransaction();

                    $data->report_type_id     = $request->test_report_type_id;
                    $data->field_testing_id   = $request->testing_method_id;
                    $data->title              = $request->title;
                    $data->creator_id         = auth()->id();
                    $data->date               = Carbon::now();

                    // Store Image to File Server
                    if($request->file != null || $request->image != null){
                        $image = $this->fileServer($request->file, $request->image);
                        if ($request->file) {
                            $uri = $image['uuid'];
                        } else {
                            $uri = $image['fileURL']['uri'];
                        }
                        $data->image_uri = $uri;
                    }

                    $data->save();

                    DB::commit();

                    $testingReport = [
                        "id"                   => $data -> id,
                        "testing_method_types" => $data -> reportTypes ? $this->testingMethod($data -> reportTypes)  : null,
                        "testing_methods"      => $data -> testingMethod ? $this->reportType($data -> testingMethod) : null,
                        "creator_id"           => $data -> creator ? $data -> creator->name : null,
                        "created_date"         => $data -> created_at ? date('d-m-Y', strtotime($data -> created_at)) : null,
                        "title"                => $data -> title,
                        "image_uri"            => $data -> image_uri
                    ];

                    return responseSuccess("ទិន្ន័យបានកែប្រែ", $testingReport);
                } else {
                    return responseError('មិនមានទិន្ន័យ');
                }
            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    private function reportType($data){
        $res = [
            'id'    => $data->id,
            'testing_method_type_id' => $data->type_id,
            'name'  => $data->name
        ];
        return $res;
    }

    private function testingMethod($data){
        $res = [
            'id'                     => $data->id,
            'name'                   => $data->name
        ];
        return $res;
    }

    public function delete($projectId, $testId)
    {
        $testReports = StandardTestReport::where([
            'project_id' => $projectId,
            'id'         => $testId
        ])->first();

        if ($testReports) {
            $testReports->delete();
            return responseSuccess('បានលុបទិន្ន័យ');
        } else {
            return responseError('មិនមានទិន្ន័យ');
        }
    }
}
