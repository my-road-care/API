<?php

namespace App\Api\V1\Controllers\Supervision\Project\Report;


// ==============>> Service <<=============
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Http\Collections\Project\ReferenceReportCollection;
use App\Api\V1\Service\FileServers;

// ===============>> Model <<=============
use App\Model\User;
use App\Model\Supervision\Projects\ReferenceReport;

class ReportReferencesController extends ApiController
{
    use FileServers;

    public function listing(Request $request, $projectId = 0)
    {
        $query = ReferenceReport::query()->select('id','title','image_uri','created_at','creator_id')
        ->with('creator')->where('project_id', $projectId);

        return new ReferenceReportCollection($query->paginate($request->per_page ?? 10));;
    }

    public function create(Request $request, $projectId)
    {
        $validator = Validator::make($request->all(), [
            "title"         => "required"
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {

                // DB::beginTransaction();

                $data = new ReferenceReport();

                $data->project_id                 = $projectId;
                $data->title                      = $request->title;
                $data->creator_id                 = auth()->id();

                // Store Image to File Server
                $file = $this->fileServer($request->file, $request->image);
                if($request->file){
                    $uri = $file['uuid'];
                }else{
                    $uri = $file['fileURL']['uri'];
                }
                $data -> image_uri = $uri;

                $data->save();
                $data->creator= User::select('id','name')->where('id',auth()->id())->first();

                DB::commit();
                return responseSuccess("បានភ្ជាប់របាយការណ៍យោង",$data);
            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function preview($projectId = 0, $referenceId = 0){
        $data = ReferenceReport::where([
            'project_id' => $projectId,
            'id'         => $referenceId
        ])->first();

        if($data){
            $report = $data->select('id','title','image_uri','created_at','creator_id')
            ->with('creator')->first();

            return $report;
        }else{

            return responseError('មិនមានទិន្ន័យ');
        }
    }

    public function update(Request $request, $projectId = 0, $referenceId = 0)
    {
        $validator = Validator::make($request->all(), [
            "title"         => "required"
        ]);

        if ($validator->fails()) {
            return responseError($validator->errors(), 422);
        } else {
            try {

                $data = ReferenceReport::where([
                    'project_id' => $projectId,
                    'id'         => $referenceId
                ])->select('id','title','image_uri','created_at','creator_id')
                ->with('creator')->first();

                if ($data) {

                    DB::beginTransaction();

                    $data->title             = $request->title;
                    $data->creator_id        = auth()->id();

                    // ===> Store Image to File Server

                    if($request->file != null || $request->image != null){

                        $file = $this->fileServer($request->file, $request->image);

                        if($request->file){
                            $uri = $file['uuid'];
                        }else{
                            $uri = $file['fileURL']['uri'];
                        }
                        $data -> image_uri = $uri;
                    }

                    $data->save();

                    DB::commit();
                    return responseSuccess("ទិន្ន័យបានកែប្រែ", $data);
                } else {
                    return responseError("មិនមានទិន្ន័យ");
                }
            } catch (\Exception $exception) {
                DB::rollBack();
                return responseError($exception);
            }
        }
    }

    public function delete($projectId, $referenceId = 0)
    {
        $data = ReferenceReport::where([
            'project_id' => $projectId,
            'id'         => $referenceId
        ])->first();

        if ($data) {
            $data->delete();
            return responseSuccess('បានលុបទិន្ន័យ');
        } else {
            return responseError('មិនមានទិន្ន័យ');
        }
    }
}
