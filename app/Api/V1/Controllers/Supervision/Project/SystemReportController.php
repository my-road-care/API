<?php

namespace App\Api\V1\Controllers\Supervision\Project;


use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;

// =============>> Service <<===========
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Service\JSReport;

// ==============>> Model <<==================
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProgressTRX;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Projects\Station;
use App\Model\Supervision\Projects\StationWork;
use App\Model\Supervision\Projects\ProjectProvince;
use App\Model\Supervision\Projects\StationWorkSide;
use App\Model\Supervision\Projects\StationWorkSideHistory;
use App\Model\Supervision\Projects\StationWorkSideTest;
use App\Model\Supervision\Projects\TestingTRX;
use App\Model\Supervision\Projects\Transaction;
use App\Model\Supervision\Setup\ProgressStatus;
use App\Model\Supervision\Setup\Work;

class SystemReportController extends ApiController
{
    use JSReport;

    public function setup($projectId = 0)
    {

        // ==============> Get Project <================
        $project = Project::with([
            'surfaces',
            'status'
        ])->where('id', $projectId)->first();


        // =============> Get PK with Station <===========
        $pks = Transaction::with('stations')->where('project_id', $projectId)->select('id', 'pk')->get();
        $stations   = Station::whereHas('point', function ($query) use ($projectId) {
            $query->where('project_id', $projectId);
        })->get();

        $staionIds = [];
        foreach ($stations as $stationId) {
            $staionIds[] = $stationId->id;
        }

        // ==============> Get Work <=================
        $works = Work::select('id', 'kh_name as name')->whereHas('stationWorks', function ($query) use ($staionIds) {
            $query->whereIn('station_id', $staionIds);
        })->orderBy('group_id', 'DESC')->get();


        // ===============> Get Location <=============
        $transactions = Transaction::with([
            'stations'
        ])->where('project_id', $project->id)->get();

        $pk = [];
        $start = null;
        $end = null;
        $i = 0;
        $len = count($transactions);

        foreach ($transactions as $transaction) {
            if ($i == 0) {
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $start = "PK" . $pk->pk . "+" . $stations->number;
            } elseif ($i == $len - 1) {
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $end = "PK" . $pk->pk . "+" . $stations->number;
            }
            $i++;
        }


        $projectInfo =  [
            'name'          => $project->name,
            'code'          => $project->code,
            'status'        => $project->status,
            'pk_start'      => $start,
            'pk_end'        => $end,
            'start_date'    => $project->project_start_date_contract ? date('Y-M', strtotime($project->project_start_date_contractt)) : null,
            'end_date'      => $project->project_end_date_contract ? date('Y-M', strtotime($project->project_end_date_contractt)) : null,
            'surface'       => $this->getProjectSurface($project->surfaces)
        ];

        return [
            'project'  => $projectInfo,
            'pks'      => $pks,
            'works'    => $works
        ];
    }

    public function listing(Request $request, $projectId = 0)
    {
        $report = $request->report;
        $general = null;
        $picture = null;

        if ($report) {
            // If need general 
            if ($report == 1) {
                $project = Project::with(['budgets', 'organizations', 'road','year'])
                ->where('id', $projectId)->first();

                $general = $this->getGeneralInfo($project);
                $statistic = self::getStatistic($projectId);
                $standardTest = $this->standardTest($projectId, $project);

                $template = [
                    "template" => [
                        "name" => "/RoadCare/SPV/info-project/overview/overview-main",
                    ],
                    "data" => [
                      "general"        => $general,
                      "statistic"      => $statistic,
                      "standard_tests" => $standardTest
                    ],
                ];
                $basic = env('JS_TOKEN');
                return $this->jsReport($template, $basic);
            }

            // If need work
            if ($report == 2) {
                $data = $this->getDiagram($projectId, $request);
                $template = [
                    "template" => [
                        "name" => "/RoadCare/SPV/working-table/working-table-main",
                    ],
                    "data" => $data,
                ];
                $basic = env('JS_TOKEN');
                return $this->jsReport($template, $basic);
            }

            // If need Procurement
            if ($report == 3) {
                $project = Project::with(['budgets', 'organizations', 'road'])->where('id', $projectId)->first();
                $general = $this->getGeneralInfo($project);
            }
            
            // ==============> If need Progress Detail Report <==============
            if ($report == 5) {

                $project = Project::with([
                    'surfaces',
                    'status'
                ])->where('id', $projectId)->first();

                $query = StationWorkSideHistory::whereHas('side', function ($query) use ($projectId) {
                    $query->whereHas('station', function ($query) use ($projectId) {
                        $query->whereHas('station', function ($query) use ($projectId) {
                            $query->whereHas('point', function ($query) use ($projectId) {
                                $query->where('project_id', $projectId);
                            });
                        });
                    });
                })->with([
                    'images',
                    'side.station.station.point',
                    'side.station.work'
                ])->where('progress_trx_id', $request->progress_trx_id)->whereIn('progress_status_id', [2, 3, 4])->orderBy('id', 'DESC')->limit(5)->get();
                $picture = $this->imageFormat($query, $project, $request->progress_trx_id);

                $template = [
                    "template" => [
                        "name" => "/RoadCare/SPV/info-project/progress-report-detail/progress-report-detail-main",
                    ],
                    "data" => $picture,
                ];
                $basic = env('JS_TOKEN');
                return $this->jsReport($template, $basic);
            }

            // If need testing
            if ($report == 6) {
                $project = Project::with([
                    'surfaces',
                    'status'
                ])->where('id', $projectId)->first();

                $query = StationWorkSideTest::whereHas('side', function ($query) use ($projectId) {
                    $query->whereHas('station', function ($query) use ($projectId) {
                        $query->whereHas('station', function ($query) use ($projectId) {
                            $query->whereHas('point', function ($query) use ($projectId) {
                                $query->where('project_id', $projectId);
                            });
                        });
                    });
                })->with([
                    'images',
                    'tester',
                    'standardTest.method.place'
                ])->where('testing_trx_id', $request->testing_trx_id)->orderBy('id', 'DESC')->get();
                $testRecord = $this->testRecord($query, $project, $request->testing_trx_id);

                $template = [
                    "template" => [
                        "name" => "/RoadCare/SPV/info-project/testing-report-detail/testing-report-detail-main",
                    ],
                    "data" => $testRecord,
                ];
                $basic = env('JS_TOKEN');
                return $this->jsReport($template, $basic);
            }

            // If need Testing Standard
            if ($report == 7) {
                $project = Project::with([
                    'surfaces',
                    'status'
                ])->where('id', $projectId)->first();

                $testingStandards = $this->getTestingStandard($projectId, $project);

                $template = [
                    "template" => [
                        "name" => "/RoadCare/SPV/technicals/testing-standard/testing-standard-main",
                    ],
                    "data" => $testingStandards,
                ];
                $basic = env('JS_TOKEN');
                return $this->jsReport($template, $basic);
            }

            // ===============> If need Progress Report <==================
            if ($request->report == 8) {

                // ===========> Get Project <==============
                $project = Project::with([
                    'surfaces',
                    'status'
                ])->where('id', $projectId)->first();

                // =============> get Progress Histories <==============
                $query = StationWorkSide::whereHas('station', function ($query) use ($projectId, $request) {
                    $query->when($request->work_id, function($query) use($request){
                        $query->where('work_id', $request->work_id);
                    });
                    $query->whereHas('station', function ($query) use ($projectId, $request) {
                        $query->whereHas('point', function ($query) use ($projectId, $request) {
                            $query->where('project_id', $projectId);
                            $query->where('id', $request->pk_id);
                        });
                        $query->whereBetween('id', [$request->station_start_id, $request->station_end_id]);
                    });
                })->when($request->side_id, function($query) use($request){
                    $query->where('side_id', $request->side_id);
                })->whereHas('histories', function($query) use($request){
                    $query->whereHas('progressTrx', function($query) use($request){
                        $query->whereBetween('created_at', [$request->from, $request->to]);
                    });
                })
                ->with([
                    'side',
                    'status',
                    'histories' => function($query) {
                        $query->whereIn('progress_status_id',[2,3]);
                        $query->with([
                            'progressTrx.images'
                        ]);
                    },
                    'station.station.point',
                    'station.work'

                ])->get();

                // ==============> Get Stations <=================
                $stations   = Station::whereHas('point', function ($query) use ($projectId) {
                    $query->where('project_id', $projectId);
                })->get();

                $staionIds = [];
                foreach ($stations as $stationId) {
                    $staionIds[] = $stationId->id;
                }

                // ==============> Get Works <=============
                $works = Work::select('id', 'kh_name as name')->whereHas('stationWorks', function ($query) use ($staionIds) {
                    $query->whereIn('station_id', $staionIds);
                })->orderBy('group_id', 'DESC');

                $works->when($request->work_id, function ($query) use ($request) {
                    $query->where('id', $request->work_id);
                });

                // ==============> Format Respone Data <===============
                $picture = $this->progressFormatReport($query, $project, $works->get(), $request);

                // ==============> Format for JS Report <=============
                $template = [
                    "template" => [
                        "name" => "/RoadCare/SPV/info-project/progress-report-pictures/progress-report-picture-main",
                    ],
                    "data" => $picture,
                ];
                $basic = env('JS_TOKEN');
                return $this->jsReport($template, $basic);
            }
        } else {
            return responseError('មិនមានការស្នើរសុំ');
        }
    }

    private function testRecord($testRecords, $project, $testingTrxID)
    {
        $response = [];
        foreach ($testRecords as $row) {
            $pk = $row->side ? $row->side->station ? $row->side->station->station ? $row->side->station->station->point ? $row->side->station->station->point->pk : null : null : null : null;
            $stationNum = $row->side ? $row->side->station ? $row->side->station->station ? $row->side->station->station->station_num : null : null : null;
            $unit = $row->standardTest ? $row->standardTest ? $row->standardTest->method ? $row->standardTest->method->unit : null : null : null;

            // ========> Check Less Than Pass <============
            $less = $row->standardTest ? $row->standardTest ? $row->standardTest->method ? $row->standardTest->method->less_than_pass : null : null : null;
            if ($less == 0) {
                $sign = "x >= ";
            } else {
                $sign = "x <= ";
            }
            $standard = $row->standardTest ? $row->standardTest->standard . " " . $unit : null;

            // ==============> Give Pass or Fail <=============
            if ($row->is_pass == 1) {
                $isPass = [
                    'name'   => 'ជាប់',
                    'color'  => '#22C55E'
                ];
            } else {
                $isPass = [
                    'name'   => 'ធ្លាក់',
                    'color'  => '#DC2626'
                ];
            }
            $response[] =  [
                "id"                    => $row->id,
                "location"              => $pk . "+" . $stationNum,
                "comment"               => $row->comment,
                "standard"              => $sign . $standard,
                "created_at"            => $row->created_at ? date('Y-m-d', strtotime($row->created_at)) : null,
                "tester"                => $row->tester ? $row->tester->name : null,
                "result"                => $row->result . " " . $unit,
                "is_pass"               => $isPass,
                "method"                => $row->standardTest ? $row->standardTest ? $row->standardTest->method ? $row->standardTest->method->name : null : null : null
            ];
        }
        // ===============> Get Location <=============
        $transactions = Transaction::with([
            'stations'
        ])->where('project_id', $project->id)->get();

        $pk = [];
        $start = null;
        $end = null;
        $i = 0;
        $len = count($transactions);

        foreach ($transactions as $transaction) {
            if ($i == 0) {
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $start = "PK" . $pk->pk . "+" . $stations->number;
            } elseif ($i == $len - 1) {
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $end = "PK" . $pk->pk . "+" . $stations->number;
            }
            $i++;
        }

        // =============> Testing Information <============
        $testingTrx = TestingTRX::with([
            'tester',
            'work',
            'images',
            'side',
            'testingStandard.method',
            'pk'
        ])->where([
            'project_id' => $project->id,
            'id' => $testingTrxID
        ])->first();

        $testingInfo = [
            'id'                => $testingTrx->id,
            'tester'            => $testingTrx->tester ? $row->tester->name : null,
            'pk'                => $testingTrx->pk ? $testingTrx->pk->pk : null,
            'station_start'     => $testingTrx->station_start,
            'station_end'       => $testingTrx->station_end,
            'side'              => $testingTrx->side,
            'work'              => $testingTrx->work,
            'comment'           => $testingTrx->comment,
            'method'            => $testingTrx->testingStandard ? $testingTrx->testingStandard->method->name : null,
            'created_at'        => $testingTrx->created_at ? date('Y-m-d', strtotime($row->created_at)) : null
        ];

        $images = $testingTrx->images;

        return [
            'project' => [
                'name'          => $project->name,
                'code'          => $project->code,
                'status'        => $project->status,
                'pk_start'      => $start,
                'pk_end'        => $end,
                'start_date'    => $project->project_start_date_contract ? date('Y-M', strtotime($project->project_start_date_contractt)) : null,
                'end_date'      => $project->project_end_date_contract ? date('Y-M', strtotime($project->project_end_date_contractt)) : null,
                'surface'       => $this->getProjectSurface($project->surfaces)
            ],
            'testing_info'      => $testingInfo,
            'test_records'      => $response,
            'images'            => $images
        ];
    }

    private function getProjectSurface($surfaces = null)
    {
        $data = [];
        foreach ($surfaces as $surface) {
            $data[] = [
                'name' => $surface->kh_name
            ];
        }
        return $data;
    }

    private function parameterFormat($parameters, $id = 0)
    {
        $param = [];
        foreach ($parameters->method ? $parameters->method->parameters : null as $row) {
            $param[] = [
                'id' => $row->id,
                'name' => $row->name,
                'value' => $row->param ? $this->value($row->param, $id) : null
            ];
        }
        $resp = [
            'id'            => $parameters->method ? $parameters->method->id : null,
            'name'          => $parameters->method ? $parameters->method->name : null,
            "place"         => $parameters->method ? $parameters->method->place  : null

        ];
        return $resp;
    }

    private function value($values, $id = 0)
    {
        $res = '';
        foreach ($values->where('station_work_side_test_id', $id) as $data) {
            $res = $data->value;
        }
        return $res;
    }

    //==============> Private Function for JS Report <===============
    private function getTestingStandard($projectId = 0, $project = null)
    {
        $transactions = Transaction::with([
            'stations'
        ])->where('project_id', $project->id)->get();

        $pk = [];
        $start = null;
        $end = null;
        $i = 0;
        $len = count($transactions);

        foreach ($transactions as $transaction) {
            if ($i == 0) {
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $start = "PK" . $pk->pk . "+" . $stations->number;
            } elseif ($i == $len - 1) {
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $end = "PK" . $pk->pk . "+" . $stations->number;
            }
            $i++;
        }
        $works      = Work::select('id', 'kh_name')
            ->whereHas('stationWorks', function ($query) use ($projectId) {
                $query->whereHas('station', function ($query) use ($projectId) {
                    $query->whereHas('point', function ($query) use ($projectId) {
                        $query->where('project_id', $projectId);
                    });
                });
            })->get();

        $data = [];
        foreach ($works as $work) {
            $data[] = [
                "name" => $work->kh_name,
                "data" => $this->testingStandard($work->id, $projectId)
            ];
        }

        $newData['testing'] = $data;

        return [
            "project" => [
                'name'          => $project->name,
                'code'          => $project->code,
                'status'        => $project->status,
                'pk_start'      => $start,
                'pk_end'        => $end,
                'start_date'    => $project->project_start_date_contract ? date('Y-M', strtotime($project->project_start_date_contractt)) : null,
                'end_date'      => $project->project_end_date_contract ? date('Y-M', strtotime($project->project_end_date_contractt)) : null,
                'surface'       => $this->getProjectSurface($project->surfaces)
            ],
            'testing' => $data
        ];
    }

    private function testingStandard($workId = null, $projectId = null)
    {

        $standardTests = ProjectStandardTest::select('id', 'project_id', 'testing_method_id', 'work_id', 'creator_id', 'standard', 'date', 'pk_start', 'pk_end', 'station_start', 'station_end')
            ->with([
                'method' => function ($q) {
                    $q->select('id', 'name', 'type_id', 'unit')->with(['TestingMethodType'])->get();
                },
                'work' => function ($q) {
                    $q->select('id', 'kh_name');
                },
                'creator' => function ($q) {
                    $q->select('id', 'name');
                }
            ])->where([
                'project_id' => $projectId,
                'work_id'    => $workId
            ])->orderBy('id', 'DESC')->get();

        $methodID = [];

        foreach ($standardTests as $standardTest) {
            $methodID[] = $standardTest->method;
        }

        $arr =  collect($methodID)->unique();
        $ids  = array_values($arr->toArray());
        $newData = [];

        foreach ($ids as $row) {
            $standardArr = [];
            foreach ($standardTests as $data) {
                if ($row['id'] == $data->method->id) {
                    $standardArr[] = $data;
                }
            }
            $newData[] = [
                'method_name' => $row['name'],
                'standards'   => $standardArr
            ];
        }
        return $newData;
    }

    private function getDiagram($projectId = 0, $request)
    {

        // =============> Filter from PK start and PK end <============
        if ($request->pk_start_id && $request->pk_end_id) {
            $stations   = Station::whereHas('point', function ($query) use ($projectId, $request) {
                $query->where('project_id', $projectId)->whereBetween('id', [$request->pk_start_id, $request->pk_end_id]);
            })->get();
        } else {
            $stations   = Station::whereHas('point', function ($query) use ($projectId) {
                $query->where('project_id', $projectId);
            })->get();
        }

        // =============> Get Project Information <=============
        $project = Project::where('id', $projectId)->first();

        $pks = Transaction::where('project_id', $projectId)->get();
        if ($request->pk_start_id && $request->pk_end_id) {
            foreach ($pks as $pk) {
                if ($pk->id == $request->pk_start_id) {
                    $pkStart = $pk->pk;
                }

                if ($pk->id == $request->pk_end_id) {
                    $pkEnd = $pk->pk;
                }
            }
        } else {
            $i = 0;
            $len = count($pks);
            $pkStart = null;
            $pkEnd = null;
            foreach ($pks as $pk) {
                $i++;
                if ($i == 1) {
                    $pkStart = $pk->pk;
                }
                if ($i == $len) {
                    $pkEnd = $pk->pk;
                }
            }
        }

        $pageNo = 0;
        $information = [
            'code' => $project->code,
            'name' => $project->name,
            'pk'   => "PK" . $pkStart . " - PK" . $pkEnd
        ];

        // ================> Chunk Pk Station <==============        
        $arrayStations = [];
        foreach ($stations as $object) {
            $arrayStations[] = $object->toArray();
        }

        $chunkStations = array_chunk($arrayStations, 71); // split into arrays of length 71
        $dataStations = [];
        foreach ($chunkStations as $chunk) {
            $dataStations[] = [
                'pk_station' => $chunk
            ];
        }

        $newData['page'] = $dataStations;

        $newArray = [];
        foreach ($newData['page'] as $array) {
            $pageNo++;
            $newArray[] = [
                'index'       => $pageNo,
                'information' => $information,
                'pk_stations' => $array['pk_station'],
                'works'       => $this->newData($array['pk_station'])
            ];
        }

        return [
            "page" => $newArray
        ];
    }

    private function newData($stations)
    {
        $staionIds = [];
        foreach ($stations as $stationId) {
            $staionIds[] = $stationId['id'];
        }
        $works = Work::whereHas('stationWorks', function ($query) use ($staionIds) {
            $query->whereIn('station_id', $staionIds);
        })->orderBy('group_id', 'DESC')->get();
        $data = [];
        $nOfWork = 0;
        foreach ($works as $work) {
            $nOfWork++;
            $l = [];
            $r = [];
            foreach ($stations as $station) {
                $stationWork = StationWork::where([
                    'station_id' => $station['id'],
                    'work_id' => $work->id
                ])
                    ->with([
                        'sides',
                        'sides.status',
                        'sides.testingRecords.standardTest'
                    ])->first();
                if ($stationWork) {
                    foreach ($stationWork->sides as $side) {
                        if ($side->side_id == 1) {
                            $l[] = [
                                'station_work_id'  => $side->station ? $side->station->id : null,
                                'work_id'          => $work->id,
                                'side_id'          => $side->id,
                                'progress'         => $side->status->id,
                                'color'            => $side->status->color,
                            ];
                        } else {
                            $r[] = [
                                'station_work_id'  => $side->station ? $side->station->id : null,
                                'work_id'          => $work->id,
                                'side_id'          => $side->id,
                                'progress'         => $side->status->id,
                                'color'            => $side->status->color
                            ];
                        }
                    }
                } else {
                    $l[] = [
                        'station_work_id'  => null,
                        'work_id'          => $work->id,
                        'side_id'          => 0,
                        'progress'      => null,
                    ];

                    $r[] = [
                        'station_work_id'  => null,
                        'work_id'          => $work->id,
                        'side_id'          => 0,
                        'progress'      => null,
                    ];
                }
            }
            $data[] = [
                'id' => $work->id,
                'no' => $nOfWork,
                'work' => $work->en_name,
                'l'    => $l,
                'r'    => $r
            ];
        }

        return $data;
    }

    private function station($stations)
    {
        $arrStation = [];

        foreach ($stations as $row) {
            $arrStation[] = [
                'id' => $row->id,
                'transaction_id' => $row->transaction_id,
                'number'         => $row->number,
                'trx'            => $row->trx,
                'station_num'    => $row->station_num
            ];
        }

        return $arrStation;
    }

    public function planFormat($plans, $project)
    {
        $projectPlans = [];
        foreach ($plans as $row) {
            $img = $row->image_uri;
            $url = "https://dev-file.mpwt.gov.kh/";
            $projectPlans[] = [
                "id"                => $row->id,
                "creator"           => $row->creator ? $row->creator->name : null,
                "title"             => $row->title,
                "docu_code"         => $row->docu_code,
                "image_uri"         => $url . $img,
                "description"       => $row->description,
                "date"              => $row->date ? date('Y-m-d', strtotime($row->date)) : null
            ];
        }
        return [
            'project' => [
                'name' => $project->name,
                'code' => $project->code
            ],
            'plans' => $projectPlans
        ];
    }

    public function imageFormat($data, $project, $progressTrxID)
    {

        // ==============> Format Progress Records <============
        $progressRecords = [];
        foreach ($data as $row) {
            $station = $row->side ? $row->side->station ? $row->side->station->station ? $row->side->station->station : null : null : null;
            $pk = $station->point ? $station->point->pk : null;

            $from = ProgressStatus::select('id', 'kh_name as name', 'icon', 'color')->where('id', $row->progress->id - 1)->first();
            $progressRecords[] = [
                "id" => $row->id,
                "pk" => "PK " . $pk . "+" . $station->station_num,
                "side" =>  $row->side ? $row->side->side->abrv : null,
                "from" => $from,
                "to"   => $row->progress,
                "work" =>  $row->side ? $row->side->station ? $row->side->station->work ? $row->side->station->work->name : null : null : null,
                "comment" => $row->comment,
                "updater" => $row->updater,
                "created_at" => $row->created_at ? date('d-m-Y', strtotime($row->created_at)) : null
            ];
        }

        // ===============> Get Progress Info <==============
        $progressTrx = ProgressTRX::with([
            'tester',
            'work',
            'images',
            'side',
            'pk'
        ])->where([
            'project_id' => $project->id,
            'id' => $progressTrxID
        ])->first();

        $progressInfo = [
            'id'                => $progressTrx->id,
            'tester'            => $progressTrx->tester ? $progressTrx->tester->name : null,
            'pk'                => $progressTrx->pk ? $progressTrx->pk->pk : null,
            'station_start'     => $progressTrx->station_start,
            'station_end'       => $progressTrx->station_end,
            'side'              => $progressTrx->side,
            'work'              => $progressTrx->work,
            'comment'           => $progressTrx->comment,
            'created_at'        => $progressTrx->created_at ? date('Y-m-d', strtotime($row->created_at)) : null
        ];

        $images = $progressTrx->images;

        // ===============> Get Location <=============
        $transactions = Transaction::with([
            'stations'
        ])->where('project_id', $project->id)->get();

        $pk = [];
        $start = null;
        $end = null;
        $i = 0;
        $len = count($transactions);

        foreach ($transactions as $transaction) {
            if ($i == 0) {
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $start = "PK" . $pk->pk . "+" . $stations->number;
            } elseif ($i == $len - 1) {
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $end = "PK" . $pk->pk . "+" . $stations->number;
            }
            $i++;
        }
        return [
            'project' => [
                'name'          => $project->name,
                'code'          => $project->code,
                'status'        => $project->status,
                'pk_start'      => $start,
                'pk_end'        => $end,
                'start_date'    => $project->project_start_date_contract ? date('Y-M', strtotime($project->project_start_date_contractt)) : null,
                'end_date'      => $project->project_end_date_contract ? date('Y-M', strtotime($project->project_end_date_contractt)) : null,
                'surface'       => $this->getProjectSurface($project->surfaces)
            ],
            'progress_info'     => $progressInfo,
            'progress_records'  => $progressRecords,
            'images'            => $images
        ];
    }

    public function progressFormatReport($data, $project, $works, $request)
    {

        // ==============> Format Progress Records <============
        $progressRecords = [];
        $res = [];
        foreach($works as $work){
            foreach ($data as $row) {
                $workId = $row->station->work->id;
                $station = $row->station ? $row->station->station ? $row->station->station : null : null;
                $pk = $station->point ? $station->point->pk : null;
                if($work->id == $workId){
                    $progressRecords[] = [
                        "id"            => $row->id,
                        "pk"            => "PK " . $pk . "+" . $station->station_num,
                        "side"          => $row->side,
                        "status"        => $row->status,
                        "images"        => $this->getImageStatus($row->histories)
                    ];
                }                
            }
            $res[] = [
                'name'   => $work->name,
                'values' => $progressRecords
            ];
        }
        

        // ===============> Get Location <=============

        $transactions = Transaction::with([
            'stations'
        ])->where('project_id', $project->id)->get();

        $start = null;
        $end = null;
        $i = 0;
        $len = count($transactions);

        foreach ($transactions as $transaction) {
            if ($i == 0) {
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $start = "PK" . $pk->pk . "+" . $stations->number;
            } elseif ($i == $len - 1) {
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $end = "PK" . $pk->pk . "+" . $stations->number;
            }
            $i++;
        }

        $progressInfo = [
            'pk_start'          => $start,
            'pk_end'            => $end,
            'work'              => $works,
            'from_to'           => date('Y-m-d', strtotime($request->from)) . " - " . date('Y-m-d', strtotime($request->to))
        ];

        return [
            'project' => [
                'name'          => $project->name,
                'code'          => $project->code,
                'status'        => $project->status,
                'pk_start'      => $start,
                'pk_end'        => $end,
                'start_date'    => $project->project_start_date_contract ? date('Y-M', strtotime($project->project_start_date_contractt)) : null,
                'end_date'      => $project->project_end_date_contract ? date('Y-M', strtotime($project->project_end_date_contractt)) : null,
                'surface'       => $this->getProjectSurface($project->surfaces)
            ],
            'progress_info'     => $progressInfo,
            'progress_records'  => $res
        ];
    }

    private function fileGetContent($images = null, $no = null)
    {
        $pdfArr = [];
        foreach ($images as $row) {
            $pdfArr[] = $row->image_uri;
        }

        return $pdfArr;
    }

    private function getImageStatus($histories){
        $preCon = null;
        $aftCon = null;

        foreach($histories as $data){
            if($data->progress_status_id == 2){
                $preCon = $data->images->first();
            }elseif($data->progress_status_id == 2){
                $aftCon = $data->images->first();
            }
        }

        return [
            'pre_con' => $preCon,
            'aft_con' => $aftCon
        ];
    }

    public function deleteFile()
    {
        $file = new Filesystem;
        $file->cleanDirectory('temp-img');
        return responseSuccess("ទិន្ន័យបានលុប");
    }

    private function getGeneralInfo($project = null)
    {
        return [
            'main' => [
                'id'                    => $project->id,
                'name'                  => $project->name,
                "code"                  => $project->code,
                "executive_director"    => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 3)->first() : null),
                "type"                  => $project->types,
                "budget_plan"           => $this->budget($project->budgets, $project->year),
                "framework"             => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 4)->first() : null),
                "project_owner"         => $this->projectOwner($project->organizations ? $project->organizations->where('type_id', 1)->first() : null),
                "entities"              => $this->data($project->organizations ? $project->organizations->where('type_id', 2) : null),
                "reviewer"              => $this->data($project->organizations ? $project->organizations->where('type_id', 5) : null),
                "category"              => $project->category ?? "",
                "road"                  => $project->road,
                "provinces"             => ProjectProvince::with(['provinces'])->where('project_id',$project['id'])->get(),
                "status"                => $project->status ?? "",
                "surfaces"              => $this->surfaces($project->surfaces)
            ],
            'procurement' => [
                "bud_request_construction" => $project->bud_request_construction,
                "bud_request_check" => $project->bud_request_check,
                "bud_after_warranty_con" => $project->bud_after_warranty_construction,
                "bud_after_warranty_check" => $project->bud_after_warranty_check,
                "bud_contract_construciton" => $project->bud_contract_construciton,
                "bud_contract_check" => $project->bud_contract_check,
            ],
            'date' => [
                "project_start_date_contract" => $project->project_start_date_contract ? date('Y-m-d', strtotime($project->project_start_date_contract)) : null,
                "project_end_date_contract" => $project->project_end_date_contract ? date('Y-m-d', strtotime($project->project_end_date_contract)) : null,
                "process_contract_duration" => $project->process_contract_duration ? $project->process_contract_duration : null,
                "delay_date" => $project->delay_date ? date('Y-m-d', strtotime($project->delay_date)) : null,
                "warranty_duration" => $project->warranty_duration ? $project->warranty_duration : null,
                "close_project_date" => $project->close_project_date ? date('Y-m-d', strtotime($project->close_project_date)) : null
            ]
        ];
    }

    private function data($entities)
    {
        $arrEntities = [];
        foreach ($entities as $entity) {
            $arrEntities[] = [
                "id"               => $entity['id'] ?? "",
                "name"             => $entity['kh_name'] ?? "",
                "en_name"          => $entity['en_name'] ?? "",
                "abbre"            => $entity['abbre'] ?? "",
            ];
        }
        return $arrEntities;
    }

    private function surfaces($surfaces)
    {
        $projectSurfaces = [];
        foreach ($surfaces as $row) {
            $projectSurfaces[] = [
                "id"               => $row['id'] ?? "",
                "name"             => $row['kh_name'] ?? ""
            ];
        }
        return $projectSurfaces;
    }

    private function projectOwner($entities)
    {
        $entities = [
            "id"               => $entities['id'] ?? "",
            "kh_name"          => $entities['kh_name'] ?? "",
            "en_name"          => $entities['en_name'] ?? "",
            "abbre"            => $entities['abbre'] ?? "",
        ];
        return $entities;
    }

    private function budget($budget)
    {
        return [
            'id'   => $budget->id,
            'name' => $budget->name
        ];
    }
    public function getStatistic($projectId = 0)
    {
        $project = Project::find($projectId); 
        $test = [];
        if($project){
            $stations = Station::whereHas('point', function ($query) use ($projectId) {
                $query->where('project_id', $projectId);
            })->get();

            if(!$stations->isEmpty()){        

                $staionIds = [];
                foreach ($stations as $stationId) {
                    $staionIds[] = $stationId->id;
                }
                $works = Work::whereHas('stationWorks', function ($query) use ($staionIds) {
                    $query->whereIn('station_id', $staionIds);
                })
                ->with([
                    'stationWorks.sides.testingRecords'
                ])
                ->get();

                $allProcess         = 0;
                $finishProcess      = 0;
                $startProcess       = 0;
                $notStartProcess    = 0;

                // =========> Check On Road Need to test

                $testings = ProjectStandardTest::where('project_id', $projectId)->get();
                $allTest            = 0;

                foreach($testings as $testing){
                    $start = $testing->pk_start * 1000 + $testing->station_start;
                    $end   = $testing->pk_end * 1000 + $testing->station_end;

                    $allTest = ($allTest + ($end - $start)*2);
                }

                // ========> Get Stations Have Test <=============
                $testStations = StationWorkSideTest::whereHas('side', function($query) use($projectId){
                    $query->whereHas('station', function($query) use($projectId){
                        $query->whereHas('station', function($query) use($projectId){
                            $query->whereHas('point', function($query) use($projectId){
                                $query->where('project_id', $projectId);
                            });
                        });
                    });
                })->get();

                $haveTest = [];
                if(!$testStations->isEmpty()){
                    foreach($testStations as $testStation){
                        $haveTest[] = [
                            "standard_id" =>$testStation->standard_test_id,
                            "test_id"    =>$testStation->station_work_side_id
                        ];
                    }
                    $newData = collect($haveTest)->unique();

                    // =================> Get Interval <================
                    $project = Project::where('id', $projectId)->first();
                    $interval = $project->interval;

                    $testingBlock = $allTest == 0 ? 0 : $allTest/$interval * 2;
                    $haveTesting = count(collect($haveTest)->unique());

                    $result = $testingBlock == 0 ? 0 : round( (float) $haveTesting / $testingBlock * 100, 2);
                }else{
                    $result = 0;
                }

                foreach ($works as $work) {

                
                    foreach ($stations as $station) {
                        $stationWork = StationWork::where([
                            'station_id' => $station->id,
                            'work_id' => $work->id
                        ])
                            ->with([
                                'sides',
                                'sides.status'
                            ])->first();
                        if ($stationWork) {
                            foreach ($stationWork->sides as $side) {

                                if(!$side->testingRecords->isEmpty()){
                                    // $haveTest++;
                                }
                                // Process
                                $allProcess++;
                                if ($side->progress_status_id == 1) {
                                    $notStartProcess++;                                    
                                }elseif($side->progress_status_id == 2) {
                                    $startProcess++;
                                }else{
                                    $finishProcess++;
                                }
                                
                            }
                        }
                    }

                    $road[] = [
                        'method' => [
                            'id'    => $work->id,
                            'name'  => $work->en_name                       
                        ],
                    ];

                }

                $series = [ 
                            "finishProcess" => number_format( (float)$finishProcess * 100 / $allProcess, 2),
                            "startProcess" => number_format( (float)$startProcess * 100 / $allProcess, 2),
                            "notStartProcess" => number_format( (float)$notStartProcess * 100 / $allProcess, 2)];

                if($project->diagramApprover && $project->technical_approved_datetime){
                    $approval = [
                        'date' => $project->technical_approved_datetime,
                        'by'   => $project->diagramApprover ? $project->diagramApprover->name : null
                    ];
                }else{
                    $approval = null;
                }
                
                $testingQuality = [
                    "testingQuality" => $result,
                    "notestingQuality" => 100 - $result
                ];
                return  [
                    "technical_approved" => $approval,
                    "data"    => [
                        "test_quality" => [
                            'series' => $testingQuality,
                            'labels' => [
                                'បានធ្វើតេស្តរួច',
                                'មិនទាន់បានតេស្ត'
                            ],
                            'colors' => [
                                '#FFB10D',
                                '#FFFFFF'
                            ]
                        ],
                        'process' => [
                            'series' => $series,
                            'labels' => [
                                'បញ្ចប់ការងារ',
                                'កំពុងអនុវត្ត',
                                'មិនទាន់អនុវត្ត',
                            ],
                            'colors' => [
                                '#00FF00',
                                '#90CAF9',
                                '#EEEEEE'
                            ]
                        ],
                    ]
                ];
                
            }else{
                if($project->diagramApprover && $project->technical_approved_datetime){
                    $approval = [
                        'date' => $project->technical_approved_datetime,
                        'by'   => $project->diagramApprover ? $project->diagramApprover->name : null
                    ];
                }else{
                    $approval = null;
                }
                
                return  [
                    "technical_approved" => $approval,
                    "data"    => [
                        'process' => [
                            'series' => [
                                "haveTest" => 0 ,
                                "finishProcess" =>0,
                                "startProcess" =>0, 
                                "notStartProcess"=>100
                            ],
                            'labels' => [
                                'បានធ្វើតេស្តរួច',
                                'បញ្ចប់ការងារ',
                                'កំពុងអនុវត្ត',
                                'មិនទាន់អនុវត្ត',
                            ],
                            'colors' => [
                                '#FFB10D',
                                '#00FF00',
                                '#90CAF9',
                                '#EEEEEE'
                            ]
                        ],
                    ]
                ];
            }
        }else{
            return responseError('មិនមានទិន្ន័យ');
        }
    }
    public function standardTest($projectId = 0, $project = null)
    {
        $works      = Work::select('id', 'kh_name')
        ->whereHas('stationWorks', function($query) use ($projectId){
            $query->whereHas('station', function($query) use ($projectId){
                $query->whereHas('point', function($query) use ($projectId){
                    $query->where('project_id', $projectId);
                });
            });
        })->get();

        $data = [];
        foreach($works as $work){
            $data[] = [
                "name" => $work->kh_name,
                "data" => $this->testingStandardChart($work->id, $projectId)
            ];
        }
        $newData['testing'] = $data;

        return $data;
    }
    private function testingStandardChart($workId = null, $projectId = null)
    {

        $standardTests = ProjectStandardTest::select('id', 'project_id', 'testing_method_id', 'work_id', 'creator_id', 'standard', 'date', 'pk_start', 'pk_end', 'station_start', 'station_end')
        ->with([
            'method' => function ($q) {
                $q->select('id', 'name','type_id','unit')->with(['TestingMethodType'])->get();
            },
            'work' => function ($q) {
                $q->select('id', 'kh_name');
            },
            'creator' => function ($q) {
                $q->select('id', 'name');
            }
        ])->where([
            'project_id' => $projectId,
            'work_id'    => $workId
        ])->orderBy('id', 'DESC')->get();

        $methodID = [];

        foreach($standardTests as $standardTest){
            $methodID[] = $standardTest->method;
        }

        $arr =  collect($methodID)->unique();
        $ids  = array_values($arr->toArray());
        $newData = [];
        
        foreach($ids as $row){
            $standardArr = [];
            foreach($standardTests as $data){
                if($row['id'] == $data->method->id){
                    $standardArr[] = $data;                    
                }
                
            }
            $newData[] = [
                'method_name' => $row['name'],
                'standards'   => $standardArr
            ];
        }
        return $newData;

    }
}
