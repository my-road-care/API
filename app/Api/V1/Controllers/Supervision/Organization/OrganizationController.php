<?php

namespace App\Api\V1\Controllers\Supervision\Organization;

// ==============>> Service <<================
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\Api\V1\Controllers\ApiController;
use Carbon\Carbon;

// ================>> Model <<=================
use App\Model\Supervision\Setup\Organization;
use App\Model\Supervision\Setup\OrganizationType;

class OrganizationController extends ApiController
{

    public function dataSetup()
    {
        $data   = OrganizationType::select('id', 'name')->get();
        return responseSuccess('success', $data, 200);
    }

    public function listing()
    {
        $limit      =   intval(isset($_GET['limit']) ? $_GET['limit'] : 10);
        $type_id    =   isset($_GET['type_id']) ? $_GET['type_id'] : 1;

        $datas = Organization::select('id', 'kh_name', 'en_name', 'abbre','type_id', 'updated_at')
        ->with(['type','MGT.project','MGT.project.status','MGT.project.budgets','MGT.project.year'])
        ->withCount('MGT')
        ->where('type_id', $type_id );
        $res   = $datas->orderBy('id', "DESC")->get();
        $newData = [];
        foreach($res as $row){
            
            $withProjects = [];
            foreach($row->MGT as $newMGT){
                    $withProjects[] = [
                        'id'    => $newMGT->project ? $newMGT->project->id : null,
                        'name'  => $newMGT->project ? $newMGT->project->name : null,
                        'code'  => $newMGT->project ? $newMGT->project->code : null,
                        'status' => $newMGT->project ? $newMGT->project->status : null,
                        'budgets' => $newMGT->project ? $newMGT->project->budgets : null,
                        'year'    => $newMGT->project ? $newMGT->project->year : null
                    ];              
            }
            $newData[] = [
                'id'            => $row->id,
                'kh_name'       => $row->kh_name,
                'en_name'       => $row->en_name,
                'updated_at'    => $row -> updated_at ? date('Y-m-d', strtotime($row -> updated_at )) : null,
                'abbre'         => $row->abbre,
                'project_count' => $row->m_g_t_count,
                'type'          => $row->type,
                'project'       => $withProjects,
            ];
        }
        return response()->json($newData, 200);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kh_name'   => 'required',
            'type_id'   =>  'required|exists:sup_organizations_type,id'
        ], [
            'kh_name.required' => 'សូមបញ្ចូលឈ្មោះអង្ទភាព',
            'type_id.required' => 'សូមជ្រើសរើសប្រភេទអង្គភាព',
            'type_id.exists'   => 'សូមជ្រើសរើសប្រភេទអង្គភាពឲ្យបានត្រឹមត្រូវ',
        ]);

        if ($validator->fails()) {
            return $this->invalidJson(new ValidationException($validator));
        }
        $data = new Organization();
        $data->kh_name  = $request->kh_name;
        $data->en_name  = $request->en_name;
        $data->abbre    = $request->abbre;
        $data->type_id  = $request->type_id;
        $data->save();
        
        $newData = [
            'id'            => $data->id,
            'kh_name'       => $data->kh_name,
            'en_name'       => $data->en_name,
            'abbre'         => $data->abbre,
            'project_count' => $data->m_g_t_count,
            'updated_at'    => $data -> updated_at ? date('Y-m-d', strtotime($data -> updated_at )) : null,
            'type'          => $data->type
        ];
        return responseSuccess('អង្គភាពត្រូវបានបង្កើត', $newData, 200);
    }

    public function delete($id = 0)
    {
        $data = Organization::where('id', $id)->first();
        if ($data) {
            $data->delete();
            return response()->json([
                "message" => "លុបអង្គភាពបានជោគជ៏យ"
            ]);
        }
        return responseError('ទិន្នដែលផ្តល់ឲ្យមិនត្រឹមត្រូវ', 400);
    }

    public function update(Request $request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'kh_name'   => 'required',
            
            'type_id'   =>  'required|exists:sup_organizations_type,id',
        ], [
            'kh_name.required' => 'សូមបញ្ចូលឈ្មោះអង្ទភាព',
            'type_id.required' => 'សូមជ្រើសរើសប្រភេទអង្គភាព',
            'type_id.exists'   => 'សូមជ្រើសរើសប្រភេទអង្គភាពឲ្យបានត្រឹមត្រូវ',
        ]);
        if ($validator->fails()) {
            return $this->invalidJson(new ValidationException($validator));
        }

        $data = Organization::where('id', $id)->withCount('MGT')->first();

        if ($data) {
            $data->kh_name  = $request->kh_name;
            $data->en_name  = $request->en_name;
            $data->abbre    = $request->abbre;
            $data->type_id  = $request->type_id;
            $data->updated_at = Carbon::now();
            $data->save();

            $newData = [
                'id'            => $data->id,
                'kh_name'       => $data->kh_name,
                'en_name'       => $data->en_name,
                'abbre'         => $data->abbre,
                'project_count' => $data->m_g_t_count,
                'updated_at'    => $data -> updated_at ? date('Y-m-d', strtotime($data -> updated_at )) : null,
                'type'          => $data->type
            ];
            return responseSuccess('អង្គភាពត្រូវបានកែប្រែ', $newData, 200);
        }
        return responseError('ទិន្នន័យផ្តល់ឲ្យមិនត្រឹមត្រូវ', 400);
    }
}
