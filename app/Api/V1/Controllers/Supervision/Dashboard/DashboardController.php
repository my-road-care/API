<?php

namespace App\Api\V1\Controllers\Supervision\Dashboard;

use App\Api\V1\Controllers\ApiController;

class DashboardController extends ApiController
{
    public function index()
    {
        $potholes = [
            'all' => 0,
            'complete' => 0
        ];

        $res = [
            'all' => 0,
            'complete' => 0
        ];

        return [
            'project' => $res,
            'pothole' => $potholes
        ];
    }
}
