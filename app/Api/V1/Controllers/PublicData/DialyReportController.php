<?php

namespace App\Api\V1\Controllers\PublicData;
use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use App\MPWT\FileUpload;
use App\Model\InviteFriend\Main as InviteFriend;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use Symfony\Component\Translation\Interval;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Carbon;
use App\Model\Location\Province as Province;

use App\Model\Pothole\Report as Report;
use App\Model\User\Main as User;
use App\Model\Pothole\Comment as Comment;
use Telegram\Bot\Laravel\Facades\Telegram;
class DialyReportController extends ApiController
{
    use Helpers;
   
    function index(){
        $today = Carbon::today()->subDays(1);
        
        // to get amount of របាយការណ៏ សំណើថ្មី Today
        $amount_new_reports = Report::whereNull('deleted_at')
                                ->whereHas('statuses',function($query){
                                    $query->where('status_id', 1);
                                })
                                ->whereDate('created_at', $today)->get()->count();
        // to get amount of របាយការបានការ Today
        $amount_useable = Report::whereNull('deleted_at')
                        ->whereHas('statuses',function($query){
                            $query->where('status_id', 2);
                        })
                        ->whereDate('created_at', $today)->get()->count();
        // to get amount of បានជួសជុលរួច Today
        $amount_fixed  = Report::where('status_id', 4)->whereNull('deleted_at')->whereDate('updated_at', $today)->get()->count();
        // To get amount of fixed by province
        $provincail_reports = $this->getProvincailReportDaily();
        // To get amount of user Today
        $amount_user = User::whereNull('deleted_at')->whereDate('created_at', $today)->get()->count();
        $total_user  = User::whereNull('deleted_at')->get()->count();
        // To get Comment Data
        $amount_user_comment = Comment::whereDate('created_at', $today)->whereHas('commenter', function($query) {
                                    $query->where('type_id', 4);
                                })->count();
        $amount_mo_mt_comment = Comment::whereDate('created_at', $today)->whereHas('commenter', function($query) {
                                    $query->whereIn('type_id', [2,3]);
                                })->count();
        // To get report less than 30 days
        //$reports_less_than_30_days    = Report::select('*')->where('status_id', 2)->whereNull('deleted_at')->where( 'created_at', '<', Carbon::now()->subDays(30))->get()->count();
        $reports_less_than_30_days = 0;
        $provincail_reports_30_less = $this->getProvincail30DaysReport();
        $carbon_today = $today->toDateString();
        $province_reports = $provincail_reports['province'];

        $reports_text = '';
        foreach($province_reports as $row){
            $number = $row['pothole']['fixed'];
            if($number > 0){
                $number = $this->addSpace($row['pothole']['fixed']);
                $reports_text .= ' '.$number.' មន្ទីរ​សក '.$row['data']['name']. "\n";
            }
        }

        $provincail_reports_30_less_retults = $provincail_reports_30_less['province'];
        $reports_30_less_text = '';
        foreach($provincail_reports_30_less_retults as $row){
            $number = $row['pothole']['paddinged'];
            if($number > 0){
                $reports_less_than_30_days += $number;
                $number = $this->addSpace($row['pothole']['paddinged']);
                $reports_30_less_text .= ' '.$number.' មន្ទីរ​សក '.$row['data']['name']. "\n";
            }
        }

        $amount_useable = $this->addSpace($amount_useable, 6);
        $amount_fixed = $this->addSpace($amount_fixed, 6);
        $amount_user = $this->addSpace($amount_user, 6);
        $total_user = $this->addSpace($total_user, 6);
        $amount_user_comment = $this->addSpace($amount_user_comment, 6);
        $amount_mo_mt_comment = $this->addSpace($amount_mo_mt_comment, 6);
        $reports_less_than_30_days = $this->addSpace($reports_less_than_30_days, 6);

        $text = "<pre>$carbon_today \nRoad Care បូកសរុបប្រចាំថ្ងៃ៖  \n\n"
        ."———————————————————————\n"  
        ."ក.​ របាយការណ៍​ (Pothole) \n"
        ."———————————————————————\n"  
        ."-$amount_useable របាយការណ៍​បានការ​ \n"
        ."-$amount_fixed បានជួសជុលរួច \n"
        ."$reports_text \n"
        ."———————————————————————\n"  
        ."ខ.​ ប្រជាពលរដ្ឋ​អ្នកប្រេីប្រាស់ (RU) \n"
        ."———————————————————————\n"  
        ."-$amount_user អ្នក​ (ប្រចាំថ្ងៃ) \n"
        ."-$total_user អ្នក​ (សរុប) \n\n"
        ."———————————————————————\n"  
        ."គ.​ មតិយោបល់​ (Comment) \n"
        ."———————————————————————\n"  
        ."-$amount_user_comment មតិ​ ប្រជាពលរដ្ឋ​ (RU) \n"
        ."-$amount_mo_mt_comment មតិ​ មន្ត្រី​ (MO+MT)​  \n\n"
        ."———————————————————————\n"  
        ."ឃ.​ រង់ចាំចំណាត់ការលេីសពី ៣០ថ្ងៃ \n"
        ."———————————————————————\n"  
        ."-$reports_less_than_30_days របាយការណ៍ (សរុប) \n"
        ."$reports_30_less_text</pre>";
        
        $this->botTelegram($text);

        return [
            // ក.របាយការណ៏
            'amount_new_reports' => $amount_new_reports,
            'amount_usedable' => $amount_useable,
            'amount_fixed'    => $amount_fixed,
            'provincail_reports'    => $provincail_reports,
            // ខ.​ប្រជាពលរដ្ឋ និង​ អ្នកប្រើប្រាស់
            'amount_user'     => $amount_user,
            'total_user'      => $total_user,
            //គ.​ មតិយោបល់​ (Comment)
            'amount_user_comment' => $amount_user_comment,
            'amount_mo_mt_comment' => $amount_mo_mt_comment,
            'reports_less_than_30_days' => $reports_less_than_30_days,
            'provincail_reports_30_less' => $provincail_reports_30_less

        ];
    }

    function weeklyReports(){
        $fromDate = Carbon::today()->subWeek()->startOfWeek();
        $tillDate = Carbon::today()->subWeek()->endOfWeek();
        
        // to get amount of របាយការណ៏ សំណើថ្មី Today
        $amount_new_reports = Report::whereNull('deleted_at')
                                ->whereHas('statuses',function($query){
                                    $query->where('status_id', 1);
                                })
                                ->whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->get()->count();
        // to get amount of របាយការបានការ Today
        $amount_useable = Report::whereNull('deleted_at')
                        ->whereHas('statuses',function($query){
                            $query->where('status_id', 2);
                        })
                        ->whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->get()->count();
        // to get amount of បានជួសជុលរួច Today
        $amount_fixed  = Report::where('status_id', 4)->whereNull('deleted_at')->whereDate('updated_at', '>=', $fromDate)->whereDate('updated_at', '<=', $tillDate)->get()->count();
        // To get amount of fixed by province
        $provincail_reports = $this->getProvincailReportWeekly();
        // To get amount of user Today
        $amount_user = User::whereNull('deleted_at')->whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->get()->count();
        $total_user  = User::whereNull('deleted_at')->get()->count();
        // To get Comment Data
        $amount_user_comment = Comment::whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->whereHas('commenter', function($query) {
                                    $query->where('type_id', 4);
                                })->count();
        $amount_mo_mt_comment = Comment::whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->whereHas('commenter', function($query) {
                                    $query->whereIn('type_id', [2,3]);
                                })->count();
        // To get report less than 30 days
        //$reports_less_than_30_days    = Report::select('*')->where('status_id', 2)->whereNull('deleted_at')->where( 'created_at', '<', Carbon::now()->subDays(30))->get()->count();
        $reports_less_than_30_days = 0;
        $provincail_reports_30_less = $this->getProvincail30DaysReport();
        $carbon_from = $fromDate->toDateString();
        $carbon_till = $tillDate->toDateString();
        $province_reports = $provincail_reports['province'];

        $reports_text = '';
        foreach($province_reports as $row){
            $number = $row['pothole']['fixed'];
            if($number > 0){
                $number = $this->addSpace($row['pothole']['fixed']);
                $reports_text .= ' '.$number.' មន្ទីរ​សក '.$row['data']['name']. "\n";
            }
        }

        $provincail_reports_30_less_retults = $provincail_reports_30_less['province'];
        $reports_30_less_text = '';
        foreach($provincail_reports_30_less_retults as $row){
            $number = $row['pothole']['paddinged'];
            if($number > 0){
                $reports_less_than_30_days += $number;
                $number = $this->addSpace($row['pothole']['paddinged']);
                $reports_30_less_text .= ' '.$number.' មន្ទីរ​សក '.$row['data']['name']. "\n";
            }
        }

        $amount_useable = $this->addSpace($amount_useable, 6);
        $amount_fixed = $this->addSpace($amount_fixed, 6);
        $amount_user = $this->addSpace($amount_user, 6);
        $total_user = $this->addSpace($total_user, 6);
        $amount_user_comment = $this->addSpace($amount_user_comment, 6);
        $amount_mo_mt_comment = $this->addSpace($amount_mo_mt_comment, 6);
        $reports_less_than_30_days = $this->addSpace($reports_less_than_30_days, 6);

        $text = "<pre>[$carbon_from - $carbon_till]\nRoad Care បូកសរុបប្រចាំសប្តាហ៍៖  \n\n"
        ."———————————————————————\n"  
        ."ក.​ របាយការណ៍​ (Pothole) \n"
        ."———————————————————————\n"  
        ."-$amount_useable របាយការណ៍​បានការ​ \n"
        ."-$amount_fixed បានជួសជុលរួច \n"
        ."$reports_text \n"
        ."———————————————————————\n"  
        ."ខ.​ ប្រជាពលរដ្ឋ​អ្នកប្រេីប្រាស់ (RU) \n"
        ."———————————————————————\n"  
        ."-$amount_user អ្នក​ (ប្រចាំសប្តាហ៍) \n"
        ."-$total_user អ្នក​ (សរុប) \n\n"
        ."———————————————————————\n"  
        ."គ.​ មតិយោបល់​ (Comment) \n"
        ."———————————————————————\n"  
        ."-$amount_user_comment មតិ​ ប្រជាពលរដ្ឋ​ (RU) \n"
        ."-$amount_mo_mt_comment មតិ​ មន្ត្រី​ (MO+MT)​  \n\n"
        ."———————————————————————\n"  
        ."ឃ.​ រង់ចាំចំណាត់ការលេីសពី ៣០ថ្ងៃ \n"
        ."———————————————————————\n"  
        ."-$reports_less_than_30_days របាយការណ៍ (សរុប) \n"
        ."$reports_30_less_text</pre>";
        
        $this->botTelegram($text);

        return [
            // ក.របាយការណ៏
            'amount_new_reports' => $amount_new_reports,
            'amount_usedable' => $amount_useable,
            'amount_fixed'    => $amount_fixed,
            'provincail_reports'    => $provincail_reports,
            // ខ.​ប្រជាពលរដ្ឋ និង​ អ្នកប្រើប្រាស់
            'amount_user'     => $amount_user,
            'total_user'      => $total_user,
            //គ.​ មតិយោបល់​ (Comment)
            'amount_user_comment' => $amount_user_comment,
            'amount_mo_mt_comment' => $amount_mo_mt_comment,
            'reports_less_than_30_days' => $reports_less_than_30_days,
            'provincail_reports_30_less' => $provincail_reports_30_less

        ];
    }

    function monthlyReports(){
        $fromDate = Carbon::now()->subMonth()->startOfMonth();
        $tillDate = Carbon::now()->subMonth()->endOfMonth();
        
        // to get amount of របាយការណ៏ សំណើថ្មី Today
        $amount_new_reports = Report::whereNull('deleted_at')
                                ->whereHas('statuses',function($query){
                                    $query->where('status_id', 1);
                                })
                                ->whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->get()->count();
        
        // To get amount of fixed by province
        $provincail_reports = $this->getProvincailReportMonthly();

        // to get amount of របាយការបានការ Today
        $amount_useable = $provincail_reports['sum']['tReports'];
        // to get amount of បានជួសជុលរួច Today
        $amount_fixed   = $provincail_reports['sum']['tFixed'];
        
        // To get Comment Data
        $amount_user_comment = Comment::whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->whereHas('commenter', function($query) {
                                    $query->where('type_id', 4);
                                })->count();
        $amount_mo_mt_comment = Comment::whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->whereHas('commenter', function($query) {
                                    $query->whereIn('type_id', [2,3]);
                                })->count();
        // To get report less than 30 days
        $reports_less_than_30_days    = Report::select('*')->where('status_id', 2)->whereNull('deleted_at')->where( 'created_at', '<', Carbon::now()->subDays(30))->get()->count();
        
        $provincail_reports_30_less = $this->getProvincailMonthlyReport();
        $reports_less_than_30_days = $provincail_reports_30_less['sum']['tReport'];
        $reparing_less_than_30_days = $provincail_reports_30_less['sum']['tPendding'];
        $fixed_less_than_30_days = $provincail_reports_30_less['sum']['tFixed'];

        $provincail_reports_all = $this->getProvincailMonthlyReport(1);
        $reports_all_days = $provincail_reports_all['sum']['tReport'];
        $reparing_all_days = $provincail_reports_all['sum']['tPendding'];
        $fixed_all_days = $provincail_reports_all['sum']['tFixed'];

        $carbon_from = $fromDate->toDateString();
        $carbon_till = $tillDate->toDateString();
        $province_reports = $provincail_reports['province'];

        $reports_text = '';
        foreach($province_reports as $row){
            $reports = $this->addSpace($row['pothole']['reports'], 4);
            if($reports > 0) {
                $fixed = $this->addSpace($row['pothole']['fixed'], 4);
                $repairing = $this->addSpace($row['pothole']['reparing'], 4);
                $percent = $this->addSpace($this->percent($reports, $fixed), 3)."%";
                $appr = $row['data']['abbre'];
                $reports_text .= $reports.' | '.$fixed.' |'.$percent.'| '.$appr. "\n";
            }
           
        }

        $provincail_reports_30_less_retults = $provincail_reports_30_less['province'];
        $reports_30_less_text = '';
        foreach($provincail_reports_30_less_retults as $row){
            $all = $this->addSpace($row['pothole']['all_report'], 6);
            if($all > 0) {
                $repairing = $this->addSpace($row['pothole']['paddinged'], 5);
                $fixed = $this->addSpace($row['pothole']['all_fixed'], 5);
                $percent = $this->addSpace($this->percent($all, $fixed), 3)."%";
                $appr = $row['data']['abbre'];
                $reports_30_less_text .= $all.'|'.$fixed.'|'.$percent.'| '.$appr. "\n";
            }
        }

        $provincail_reports_all_retults = $provincail_reports_all['province'];
        $reports_all_text = '';
        foreach($provincail_reports_all_retults as $row){
            $all = $this->addSpace($row['pothole']['all_report'], 6);
            if($all > 0) {
                $repairing = $this->addSpace($row['pothole']['paddinged'], 5);
                $fixed = $this->addSpace($row['pothole']['all_fixed'], 5);
                $percent = $this->addSpace($this->percent($all, $fixed), 3)."%";
                $appr = $row['data']['abbre'];
                $reports_all_text .= $all.'|'.$fixed.'|'.$percent.'| '.$appr. "\n";
            }
        }

        // To get amount of user Today
        $year = Carbon::today()->year;
        $month = Carbon::today()->month;
        $amount_user = User::whereNull('deleted_at')->whereYear('created_at', $year)->get()->count();
        $total_user  = User::whereNull('deleted_at')->get()->count();
        $each_month_user = '';
        $tmpOldMonth = User::whereNull('deleted_at')->whereYear('created_at', $year-1)->whereMonth('created_at', 12)->get()->count();
        $myArr = [$tmpOldMonth];
        for ($i=1; $i <= $month; $i++) { 
            $tmp = User::whereNull('deleted_at')->whereYear('created_at', $year)->whereMonth('created_at', $i)->get()->count();
            array_push($myArr, $tmp);
        }

        for ($i=$month; $i > 0; $i--) { 
            $tmp1 = $myArr[$i];
            $tmp2 = $myArr[$i-1];
            $tmp = $tmp1 - $tmp2;
            $percent = $this->addSpace($this->percent($tmp2, $tmp),5).'%';
            $each_month_user .= $this->addSpace($tmp1, 6).' |'.$percent.'| '.($i<10?'0'.$i:$i).'-'.$year."\n";
        }

        // $amount_useable = $this->addSpace($amount_useable);
        // $amount_fixed = $this->addSpace($amount_fixed);
        // $amount_user = $this->addSpace($amount_user);
        // $total_user = $this->addSpace($total_user);
        // $amount_user_comment = $this->addSpace($amount_user_comment);
        // $amount_mo_mt_comment = $this->addSpace($amount_mo_mt_comment);
        // $reports_less_than_30_days = $this->addSpace($reports_less_than_30_days);

        $text = "<pre>[$carbon_from - $carbon_till] \nRoad Care បូកសរុបប្រចាំខែ៖  \n\n"
        ."=======================\n"  
        ."ក.​ របាយការណ៍​ (Pothole) \n"
        ."=======================\n"
        ."* ចំនួនជួសជុលរួច ធៀបនឹង ចំនួនថ្មី\n"
        ."———————————————————————\n"
        ."ថ្មី    |  ជួសជុលរួច\n"
        ."———————————————————————\n"
        .$this->addSpace($amount_useable, 4)." | "
        .$this->addSpace($amount_fixed, 4)." |"
        .$this->addSpace($this->percent($amount_useable, $amount_fixed), 3)."%| សរុប\n"
        ."———————————————————————\n"  
        ."$reports_text"
        ."———————————————————————\n\n"  
        ."=======================\n"
        ."ខ.​ រង់ចាំចំណាត់ការលេីសពី ៣០ថ្ងៃ \n"
        ."=======================\n"
        ."  សរុប |  រង់ចាំ     | សរុប\n"
        ."———————————————————————\n"
        .$this->addSpace($reports_less_than_30_days, 6)."|"
        .$this->addSpace($reparing_less_than_30_days, 5)."|"
        .$this->addSpace($this->percent($reports_less_than_30_days, $fixed_less_than_30_days),3)."%| សរុប \n"
        ."$reports_30_less_text"
        ."———————————————————————\n\n"
        ."=======================\n"
        ."គ.​ រង់ចាំចំណាត់ការសរុប \n"
        ."=======================\n"
        ."  សរុប |  រង់ចាំ     | សរុប\n"
        ."———————————————————————\n"
        .$this->addSpace($reports_all_days, 6)."|"
        .$this->addSpace($reparing_all_days, 5)."|"
        .$this->addSpace($this->percent($reports_all_days, $fixed_all_days),3)."%| សរុប \n"
        ."$reports_all_text"
        ."———————————————————————\n\n"
        ."=======================\n"
        ."ឃ.​ មតិយោបល់​ (Comment) \n"
        ."=======================\n"
        ."-".$this->addSpace($amount_user_comment, 6)." មតិ​ ប្រជាពលរដ្ឋ​ (RU) \n"
        ."-".$this->addSpace($amount_mo_mt_comment, 6)." មតិ​ មន្ត្រី​ (MO+MT)​  \n"
        ."———————————————————————\n\n"
        ."=======================\n"
        ."ង.​ ​អ្នកប្រេីប្រាស់ (RU) \n"
        ."=======================\n"
        ."ចំនួនអ្នកប្រើប្រាស់សរុប".$this->addSpace($total_user, 8)."\n"
        ."———————————————————————\n"
        .'   ចំនួន | កំណើនធៀបខែមុន'."\n"
        ."———————————————————————\n"
        .$this->addSpace($amount_user,6).' |      | សរុប'.Carbon::today()->year."\n"
        ."———————————————————————\n"
        .$each_month_user."\n"
        ."</pre>";

        // $text = $text."\n\n".$this->getMonthlyReport($fromDate, $tillDate);
        
        $this->botTelegram($text);

        return [
            // ក.របាយការណ៏
            'amount_new_reports' => $amount_new_reports,
            'amount_usedable' => $amount_useable,
            'amount_fixed'    => $amount_fixed,
            'provincail_reports'    => $provincail_reports,
            // ខ.​ប្រជាពលរដ្ឋ និង​ អ្នកប្រើប្រាស់
            'amount_user'     => $amount_user,
            'total_user'      => $total_user,
            //គ.​ មតិយោបល់​ (Comment)
            'amount_user_comment' => $amount_user_comment,
            'amount_mo_mt_comment' => $amount_mo_mt_comment,
            'reports_less_than_30_days' => $reports_less_than_30_days,
            'provincail_reports_30_less' => $provincail_reports_30_less

        ];
    }

    function botTelegram($text) {
        $website="https://api.telegram.org/bot1367774432:AAH2z9SvA1REy0cKH3xaqReMZkXKs0f5bWI";
        $chatId=430174367;  //Receiver Chat Id 
        $params=[
            'chat_id'=>$chatId,
            'text'=>$text,
            'parse_mode' => 'HTML',
        ];
        $ch = curl_init($website . '/sendMessage');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    function addSpace($value, $space=6) {
        $length = strlen("$value");
        if($space == 3) {
            if($length < 2) {
                return "  $value";
            }
            if($length < 3) {
                return " $value";
            }
            return "$value";
        } 
        if($space == 4) {
            if($length < 2) {
                return "   $value";
            }
            if($length < 3) {
                return "  $value";
            }
            if($length < 4) {
                return " $value";
            }
            return "$value";
        } 
        if($space == 5) {
            if($length < 2) {
                return "    $value";
            }
            if($length < 3) {
                return "   $value";
            }
            if($length < 4) {
                return "  $value";
            }
            if($length < 5) {
                return " $value";
            }
            return "$value";
        } 
        if($space == 6) { 
            if($length < 2) {
                return "     $value";
            }
            if($length < 3) {
                return "    $value";
            }
            if($length < 4) {
                return "   $value";
            }
            if($length < 5) {
                return "  $value";
            }
            if($length < 6) {
                return " $value";
            }
            return "$value";
        }

        if($space == 8) { 
            if($length < 2) {
                return "       $value";
            }
            if($length < 3) {
                return "      $value";
            }
            if($length < 4) {
                return "     $value";
            }
            if($length < 5) {
                return "    $value";
            }
            if($length < 6) {
                return "   $value";
            }
            if($length < 7) {
                return "  $value";
            }
            if($length < 8) {
                return " $value";
            }
            return "$value";
        }
        
    }

    function percent($total, $fixed) {
        if($total == 0) {
            return 0;
        }
        $result = 100*($fixed/$total);
        return intval($result);
    }
    
    function getProvincailReportDaily(){
        $data       =   Province::select('id', 'code', 'name', 'en_name', 'abbre', 'updated_at'); 
        //$data       =   $data->where('id', 12);
        
        $from   =   isset($_GET['from'])?$_GET['from']:"";
        $to     =   isset($_GET['to'])?$_GET['to']:"";

        $myRow = [];
        $province = []; 
        $data= $data->orderBy('code', 'asc')->get();
        $tFixed = 0; 
        foreach( $data as $row ){
            $myRow['data'] = $row; 
           
            $fixed    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->where('status_id', 4)->whereNull('deleted_at')->whereDate('updated_at', Carbon::today()->subDays(1));
            $fixed      = $fixed->count();
           
            $tFixed     += $fixed; 
            
            $myRow['pothole'] = [
                'fixed'         =>  $fixed, 
            ];

            $myRow['fixed'] =  $fixed;

            $province[] = $myRow; 
        }
        
        $sum  = [ 
            'tFixed'        => $tFixed, 
        ];
        $result = array_multisort(array_column($province, 'fixed'), SORT_DESC, $province);;

        return ['province'=>$province, 'sum'=>$sum];
    
    }

    function getProvincailReportWeekly(){
        $fromDate = Carbon::today()->subWeek()->startOfWeek();
        $tillDate = Carbon::today()->subWeek()->endOfWeek();
        $data       =   Province::select('id', 'code', 'name', 'en_name', 'abbre', 'updated_at'); 

        $myRow = [];
        $province = []; 
        $data= $data->orderBy('code', 'asc')->get();
        $tFixed = 0; 
        foreach( $data as $row ){
            $myRow['data'] = $row; 
           
            $fixed    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->where('status_id', 4)->whereNull('deleted_at')->whereDate('updated_at', '>=', $fromDate)->whereDate('updated_at', '<=', $tillDate);
            $fixed      = $fixed->count();
           
            $tFixed     += $fixed; 
            
            $myRow['pothole'] = [
                'fixed'         =>  $fixed, 
            ];

            $myRow['fixed'] =  $fixed;

            $province[] = $myRow; 
        }
        
        $sum  = [ 
            'tFixed'        => $tFixed, 
        ];
        $result = array_multisort(array_column($province, 'fixed'), SORT_DESC, $province);;

        return ['province'=>$province, 'sum'=>$sum];
    
    }

    function getProvincailReportMonthly(){
        $fromDate = Carbon::now()->subMonth()->startOfMonth();
        $tillDate = Carbon::now()->subMonth()->endOfMonth();
        $data       =   Province::select('id', 'code', 'name', 'en_name', 'abbre', 'updated_at'); 

        $myRow = [];
        $province = []; 
        $data= $data->orderBy('code', 'asc')->get();
        $tFixed = 0; 
        $tReparing = 0;
        $tReports = 0;
        foreach( $data as $row ){
            $myRow['data'] = $row; 

            $reports    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->whereNull('deleted_at')->whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->count();
           
            $fixed    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->where('status_id', 4)->whereNull('deleted_at')->whereDate('updated_at', '>=', $fromDate)->whereDate('updated_at', '<=', $tillDate)->count();

            $repairing    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->where('status_id', 2)->whereNull('deleted_at')->whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate)->count();
           
            $tFixed     += $fixed; 
            $tReparing  += $repairing;
            $tReports   += $reports;
            
            $myRow['pothole'] = [
                'fixed'         => $fixed, 
                'reparing'      => $repairing,
                'reports'       => $reports
            ];

            $myRow['fixed']     =  $fixed;
            $myRow['reparing']  =  $repairing;
            $myRow['reports']   =  $reports;

            $province[] = $myRow; 
        }
        
        $sum  = [ 
            'tFixed'        => $tFixed, 
            'tReparing'     => $tReparing, 
            'tReports'       => $tReports,
        ];

        $result = array_multisort(array_column($province, 'fixed'), SORT_DESC, $province);

        return ['province'=>$province, 'sum'=>$sum];
    
    }

    public function getMonthlyReport($fromDate, $tillDate){
        $all_report  = Report::select('id');
        $pending     = Report::select('id')->where('status_id', 1);
        $repairing   = Report::select('id')->where('status_id', 2);
        $declined    = Report::select('id')->where('status_id', 3);
        $fixed       = Report::select('id')->where('status_id', 4);
        $in_planning = Report::select('id')->where('status_id', 5);
        $done        = Report::select('id')->where('status_id', 6);
        $specialist  = Report::select('id')->where('status_id', 7);
        $user        = User::select('id');

        if($fromDate){
            $all_report     = $all_report->whereNull('deleted_at')->whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate);
            $pending        = $pending->whereNull('deleted_at')->whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate);
            $repairing      = $repairing->whereNull('deleted_at')->whereDate('updated_at', '>=', $fromDate)->whereDate('updated_at', '<=', $tillDate);
            $declined       = $declined->whereNull('deleted_at')->whereDate('updated_at', '>=', $fromDate)->whereDate('updated_at', '<=', $tillDate);
            $fixed          = $fixed->whereNull('deleted_at')->whereDate('updated_at', '>=', $fromDate)->whereDate('updated_at', '<=', $tillDate);
            $in_planning    = $in_planning->whereNull('deleted_at')->whereDate('updated_at', '>=', $fromDate)->whereDate('updated_at', '<=', $tillDate);
            $specialist     = $specialist->whereNull('deleted_at')->whereDate('updated_at', '>=', $fromDate)->whereDate('updated_at', '<=', $tillDate);
            // $done           = $fixed->count() + $in_planning->count() + $specialist->count();
            
            $user           = $user->whereNull('deleted_at')->whereDate('created_at', '>=', $fromDate)->whereDate('created_at', '<=', $tillDate);
        }

        $all_report = $all_report->count();
        $pending    = $pending->count();
        $repairing    = $repairing->count();
        $declined    = $declined->count();
        $fixed    = $fixed->count();
        $in_planning    = $in_planning->count();
       
        $specialist    = $specialist->count();
        $user    = $user->count();

        // $text = " $carbon_today Road Care បូកសរុប៖ \n"
        $text = "<strong><u>របាយការណ៍ប្រចាំខែរបស់កម្មវិធីថែទាំផ្លូវ</u></strong> \n\n".
        "<pre>+ Monthly Total Report : ".$all_report."</pre> \n".
        "<pre>  - Pending Reports    : ".$pending."</pre> \n".
        "<pre>  - Reparing Reports   : ".$repairing."</pre> \n".
        "<pre>  - Declined Reports   : ".$declined."</pre> \n".
        "<pre>  - Fixed Reports      : ".($fixed+$in_planning+$specialist)."</pre> \n".
        "<pre>                  Done : ".$fixed."</pre> \n".
        "<pre>           In Planning : ".$in_planning."</pre> \n".
        "<pre>            Specialist : ".$specialist."</pre> \n".
        "<pre>===========================</pre>";

        return $text;
    }

    function getProvincail30DaysReport(){
        $data       =   Province::select('id', 'code', 'name', 'en_name', 'abbre', 'updated_at'); 
        //$data       =   $data->where('id', 12);
        
        $from   =   isset($_GET['from'])?$_GET['from']:"";
        $to     =   isset($_GET['to'])?$_GET['to']:"";

        $myRow = [];
        $province = []; 
        $data= $data->orderBy('code', 'asc')->get();
        $tPendding = 0; 
        foreach( $data as $row ){
            $myRow['data'] = $row; 
           
            $paddinged    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->whereNull('deleted_at')->where('status_id', 2)->where( 'created_at', '<', Carbon::now()->subDays(30));
            $paddinged      = $paddinged->count();
           
            $tPendding     += $paddinged; 
            
            $myRow['pothole'] = [
                'paddinged'         =>  $paddinged, 
            ];

            $myRow['paddinged'] =  $paddinged;

            $province[] = $myRow; 
        }
        
        $sum  = [ 
            'tPendding'        => $tPendding, 
        ];
        $result = array_multisort(array_column($province, 'paddinged'), SORT_DESC, $province);;

        return ['province'=>$province, 'sum'=>$sum];
    
    }

    function getProvincailMonthlyReport($isAll = 0){
        $today = Carbon::now()->subMonth();
        if($isAll == 1) {
            $today = Carbon::now();
        }
        $data       =   Province::select('id', 'code', 'name', 'en_name', 'abbre', 'updated_at'); 
        //$data       =   $data->where('id', 12);
        
        $from   =   isset($_GET['from'])?$_GET['from']:"";
        $to     =   isset($_GET['to'])?$_GET['to']:"";

        $myRow = [];
        $province = []; 
        $data= $data->orderBy('code', 'asc')->get();
        $tPendding = 0; 
        $tAllReport = 0;
        $tFixed = 0;
        foreach( $data as $row ){
            $myRow['data'] = $row; 
           
            $all_report    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->whereNull('deleted_at')->where( 'created_at', '<', $today);
            $all_report      = $all_report->count();

            $paddinged    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->whereNull('deleted_at')->where('status_id', 2)->where( 'created_at', '<', $today);
            $paddinged      = $paddinged->count();

            $all_fixed    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->whereNull('deleted_at')->where('status_id', 4)->where( 'created_at', '<', $today);
            $all_fixed      = $all_fixed->count();
           
            $tPendding      += $paddinged; 
            $tAllReport     += $all_report; 
            $tFixed         += $all_fixed;
            
            $myRow['pothole'] = [
                'paddinged'         =>  $paddinged, 
                'all_report'        =>  $all_report,  
                'all_fixed'         => $all_fixed,
            ];

            $myRow['paddinged'] =  $paddinged;
            $myRow['all_report'] =  $all_report;
            $myRow['all_fixed'] =  $all_fixed;

            $province[] = $myRow; 
        }
        
        $sum  = [ 
            'tPendding'        => $tPendding, 
            'tReport'           => $tAllReport,
            'tFixed'           => $tFixed,
        ];
        $result = array_multisort(array_column($province, 'all_fixed'), SORT_DESC, $province);;

        return ['province'=>$province, 'sum'=>$sum];
    
    }


    public function getReportID($report_id){
        //$decrypted = base64_decode($report_id);
        $decrypted = Crypt::decryptString($report_id);
        return response()->json($decrypted, 200);

    }
}
