<?php

namespace App\Api\V1\Controllers\PublicData;
use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use App\Model\Pothole\Report as Report;
use App\Model\InviteFriend\Main as InviteFriend;
use App\Model\EvaluateCriteria\Main as EvaluateCriteria;
use App\Model\User\Main as User;
use Illuminate\Http\Request;

class Controller extends ApiController
{
    use Helpers;
   
    function reports(){
        
        $data = Report::select('id', 'pothole_id', 'description', 'lat', 'lng', 'member_id', 'commune_id', 'additional_location', 'is_posted', 'status_id', 'created_at')
        ->with([
            'ru:id,user_id',
            'ru.user:id,name,avatar,email,phone',
            'files'=>function($query){
                $query->select('id', 'report_id', 'uri', 'lat', 'lng', 'is_accepted', 'is_fixed_image')->orderBy('id', 'ASC')->orderBy('is_fixed_image', 'DESC');
            }, 
            'comments'=>function($query){
                $query->select('id', 'report_id', 'creator_id', 'comment', 'created_at')
                ->with('commenter:id,name,avatar,email,phone')->orderBy('id', 'DESC')->get();
            }, 
            'pothole'=>function($query){
                $query->select('id', 'point_id', 'code')
                ->with([
                    // 'comments'=>function($query){
                    //     $query->select('id', 'pothole_id', 'creator_id', 'comment', 'created_at')
                    //     ->with('commenter:id,name,avatar,email,phone')->orderBy('id', 'DESC')->get();
                    // }, 

                    'location:id,pothole_id,village_id,commune_id,district_id,province_id,lat,lng', 
                    'location.village:id,name,code', 
                    'location.commune:id,name,code', 
                    'location.district:id,name,code', 
                    'location.province:id,name,code', 


                    'point:id,pk_id,meter', 
                    'point.pk:id,code,road_id',
                    'point.pk.road:id,name,start_point,end_point',

                    
                    //'files:uri,lat,lng'
                   
                ]); 
            }, 

            'status:id,name,kh_name'

           
        ])->withCount('comments as num_of_comments'); 

        $data = $data->where(function($query){
            $query->whereIn('status_id', [4, 5, 7]); 
        });

        $data = $data->orderBy('id', 'desc')->paginate(intval(isset($_GET['limit'])?$_GET['limit']:10));
        return response()->json($data, 200);
    }
    
    function view($id = 0){
        //Fetch authenticated user information
        //$user = JWTAuth::parseToken()->authenticate();
        // $roles = $this->checkUserPosition($user->id); 

        $data = Report::select('id', 'pothole_id', 'description', 'lat', 'lng', 'member_id', 'commune_id', 'additional_location', 'is_posted', 'created_at', 'status_id')
            ->with('status:id,name,kh_name')
        ->with([
            'ru:id,user_id',
            'ru.user:id,name,avatar,email,phone',
            'files'=>function($query){
                $query->select('id', 'report_id', 'uri', 'lat', 'lng', 'is_accepted', 'is_fixed_image')->orderBy('id', 'ASC')->orderBy('is_fixed_image', 'DESC');
            }, 
            'comments'=>function($query){
                $query->select('id', 'report_id', 'creator_id', 'comment', 'created_at')
                ->with('commenter:id,name,avatar,email,phone')->orderBy('id', 'DESC')->get();
            }, 
            'pothole'=>function($query) {
                $query->select('id', 'created_at', 'point_id', 'code', 'maintence_id')
                ->with([
                     'comments'=>function($query){
                        $query->select('id', 'pothole_id', 'creator_id', 'comment', 'created_at')
                        ->with('commenter:id,name,avatar,email,phone')->orderBy('id', 'DESC')->get();
                    }, 

                    'location:id,pothole_id,village_id,commune_id,district_id,province_id,lat,lng', 
                    'location.village:id,name,code', 
                    'location.commune:id,name,code', 
                    'location.district:id,name,code', 
                    'location.province:id,name,code', 


                    'point:id,pk_id,meter', 
                    'point.pk:id,code,road_id',
                    'point.pk.road:id,name,start_point,end_point',

                    'statuses'=>function($query){
                        $query->select('id', 'pothole_id', 'status_id', 'mt_id', 'updater_id', 'comment', 'updated_at')
                        ->with([
                            'status:id,name', 
                            'updater:id,name,avatar,email,phone', 
                            'mt:id,user_id', 
                            'mt.user:id,name,avatar,email,phone'
                        ])
                        ->orderBy('id', 'DESC')->get(); 
                    }, 
                    'files:uri,lat,lng'
                   
                ]); 
            }, 

            'locations'=>function($query){
                $query->select('id', 'report_id', 'village_id', 'distance')
                ->with([
                    'village:id,name,code,commune_id', 
                    'village.commune:id,name,code,district_id',
                    'village.commune.district:id,name,code,province_id', 
                    'village.commune.district.province:id,name,code'
                ]); 
            }, 

            // 'statuses'=>function($query) use ($roles){
            //     $query->select('id', 'report_id', 'status_id', 'mt_id', 'updater_id', 'comment', 'updated_at')
            //     ->with([
            //         'status'=>function($query) use ($roles){
            //             $query->select('id', 'name'); 
            //             if(in_array('mo', $roles)){
            //                 $query->addSelect('mo as permissions'); 
            //             }else if(in_array('mt', $roles)){
            //                 $query->addSelect('mt as permissions'); 
            //             }

            //         }, 
            //         'updater:id,name,avatar,email,phone', 
            //         'mt:id,user_id', 
            //         'mt.user:id,name,avatar,email,phone'
            //     ])
            //     ->orderBy('id', 'DESC')->get(); 
            // }
        ])->withCount('comments as num_of_comments'); 

       
        $data = $data->where('is_posted', 1)
        ->find($id);

        if($data){
            $data['status_code'] = 200; 
            return response()->json($data, 200);
        }else{
           return response()->json([
                'status_code'   =>  403,
                'errors'        =>  ['message'  =>  ['no data found or invalid access']]
            ], 403);
        }
    }

    public function inviteFriend(){
        $data = InviteFriend::first();
        return response()->json($data, 200);
    }

    public function evaluateCriteria(){
        $data = EvaluateCriteria::get();
        return response()->json(
            [
                'status_code' => 200,
                'data' => $data
            ], 200
        );
    }

    public function testTelegram(){
        $website="https://api.telegram.org/bot1367774432:AAH2z9SvA1REy0cKH3xaqReMZkXKs0f5bWI";
        $chatId=-399210318;  //Receiver Chat Id 
        $params=[
            'chat_id'=>$chatID,
            'text'=>'MonoText for Road Care, this is for testing. just ingore it',
        ];
        $ch = curl_init($website . '/sendMessage');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return response()->json(
            [
                'status_code' => 200,
                'data' => 'sendMessage'
            ], 200
        );
    }

    public function monthlyReport(Request $request){
        $all_report  = Report::select('id');
        $pending     = Report::select('id')->where('status_id', 1);
        $repairing   = Report::select('id')->where('status_id', 2);
        $declined    = Report::select('id')->where('status_id', 3);
        $fixed       = Report::select('id')->where('status_id', 4);
        $in_planning = Report::select('id')->where('status_id', 5);
        $done        = Report::select('id')->where('status_id', 6);
        $specialist  = Report::select('id')->where('status_id', 7);
        $user        = User::select('id');
        if($request->month){
            $search = explode("-", $request->month);
            $month = $search[0];
            $year  = $search[1];
            $all_report = $all_report->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);
            $pending    = $pending->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);
            $repairing    = $repairing->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);
            $declined    = $declined->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);
            $fixed    = $fixed->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);
            $in_planning    = $in_planning->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);
            $specialist    = $specialist->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);
            // $done    = $fixed->count() + $in_planning->count() + $specialist->count();
            
            $user    = $user->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);
        }
        $all_report = $all_report->count();
        $pending    = $pending->count();
        $repairing    = $repairing->count();
        $declined    = $declined->count();
        $fixed    = $fixed->count();
        $in_planning    = $in_planning->count();
       
        $specialist    = $specialist->count();
        $user    = $user->count();
        return response()->json([
                'all_report' => $all_report,
                'pending' => $pending,
                'repairing' => $repairing,
                'declined' => $declined,
                // 'fixed' => $fixed,
                // 'in_planning' => $in_planning,
                'done' => "{'fixed':".$fixed.",'in_planning':".$in_planning.",'specialist':".$specialist."}",
                // 'specialist' => $specialist,
                'user' => $user,
        ], 200);
    }

    
    
}
