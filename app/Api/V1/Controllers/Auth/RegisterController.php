<?php

namespace App\Api\V1\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Service\FileServers;
use App\Enum\Role;
use App\Model\User\Main as User;
use App\Model\User\Code;
use App\Model\Member\Main as Member;
use JWTAuth;
use App\MPWT\SMS;


//========================== Use Mail
class RegisterController extends ApiController
{
    use FileServers;

    public function register(Request $request) {

        $this->validate($request, [
            'name'      => 'required|max:60',
            'phone'     =>  [
                            'sometimes', 
                            'required',  
                            'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                            // Rule::unique('user', 'phone')
                        ],
            
            'password'  => 'required|min:6|max:60|confirmed', 

            'socialType'  => [
                            'sometimes', 
                            'required'
                        ],
            'socialId'  => [
                            'sometimes', 
                            Rule::unique('user', 'social_id')
                        ]
        ],  [
                'name.required'         =>  __('register.name-required'),
                
                // 'phone.unique'          =>  __('register.phone-unique'),
                'phone.required'        =>  __('register.phone-required'), 
                'phone.regex'           =>  __('register.phone-regex'),

                'email.required'        =>  __('register.email-required'), 
                'email.email'           =>  __('register.email-email'), 
                'email.max'             =>  __('register.email-max'), 
                'email.unique'          =>  __('register.email-unique'), 

                'password.required'     =>  __('register.password-required'),
                'password.min'          =>  __('register.password-min'),
                'password.max'          =>  __('register.password-max'), 
                'password.confirmed'    =>  __('register.password-confirmed'), 

                'socialId.unique'       =>  __('register.social-id-unique'), 
                'socialType.required'   =>  __('register.social-id-type'), 
        ]);

        $findUser = User::where([
            'phone'             => $request->input('phone')
        ])->first();

        $ranCode = rand(100000, 999999);
        $checkCode = Code::where('code', $ranCode)->first();

        if($findUser){
            if($findUser->is_phone_verified == 0){
                $code = new Code; 
                $code->user_id = $findUser->id; 
                $code->type = 'ACTIVATE';
                $code->is_verified = 0; 

                if($checkCode){
                    $newCode = rand(100000, 999999);
                }else{
                    $newCode = $ranCode;
                }
                $code->code = $newCode;

                $code->save();
    
                SMS::sendSMS($findUser->phone, "សូមប្រើប្រាស់លេខកូដនេះ code : ".$code->code." ដើម្បីផ្ទៀងផ្ទាត់គណនី។ សូមអគុណ!");
                return response()->json([
                    'status_code'   => 200,
                    'message'       => __('register.nexmo-message'),
                ], 200);
            }else{
                return responseError('លេខទូរស័ព្ទនេះត្រូវបានគេយករួចហើយ។ សូមជ្រើសរើសមួយផ្សេងទៀត។', 422);
            }
        }
        

        //====================================>> Create New user
        $user = new User();
        
        $user->type_id          = Role::RU; //RU:Road User
        $user->name             = $request->input('name');
        $user->phone            = $request->input('phone');
        $user->is_active        = 1;
        $user->social_type_id   = 1;
        $user->password         = bcrypt($request->input('password'));

        if($request->avatar){
            // Store Image to File Server
            $image = $this->fileServer($request->file, $request->avatar);
            if($image){
                if ($request->file) {
                    $uri = $image['uuid'];
                } else {
                    $uri = $image['fileURL']['uri'];
                }
            }else{
                $uri= "uploads/TEST/L-1500000px/build/20230320/08/641812c9c1333.jpeg"; 
            }            
            $user->avatar = $uri;
        }
        else{
            $user->avatar = "uploads/TEST/L-1500000px/build/20230320/08/641812c9c1333.jpeg";
        }
        
        if($request->input('socialType') && $request->input('socialId')){
            if($request->input('socialType') == 2 || $request->input('socialType') == 3){
               $user->social_type_id    = $request->input('socialType'); 
               $user->social_id         = $request->input('socialId');
               $user->is_phone_verified = 1;
            }
        }else{
            $this->validate($request, [
                'phone'     =>  [ 
                                'required',  
                                'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                Rule::unique('user', 'phone')
                            ]
            ],  [
                  
                    'phone.unique'          =>  __('register.phone-unique'),
                    'phone.required'        =>  __('register.phone-required'), 
                    'phone.regex'           =>  __('register.phone-regex'),
            ]);
        }

        $user->save();

        //====================================>> Create New RU
        $member                           = new Member();
        $member->user_id                  = $user->id;
        $member->save();

        if($request->input('socialType') != 2 && $request->input('socialType') != 3){
            
            $code = new Code; 
            $code->user_id = $user->id;
            $code->type = 'ACTIVATE';
            $code->is_verified = 0; 
            if($checkCode){
                $newCode = rand(100000, 999999);
            }else{
                $newCode = $ranCode;
            }
            $code->code = $newCode;
            $code->save();

            $sms = SMS::sendSMS($user->phone, "សូមប្រើប្រាស់លេខកូដនេះ code : ".$code->code." ដើម្បីផ្ទៀងផ្ទាត់គណនី។ សូមអគុណ!");
                return response()->json([
                    'status_code'   => 200,
                    'message'       => __('register.nexmo-message'),
                ], 200);
        }else{
            
            return response()->json([
                'token'         => JWTAuth::fromUser($user), 
                'roles'         =>  $this->checkUserPosition($user->id), 

                'status_code'   => 200,
                'message'       => __('register.register-success'),
            ], 200); 
        }
               
    }

   

}
