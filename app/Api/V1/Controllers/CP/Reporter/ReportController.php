<?php

namespace App\Api\V1\Controllers\CP\Reporter;

use App\Api\V1\Controllers\ApiController;
use App\Model\Pothole\Report;


class ReportController extends ApiController
{

    public function list($id = 0)
    {
        $data = Report::select('id', 'pothole_id', 'description', 'lat', 'lng', 'member_id', 'commune_id', 'additional_location', 'is_posted', 'status_id', 'created_at')
            ->with([
                'ru:id,user_id',
                'ru.user:id,name,avatar,email,phone',
                'files' => function ($query) {
                    $query->select('id', 'report_id', 'uri', 'lat', 'lng', 'is_accepted', 'is_fixed_image')->orderBy('id', 'ASC')->orderBy('is_fixed_image', 'DESC');
                },
                'comments' => function ($query) {
                    $query->select('id', 'report_id', 'creator_id', 'comment', 'created_at')
                        ->with('commenter:id,name,avatar,email,phone')->orderBy('id', 'DESC')->get();
                },
                'pothole' => function ($query) {
                    $query->select('id', 'point_id', 'code')
                        ->with([
                            // 'comments'=>function($query){
                            //     $query->select('id', 'pothole_id', 'creator_id', 'comment', 'created_at')
                            //     ->with('commenter:id,name,avatar,email,phone')->orderBy('id', 'DESC')->get();
                            // }, 

                            'location:id,pothole_id,village_id,commune_id,district_id,province_id,lat,lng',
                            'location.village:id,name,code',
                            'location.commune:id,name,code',
                            'location.district:id,name,code',
                            'location.province:id,name,code',


                            'point:id,pk_id,meter',
                            'point.pk:id,code,road_id',
                            'point.pk.road:id,name,start_point,end_point',


                            //'files:uri,lat,lng'

                        ]);
                },

                'status:id,name,kh_name'


            ])
            ->where('member_id', $id)->orderBy('id', 'desc')->get();
        return response()->json($data, 200);
    }
}
