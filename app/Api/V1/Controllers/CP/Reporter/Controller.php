<?php

namespace App\Api\V1\Controllers\CP\Reporter;

use App\Model\Member\Main;
use App\Model\Pothole\Report;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Model\User\Main as User;
use App\Api\V1\Controllers\ApiController;
use App\Api\V1\Http\Collections\CP\Reporter\ReporterCollection;
use App\Api\V1\Http\Collections\CP\Reporter\ReportsCollection;

class Controller extends ApiController
{

    public function list(Request $req)
    {
        $data = Main::select('id', 'created_at', 'user_id')
            ->whereHas('user')
            ->with(['user:id,name,phone,email,avatar,is_phone_verified,is_email_verified,social_type_id', 'user.social:id,name'])
            ->withCount(['reports as n_of_reports']);
        if ($req->key != "") {
            $key = $req->key;
            $data = $data->whereHas('user', function ($query) use ($key) {
                $query->where('name', 'like', '%' . $key . '%')->orWhere('phone', 'like', '%' . $key . '%')->orWhere('email', 'like', '%' . $key . '%');
            });
        }
        if (isValidDate($req->from) && isValidDate($req->to)) {
            $data = $data->whereBetween('created_at', [$req->from . " 00:00:00", $req->to . " 23:59:59"]);
        }
        $data = new ReporterCollection($data->orderBy('n_of_reports', 'desc')->paginate($req->limit ? $req->limit : 10));
        return response()->json($data, Response::HTTP_OK);
    }

    public function view($id = 0)
    {
        $data = Main::select('*')->with(['user', 'user.social:id,name'])->where('id', $id)->first();
        if ($data && $data->user) {
            $user = [
                'id'                => $data->user->id,
                'name'              => $data->user->name,
                'email'             => $data->user->email,
                'phone'             => $data->user->phone,
                'is_phone_verified' => $data->user->is_phone_verified,
                'is_email_verified' => $data->user->is_email_verified,
                'social_type_id'    => $data->user->social_type_id,
                'avatar'            => $data->user->avatar,
                'social'            => $data->user->social
            ];
            return response()->json($user, 200);
        } else {
            return response()->json([
                'status_code'   => 400,
                'message'       => 'មិនមានទិន្នន័យ'
            ], 400);
        }
    }

    public function delete($id = 0)
    {
        $data = User::where('id', $id)->first();
        if (!$data) {
            return response()->json([
                'message' => 'រកមិនឃើញទន្និន័យ',
            ], 400);
        }
        $data->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'ការលុប បានជោគជ័យ',
        ], 200);
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'password'         => 'required|min:6|max:18',
        ]);
        //========================================================>>>> Start to update user
        $user = User::where('id', $request->id)->first();
        if (!$user) {
            return response()->json([
                'message' => 'រកមិនឃើញទន្និន័យ',
            ], 400);
        }
        $user->password = bcrypt($request->input('password'));
        $user->save();
        return response()->json([
            'status' => 'success',
            'message' => 'លេខសម្ងាត់បានកែប្រែជោគជ័យ!'
        ], 200);
    }

    public function reports(Request $req, $id = 0)
    {
        $data = Report::select('id', 'pothole_id', 'description', 'additional_location', 'is_posted', 'status_id', 'created_at')
            ->with([
                'files' => function ($query) {
                    $query->select('id', 'report_id', 'uri', 'lat', 'lng', 'is_accepted', 'is_fixed_image')->orderBy('id', 'ASC');
                },
                'pothole' => function ($query) {
                    $query->select('id', 'point_id', 'code')
                        ->with([
                            'location:id,pothole_id,village_id,commune_id,district_id,province_id,lat,lng',
                            'location.village:id,name',
                            'location.commune:id,name',
                            'location.district:id,name',
                            'location.province:id,name',
                            'point:id,pk_id,meter',
                            'point.pk:id,code,road_id',
                            'point.pk.road:id,name,start_point,end_point',
                        ]);
                }
            ])
            ->where('member_id', $id);
            $data = new ReportsCollection($data->orderBy('id', 'desc')->paginate($req->limit ? $req->limit : 10));
            return response()->json($data, Response::HTTP_OK);
    }
}
