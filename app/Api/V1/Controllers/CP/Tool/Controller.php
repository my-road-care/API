<?php

namespace App\Api\V1\Controllers\CP\Tool;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Illuminate\Http\Request;
use App\Api\V1\Controllers\ApiController;
use App\Model\Location\Province as Main;
use App\Model\Pothole\Report as Report;
use App\Model\User\Main as User;
use App\MPWT\Notification as Notify;
use App\MPWT\SMS;
use Carbon\Carbon;

use App\Model\Authority\MO\Main as MO;
use App\Model\Authority\MT\Main as MT;
use App\Model\Authority\Member\Main as RU;



class Controller extends ApiController
{
    use Helpers;
   
    function mapping(Request $request){
       $type = ['daily']; 
       $today = Carbon::today(); 

       if($today->dayOfWeek == 1){
            $type[] = 'weekly'; 
       }

       if($today == Carbon::now()->startOfMonth()){
            $type[] = 'monthly'; 
       }

        //$test = Carbon::now()->startOfMonth();
       return $today->toArray(); 
    }

    function send(Request $request){
        
        $yesterday = Carbon::yesterday(); 
        $period = [];
        $receivers = []; 
        $textDisplay = ''; 

        if($request->get('period') == 'daily'){
            $period = [Carbon::yesterday()->format('Y-m-d').' 00:00:00', $yesterday.' 23:59:59']; 
            $textDisplay = 'ប្រចាំថ្ងៃ'; 
        }elseif($request->get('period') == 'weekly'){
            $period = [Carbon::yesterday()->subDay(7)->format('Y-m-d').' 00:00:00', $yesterday.' 23:59:59'];
            $textDisplay = 'ប្រចាំសប្តាហ៍'; 
        }elseif($request->get('period') == 'monthly'){
            $period = [Carbon::yesterday()->subDay(30)->format('Y-m-d').' 00:00:00', $yesterday.' 23:59:59']; 
            $textDisplay = 'ប្រចាំខែ'; 
        }elseif($request->get('period') == 'quarterly'){
            $period = [Carbon::yesterday()->subDay(90)->format('Y-m-d').' 00:00:00', $yesterday.' 23:59:59']; 
            $textDisplay = 'ប្រចាំត្រីមាស​'; 
        }elseif($request->get('period') == 'semester'){
            $period = [Carbon::yesterday()->subDay(180)->format('Y-m-d').' 00:00:00', $yesterday.' 23:59:59']; 
            $textDisplay = 'ប្រចាំឆមាស'; 
        }elseif($request->get('period') == 'anually'){
            $period = [Carbon::yesterday()->subDay(365)->format('Y-m-d').' 00:00:00', $yesterday.' 23:59:59']; 
            $textDisplay = 'ប្រចាំឆ្នាំ'; 
        }

        if(count($period) > 0){
            $ways = ['sms']; 
            if(in_array($request->get('way'), $ways)){

                if($request->input('way') == 'sms'){
                    
                    // if($request->input('mo') == true){
                        
                    //     $users = MO::select('id', 'user_id')->with('user:id,name')->whereHas('user', function($query){
                    //         $query->where('is_active', 1); 
                    //     })->get(); 

                    //     foreach($users as $row){
                    //         $receivers[$row->user_id] = $row->user->name;  
                    //     }
                    // }

                    if($request->input('mt') == true){
                        
                        $mts = MT::select('id', 'name', 'user_id', 'province_id')->with('user:id,name,phone')->whereHas('user', function($query){
                            $query->where('is_active', 1); 
                        })->get(); 

                        foreach($mts as $mt){

                            $data = $this->getMTReport($mt, $period); 


                            $msg = "របាយការណ៍".$textDisplay." ".$mt->name." "; 
                            
                            $msg .= " | ស្នើរជួសជុល: ".$data['pending']; 
                            $msg .= " | ជូនMTចាត់ចែង: ".$data['repairing']; 
                            $msg .= " | បដិសេធ​: ".$data['declined']; 
                            $msg .= " | ធ្វើរួច: ".$data['fixed']; 
                            $msg .= " | ដាក់ចូលផែនការឆ្នាំ: ".$data['planning']; 

                            //$sms = SMS::sendSMS($mt->user->phone, $msg);
                            if($mt->id == 12){
                                $sms = SMS::sendSMS('077677599', $msg);
                                $sms = SMS::sendSMS('0965416704', $msg);

                            }
                           
                            $receivers[$mt->id] = $data;


                        }
                    }

                }

            }
        }

       return response()->json([
            'status' => 'success',
            'message' => 'របាយការណ៍ត្រូវបានផ្ញើ!', 
            'data' => $receivers, 
        ], 200);
    }

    function getMTReport($mt, $period){
        
        $pending    = $this->countStatus($mt->province_id, 1, $period); 
        $repairing  = $this->countStatus($mt->province_id, 2, $period); 
        $declined   = $this->countStatus($mt->province_id, 3, $period); 
        $fixed      = $this->countStatus($mt->province_id, 4, $period); 
        $planning   = $this->countStatus($mt->province_id, 5, $period); 
        $specialist = $this->countStatus($mt->province_id, 7, $period); 

        return [
            'total'         =>  $pending+$repairing+$declined+$fixed+$planning+$specialist,
            'pending'       =>  $pending, 
            'repairing'     =>  $repairing, 
            'declined'      =>  $declined, 
            'done'          =>  [
                'total'         =>  $fixed+$planning+$specialist,
                'fixed'         =>  $fixed, 
                'planning'      =>  $planning, 
                'specialist'    =>  $specialist
            ], 
            'fixed'         =>  $fixed, 
            'planning'      =>  $planning, 
            'specialist'    =>  $specialist

        ];
       
       
    }

    function countStatus($provinceId, $statusId, $period){
        
        $data = Report::select('*')
                ->whereHas('pothole', function($query) use ($provinceId){
                    $query->whereHas('location', function($query) use ($provinceId){
                        $query->where('province_id', $provinceId);
                    }); 
                })
                ->where('status_id', $statusId)
                ->whereHas('statuses', function($query) use ($period, $statusId){
                    $query->where('status_id', $statusId)->whereBetween('created_at', $period);
                })->count(); 

        return $data;
    }

}
