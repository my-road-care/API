<?php

namespace App\Api\V1\Controllers\CP\WhatNews;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\Model\WhatNews\Main;

class Controller extends ApiController
{
    use Helpers;
    function list(){
        $data = Main::select('*');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('title', 'like', '%'.$key.'%');
        }

        $from=isset($_GET['from'])?$_GET['from']:"";
        $to=isset($_GET['to'])?$_GET['to']:"";
        if(isValidDate($from)){
            if(isValidDate($to)){
                $appends['from'] = $from;
                $appends['to'] = $to;
                $from .=" 00:00:00";
                $to .=" 23:59:59";
                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }
        $data= $data->orderBy('id', 'desc')->paginate($limit);
        return response()->json($data, 200);
    }

    function view($id = 0){
         if($id!=0){
            $data = Main::select('*')->findOrFail($id);
            if($data){
                return response()->json(['data'=>$data], 200);
            }else{
                return response()->json(['status_code'=>404], 404);
            }
        }
    }

    function post(Request $request){
        $this->validate($request, [
            'title'     	=> 'required', 
            'posted_at'     => 'required', 
        ]);


        $data = new Main;
        $data->title        = $request->title;
        $data->content      = $request->content;
        $data->posted_at    = $request->posted_at;
        $data->created_at   = now();
        $data->updated_at   = now();
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវបានបង្កើត!', 
            'data' => $data
        ], 200);

    }

    function put(Request $request, $id=0){
        $this->validate($request, [
            'title'     	=> 'required', 
            'posted_at'     => 'required', 
        ]);


        $data = Main::find($id);
        if(!$data){
            return response()->json([
                'message' => 'រកមិនឃើញទិន្នន័យ', 
            ], 404);
        }

        $data->title        = $request->title;
        $data->content      = $request->content;
        $data->posted_at    = $request->posted_at;
        $data->updated_at   = now();
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវកែប្រែ!', 
            'data' => $data
        ], 200);

    }
   
    function delete($id=0){
        $data = Main::find($id);
        if(!$data){
            return response()->json([
                'message' => 'រកមិនឃើញទន្និន័យ', 
            ], 404);
        }
        $data->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'ការលុប បានជោគជ័យ',
        ], 200);
    }

   


}
