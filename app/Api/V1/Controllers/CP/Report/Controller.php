<?php

namespace App\Api\V1\Controllers\CP\Report;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Illuminate\Http\Request;
use App\Api\V1\Controllers\ApiController;
use App\Model\Location\Province as Main;
use App\Model\Pothole\Report as Report;
use App\Model\Authority\MT\Main as MT;
use App\Model\User\Main as User;
use App\MPWT\Notification as Notify;
use App\MPWT\SMS;



class Controller extends ApiController
{
    use Helpers;
    function province(){
       

        $data       =   Main::select('id', 'code', 'name', 'en_name', 'abbre', 'updated_at'); 
        //$data       =   $data->where('id', 12);
        
        $from   =   isset($_GET['from'])?$_GET['from']:"";
        $to     =   isset($_GET['to'])?$_GET['to']:"";

        $tPending = 0; 
        $tRepairing = 0; 
        $tDeclinced = 0; 
        $tFixed = 0; 
        $tPlanning = 0; 
        $tSpecialist = 0;
        $myRow = [];
        $province = []; 
        $data= $data->orderBy('code', 'asc')->get();

        foreach( $data as $row ){
            $myRow['data'] = $row; 
           
            $all_report    = Report::select('*')->whereHas('pothole', function($query) use ($row, $from, $to){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            }); 

            $pending    = Report::select('*')->whereHas('pothole', function($query) use ($row, $from, $to){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->where('status_id', 1); 
           

            $repairing    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->where('status_id', 2);

            $declined    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->where('status_id', 3);  

            $fixed    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->where('status_id', 4); 

            $planning    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->where('status_id', 5); 

            $specialist    = Report::select('*')->whereHas('pothole', function($query) use ($row){
                $query->whereHas('location', function($query) use ($row){
                    $query->where('province_id', $row->id);
                }); 
            })->where('status_id', 7);


            if(isValidDate($from)){
                if(isValidDate($to)){

                    
                    $from .=" 00:00:00";
                    $to .=" 23:59:59";

                   
                    $pending    = $pending->whereHas('statuses', function($query) use ($from, $to){
                        $query->where('status_id', 1)->whereBetween('created_at', [$from, $to]); 
                    });
                    $repairing  = $repairing->whereHas('statuses', function($query) use ($from, $to){
                        $query->where('status_id', 2)->whereBetween('created_at', [$from, $to]); 
                    });

                    $declined   = $declined->whereHas('statuses', function($query) use ($from, $to){
                        $query->where('status_id', 3)->whereBetween('created_at', [$from, $to]); 
                    });

                    $fixed      = $fixed->whereHas('statuses', function($query) use ($from, $to){
                        $query->where('status_id', 4)->whereBetween('created_at', [$from, $to]); 
                    });

                    $planning   = $planning->whereHas('statuses', function($query) use ($from, $to){
                        $query->where('status_id', 5)->whereBetween('created_at', [$from, $to]); 
                    });

                }
            }
            $all_report = $all_report->count();
            $pending    = $pending->count();
            $repairing  = $repairing->count();
            $declined   = $declined->count();
            $fixed      = $fixed->count();
            $planning   = $planning->count();
            $specialist = $specialist->count();
           
            $tPending    += $pending; 
            $tRepairing += $repairing; 
            $tDeclinced  += $declined; 
            $tFixed     += $fixed; 
            $tPlanning  += $planning; 
            $tSpecialist += $specialist;
            
            $myRow['pothole'] = [
                'all_report'    => $all_report,    
                'pending'       =>  $pending,
                'repairing'     =>  $repairing,
                'declined'      =>  $declined,
                'fixed'         =>  $fixed, 
                'planning'      =>  $planning,
                'specialist'    => $specialist,
                'done'          => $fixed + $planning + $specialist,
            ];

            $myRow['fixed'] =  $fixed;

            $province[] = $myRow; 
        }
        
        $sum  = [
            'tPending'       =>$tPending, 
            'tRepairing'    => $tRepairing, 
            'tDeclinced'    => $tDeclinced, 
            'tFixed'        => $tFixed, 
            'tPlanning'     => $tPlanning,
            'tSpecialist'   => $tSpecialist, 
        ];
        $result = array_multisort(array_column($province, 'fixed'), SORT_DESC, $province);;

        return ['province'=>$province, 'sum'=>$sum];
    }


    function mts(){
        $data = MT::select('id', 'name', 'user_id', 'province_id')->get(); 
        return response()->json($data, 200);
    }

    function send(Request $request){
        $this->validate($request, [
            'way' => 'required',
            'objs' => 'required|json',
        ]);

        $mts = json_decode($request->input('objs'));
        $data = $this->province(); 
        $res = []; 
        foreach($mts as $mt){
            foreach($data['province'] as $row){
                if($mt->province_id == $row['data']['id']){
                    
                    $user = User::select('phone')->find($mt->user_id);
                    if($user){
                        $msg = "របាយការណ៍ប្រចាំ​ស​ប្តា​ហ៍".$mt->name." "; 

                        if($request->input('from') && $request->input('to')){
                            $msg .= "(".$request->input('from')."-".$request->input('to').")"; 
                        }

                        
                        $msg .= " | ស្នើរជួសជុល: ".$row['pothole']['pending']; 
                        $msg .= " | ជូនMTចាត់ចែង: ".$row['pothole']['repairing']; 
                        $msg .= " | បដិសេធ​: ".$row['pothole']['declined']; 
                        $msg .= " | ធ្វើរួច: ".$row['pothole']['fixed']; 
                        $msg .= " | ដាក់ចូលផែនការឆ្នាំ: ".$row['pothole']['planning'];
                         
                        
                        $sms = SMS::sendSMS($user->phone, $msg);
                        //$sms = SMS::sendSMS('077677599', $msg);
                        // $sms = SMS::sendSMS('0965416704', $msg);
                        $res[] = $user; 
                    }

                        
                }
            }
        }

        return $res; 
        
    }


    function monthly(Request $request){

        if(!$request->month_from || !$request->month_from){
            return response()->json([], 200);
        }
        
        $month_from = explode("-", $request->month_from);
        $month_to   = explode("-", $request->month_to);
        // Check validation 
        if($month_from[1] > $month_to[1]){
            return response()->json([
                'message' => 'Year From can not bigger than Year Month',
                'status_code' => 422
            ], 422);
        }
        if($month_from[1] == $month_to[1] && $month_from[0] > $month_to[0]){
            return response()->json([
                'message' => 'Month From can not bigger than Month To',
                'status_code' => 422
            ], 422);
        }
        // Validation Month Can not bigger than 12
        if( $month_from[0] > 12 || $month_to[0] > 12){
            return response()->json([
                'message' => 'Month can biger than 12',
                'status_code' => 422
            ], 422);
        }

        //Loop Data
        $year = [];
        $months = [];
        $year_start = intval($month_from[1]);
        $year_end = intval($month_to[1]);
        $month_start = intval($month_from[0]);
        $month_end = intval($month_to[0]);
        // Check if year not the same
        if($year_start < $year_end){
            // Loop Year
            for ($x = $year_start; $x <= $year_end; $x++) {
                $year[] = $x;
            }
            $len = count($year);
            foreach($year as $index => $row){
                
                
                if ($index == 0) {
                    for ($x = $month_start; $x <= 12; $x++) {
                        $month[] = ['month' => $x, 'year' => $row ];
                    }
                } else if ($index == $len - 1) {
                    for ($x = 1; $x <= $month_end; $x++) {
                        $month[] = ['month' => $x, 'year' => $row ];
                    }
                }else{
                    for ($x = 1; $x <= 12; $x++) {
                        $month[] = ['month' => $x, 'year' => $row ];
                    } 
                }
            }
            
        }

        //check if year the same 
        if($year_start == $year_end){
            for ($x = $month_start; $x <= $month_end; $x++) {
                $month[] = ['month' => $x, 'year' => $month_to[1] ];
            }
        }

        //Get Data
        $result = [];
        foreach($month as $row){
           
            $all_report  = Report::select('id');
            $pending     = Report::select('id')->where('status_id', 1);
            $repairing   = Report::select('id')->where('status_id', 2);
            $declined    = Report::select('id')->where('status_id', 3);
            $fixed       = Report::select('id')->where('status_id', 4);
            $in_planning = Report::select('id')->where('status_id', 5);
            $done        = Report::select('id')->where('status_id', 6);
            $specialist  = Report::select('id')->where('status_id', 7);
            $user        = User::select('id');
           
            $all_report = $all_report->whereYear('created_at', '=', $row['year'])->whereMonth('created_at', '=', $row['month']);
            $pending    = $pending->whereYear('created_at', '=', $row['year'])->whereMonth('created_at', '=', $row['month']);
            $repairing    = $repairing->whereYear('updated_at', '=', $row['year'])->whereMonth('updated_at', '=', $row['month']);
            $declined    = $declined->whereYear('updated_at', '=', $row['year'])->whereMonth('updated_at', '=', $row['month']);
            $fixed    = $fixed->whereYear('updated_at', '=', $row['year'])->whereMonth('updated_at', '=', $row['month']);
            $in_planning    = $in_planning->whereYear('updated_at', '=', $row['year'])->whereMonth('updated_at', '=', $row['month']);
            $specialist    = $specialist->whereYear('updated_at', '=', $row['year'])->whereMonth('updated_at', '=', $row['month']);
            // $done    = $fixed->count() + $in_planning->count() + $specialist->count();
            
            $user    = $user->whereYear('created_at', '=', $row['year'])->whereMonth('created_at', '=', $row['month']);
            
            $all_report = $all_report->count();
            $pending    = $pending->count();
            $repairing    = $repairing->count();
            $declined    = $declined->count();
            $fixed    = $fixed->count();
            $in_planning    = $in_planning->count();
        
            $specialist    = $specialist->count();
            $user    = $user->count();

            $result[] = [
                            'year' => $row['year'], 
                            'month'=> $row['month'],
                            'all_report' => $all_report,
                            'pending'   => $pending,
                            'repairing' => $repairing,
                            'declined'  => $declined,
                            'done'     => $fixed + $in_planning + $specialist,
                            'fixed'     => $fixed,
                            'in_planning'     => $in_planning,
                            'specialist'     => $specialist,
                            'user'      => $user,
                        ];
        }


        return response()->json($result, 200);
    }



}
