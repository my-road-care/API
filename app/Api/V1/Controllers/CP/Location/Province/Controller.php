<?php

namespace App\Api\V1\Controllers\CP\Location\Province;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Location\Province as Main;
use App\Model\Pothole\Main as Pothole;

use App\Model\User\Main as User;
use Dingo\Api\Routing\Helpers;
use JWTAuth;


class Controller extends ApiController
{
    use Helpers;
    function list(){
       

        $data       =   Main::select('id', 'code', 'name', 'en_name', 'abbre', 'updated_at');
        $key        =   isset($_GET['key'])?$_GET['key']:"";
        
        if( $key != "" ){
            $data = $data->where('name', 'like', '%'.$key.'%')->orWhere('en_name', 'like', '%'.$key.'%')->orWhere('code', 'like', '%'.$key.'%');
        }

      
        $province = []; 
        //$potholes = 
        $data= $data->orderBy('en_name', 'asc')->get();

        foreach( $data as $row ){
            $myRow['data'] = $row; 
           

            $total        = Pothole::select('*')->whereHas('location', function($query) use ($row){
                $query->where('province_id', $row->id);
            })->count(); 

            $requesting    = Pothole::select('*')->whereHas('location', function($query) use ($row){
                $query->where('province_id', $row->id);
            })->where('status_id', 2)->count(); 

            $fixing    = Pothole::select('*')->whereHas('location', function($query) use ($row){
                $query->where('province_id', $row->id);
            })->where('status_id', 4)->count(); 

            $fixed    = Pothole::select('*')->whereHas('location', function($query) use ($row){
                $query->where('province_id', $row->id);
            })->where('status_id', 3)->count(); 

            $rejected    = Pothole::select('*')->whereHas('location', function($query) use ($row){
                $query->where('province_id', $row->id);
            })->where('status_id', 5)->count(); 


         
            $myRow['pothole'] = [
                'total'         =>  $total,
                'requesting'    =>  $requesting,
                'fixing'        =>  $fixing,
                'fixed'         =>  $fixed, 
                'rejected'      =>  $rejected 
            ];

            $province[] = $myRow; 
        }


        return response()->json($province, 200);
    }


    function view($id = 0){
        if($id!=0){
            $data = Main::select('*')->findOrFail($id);
            if($data){
                return response()->json(['data'=>$data], 200);
            }else{
                return response()->json(['status_code'=>404], 404);
            }
        }
    }



    function put(Request $request, $id=0){
        $admin_id = JWTAuth::parseToken()->authenticate()->id;
        $this->validate($request, [
            'code'      => 'required|max:160',
            'name'      => 'required|max:160',
            'en_name'   => 'required|max:160',
            'abbre'     => 'required|max:160',

        ]);

        $data                   = Main::findOrFail($id);
        $data->code             = $request->input('code');
        $data->name             = $request->input('name');
        $data->en_name          = $request->input('en_name');
        $data->abbre            = $request->input('abbre');
        $data->created_at       = now();
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវកែប្រែ!', 
            'data' => $data
        ], 200);
    }

    


}
