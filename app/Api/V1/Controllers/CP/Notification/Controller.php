<?php

namespace App\Api\V1\Controllers\CP\Notification;

use Illuminate\Http\Request;
use App\MPWT\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Notification\Main;
use App\Model\User\Main as User;

use App\Model\Authority\MO\Main as MO;
use App\Model\Authority\MT\Main as MT;
use App\Model\Authority\Member\Main as RU;

use App\MPWT\Notification as Notify;


class Controller extends ApiController
{
    //use Helpers;

    function list(){
        
        $data       =   Main::select('*')->withCount(['receivers as n_of_receivers']); 
        $key        =   isset($_GET['key'])?$_GET['key']:"";
        
        if( $key != "" ){
            $data = $data->where(function($query) use ($key){
                $query->where('topic', 'like', '%'.$key.'%')->orWhere('description', 'like', '%'.$key.'%');
            });    
        }

        $from=isset($_GET['from'])?$_GET['from']:"";
        $to=isset($_GET['to'])?$_GET['to']:"";
        if(isValidDate($from)){
            if(isValidDate($to)){


                $from .=" 00:00:00";
                $to .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }

        $data= $data->orderBy('id', 'desc')->paginate(intval(isset($_GET['limit'])?$_GET['limit']:10));
        return response()->json($data, 200);
    }

    function post(Request $request){

        
        $this->validate($request, [
            'topic'         => 'required|max:50',
            'description'   => 'required|max:150',
            'type'          => 'required|max:150',

            'target'        => 'required|json',
            'way'           => 'required|json',
            'platform'      => 'required|json',

        ]);


        $data                   = new Main();
        $data->topic            = $request->input('topic');
        $data->description      = $request->input('description');
        $data->type             = $request->input('type');
        $data->action             = $request->input('action');
        
        $target          = json_decode($request->input('target'));
        $way             = json_decode($request->input('way'));
        $platform        = json_decode($request->input('platform'));
        $selectedList   = json_decode($request->input('selected_list'));


        if($target){
            $i = 0; 
            foreach($target as $row){
                $data->target .= $row;
                $i++; 
                if($i < count($target)) { $data->target.= "-";} 
            }
        }

        if($way){
             $i = 0; 
            foreach($way as $row){
                $data->way .= $row; 
                 $i++; 
                if($i < count($way)) { $data->way.= "-";} 
            }
        }

        if($platform){
             $i = 0; 
            foreach($platform as $row){
                $data->platform .= $row; 
                $i++; 
                if($i < count($platform)) { $data->platform.= "-";} 
            }
        }

    

         if($selectedList){
             $i = 0; 
            foreach($selectedList as $row){
                $data->selected_list .= $row->id; 
                $i++; 
                if($i < count($selectedList)) { $data->selected_list.= "-";} 
            }
        }


        if($request->input('image')){
            $image = FileUpload::forwardImage2($request->input('image'), 'public/uploads/roadcare/notify/', '100x100');
            if($image != ""){
                $data->image = $image; 
            }
        }



        $data->save();

   
        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវបានបង្កើត!', 
            'data' => $data, 
        ], 200);
    }

    function users(Request $request){
        
        $key = $request->input('key'); 
        if($key!= ""){
            $data = User::select('id','name','phone','email')->where('name', 'like', '%'.$key.'%')->orWhere('phone', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%');
            $data = $data->with(['admin:id,user_id', 'mo:id,user_id', 'mt:id,user_id', 'ru:id,user_id']); 
            $data= $data->orderBy('name', 'asc')->limit(50)->get();
            return response()->json($data, 200);

        }else{
            return response()->json([], 200);
        }
    }

    function send(Request $request){
        
        $id     = $request->input('id'); 
       
        $isTimeForRU   = $request->input('isTimeForRU'); 
        $page   = $request->input('page'); 

        if($id!= 0){
            $receivers = []; 
            $data = Main::select('*')->find($id);

            //Notification
            $dataMessage = [
                    'way'           =>      'firebase', 
                    'title'         =>      $data->topic, 
                    'description'   =>      $data->description, 
                    'type'          =>  $data->type, 
                    'image'         =>  $data->image, 
                    'action'        =>  $data->action, 
                    'action_id'     =>  $data->action_id,
                    'notification_id' => $id
            ]; 

            if($data->target != ""){
                
                $target     = explode("-", $data->target);
                $platform   = explode("-", $data->platform);

                if($target){
                    
                    if(in_array("mo", $target)){

                        $users = MO::select('id', 'user_id')->with('user:id,name')->whereHas('user', function($query) use ($platform){
                            $query->where('is_active', 1); 
                            if($platform){
                                $query->whereIn('device', $platform); 
                            }

                        })->get(); 

                        foreach($users as $row){
                            $receivers[$row->user_id] = $row->user->name;  
                        }
                    }

                    if(in_array("mt", $target)){
                        $users = MT::select('id', 'user_id')->with('user:id,name')->whereHas('user', function($query) use ($platform){
                            $query->where('is_active', 1); 
                            if($platform){
                                $query->whereIn('device', $platform); 
                            }
                        })->get(); 

                        foreach($users as $row){
                            $receivers[$row->user_id] = $row->user->name;  
                        }
                    }

                    if(in_array("su", $target)){
                        $target = explode("-", $data->selected_list);

                        foreach($target as $row){
                            $user = User::select('id', 'name'); 
                            if($platform){
                                $user = $user->whereIn('device', $platform); 
                            }
                            $user = $user->find($row); 
                            if($user){
                                $receivers[$user->id] = $user->name;  
                            }
                        }
                    }

                    if($isTimeForRU != 1){
                        foreach($receivers as $user => $name){
                            $notify = Notify::send($user, $dataMessage);
                        }

                        return response()->json([
                            // 'status' => 'success',
                            //'message' => 'Message has been sent!', 
                            'receivers' => $receivers, 
                            'DataMessage' => $dataMessage, 
                            'next_page' => 1, 
                        ], 200);

                    }else{
                        if(in_array("ru", $target)){

                            if($page > 0){
                                $users = User::select('id', 'name')->where('is_active', 1); 

                                if($platform){
                                    $users = $users->whereIn('device', $platform); 
                                }

                                $users = $users->offset($page*100)->limit(100)->get(); 
                                foreach($users as $user){
                                    if(!in_array($user->id, $receivers)){
                                        $notify = Notify::send($user->id, $dataMessage);
                                    }
                                }
                            }

                            $nOfUsers = User::count();
                            if( ($page+1)*100 > $nOfUsers){
                                $data->is_sent = 1; 
                                $data->sent_at = now(); 
                                $data->save(); 

                                return response()->json([
                                    'status' => 'success',
                                    'message' => 'Message has been sent!', 
                                    'receivers' => $receivers, 
                                    'DataMessage' => $dataMessage
                                ], 200);

                            }else{
                                return response()->json([
                                    'next_page' => $page+1, 
                                ], 200);
                            }

                        }else{
                            $data->is_sent = 1; 
                            $data->sent_at = now(); 
                            $data->save(); 

                            return response()->json([
                                'status' => 'success',
                                'message' => 'Message has been sent!', 
                                'receivers' => $receivers, 
                                'DataMessage' => $dataMessage, 
                                'next_page' => 1, 
                            ], 200);
                        }
                    }
                }
            }
        }else{
            return response()->json([], 200);
        }
    }

    function sending($receivers = [], $dataMessage){
        foreach($receivers as $user => $name){
            $notify = Notify::send($user, $dataMessage);
        }
        
    }
   
}
