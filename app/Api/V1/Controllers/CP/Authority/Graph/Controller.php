<?php

namespace App\Api\V1\Controllers\CP\Authority\Graph;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use App\Model\Authority\Node\Main as Node;

use Dingo\Api\Routing\Helpers;
use JWTAuth;


class Controller extends ApiController
{
    use Helpers;
    function index(){

        $data = Node::get(); 
    	$image = 'http://file.mpwt.gov.kh//public/user/profile.png'; 

    	foreach($data as $row){

        	$nodes[] = [
                    'id'=>$row->id, 
                    'label'=>$row->user->name, 
                    'image'=>$image
                ];
            $edges[] = ['from'=>$row->parent_id, 'to'=>$row->id];
    	}


        

        return [
        	'nodes' => $nodes, 
        	'edges' => $edges
        ]; 

    }

    
}
