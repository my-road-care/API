<?php

namespace App\Api\V1\Controllers\CP\Authority\MT;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\MPWT\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Authority\MT\Main;

use App\Model\User\Main as User;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\Model\Location\Province;
use Illuminate\Validation\ValidationException;

class Controller extends ApiController
{
    use Helpers;
    function list(){
        $data = Main::select('id', 'user_id', 'province_id', 'description', 'name')->with(['user:id,name,phone,email,avatar', 'mos:id,mt_id,mo_id', 'mos.mo:id,name', 'province:id,name'])->withCount('mMos as n_of_mos')->withCount('mRoads as n_of_m_roads');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10);
        $key       =   isset($_GET['key'])?$_GET['key']:"";

        if( $key != "" ){
            $data = $data->whereHas('user', function($query) use ($key){
                $query->where('name', 'like', '%'.$key.'%')->orWhere('phone', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%');
            });
        }

        $from=isset($_GET['from'])?$_GET['from']:"";
        $to=isset($_GET['to'])?$_GET['to']:"";
        if(isValidDate($from)){
            if(isValidDate($to)){
                $appends['from'] = $from;
                $appends['to'] = $to;
                $from .=" 00:00:00";
                $to .=" 23:59:59";
                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }

        $data= $data->orderBy('id', 'desc')->paginate($limit);
        return response()->json($data, 200);
    }


    function province(){
        $data = Province::select('id', 'code', 'name', 'en_name')->get();
        return response()->json($data, 200);
    }

    function view($id = 0){
        if($id!=0){
            $data = Main::select('id', 'user_id', 'province_id', 'description', 'name')->with(['user:id,name,phone,email,avatar'])->withCount('mMos as n_of_mos')->withCount('mRoads as n_of_m_roads')->findOrFail($id);
            if($data){
                return response()->json(['data'=>$data], 200);
            }else{
                return response()->json(['status_code'=>404], 404);
            }
        }
    }

    function post(Request $request){
        $admin_id = JWTAuth::parseToken()->authenticate()->id;
        //===================== Check if phone existing in user
        $existing_user = User::where('phone', $request->phone)->first();
        //===================== To Parent Id
        $parent = Main::select('id', 'parent_id', 'province_id')
                        ->where(['province_id' => $request->province, 'parent_id'=>null])
                        ->first();
        if($existing_user){
            $validator = Validator::make($request->all(),[
                'name'              => 'required|min:4|max:50',
                'email'             => 'required|unique:user,email,'.$existing_user->id,
                'phone'             => 'required|unique:user,phone,'.$existing_user->id,'|regex:/^(\+855|0)[1-9]\d{7,8}$/'
            ],[
                'phone.unique'      =>'លេខទូរស័ព្ទនេះត្រូវបានយករួចហើយ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
                'phone.regex'       =>'លេខទូរស័ព្ទមិនត្រឹមត្រូវ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
                'phone.required'    =>'លេខទូរស័ព្ទត្រូវបានទាមទារ។', 
                'email.required'    =>'បំពេញអ៊ីមែល',
                'email.unique'      =>'អ៊ីមែលត្រូវបានយករួចហើយ។'
            ]);
    
            if ($validator->fails()) {
                return $this->invalidJson(new ValidationException($validator));
            } else {

                $user = User::findOrFail($existing_user->id);
                $user->type_id      = 3;
                //$user->name         = $request->input('name');
                $user->email        = $request->input('email');
                $user->is_email_verified        = 1;
                $user->is_active    = 1;
                $user->password     = bcrypt($request->input('password'));
                $user->updater_id   = $admin_id;
                $user->updated_at     = now();
                if($request->input('avatar')){
                    $avatar = FileUpload::forwardFile($request->input('avatar'), 'mt');
                    if($avatar != ""){
                        $user->avatar = $avatar;
                    }
                }
                $user->save();

                $mt                 = new Main();
                $mt->province_id    = $request->province;
                $mt->parent_id      = $parent->id ?? null;
                $mt->name           = $request->input('mt_name');
                $mt->description    = $request->input('description');
                $mt->user_id        = $user->id;
                $mt->creator_id     = $admin_id;
                $mt->updater_id     = $admin_id;
                $mt->created_at     = now();
                $mt->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'ទិន្នន័យត្រូវបានបង្កើត',
                    'data' => $mt,
                ], 200);
            }

        }else{
            $validator = Validator::make($request->all(),[
                'name'              => 'required|min:4|max:50',
                'email'             => ['required', Rule::unique('user', 'email')],
                'phone'             => ['required', Rule::unique('user', 'phone'),'regex:/^(\+855|0)[1-9]\d{7,8}$/'],
                'password'          => 'required|min:6|max:255'
            ],[
                'phone.unique'      =>'លេខទូរស័ព្ទនេះត្រូវបានយករួចហើយ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
                'phone.regex'       =>'លេខទូរស័ព្ទមិនត្រឹមត្រូវ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
                'phone.required'    =>'លេខទូរស័ព្ទត្រូវបានទាមទារ។', 
                'email.required'    =>'បំពេញអ៊ីមែល',
                'email.unique'      =>'អ៊ីមែលត្រូវបានយករួចហើយ។',
                'password.required' =>'បំពេញពាក្យសម្ងាត់'
            ]);
    
            if ($validator->fails()) {
                return $this->invalidJson(new ValidationException($validator));
            } else {

                $user = new User();
                $user->type_id      = 3;
                $user->name         = $request->input('name');
                $user->phone        = $request->input('phone');
                $user->is_phone_verified        = 1;
                $user->email        = $request->input('email');
                $user->is_email_verified        = 1;
                $user->is_active    = 1;
                $user->password     = bcrypt($request->input('password'));
                $user->creator_id   = $admin_id;
                $user->updater_id   = $admin_id;
                $user->created_at     = now();

                $last = User::select('id')->orderBy('id', 'DESC')->first();
                $id = 0;
                if($last){
                    $id = $last->id+1;
                }
                if($request->input('avatar')){
                    $avatar = FileUpload::forwardFile($request->input('avatar'), 'mt');
                    if($avatar != ""){
                        $user->avatar = $avatar;
                    }
                }
                $user->save();

                $mt                 = new Main();
                $mt->province_id    = $request->province;
                $mt->parent_id      = $parent->id ?? null;
                $mt->name           = $request->input('mt_name');
                $mt->description    = $request->input('description');
                $mt->user_id        = $user->id;
                $mt->creator_id     = $admin_id;
                $mt->updater_id     = $admin_id;
                $mt->created_at     = now();
                $mt->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'ទិន្នន័យត្រូវបានបង្កើត',
                    'data' => $mt,
                ], 200);
            }
        }
    }

    function put(Request $request, $id=0){
        $admin_id = JWTAuth::parseToken()->authenticate()->id;
        $user_id = $request->input('user_id');
        //========================================================>>>> Start to update
        $user = User::findOrFail($user_id);

        $validator = Validator::make($request->all(),[
            'name'              => 'required|min:4|max:50',
            'email'             => 'required|unique:user,email,'.$user_id,
            'phone'             => 'required|unique:user,phone,'.$user_id,'|regex:/^(\+855|0)[1-9]\d{7,8}$/'
        ],[
            'phone.unique'      =>'លេខទូរស័ព្ទនេះត្រូវបានយករួចហើយ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
            'phone.regex'       =>'លេខទូរស័ព្ទមិនត្រឹមត្រូវ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
            'phone.required'    =>'លេខទូរស័ព្ទត្រូវបានទាមទារ។', 
            'email.required'    =>'បំពេញអ៊ីមែល',
            'email.unique'      =>'អ៊ីមែលត្រូវបានយករួចហើយ។'
        ]);

        if ($validator->fails()) {
            return $this->invalidJson(new ValidationException($validator));
        } else {

            $user->type_id      = 2;
            $user->name         = $request->input('name');
            $user->phone        = $request->input('phone');
            $user->email        = $request->input('email');
            $user->is_active    = 1;
            $user->updater_id   = $admin_id;
            $user->created_at     = now();
            if($request->hasFile('avatar')){
                $avatar = FileUpload::forwardFile($request->input('avatar'), 'mt');
                if($avatar != ""){
                    $user->avatar = $avatar;
                }
            }
            $user->save();

            $data = Main::findOrFail($id);
            $data->user_id              = $user_id;
            $data->name                 = $request->input('mt_name');
            $data->description          = $request->input('description');
            $data->province_id           = $request->input('province');
            $data->updated_at           = now();
            $data->save();
            return response()->json([
                'status' => 'success',
                'message' => 'ទិន្នន័យត្រូវកែប្រែ!',
                'data' => $user
            ], 200);
        }
    }

    function delete($id=0){
        $data = Main::find($id);
        if(!$data){
            return response()->json([
                'message' => 'រកមិនឃើញទិន្នន័យ',
            ], 404);
        }
        $data->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវបានលុប!',
        ], 200);
    }

    public function changePassword (Request $request){
        $this->validate($request, [
            'password'         => 'required|min:6|max:18',
        ]);

            //========================================================>>>> Start to update user
            $user = User::findOrFail($request->user_id);
            if(!$user){
                return response()->json([
                    'message' => 'រកមិនឃើញទន្និន័យ',
                ], 404);
            }
            $user->password = bcrypt($request->input('password'));
            $user->save();

            return response()->json([
            'status' => 'success',
            'message' => 'លេខសម្ងាត់បានកែប្រែជោគជ័យ!'
            ], 200);
    }


}
