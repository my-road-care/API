<?php

namespace App\Api\V1\Controllers\CP\Authority\MO;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use DB;

use App\Model\Authority\MO\Main as MO;
use App\Model\Authority\MO\Road as Main;
use App\Model\Road\Main as Road;

class RoadController extends ApiController
{
    use Helpers;
    
     function list($moId = 0){
     	$mo = MO::select('id', 'management_type_id')->with(['mts:id,mo_id,mt_id'])->find($moId); 

     	$data = Road::select('id', 'name')->withCount(['potholes as n_of_potholes'])->with(['mts:id,road_id,mt_id,start_pk,end_pk', 'mts.mt:id,user_id', 'mts.mt.user:id,name']);  

     	if($mo->management_type_id == 2){
     		$data = $data->whereHas('mos', function($query) use ($mo){
     			$query->where('mo_id', $mo->id); 
     		}); 
     	}else{
     		
     		$mts = []; 
            foreach($mo->mts as $row){
                $mts[] = $row->mt_id; 
            }
           //return $mts; 
     		$data = $data->whereHas('mts', function($query) use ($mts){
     			$query->whereIn('mt_id', $mts);
     		}); 
     	}

     	$data = $data->orderBy('id', 'desc')->paginate(intval(isset($_GET['limit'])?$_GET['limit']:10));
	       
        return response()->json($data, 200);
    }

}
