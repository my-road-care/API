<?php

namespace App\Api\V1\Controllers\CP\Authority\MO;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\MPWT\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Authority\MO\Main;

use App\Model\User\Main as User;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use JWTAuth;


class Controller extends ApiController
{
    use Helpers;
    function list(){
       
        $data       =   Main::select('id', 'user_id', 'management_type_id', 'description', 'name')->with(['user:id,name,phone,email,avatar', 'management:id,name'])->withCount('mMinistries as n_of_m_ministries')->withCount('mMts as n_of_mts')->withCount('mRoads as n_of_m_roads');
        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key        =   isset($_GET['key'])?$_GET['key']:"";
        if( $key != "" ){
            $data = $data->whereHas('user', function($query) use ($key){
                $query->where('name', 'like', '%'.$key.'%')->orWhere('phone', 'like', '%'.$key.'%')->orWhere('email', 'like', '%'.$key.'%');
            });
        }

        $ministry       =   isset($_GET['ministry'])?$_GET['ministry']:0;
        if( $ministry != 0 ){
            $data = $data->where('ministry_id', $ministry);
        }
        $from=isset($_GET['from'])?$_GET['from']:"";
        $to=isset($_GET['to'])?$_GET['to']:"";
        if(isValidDate($from)){
            if(isValidDate($to)){
                $appends['from'] = $from;
                $appends['to'] = $to;
                $from .=" 00:00:00";
                $to .=" 23:59:59";
                $data = $data->whereBetween('created_at', [$from, $to]);
            }
        }
        $data= $data->orderBy('id', 'desc')->paginate($limit);
        return response()->json($data, 200);
    }


    function view($id = 0){
        if($id!=0){
            $data = Main::select('id', 'user_id', 'management_type_id', 'description', 'name')->with(['user:id,name,phone,email,avatar'])->withCount('mMinistries as n_of_m_ministries')->withCount('mMts as n_of_mts')->withCount('mRoads as n_of_m_roads')->findOrFail($id);
            if($data){
                return response()->json(['data'=>$data], 200);
            }else{
                return response()->json(['status_code'=>404], 404);
            }
        }
    }

    function post(Request $request){
        $admin_id = JWTAuth::parseToken()->authenticate()->id;

        $validator = Validator::make($request->all(),[
            'name'              => 'required|min:4|max:50',
            'email'             => ['required', Rule::unique('user', 'email')],
            'phone'             => ['required', Rule::unique('user', 'phone'),'regex:/^(\+855|0)[1-9]\d{7,8}$/'],
            'password'          => 'required|min:6|max:255'
        ],[
            'phone.unique'      =>'លេខទូរស័ព្ទនេះត្រូវបានយករួចហើយ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
            'phone.regex'       =>'លេខទូរស័ព្ទមិនត្រឹមត្រូវ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
            'phone.required'    =>'លេខទូរស័ព្ទត្រូវបានទាមទារ។', 
            'email.required'    =>'បំពេញអ៊ីមែល',
            'email.unique'      =>'អ៊ីមែលត្រូវបានយករួចហើយ។',
            'password.required' =>'បំពេញពាក្យសម្ងាត់'
        ]);

        if ($validator->fails()) {
            return $this->invalidJson(new ValidationException($validator));
        } else {

            $user = new User();
            $user->type_id      = 2;
            $user->name         = $request->input('name');
            $user->phone        = $request->input('phone');
            $user->is_phone_verified        = 1;
            $user->email        = $request->input('email');
            $user->is_email_verified        = 1;
            $user->is_active    = 1;
            $user->password     = bcrypt($request->input('password'));
            $user->creator_id   = $admin_id;
            $user->updater_id   = $admin_id;
            $user->created_at     = now();

            $last = Main::select('id')->orderBy('id', 'DESC')->first();
            $id = 0;
            if($last){
                $id = $last->id+1;
            }

            if($request->input('avatar')){
                $avatar = FileUpload::forwardFile($request->input('avatar'), 'mo');
                if($avatar != ""){
                    $user->avatar = $avatar; 
                }
            }
            $user->save();

            $mo                      = new Main();
            $mo->user_id             = $user->id;
            $mo->management_type_id  = $request->management_type_id;
            $mo->name                = $request->input('mo_name');
            $mo->description         = $request->input('description');
            $mo->creator_id          = $admin_id;
            $mo->updater_id          = $admin_id;
            $mo->created_at          = now();
            $mo->save();

            return response()->json([
                'status' => 'success',
                'message' => 'ទិន្នន័យត្រូវបានបង្កើត', 
                'data' => $mo, 
            ], 200);
        }
    }

    function put(Request $request, $id=0){
        $admin_id = JWTAuth::parseToken()->authenticate()->id;
        $user_id = $request->input('user_id');

        $validator = Validator::make($request->all(),[
            'name'              => 'required|min:4|max:50',
            'email'             => 'required|unique:user,email,'.$user_id,
            'phone'             => 'required|unique:user,phone,'.$user_id,'|regex:/^(\+855|0)[1-9]\d{7,8}$/'
        ],[
            'phone.unique'      =>'លេខទូរស័ព្ទនេះត្រូវបានយករួចហើយ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
            'phone.regex'       =>'លេខទូរស័ព្ទមិនត្រឹមត្រូវ។ សូមព្យាយាមមួយផ្សេងទៀត។', 
            'phone.required'    =>'លេខទូរស័ព្ទត្រូវបានទាមទារ។', 
            'email.required'    =>'បំពេញអ៊ីមែល',
            'email.unique'      =>'អ៊ីមែលត្រូវបានយករួចហើយ។'
        ]);

        if ($validator->fails()) {
            return $this->invalidJson(new ValidationException($validator));
        } else {

            
            //========================================================>>>> Start to update
            $user = User::findOrFail($user_id);
            $user->type_id      = 2;
            $user->name         = $request->input('name');
            $user->phone        = $request->input('phone');
            $user->email        = $request->input('email');
            $user->is_active    = 1;
            $user->updater_id   = $admin_id;
            $user->created_at     = now();
            
            if($request->hasFile('avatar')){
                $avatar = FileUpload::forwardFile($request->input('avatar'), 'mo');
                if($avatar != ""){
                    $user->avatar = $avatar; 
                }
            }
            
            
            $user->save();

            $data = Main::findOrFail($id);
            $data->user_id              = $user_id;
            $data->name                 = $request->input('mo_name');
            $data->description          = $request->input('description');
            $data->management_type_id   = $request->input('management');
            $data->updated_at           = now();
            $data->save();
            return response()->json([
                'status' => 'success',
                'message' => 'ទិន្នន័យត្រូវកែប្រែ!', 
                'data' => $data
            ], 200);
        }
    }

    function delete($id=0){
        $data = Main::find($id);
        if(!$data){
            return response()->json([
                'message' => 'រកមិនឃើញទិន្នន័យ', 
            ], 404);
        }
        $data->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវបានលុប!',
        ], 200);
    }

    
    public function changePassword (Request $request){
        $this->validate($request, [
            'password'         => 'required|min:6|max:18',
        ]);

            //========================================================>>>> Start to update user
            $user = User::findOrFail($request->user_id);
            if(!$user){
                return response()->json([
                    'message' => 'រកមិនឃើញទន្និន័យ', 
                ], 404);
            }    
            $user->password = bcrypt($request->input('password'));
            $user->save();

            return response()->json([
            'status' => 'success',
            'message' => 'លេខសម្ងាត់បានកែប្រែជោគជ័យ!'
            ], 200);
    }

}
