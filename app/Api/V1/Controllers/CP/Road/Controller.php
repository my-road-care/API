<?php

namespace App\Api\V1\Controllers\CP\Road;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use App\Model\Road\Main;
use Illuminate\Support\Facades\Cache;

class Controller extends ApiController
{
    use Helpers;
    
    function list(){
        //return $id;
        
        $data      =    Main::select('id', 'name', 'status', 'start_point', 'end_point', 'is_point_migrated', 'length', 'updated_at')
        ->with([
            'provinces:id,road_id,province_id', 
            'provinces.province:id,abbre,name,en_name', 
            'ministries'=>function($query){
                $query->select('ministry_id', 'road_id')->distinct()
                ->with(['ministry:id,abbre']);
            }
        ])
        ->withCount([
            'mos as n_of_mos', 
            'mts as n_of_mts', 
            'pks as n_of_pks',
            'projects as n_of_project'
        ]); 

        $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key        =   isset($_GET['key'])?$_GET['key']:"";
       
        if( $key != "" ){
            $data = $data->where(function($query) use ($key){
                $query->where('name', 'like', '%'.$key.'%');
            });
            
        }

        // $from=isset($_GET['from'])?$_GET['from']:"";
        // $to=isset($_GET['to'])?$_GET['to']:"";
        // if(isValidDate($from)){
        //     if(isValidDate($to)){
              
        //         $from .=" 00:00:00";
        //         $to .=" 23:59:59";
        //         $data = $data->whereBetween('created_at', [$from, $to]);
        //     }
        // }

        $data= $data->orderBy('name', 'asc')->orderBy('is_point_migrated', 'desc')->paginate($limit);
        return response()->json($data, 200);
    }

    function listRoadMap(){
        $cache_key = 'listRoadMap';
        $data = Cache::get($cache_key, function() use ($cache_key) {
            // get roads
            $roads      =    Main::select('id', 'name', 'start_point', 'end_point', 'length')
            ->with([
                'provinces:id,road_id,province_id', 
                'provinces.province:id,abbre,name,en_name', 
            ])
            ->where('status', 1)
            ->orderBy('name', 'asc')->get();
            
            // group by main road
            $main_roads = [];
            foreach ($roads as $road) {
                // assign road stations
                $road->stations = $road->provinces->map(function($obj) {
                    return $obj->province->name;
                });

                // filter main road
                if(strlen($road->name) === 1) {
                    $road->subRoads = [];
                    $main_roads[$road->name] = $road;
                }
                // filter sub roads
                else {
                    $road->check = 0;
                    $main_road = $main_roads[$road->name[0]];
                    $subRoads = $main_road->subRoads;
                    array_push($subRoads, $road);
                    $main_road->subRoads = $subRoads;
                }
            }
            $data = json_encode(array_values($main_roads));
            Cache::put($cache_key, $data, now()->addMinutes(5));
            return $data;
        });
        return responseSuccess('success', json_decode($data, true));
    }

    function updateStatus(Request $request ,$roadId = 0){
        $road = Main::where('id', $roadId)->first();
        $road->update([
            'status' => $request->status
        ]);
        return responseSuccess('បានកែប្រែទិន្ន័យ');
    }

    function view($id = 0){
        if($id!=0){
             $data      =    Main::select('id', 'name', 'start_point', 'end_point', 'is_point_migrated', 'length', 'updated_at')
            ->with([
                'provinces:id,road_id,province_id', 
                'provinces.province:id,abbre', 
                'ministries'=>function($query){
                    $query->select('ministry_id', 'road_id')->distinct()
                    ->with(['ministry:id,abbre']);
                }
            ])
            ->withCount(['mos as n_of_mos', 'mts as n_of_mts', 'parts as n_of_parts', 'pks as n_of_pks'])
            ->find($id);

            if($data){
                return response()->json(['data'=>$data], 200);
            }else{
                return response()->json(['status_code'=>404], 200);
            }
        }
    }

    // function post(Request $request){
    //     $admin_id = JWTAuth::parseToken()->authenticate()->id;

    //     $this->validate($request, [ 
    //         'start_point'   => 'required|max:160',
    //         'end_point'     => 'required|max:160',
    //     ]);

   
    //     $data = new Main();
    //     $data->start_point                      = $request->input('start_point');
    //     $data->end_point                        = $request->input('end_point');

    //     $data->save();
    //     return response()->json([
    //         'status' => 'success',
    //         'message' => 'ទិន្នន័យត្រូវបានបង្កើត!', 
    //         'data' => $data, 
    //     ], 200);
    // }

    function put(Request $request, $id=0){
        $this->validate($request, [
           
            'start_point'   => 'required|max:160',
            'end_point'     => 'required|max:160',
        ]);

        $data = Main::findOrFail($id);
        $data->start_point  = $request->input('start_point');
        $data->end_point    = $request->input('end_point');
        $data->updated_at   = now(); 
        $data->save();
        
        return response()->json([
            'status' => 'success',
            'message' => 'ទិន្នន័យត្រូវកែប្រែ!', 
            'data' => $data
        ], 200);
    }

    // function delete($id=0){
    //     $data = Main::find($id);
    //     if(!$data){
    //         return response()->json([
    //             'message' => 'រកមិនឃើញទិន្នន័យ', 
    //         ], 404);
    //     }
    //     $data->delete();
    //     return response()->json([
    //         'status' => 'success',
    //         'message' => 'ទិន្នន័យត្រូវបានលុប!',
    //     ], 200);
    // }

}
