<?php

namespace App\Api\V1\Controllers\CP\Road;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Road\PK as Main;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Cache;
use JWTAuth;


class PKController extends ApiController
{
    use Helpers;
    function list($roadId = 0)
    {
        $data       =   Main::select('id', 'code')->where('road_id', $roadId)
            ->with([
                'points' => function ($query) {
                    $query->select('id', 'pk_id', 'meter', 'latlng')->withCount(['potholes as n_of_potholes'])->orderBy('meter', 'ASC');
                    $wp = isset($_GET['with_potholes']) ? $_GET['with_potholes'] : '';

                    if ($wp === "1") {
                        $query->with(['potholes:id,point_id', 'potholes.files']);
                    }
                },
                'parts' => function ($query) {
                    $query->select('pk_id', 'part_id')->distinct()
                        ->with(['part:id,object_id']);
                }
            ])
            ->withCount([
                'points as n_of_points',
                'potholes as n_of_potholes'
            ]);

        $key        =   isset($_GET['key']) ? $_GET['key'] : "";
        if ($key != "") {
            $key = str_replace("PK", "", $key);
            $key = str_replace("pk", "", $key);
            $key = str_replace(" ", "", $key);
            $key = $data->where(function ($query) use ($key) {
                $query->where('code', 'like', '%' . $key . '%');
            });
        }

        $limit      =   intval(isset($_GET['limit']) ? $_GET['limit'] : 100);
        $data = $data->orderBy('code', 'asc')->paginate($limit);

        // convert point's latlng string to json object
        foreach ($data as $road) {
            foreach ($road->points as $point) {
                $point->latlng = json_decode($point->latlng);
            }
        }
        $data = json_encode($data);
        return response()->json(json_decode($data, true), 200);
    }

    function pks_on_map($roadId = 0)
    {
        $cache_key = "pks_on_map_$roadId";
        $data = Cache::get($cache_key, function () use ($roadId, $cache_key) {

            $road_pks       =   Main::select('id', 'code')->where('road_id', $roadId)
                ->with([
                    'points' => function ($query) {
                        $query->select('id', 'pk_id', 'meter', 'latlng')->orderBy('meter', 'ASC');
                    }
                ])->orderBy('code', 'asc')->get();

            // convert point's latlng string to json object
            foreach ($road_pks as $road_pk) {
                foreach ($road_pk->points as $point) {
                    $point->latlng = json_decode($point->latlng);
                }
            }

            $last_pk = $road_pks[count($road_pks) - 1];
            $data = (object)[];
            $data->road_id = $roadId;
            $data->road_pks = $road_pks;
            $data->origin = $road_pks[0]->points[0]->latlng;
            $data->destination = $last_pk->points[count($last_pk->points) - 1]->latlng;
            $data = json_encode($data);
            Cache::put($cache_key, $data, now()->addMinutes(5));
            return $data;
        });
        return response()->json(json_decode($data, true), 200);
    }
}
