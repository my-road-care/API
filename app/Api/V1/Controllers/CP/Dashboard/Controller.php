<?php

namespace App\Api\V1\Controllers\CP\Dashboard;

use Illuminate\Http\Request;

use App\Api\V1\Controllers\ApiController;
use App\Model\Authority\MO\Main as MO;
use App\Model\Authority\MT\Main as MT;
use App\Model\Member\Main as Member;
use App\Model\Pothole\Main as Pothole;
use App\Model\Pothole\Report as Report;
use App\Model\User\Main as User;

class Controller extends ApiController
{
    //use Helpers;

    public function getData(Request $request)
    {

        // =============>> Count MO <<=================
        $mo             = MO::query();
        $mo->when($request->year, function($query) use($request){
            $query->whereRaw('YEAR(created_at) = ?', [$request->year]);
        });
        $countMO = $mo->count();

        // ==============>> Count MT <<===============
        $mt             = MT::query(); 
        $mt->when($request->year, function($query) use($request){
            $query->whereRaw('YEAR(created_at) = ?', [$request->year]);
        });
        $countMT = $mt->count();

        // =============>> Count Member <<=============
        $member         = User::query();
        $member->when($request->year, function($query) use($request){
            $query->whereRaw('YEAR(created_at) = ?', [$request->year]);
        });
        $countMember = $member->count();

        // =============>> Count Pothole <<=============
        $pothole        = Report::query(); 
        $pothole->when($request->year, function($query) use($request){
            $query->whereRaw('YEAR(created_at) = ?', [$request->year]);
        });
        $countPothole = $pothole->count();

        // =============>> Count Pendding <<==============
        $pending        = Report::query();
        $pending->where('status_id', 1);
        $pending->when($request->year, function($query) use($request){
            $query->whereRaw('YEAR(created_at) = ?', [$request->year]);
        });
        $countPending = $pending->count();

        // =============>> Count Repairing <<=============
        $repairing      = Report::query();
        $repairing->where('status_id', 2);
        $repairing->when($request->year, function($query) use($request){
            $query->whereRaw('YEAR(created_at) = ?', [$request->year]);
        });
        $countRepairing = $repairing->count();

        // =============>> Count Declined <<==============
        $declined       = Report::query();
        $declined->where('status_id', 3); 
        $declined->when($request->year, function($query) use($request){
            $query->whereRaw('YEAR(created_at) = ?', [$request->year]);
        });
        $countDeclined = $declined->count();

        // =============>> Count Fixed <<==============
        $fixed          = Report::query();
        $fixed->where('status_id', 4);
        $fixed->when($request->year, function($query) use($request){
            $query->whereRaw('YEAR(created_at) = ?', [$request->year]);
        });
        $countFixed = $fixed->count();

        // =============>> Count Planning <<============
        $planning       = Report::query();
        $planning->where('status_id', 5);
        $planning->when($request->year, function($query) use($request){
            $query->whereRaw('YEAR(created_at) = ?', [$request->year]);
        });
        $countPlanning = $planning->count();

        // =============>> Count Specialist <<===============
        $specialist       = Report::query();
        $specialist->where('status_id', 7);
        $specialist->when($request->year, function($query) use($request){
            $query->whereRaw('YEAR(created_at) = ?', [$request->year]);
        });
        $countSpecialist = $specialist->count();

        // =============>> Count All Done <<================
        $all_done       = $countFixed + $countPlanning + $countSpecialist;

        $percentagePending = 0;
        $percentageDeclined = 0;
        $percentageRepairing = 0;
        $percentageAllDone = 0;
        if ($countPothole > 0) {
            if ($countPending > 0) $percentagePending = ($countPending / $countPothole) * 100;
            if ($countDeclined > 0) $percentageDeclined = ($countDeclined / $countPothole) * 100;
            if ($countRepairing > 0) $percentageRepairing = ($countRepairing / $countPothole) * 100;
            if ($all_done > 0) $percentageAllDone = ($all_done / $countPothole) * 100;
            // Round the percentages to two decimal places
            $percentagePending = round($percentagePending, 2);
            $percentageDeclined = round($percentageDeclined, 2);
            $percentageRepairing = round($percentageRepairing, 2);
            $percentageAllDone = round($percentageAllDone, 2);
        }
        $percentageFixed = 0;
        $percentagePlanning = 0;
        $percentageSpecialist = 0;
        if ($all_done > 0) {
            if ($countFixed > 0) $percentageFixed = ($countFixed / $all_done) * 100;
            if ($countPlanning > 0) $percentagePlanning = ($countPlanning / $all_done) * 100;
            if ($countSpecialist > 0) $percentageSpecialist = ($countSpecialist / $all_done) * 100;
            // Round the percentages to two decimal places
            $percentageFixed = round($percentageFixed, 2);
            $percentagePlanning = round($percentagePlanning, 2);
            $percentageSpecialist = round($percentageSpecialist, 2);
        }

        $res = [
            'mo'            => $countMO,
            'mt'            => $countMT,
            'member'        => $countMember,
            'pothole'       => $countPothole,
            'pending'       => [
                'total'     => $countPending,
                'percent'   => $percentagePending
            ],
            'repairing'     => [
                'total'     => $countRepairing,
                'percent'   => $percentageRepairing
            ],
            'declined'      => [
                'total'     => $countDeclined,
                'percent'   => $percentageDeclined
            ],
            'fixed'         => [
                'total'     => $countFixed,
                'percent'   => $percentageFixed
            ],
            'planning'      => [
                'total'     => $countPlanning,
                'percent'   => $percentagePlanning
            ],
            'specialist'    => [
                'total'     => $countSpecialist,
                'percent'   => $percentageSpecialist
            ],
            'all_done'      => [
                'total'     => $all_done,
                'percent'   => $percentageAllDone
            ],
        ];
        return responseSuccess("Success", $res);
    }
}
