<?php

namespace App\Api\V1\Controllers\CP\Migrate;

use Illuminate\Http\Request;
use App\Api\V1\Controllers\ApiController;

use App\Model\Pothole\Main as Pothole;
use App\Model\Pothole\Report as Report;
use App\Model\Pothole\ReportStatus as ReportStatus;


class ReportController extends ApiController
{
   

   
    function updateReport(){
        $potholes = Pothole::select('id')->with(['statuses:id,pothole_id,status_id,mt_id,updater_id,created_at', 'reports:id,pothole_id'])->where('status_id', null)->get();
       
        foreach($potholes as $pothole){
            $pothole->status_id = 1; 
            $pothole->save(); 

            foreach($pothole->reports as $report){
                $myReport = Report::find($report->id); 
                foreach($pothole->statuses as $status){

                    $reportStatus               = New ReportStatus; 
                    $reportStatus->report_id    = $report->id; 
                    $reportStatus->status_id    = $status->status_id; 
                    $reportStatus->mt_id        = $status->mt_id; 
                    $reportStatus->updater_id   = $status->updater_id; 
                    $reportStatus->created_at   = $status->created_at; 
                    $reportStatus->save(); 

                    $myReport->status_id = $status->status_id; 

                } 
                $myReport->save();    
            }
        } 
        return $potholes; 
    }
  

}
