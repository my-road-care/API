<?php

namespace App\Api\V1\Controllers\CheckToken;
use App\Api\V1\Controllers\ApiController;
use JWTAuth;
use App\MPWT\CheckExpiredToken;
class Controller extends ApiController
{
   
    function checkToken(){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            //return $user;
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['status' => 'Token is Invalid']);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['response_code' => '403', 'response_msg' => 'EXIPIRED_TOKEN']);
            }else{
                return response()->json(['status' => 'Authorization Token not found']);
            }
        }
        $token = '';
        $token = CheckExpiredToken::check();
        return response()->json(['response_code' => '200', 'response_msg' => 'TOKEN_TRUE', 'token' => $token]);
    }
    
    
}
