<?php

namespace App\Api\V1\Controllers\Bot;
use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;

use TelegramBot;

class Controller extends ApiController
{
    use Helpers;
   
    function webhook(){
         return response()->json([], 200);
    }
    
    function send(){
        
        $response = TelegramBot::sendMessage([
            'chat_id' => 229388689, 
            'text' => '<b>New Logged In Information</b>: You have logged to system with following information,
                    IP Address: <code>123</code>,
                    Date and Time: '.now().'
                    Please review it something went wrong. Thanks
                    ',
            'parse_mode' => 'HTML'
        ]);

        return response()->json([], 200);
    }
   
    
}
