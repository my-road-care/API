<?php

namespace App\Api\V1\Controllers\MO\Dashboard;

use Illuminate\Http\Request;

use App\Api\V1\Controllers\ApiController;
use App\Model\Pothole\Main as Pothole;
use App\Model\Authority\MT\Main as MT;
use App\Model\Authority\MO\Road as MORoad;
use JWTAuth;

class Controller extends ApiController
{
    //use Helpers;

    function getData(){
        $user   = JWTAuth::parseToken()->authenticate();
        $mts    = MT::where('deleted_at', Null); 
       
        $pothole        = Pothole::whereHas('action', function($query) use ($user){
            $query->whereHas('assigner', function($query) use ($user){
                $query->where('user_id', $user->id);
            });
        }); 

        $newPothole        = Pothole::where('status_id', 1); 
        if($user->mo){
            if($user->mo->management_type_id == 2){ //by road
                //Select all roads that manage by this MO
                $moRoadData = MORoad::select('road_id', 'start_pk', 'end_pk')->where('mo_id', $user->mo->id)->get(); 
                $moRoads = []; 
                foreach($moRoadData as $row){
                   $moRoads[] = $row->road_id; 
                }
                $newPothole = $newPothole->whereIn('road_id', $moRoads);

                $mts = $mts->whereHas('roads', function($query) use ($moRoads){
                    $query->whereIn('road_id', $moRoads); 
                });

            }else{ //by boundary
                $provinces = []; 
                foreach($user->mo->mts as $row){
                    if($row->mt->province_id != null){
                        $provinces[] = $row->mt->province_id; 
                    } 
                }
                $newPothole = $newPothole->whereHas('location', function($query) use ($provinces){
                    $query->whereIn('province_id', $provinces);
                });

                $mts = $mts->whereHas('mos', function($query) use ($user){
                    $query->whereHas('mo', function ($query) use ($user){
                        $query->where('user_id', $user->id);
                    });
                });
            }
        }
        
        return response()->json([
            'new'       => $newPothole->count(),
            'mts'       =>  $mts->count(),
            'total'     =>  $pothole->count(),
            'fixing'    =>  $pothole->where('status_id', 2)->count(),
            'fixed'     =>  $pothole->where('status_id', 4)->count(), 
            'rejected'  =>  $pothole->where('status_id', 3)->count(), 
            'consulting'=>  $pothole->where('status_id', 5)->count() 
        ], 200);
    }

  
   
}
