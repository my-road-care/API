<?php

namespace App\Api\V1\Service;

use App\Model\User;
use Illuminate\Support\Facades\File;
use CURLFile;

trait Firebase
{

  public function updateDeviceToken($request, $userId = 0)
    {

        $user = User::where('id',$userId)->first();

        $user->update([
          'app_token' => $request->token
        ]);
    }

    public function sendNotification($project, $userId = 0, $message = null)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $FcmToken = [User::where('id', $userId)->pluck('app_token')->first()];
            
        // ADD SERVER KEY HERE PROVIDED BY FCM
    
        $data = [
            "registration_ids" => $FcmToken,
            "notification" => [
                "title" => "RoadCare Supervision",
                "body" => $message,  
            ],
            "data" => $project
        ];
        $encodedData = json_encode($data);
    
        $headers = [
            'Authorization:key=' . env('FCM_KEY'),
            'Content-Type: application/json',
        ];
    
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
        // Close connection
        curl_close($ch);
        // FCM response
        // dd($result);
        
    }
    
    
}