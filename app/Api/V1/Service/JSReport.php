<?php

namespace App\Api\V1\Service;

use CURLFile;

trait JSReport
{


  // public function jsReportFormat($template = null, $data = null)
  // {

  //   $template = [
  //     "template" => [
  //         "name" => "/system-report/system-report-main",
  //     ],
  //     "data" => $data,
  //   ];
  //   return $template;
  // }
  public function jsReport($template = null, $basic = null)
  {
    $header = array(
      'Authorization: Basic ' . $basic,  // 'Authorization: ''Basic 'asdfasdf
      'Content-type: application/json',
    );
    
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, env('JS_URL')); // Call JS Report 
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_ENCODING, "");
    curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
    curl_setopt($curl, CURLOPT_TIMEOUT, 60);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($template));
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    $response = curl_exec($curl);
    $error = curl_error($curl);
    curl_close($curl);

    return [
      'file_base64' => base64_encode($response),
      'error' => $error
    ];
  }
}
