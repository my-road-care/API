<?php

namespace App\Api\V1\Service;
use Illuminate\Support\Facades\File;
use CURLFile;

trait FileServers
{

    public function fileServer($file, $image){
      // $base64 = base64_encode(file_get_contents($image));
      // $type = $image->getClientOriginalExtension();

      // if($file){
      //   $base64 = base64_decode($file);
      //   $pdf = "file.pdf";
      //   file_put_contents($pdf, $base64);
      // }

      if($file){
        $post = [
          'projectKey' => env('PROJECT_KEY'),
          'projectSecret' => env('PROJECT_SECRET'),
          'file' => $file,
          'doc'=> new CURLFile($file),
          'folder' => 'RoadCare',
          'fileType' => 'Pdf',
          'documentTypeId' => '2',
          'isMultipleFileSize' => '1'
        ];
      }else{
        $post = [
          'projectKey' => env('PROJECT_KEY'),
          'projectSecret' => env('PROJECT_SECRET'),
          'file' => $image,
          'folder' => 'RoadCare',
          'fileType' => 'Image',
          'documentTypeId' => '2',
          'isMultipleFileSize' => '1'
        ];
      }


      $curl = curl_init();
  
      curl_setopt_array($curl, array(
        CURLOPT_URL => env('FILE_URL'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $post,
        CURLOPT_HTTPHEADER => array(
          "Authorization: Basic ".env('FILE_BASIC_AUTH')
        ),
      ));
      
      $response = curl_exec($curl);
      
      curl_close($curl);

      // Delete File from Public folder
      if (File::exists(public_path('file.pdf'))) {
        File::delete(public_path('file.pdf'));
      }

      $response = json_decode($response,true);
      return $response;

    }
    
    
}