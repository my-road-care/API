<?php

namespace App\Api\V1\Service;

use CURLFile;

trait Picture
{
  public function jsReportFormat($general = null, $statistic = null, $procurement = null, $standardTest = null)
  {

    $template = [
      "template" => [
          // "name" => "/warming-letter/warming-letter-main",
          "name" => "/SPV/Projects/Info-Projects/Overview/overview-main",
      ],
      "data" => [
        "general"        => $general,
        "statistic"      => $statistic,
        "procurement"    => $procurement,
        "standard_tests" => $standardTest
      ],
    ];
    return $template;
  }
  public function jsReport($general = null, $statistic = null,$procurement = null, $standardTest = null)
  {
    $header = array(
      'Authorization: Basic dmF0aGFuYWs6MkA4NDg0KjQ3',
      'Content-type: application/json',
    );
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://uat-jsrpt.mpwt.gov.kh/api/report'); // Call JS Report 
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_ENCODING, "");
    curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
    curl_setopt($curl, CURLOPT_TIMEOUT, 60);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->jsReportFormat($general, $statistic, $procurement, $standardTest)));
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    $response = curl_exec($curl);
    $error = curl_error($curl);
    curl_close($curl);

    return [
      'file_base64' => base64_encode($response),
      'error' => $error
    ];
  }
}
