<?php

namespace App\Api\V1\Service;

use App\Enum\Role;
use App\Model\Supervision\Permission\PerPermission;
use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProjectWorkingGroup;
use App\Model\User;

trait CheckPermission
{

  public function checkPermission($slug = "", $action = "", $projectId = 0, $userID = 0){
    // Find User ID
    if($userID == 0){
      // dd('hi');
      $userId = auth()->id();
    }else{
      // dd('hello');
      $userId = $userID;
    }

    //check user role is Super Admin or Supervision Manager
    $newUser = User::where('id', $userId)->first();
    $user = $newUser->type_id;

    // Find System Role ID
    $normal = ProjectWorkingGroup::where([
      'project_id'  => $projectId,
      'user_id'     => $userId
    ])->first();

    // Special User
    $specialRole = ProjectWorkingGroup::with('sys')->where([
      'user_id' => auth()->id(),
      'project_id' => $projectId])->whereIn('sys_id',[6,7])->first();

    if($specialRole){
      $projectWorkingGroup = $specialRole;
    }else{
      $projectWorkingGroup = $normal;
    }

    if($user == Role::SuperAdmin || $user == Role::SupervisionManager || $projectWorkingGroup){

      if($projectWorkingGroup){

        $sysRoleId = $projectWorkingGroup->sys_id;

        $permision = PerPermission::with([
          'sys',
          'action',
          'part'
          ])
        ->where([
          'role_id' => $sysRoleId
        ])      
        ->whereHas('action', function($q) use($action){
          $q->where('name', $action);
        })
        ->whereHas('part', function($q) use($slug){
          $q->where('slug', $slug);
        })      
        ->first();

        if($user == Role::SuperAdmin || $user == Role::SupervisionManager){
          return [
            'check'    => 1,
            'sys_role' => "Has role as ". auth()->user()->type->name
          ]; 
        }else{
          return [
            'check'    => $permision ? 1 : 0,
            'sys_role' => $permision ? $permision->sys ? $permision->sys->name : '' : ''
          ];
        }       

      }else{
        return [
          'check'    => 1,
          'sys_role' => "Has role as ". auth()->user()->type->name
        ]; 
      }     

    }else{
      return  [
        'check'    => 0,              
        'message'  => 'No permission allowed.'
      ];

    }
  }
    
    
}