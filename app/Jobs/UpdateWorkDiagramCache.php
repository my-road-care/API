<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\Station;
use App\Model\Supervision\Setup\Work;
use App\Model\Supervision\Projects\StationWork;
use Illuminate\Support\Facades\Cache;

class UpdateWorkDiagramCache implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $projectId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($projectId)
    {
        $this->projectId = $projectId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $projectId = $this->projectId;
        $cache_key = 'work_diagram_' . $projectId;
        $project       = Project::with('diagramCreator', 'diagramUpdater', 'diagramApprover')->where('id', $projectId)->first();

        $stations   = Station::whereHas('point', function ($query) use ($projectId) {
            $query->where('project_id', $projectId);
        })->get();

        $staionIds = [];
        foreach ($stations as $stationId) {
            $staionIds[] = $stationId->id;
        }
        $works = Work::whereHas('stationWorks', function ($query) use ($staionIds) {
            $query->whereIn('station_id', $staionIds);
        })->orderBy('group_id', 'DESC')->get();

        // $works = Work::where('id',18)->get();
        $data = [];

        foreach ($works as $work) {
            $l = [];
            $r = [];
            foreach ($stations as $station) {
                $stationWork = StationWork::where([
                    'station_id' => $station->id,
                    'work_id' => $work->id
                ])
                    ->with([
                        'sides',
                        'sides.status',
                        'sides.testingRecords.standardTest'
                    ])->first();
                if ($stationWork) {
                    foreach ($stationWork->sides as $side) {
                        if ($side->side_id == 1) {
                            $l[] = [
                                'station_work_id'  => $side->station ? $side->station->id : null,
                                'work_id'          => $work->id,
                                'side_id'          => $side->id,
                                'progress'         => $side->status ? $side->status->id : null,
                                'color'            => $side->status ? $side->status->color : null,
                                'have_test'        => $side->testingRecords ? $this->checkTest($side->testingRecords) : null
                            ];
                        } else {
                            $r[] = [
                                'station_work_id'  => $side->station ? $side->station->id : null,
                                'work_id'          => $work->id,
                                'side_id'          => $side->id,
                                'progress'         => $side->status ? $side->status->id : null,
                                'color'            => $side->status ? $side->status->color : null,
                                'have_test'        => $side->testingRecords ? $this->checkTest($side->testingRecords) : null
                            ];
                        }
                    }
                } else {
                    $l[] = [
                        'station_work_id'  => null,
                        'work_id'          => $work->id,
                        'side_id'          => 0,
                        'progress'      => null,
                    ];

                    $r[] = [
                        'station_work_id'  => null,
                        'work_id'          => $work->id,
                        'side_id'          => 0,
                        'progress'      => null,
                    ];
                }
            }

            $data[] = [
                'id' => $work->id,
                'work' => $work->en_name,
                'l'    => $l,
                'r'    => $r,
            ];
        }

        if ($project->diagram_creator_id) {

            // =============> Creator
            if ($project->diagramCreator && $project->diagram_create_date) {
                $creator = [
                    'name'   => $project->diagramCreator ? $project->diagramCreator->name : null,
                    'date'   => $project->diagram_create_date ? date('Y-m-d', strtotime($project->diagram_create_date)) : null
                ];
            } else {
                $creator = null;
            }

            // =============> Updater 
            if ($project->diagramUpdater && $project->diagram_update_date) {
                $updater = [
                    'name'   => $project->diagramUpdater ? $project->diagramUpdater->name : null,
                    'date'   => $project->diagram_update_date ? date('Y-m-d', strtotime($project->diagram_update_date)) : null
                ];
            } else {
                $updater = null;
            }

            // =============> Approval 
            if ($project->diagramApprover && $project->technical_approved_datetime) {
                $approval = [
                    'name'   => $project->diagramApprover ? $project->diagramApprover->name : null,
                    'date'   => $project->technical_approved_datetime ? date('Y-m-d', strtotime($project->technical_approved_datetime)) : null
                ];
            } else {
                $approval = null;
            }

            $users = [
                "created_by"  => $creator,
                "updated_by"  => $updater,
                "approved_by" => $approval
            ];
        } else {
            $users = null;
        }

        $response = [
            "user"          => $users,
            "pk_stations"   => $this->station($stations),
            "works"         => $data
        ];
        Cache::put($cache_key, json_encode($response), now()->addMinutes(env('CACHE_TTL', 1)));
        return json_encode($response);
    }

    private function checkTest($testRecords = null)
    {
        $test = [];
        if($testRecords){
            foreach ($testRecords as $row) {
                if ($row->is_pass == 1) {
                    $test[] = 1;
                } else {
                    $test[] = 2;
                }
            }
        }else{
            $test[] = 1;
        }

        $diff = array_diff([1, 2], $test);
        if (empty($diff)) {
            $result = "4";
        } elseif ($diff == [1, 2]) {
            $result = "1";
        } elseif ($diff == [1]) {
            $result = "3";
        } else {
            $result = "2";
        }

        return $result;
    }

    private function station($stations)
    {
        $arrStation = [];

        foreach ($stations as $row) {
            $arrStation[] = [
                'id' => $row->id,
                'transaction_id' => $row->transaction_id,
                'number'         => $row->number,
                'trx'            => $row->trx,
                'station_num'    => $row->station_num
            ];
        }

        return $arrStation;
    }
}
