<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;

class Thread implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $cmd;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($cmd = '')
    {
        $this->cmd = $cmd;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $cmd = urlencode($this->cmd);
        $res = `curl localhost:8001?cmd=$cmd`;
        if($this->cmd !== $res) { // request fail
            // TODO: send notification
        }
    }
}
