<?php

namespace App\Jobs;

use App\Model\Supervision\Projects\Project;
use App\Model\Supervision\Projects\ProjectStandardTest;
use App\Model\Supervision\Projects\Station;
use App\Model\Supervision\Projects\StationWork;
use App\Model\Supervision\Projects\StationWorkSideTest;
use App\Model\Supervision\Projects\Transaction;
use App\Model\Supervision\Setup\Work;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;

class ProjectStatisticJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $projectId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($projectId)
    {
        $this->projectId = $projectId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $projectId = $this->projectId;
        $cache_key = 'project_statistic_' . $projectId;

        $project = Project::with([
            'status',
            'surfaces'
        ])->find($projectId);

        // ===============> Get Location <=============
        $transactions = Transaction::with([
            'stations'
        ])->where('project_id', $projectId)->get();

        $start = null;
        $end = null;
        $i = 0;
        $len = count($transactions);
        
        foreach($transactions as $transaction){
            if($i == 0){
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $start = "PK". $pk->pk . "+" .$stations->number;
            }elseif($i == $len - 1){
                $pk = $transaction;
                $stations = $pk->stations ? $pk->stations->first() : null;
                $end = "PK". $pk->pk . "+".$stations->number;
            }
            $i++;            
        } 

        $location = $start." - ".$end;
        
        if ($project) {
            $surfaces = $project->surfaces;
            $surface = [];
            foreach ($surfaces as $row) {
                $surface[] = [
                    'name' => $row->kh_name
                ];
            }

            $stations = Station::select('id')->whereHas('point', function ($query) use ($projectId) {
                $query->where('project_id', $projectId);
            })->get();

            if (!$stations->isEmpty()) {

                $staionIds = [];
                
                foreach ($stations as $stationId) {
                    $staionIds[] = $stationId->id;
                }

                $works = Work::whereHas('stationWorks', function ($query) use ($staionIds) {
                    $query->whereIn('station_id', $staionIds);
                })
                    ->with([
                        'stationWorks.sides.testingRecords'
                    ])
                    ->get();

                $allProcess         = 0;
                $finishProcess      = 0;
                $startProcess       = 0;
                $notStartProcess    = 0;

                // =========> Check On Road Need to test

                $testings = ProjectStandardTest::where('project_id', $projectId)->get();
                $allTest            = 0;

                foreach ($testings as $testing) {
                    $start = $testing->pk_start * 1000 + $testing->station_start;
                    $end   = $testing->pk_end * 1000 + $testing->station_end;

                    $allTest = ($allTest + ($end - $start) * 2);
                }

                // ========> Get Stations Have Test <=============
                $testStations = StationWorkSideTest::whereHas('side', function ($query) use ($projectId) {
                    $query->whereHas('station', function ($query) use ($projectId) {
                        $query->whereHas('station', function ($query) use ($projectId) {
                            $query->whereHas('point', function ($query) use ($projectId) {
                                $query->where('project_id', $projectId);
                            });
                        });
                    });
                })->get();

                $haveTest = [];
                // $newData = collect($haveTest)->unique();

                if (!$testStations->isEmpty()) {
                    foreach ($testStations as $testStation) {
                        $haveTest[] = [
                            "standard_id" => $testStation->standard_test_id,
                            "test_id"    => $testStation->station_work_side_id
                        ];
                    }
                    // =================> Get Interval <================
                    $project = Project::where('id', $projectId)->first();
                    $interval = $project->interval;
                    $testingBlock = $allTest / $interval * 2;
                    $haveTesting = count(collect($haveTest)->unique());
                    $result = round((float) $haveTesting / $testingBlock * 100, 2);
                } else {
                    $result = 0;
                }
                foreach ($works as $work) {
                    foreach ($stations as $station) {
                        $stationWork = StationWork::where([
                            'station_id' => $station->id,
                            'work_id' => $work->id
                        ])
                            ->with([
                                'sides'
                            ])->first();
                        if ($stationWork) {
                            foreach ($stationWork->sides as $side) {
                                // Process
                                $allProcess++;
                                if ($side->progress_status_id == 1) {
                                    $notStartProcess++;
                                } elseif ($side->progress_status_id == 2) {
                                    $startProcess++;
                                } else {
                                    $finishProcess++;
                                }
                            }
                        }
                    }
                }

                $series = [
                    round((float)$finishProcess * 100 / $allProcess, 2),
                    round((float)$startProcess * 100 / $allProcess, 2),
                    round((float)$notStartProcess * 100 / $allProcess, 2)
                ];

                if ($project->diagramApprover && $project->technical_approved_datetime) {
                    $approval = [
                        'date' => $project->technical_approved_datetime,
                        'by'   => $project->diagramApprover ? $project->diagramApprover->name : null
                    ];
                } else {
                    $approval = null;
                }

                $testingQuality = [
                    $result,
                    100 - $result
                ];
                
                $response =  [
                    "technical_approved" => $approval,
                    "data"    => [
                        "test_quality" => [
                            'series' => $testingQuality,
                            'labels' => [
                                'បានធ្វើតេស្តរួច',
                                'មិនទាន់បានតេស្ត'
                            ],
                            'colors' => [
                                '#FFB10D',
                                '#FFFFFF'
                            ]
                        ],
                        'process' => [
                            'series' => $series,
                            'labels' => [
                                'បញ្ចប់ការងារ',
                                'កំពុងអនុវត្ត',
                                'មិនទាន់អនុវត្ត',
                            ],
                            'colors' => [
                                '#00FF00',
                                '#90CAF9',
                                '#EEEEEE'
                            ]
                        ],
                        'overview' => [
                            'code' => $project->code,
                            'name' => $project->name,
                            'pk_start_end' => $location,
                            'surfaces' => $surface,
                            'status' => [
                                'name' => $project->status->kh_name,
                                'color' => $project->status->color
                            ],
                            'project_start_date' => $project->project_start_date_contract,
                            'project_end_date' => $project->project_end_date_contract,
                        ]
                    ]
                ];

                Cache::put($cache_key, json_encode($response), now()->addMinutes(env('CACHE_TTL', 1)));
                return json_encode($response, true);

            } else {
                if ($project->diagramApprover && $project->technical_approved_datetime) {
                    $approval = [
                        'date' => $project->technical_approved_datetime,
                        'by'   => $project->diagramApprover ? $project->diagramApprover->name : null
                    ];
                } else {
                    $approval = null;
                }

                $testingQuality = [
                    0,
                    100
                ];

                $response = [
                    "technical_approved" => $approval,
                    "data"    => [
                        "test_quality" => [
                            'series' => $testingQuality,
                            'labels' => [
                                'បានធ្វើតេស្តរួច',
                                'មិនទាន់បានតេស្ត'
                            ],
                            'colors' => [
                                '#FFB10D',
                                '#FFFFFF'
                            ]
                        ],
                        'process' => [
                            'series' => [0, 0, 0, 100],
                            'labels' => [
                                'បានធ្វើតេស្តរួច',
                                'បញ្ចប់ការងារ',
                                'កំពុងអនុវត្ត',
                                'មិនទាន់អនុវត្ត',
                            ],
                            'colors' => [
                                '#FFB10D',
                                '#00FF00',
                                '#90CAF9',
                                '#EEEEEE'
                            ]
                        ],
                        'overview' => [
                            'code' => $project->code,
                            'name' => $project->name,
                            'pk_start_end' => $location,
                            'surfaces' => $surface,
                            'status' => [
                                'name' => $project->status->kh_name,
                                'color' => $project->status->color
                            ],
                            'project_start_date' => $project->project_start_date_contract,
                            'project_end_date' => $project->project_end_date_contract,
                        ]
                    ]
                ];

                Cache::put($cache_key, json_encode($response), now()->addMinutes(env('CACHE_TTL', 1)));
                return json_encode($response, true);
            }
        } else {
            return responseError('មិនមានទិន្ន័យ');
        }
    }
}
