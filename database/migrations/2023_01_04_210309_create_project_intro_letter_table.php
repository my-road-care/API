<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectIntroLetterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_project_letters', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->integer('project_id')->unsigned()->index()->nullable();
            $table->foreign('project_id')->references('id')->on('sup_project')->onDelete('cascade');

            $table->integer('project_intro_letters_type_id')->unsigned()->index()->nullable();
            $table->foreign('project_intro_letters_type_id')->references('id')->on('sup_letters_type')->onDelete('cascade');
            
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('user')->onDelete('cascade');

            $table->string('title',150)->nullable();
            $table->string('image_uri',150)->nullable();
            $table->dateTime('date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_project_letters');
    }
}
