<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupProjectWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_project_works', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('project_id')->unsigned()->index()->nullable();
            $table->foreign('project_id')->references('id')->on('sup_project')->onDelete('cascade');
            
            $table->integer('provinces_id')->unsigned()->index()->nullable();
            $table->foreign('provinces_id')->references('id')->on('province')->onDelete('cascade');

            $table->integer('districts_id')->unsigned()->index()->nullable();
            $table->foreign('districts_id')->references('id')->on('district')->onDelete('cascade');

            $table->integer('communes_id')->unsigned()->index()->nullable();
            $table->foreign('communes_id')->references('id')->on('commune')->onDelete('cascade');

            $table->integer('villages_id')->unsigned()->index()->nullable();
            $table->foreign('villages_id')->references('id')->on('village')->onDelete('cascade');

            $table->string('length',150);
            $table->string('width',150);
            $table->string('height',150);
            $table->string('pk_start',150);
            $table->string('pk_end',150);
            $table->string('from_meter',150);
            $table->string('to_meter',150);
            $table->string('start_cor',150);
            $table->string('end_cor',150);
            $table->string('start_location',150);
            $table->string('end_location',150);
            

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_project_works');
    }
}
