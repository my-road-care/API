<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectMgtStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_project_mgt_structures', function (Blueprint $table) {
            $table->increments('id',11);

            $table->integer('project_id')->unsigned()->index()->nullable();
            $table->foreign('project_id')->references('id')->on('sup_project')->onDelete('cascade');

            $table->integer('organization_id')->unsigned()->index()->nullable();
            $table->foreign('organization_id')->references('id')->on('sup_organization')->onDelete('cascade');

            // $table->integer('framework_id')->unsigned()->index()->nullable();
            // $table->foreign('framework_id')->references('id')->on('sup_framework')->onDelete('cascade');

            // $table->integer('entity_id')->unsigned()->index()->nullable();
            // $table->foreign('entity_id')->references('id')->on('sup_organization')->onDelete('cascade');

            // $table->integer('reviewer_id')->unsigned()->index()->nullable();
            // $table->foreign('reviewer_id')->references('id')->on('sup_reviewer')->onDelete('cascade');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_project_mgt_structures');
    }
}
