<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupStationWorkSidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_project_station_works_sides', function (Blueprint $table) {
            $table->increments('id', 1);

            $table->integer('station_work_id')->unsigned()->index()->nullable();
            $table->foreign('station_work_id')->references('id')->on('sup_project_station_works')->onDelete('cascade');

            $table->integer('side_id')->unsigned()->index()->nullable();
            $table->foreign('side_id')->references('id')->on('sup_side')->onDelete('cascade');

            $table->integer('progress_status_id')->unsigned()->index()->nullable();
            $table->foreign('progress_status_id')->references('id')->on('sup_progress_status')->onDelete('cascade');

            $table->string('image_uri')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_project_station_works_sides');
    }
}
