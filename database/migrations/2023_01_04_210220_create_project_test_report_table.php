<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTestReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_project_test_reports', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->integer('project_id')->unsigned()->index()->nullable();
            $table->foreign('project_id')->references('id')->on('sup_project')->onDelete('cascade');

            $table->integer('field_testing_id')->unsigned()->index()->nullable();
            $table->foreign('field_testing_id')->references('id')->on('sup_testing_method')->onDelete('cascade');

            $table->integer('report_type_id')->unsigned()->index()->nullable();
            $table->foreign('report_type_id')->references('id')->on('sup_test_report_type')->onDelete('cascade');

            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('user')->onDelete('cascade');

            $table->dateTime('date');
            $table->string('image_uri', 150);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_project_test_reports');
    }
}
