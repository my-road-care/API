<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupStationWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_project_station_works', function (Blueprint $table) {
            $table->increments('id', 1);

            $table->integer('station_id')->unsigned()->index()->nullable();
            $table->foreign('station_id')->references('id')->on('sup_project_stations')->onDelete('cascade');

            $table->integer('work_id')->unsigned()->index()->nullable();
            $table->foreign('work_id')->references('id')->on('sup_work')->onDelete('cascade');

            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('user')->onDelete('cascade');

            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->foreign('updater_id')->references('id')->on('user')->onDelete('cascade');

            $table->integer('approve_id')->unsigned()->index()->nullable();
            $table->foreign('approve_id')->references('id')->on('user')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_project_station_works');
    }
}
