<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_project', function (Blueprint $table) {
            $table->increments('id',11);
            $table->text('name')->nullable();
            $table->string('code',150)->nullable();

            $table->integer('provinces_id')->unsigned()->index()->nullable();
            $table->foreign('provinces_id')->references('id')->on('province')->onDelete('cascade');

            $table->integer('type_id')->unsigned()->index()->nullable();
            $table->foreign('type_id')->references('id')->on('sup_projects_working_type')->onDelete('cascade');

            $table->integer('budget_id')->unsigned()->index()->nullable();
            $table->foreign('budget_id')->references('id')->on('sup_budget')->onDelete('cascade');

            // $table->integer('project_owner_id')->unsigned()->index()->nullable();
            // $table->foreign('project_owner_id')->references('id')->on('sup_project_owner')->onDelete('cascade');

            $table->integer('type_project_id')->unsigned()->index()->nullable();
            $table->foreign('type_project_id')->references('id')->on('sup_category')->onDelete('cascade');

            $table->integer('project_status_id')->unsigned()->index()->nullable();
            $table->foreign('project_status_id')->references('id')->on('sup_status')->onDelete('cascade');

            $table->integer('road_id')->unsigned()->index()->nullable();
            $table->foreign('road_id')->references('id')->on('road')->onDelete('cascade');

            $table->integer('diagram_creator_id')->unsigned()->index()->nullable();
            $table->foreign('diagram_creator_id')->references('id')->on('user')->onDelete('cascade');

            $table->integer('diagram_updater_id')->unsigned()->index()->nullable();
            $table->foreign('diagram_updater_id')->references('id')->on('user')->onDelete('cascade');

            $table->integer('diagram_approver_id')->unsigned()->index()->nullable();
            $table->foreign('diagram_approver_id')->references('id')->on('user')->onDelete('cascade');

            $table->dateTime('diagram_create_date')->nullable();
            $table->dateTime('diagram_update_date')->nullable();
            $table->dateTime('diagram_approve_date')->nullable();


            $table->integer('pk_start')->nullable();
            $table->integer('pk_end')->nullable();

            $table->string('budget_plan',150)->nullable();
            $table->string('contract_no',150)->nullable();
            $table->string('bud_build_req',150)->nullable();
            $table->string('bud_review_req',150)->nullable();
            $table->string('bud_after_build',150)->nullable();
            $table->string('bud_after_review',150)->nullable();
            $table->string('bud_build_contract',150)->nullable();
            $table->string('bud_review_contract',150)->nullable();
            $table->string('total_bud_contract',150)->nullable();
            $table->dateTime('project_start_date_contract')->nullable();
            $table->dateTime('project_end_date_contract')->nullable();
            $table->string('process_contract_duration', 150)->nullable();
            $table->dateTime('delay_date')->nullable();
            $table->string('warranty_duration', 150)->nullable();
            $table->dateTime('close_project_date')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_project');
    }
}
