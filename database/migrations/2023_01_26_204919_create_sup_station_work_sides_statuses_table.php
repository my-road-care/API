<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupStationWorkSidesStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_pr_station_works_sides_statuses', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->integer('station_work_side_id')->unsigned()->index()->nullable();
            $table->foreign('station_work_side_id')->references('id')->on('sup_project_station_works_sides')->onDelete('cascade');

            $table->integer('progress_status_id')->unsigned()->index()->nullable();
            $table->foreign('progress_status_id')->references('id')->on('sup_progress_status')->onDelete('cascade');

            $table->integer('standard_test_id')->unsigned()->index()->nullable();
            $table->foreign('standard_test_id')->references('id')->on('sup_testing_method')->onDelete('cascade');

            $table->integer('tester_id')->unsigned()->index()->nullable();
            $table->foreign('tester_id')->references('id')->on('user')->onDelete('cascade');

            $table->integer('verifier_id')->unsigned()->index()->nullable();
            $table->foreign('verifier_id')->references('id')->on('user')->onDelete('cascade');

            $table->integer('approver_id')->unsigned()->index()->nullable();
            $table->foreign('approver_id')->references('id')->on('user')->onDelete('cascade');

            $table->dateTime('confirmed_at')->nullable();
            $table->dateTime('verified_at')->nullable();
            $table->dateTime('tested_at')->nullable();
            $table->string('comment')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_pr_station_works_sides_statuses');
    }
}
