<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectStandardTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sup_project_standard_tests', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->integer('project_id')->unsigned()->index()->nullable();
            $table->foreign('project_id')->references('id')->on('sup_project')->onDelete('cascade');

            $table->integer('testing_method_id')->unsigned()->index()->nullable();
            $table->foreign('testing_method_id')->references('id')->on('sup_testing_method')->onDelete('cascade');

            $table->integer('work_id')->unsigned()->index()->nullable();
            $table->foreign('work_id')->references('id')->on('sup_work')->onDelete('cascade');

            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->foreign('creator_id')->references('id')->on('user')->onDelete('cascade');

            $table->string('standard',150);
            $table->dateTime('date');            
            $table->string('pk_start',150)->nullable();
            $table->string('pk_end',150)->nullable();
            $table->string('station_start',150)->nullable();
            $table->string('station_end',150)->nullable();
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sup_project_standard_tests');
    }
}
