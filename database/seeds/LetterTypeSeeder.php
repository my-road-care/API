<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LetterTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sup_letters_type')->insert([
            [
                'name' => 'បញ្ជីត្រួតពិនិត្យ'
            ],
            [
                'name' => 'ការណែនាំ'
            ],
            [
                'name' => 'លិខិតព្រមាន'
            ]
        ]); 
    }
}
