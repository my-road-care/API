<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgressStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sup_progress_status')->insert([
            [
                'id' => 1,
                'kh_name' => 'មិនទាន់អនុវត្ត',
                'en_name'      => '',
                'icon'    => ''
            ],[
                'id' => 2,
                'kh_name' => '',
                'en'      => 'កំពុងអនុវត្ត',
                'icon'    => ''
            ],[
                'id' => 3,
                'kh_name' => '',
                'en_name'      => 'បញ្ចប់ការងារ',
                'icon'    => ''
            ],[
                'id' => 4,
                'kh_name' => 'បានឆែកកូដរួច',
                'en'      => '',
                'icon'    => ''
            ],[
                'id' => 5,
                'kh_name' => 'តេស្តរួច',
                'en'      => '',
                'icon'    => ''
            ],
        ]); 
    }
}
