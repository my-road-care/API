<?php
use App\Model\Road\Point as RoadPoint;
use App\MPWT\LatLngUMTConvert as UTM;
use App\Model\Road\PK as Main;
use Illuminate\Database\Seeder;

class RoadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->test();
        // $roads = [];
        // $i = 0;
        // do {
        //     foreach ($roads as $road) {
        //         $this->llFromPoint($road);
        //     }
        //     $roads = RoadPoint::where('latlng', null)->whereNotNull('point')->limit(1000)->get();
        //     echo $i++ . PHP_EOL;
        // } while($roads->count() > 0);

    }

    private function llFromPoint(RoadPoint $road)
    {
        $x = $road->point->getLng();
        $y = $road->point->getLat();
        $latlng = UTM::utm2ll($x, $y);
        $road->latlng = json_encode($latlng);
        $road->save();
    }

    private function test()
    {
        $data = Main::select('id', 'code')->where('road_id', 16)
            ->with([
                'points'=>function($query){
                    $query->select('id', 'pk_id', 'meter', 'latlng')->withCount(['potholes as n_of_potholes'])->orderBy('meter', 'ASC');
                    $query->with(['potholes:id,point_id', 'potholes.files']);
                },
            ])->orderBy('code', 'ASC')->limit(1)->get();
        

        echo $data;

        // echo $data->map(function($p) {
        //     return $p->points->map(function($point) {
        //         return $point->point;
        //     });
        // });
    }
}
