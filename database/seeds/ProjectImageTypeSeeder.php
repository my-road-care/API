<?php

use App\Model\Supervision\Setup\ImageType;
use Illuminate\Database\Seeder;

class ProjectImageTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectImageType::insert([
            [
                "id" => 1,
                "name" => "មុនពេលសាងសង់"
            ],[
                "id" => 2,
                "name" => "ពេលសាងសង់"
            ],[
                "id" => 3,
                "name" => "ក្រោយពេលសាងសង់"
            ],[
                "id" => 4,
                "name" => "កំហុសឆ្គង"
            ]
        ]);
    }
}
