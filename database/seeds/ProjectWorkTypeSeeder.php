<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectWorkTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sup_works_type')->insert([
            [
                "id"   => 1,
                "name" => "សំណង់ផ្លូវថ្នល់"
            ],[
                "id"   => 2,
                "name" => "សំណង់សិល្បការណ៍"
            ]
            ]);
    }
}
