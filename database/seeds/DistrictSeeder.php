<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path() . "/json/district.json";
        $json = json_decode(file_get_contents($path), true);
        
        foreach($json as $row){
            DB::table('sup_districts')->insert([
                "provinces_code" => $row['PROVINCE_CODE'],
                "code" => $row['CODE'],
                "name" => $row['NAME_KH']
            ]);   
        }
    }
}
