<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectOwnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sup_project_owner')->insert([
            [
                'kh_name' => 'ក្រសួងសាធារណការ និងដឹកជញ្ជូន',
                'en_name' => 'Ministry of Public Work and Transpot',
                'abbre'   => 'MPWT'

            ],
            [
                'kh_name' => 'ក្រសួងការពារជាតិ',
                'en_name' => 'Ministry of National Defense',
                'abbre'   => 'MND'
            ],
            [
                'kh_name' => 'ក្រសួងមហាផ្ទៃ',
                'en_name' => 'Ministry of Interior',
                'abbre'   => 'MI'
            ]
        ]); 
    }
}
