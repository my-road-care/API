<?php

use App\Model\Supervision\Setup\Category;
use App\Model\Supervision\Projects\TypeProject;
use Illuminate\Database\Seeder;

class TypeProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            [
                "id"   => 1,
                "name" => "ថ្នាក់ជាតិ"
            ],[
                "id"   => 2,
                "name" => "ថ្នាក់អន្តរជាតិ"
            ]
        ]);
    }
}
