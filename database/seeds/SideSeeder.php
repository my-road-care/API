<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sup_side')->insert([
            [
                'id' => 1,
                'name' => 'Left',
                'abrv'      => 'L'
            ],[
                'id' => 2,
                'name' => 'Right',
                'abrv'      => 'R'
            ]
        ]); 
    }
}
