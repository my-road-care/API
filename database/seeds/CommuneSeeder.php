<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommuneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path() . "/json/communes.json";
        $json = json_decode(file_get_contents($path), true);
        foreach($json as $row){
            DB::table('sup_communes')->insert([
                "districts_code" => $row['DISTRICT_CODE'],
                "code" => $row['CODE'],
                "name" => $row['NAME_KH']
            ]);
        }
    }
}
