<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sup_per_action')->insert([

            [ "name"=>"R" ],
            [ "name"=>"C" ],
            [ "name"=>"U" ],
            [ "name"=>"D" ]

        ]);

        DB::table('sup_per_part')->insert([
            [
                "section" => "dashboard",
                "part"    => "dashboard",
                "name"    => "dashboard",
                "slug"    => "dashboard"
            ],
            [ 
                "section" => "Create project",
                "part"    => "Create project",
                "name"    => "Create project",
                "slug"    => "Create-project"
            ],
            [ 
                "section" => "Manage project",
                "part"    => "Info",
                "name"    => "General",
                "slug"    => "project-info-general"
            ],
            [ 
                "section" => "Manage project",
                "part"    => "Info",
                "name"    => "General",
                "slug"    => "project-info-work"
            ],
            [ 
                "section" => "Manage project",
                "part"    => "Info",
                "name"    => "General",
                "slug"    => "project-info-plan"
            ],
            [ 
                "section" => "Manage project",
                "part"    => "Info",
                "name"    => "General",
                "slug"    => "project-info-audit"
            ],
            
        ]);

        $roles   = DB::table('sup_per_role')->get();
        $parts   = DB::table('sup_per_part')->get();
        $actions = DB::table('sup_per_action')->get();

        $permissions = [];

        foreach($roles as $role){
            foreach($parts as $part) {
                foreach($actions as $action){
                    $permissions[] = [
                        'role_id'   => $role->id,
                        'part_id'   => $part->id,
                        'action_id' => $action->id
                    ];
                }
            }
        }
        DB::table('sup_per_permission')->insert($permissions);


    }
}
