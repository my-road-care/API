<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoilLayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "Embankment",
            "Middle Subgrade",
            "Upper-Subgrade",
            "Sub-Base",
            "Base Course",
            "Surface Treatment",
            "AC",
            "RC"
        ];
        foreach($data as $key => $row){
            DB::table('sup_soil_layer')->insert([
                "name"=>$data[$key]
            ]);
        }
    }
}