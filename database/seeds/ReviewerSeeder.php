<?php

use Illuminate\Database\Seeder;

class ReviewerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sup_reviewer')->insert([
            [
                'name' => 'នាយកដ្ឋានបច្ចេកទេស'
            ],
            [
                'name' => 'នាយកដ្ឋានផ្លូវថ្នល់'
            ],
            [
                'name' => 'នាយកដ្ឋានបច្ចេកវិទ្យា'
            ]
        ]); 
    }
}
