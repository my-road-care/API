<?php

use App\Model\Supervision\Setup\ProjectType;
use App\Model\Supervision\Setup\Work;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Work::insert([
            [
                'id'   => 1,
                'type_id' => 1,
                'color' => "#0909FF",
                'en_name' => "Embankment",
                'kh_name' => "Embankment"
            ],[
                'id'   => 2,
                'type_id' => 1,
                'color' => "#12AD2C",
                'en_name' => "Middle Subgrade",
                'kh_name' => "Middle Subgrade"
            ],[
                'id'   => 3,
                'type_id' => 1,
                'color' => "#FFB20E",
                'en_name' => "Upper-Subgrade",                
                'kh_name' => "Upper-Subgrade"                
            ],[
                'id'   => 4,
                'type_id' => 1,
                'color' => "#FF3636",
                'en_name' => "Sub-Base",
                'kh_name' => "Sub-Base"
            ],[
                'id'   => 5,
                'type_id' => 1,
                'color' => "#03BFCO",
                'en_name' => "Base Course",                
                'kh_name' => "Base Course"                
            ],[
                'id'   => 6,
                'type_id' => 1,
                'color' => "#DE542C",
                'en_name' => "Surface Treatment",                
                'kh_name' => "Surface Treatment"                
            ],[
                'id'   => 7,
                'type_id' => 1,
                'color' => "#29066B",
                'en_name' => "AC",                
                'kh_name' => "AC"                
            ],[
                'id'   => 8,
                'type_id' => 1,
                'color' => "#142459",
                'en_name' => "RC",                
                'kh_name' => "RC"                
            ],[
                'id'   => 9,
                'type_id' => 1,
                'color' => "#176BA0",
                'en_name' => "Road Excavtation",                
                'kh_name' => "Road Excavtation"                
            ],[
                'id'   => 10,
                'type_id' => 1,
                'color' => "#7D3AC1",
                'en_name' => "Unsuitatble Soil",                
                'kh_name' => "Unsuitatble Soil"                
            ],
            // Artistic Work
            [
                'id'   => 11,
                'type_id' => 2,
                'color' => "#DB4CB2",
                'en_name' => "Slope",                
                'kh_name' => "Slope"                
            ],[
                'id'   => 12,
                'type_id' => 2,
                'color' => "#EB548C",
                'en_name' => "Side Walk",                
                'kh_name' => "Side Walk"                
            ],[
                'id'   => 13,
                'type_id' => 2,
                'color' => "#4A2377",
                'en_name' => "Curb",                
                'kh_name' => "Curb"                
            ],[
                'id'   => 14,
                'type_id' => 2,
                'color' => "#FBFFA3",
                'en_name' => "Road Furniture",                
                'kh_name' => "Road Furniture"                
            ],[
                'id'   => 15,
                'type_id' => 2,
                'color' => "#1D556F",
                'en_name' => "Street Light",                
                'kh_name' => "Street Light"                
            ],[
                'id'   => 16,
                'type_id' => 2,
                'color' => "#449187",
                'en_name' => "Road Marking",                
                'kh_name' => "Road Marking"                
            ],[
                'id'   => 17,
                'type_id' => 2,
                'color' => "#20655F",
                'en_name' => "Road Stud",                
                'kh_name' => "Road Stud"                
            ],[
                'id'   => 18,
                'type_id' => 2,
                'color' => "#C97B1A",
                'en_name' => "Refletive",                
                'kh_name' => "Refletive"                
            ],[
                'id'   => 19,
                'type_id' => 2,
                'color' => "#FFC620",
                'en_name' => "Excavation",                
                'kh_name' => "Excavation"                
            ],[
                'id'   => 20,
                'type_id' => 2,
                'color' => "#00855A",
                'en_name' => "Foundation",                
                'kh_name' => "Foundation"                
            ],[
                'id'   =>21,
                'type_id' => 2,
                'color' => "#5090CD",
                'en_name' => "Pipe Installation",                
                'kh_name' => "Pipe Installation"                
            ],[
                'id'   => 22,
                'type_id' => 2,
                'color' => "#D3FF00",
                'en_name' => "Manhole",                
                'kh_name' => "Manhole"                
            ],[
                'id'   => 23,
                'type_id' => 2,
                'color' => "#FFCC00",
                'en_name' => "Back Fill",                
                'kh_name' => "Back Fill"                
            ]
            ]);
    }
}
