<?php

use App\Model\Supervision\Setup\TestReportType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestReportTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TestReportType::insert([
            [
                'id' => 1,
                'name' => 'Labotary'
            ],
            [
                'id' => 2,
                'name' => 'Field'
            ]
            ]);
    }
}