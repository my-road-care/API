<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sup_per_role')->insert([
            [
                'id'   => 1,
                'abbrivation' => 'ADMIN',
                'name'  => 'ADMIN',
                'en_name' => 'ADMIN'
            ],
            [
                'id'   => 2,
                'abbrivation' => 'MG',
                'name'  => 'អ្នកគ្រប់គ្រង់ត្រួតពិនិត្យ',
                'en_name' => 'Management'
            ],
            [
                'id'   => 3,
                'abbrivation' => 'DE',
                'name'  => 'អ្នកបញ្ជូលទិន្នន័យ',
                'en_name' => 'Data Entry'
            ],
            [
                'id'   => 4,
                'abbrivation' => 'SI',
                'name'  => 'ក្រុមត្រួតពិនិត្យពិសេស',
                'en_name'=> 'Special Inspector'
            ],
            [
                'id'   => 5,
                'abbrivation' => 'PIU',
                'name'  => 'អង្គភាពអនុវត្តគម្រោង',
                'en_name' => 'PIU'
            ],
            [
                'id'   => 6,
                'abbrivation' => 'PMU',
                'name'  => 'អ្នកគ្រប់គ្រងគម្រោង',
                'en_name' => 'PMU'
            ],
            [
                'id'   => 7,
                'abbrivation' => 'AP',
                'name'  => 'អ្នកអនុម័ត',
                'en_name'=> 'Approver'
            ],
            [
                'id'   => 8,
                'abbrivation' => 'MN',
                'name'  => 'ថ្នាក់ដឹកនាំ',
                'en_name'=> 'Management'
            ],
        ]); 
    }
}
