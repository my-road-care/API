<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(RoleSeeder::class);
        // $this->call(OrganizationTypeSeeder::class);
        // $this->call(OrganizationSeeder::class);
        // $this->call(RoleSeeder::class);
        // $this->call(RolePermissionTableSeeder::class);
        // $this->call(SetupTableSeeder::class);
        $this->call(UserTableSeeder::class);
        // $this->call(BudgetSeeder::class);
        // $this->call(WorkTypeSeeder::class);
        // $this->call(TestTypeSeeder::class);
        // $this->call(ProjectWorkTypeSeeder::class);

        // $this->call(PermissionSeeder::class);
        // $this->call(RoleHasPermissionSeeder::class);
        // $this->call(ProjectImageTypeSeeder::class);
        // $this->call(WorkSeeder::class);
        // $this->call(SideSeeder::class);
        // $this->call(ProgressStatusSeeder::class);
        // $this->call(LetterTypeSeeder::class);
        // $this->call(TestReportTypeSeeder::class);
        // $this->call(TypeProjectSeeder::class);
        // $this->call(ProjectStatusSeeder::class);

    }
}
