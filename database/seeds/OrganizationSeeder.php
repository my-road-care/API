<?php

use Illuminate\Database\Seeder;
use App\Model\Supervision\Setup\Organization;
class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Organization::insert([
            [
                'id'      => 1,
                'type_id' => 2,
                'kh_name' => 'នាយកដ្ធានផ្លូវថ្នល់',
                'en_name' => '',
                'abbre'   => 'DR'
            ],[
                'id'      => 2,
                'type_id' => 2,
                'kh_name' => 'នាយកដ្ឋានហេដ្ឋារចនាសម្ព័ន្ធសាធារណៈ',
                'en_name' => '',
                'abbre'   => 'DI'
            ],[
                'id'      => 3,
                'type_id' => 2,
                'kh_name' => 'នាយកដ្ឋានបច្ចេកទេសសាធារណៈ និងបច្ចេកទេស',
                'en_name' => '',
                'abbre'   => 'DTPT'
            ],[
                'id'      => 4,
                'type_id' => 2,
                'kh_name' => 'នាយកដ្ឋានប្រព័ន្ធទឹកកខ្វក់',
                'en_name' => '',
                'abbre'   => 'DW'
            ], 
            [
                'id'      => 5,
                'type_id' => 4,
                'kh_name' => 'ក្រសួងសាធារណការ និងដឹកជញ្ជូន',
                'en_name' => '',
                'abbre'   => 'DW'
            ],
            [
                'id'      => 6,
                'type_id' => 4,
                'kh_name' => 'ក្រសួងការពារជាតិ',
                'en_name' => '',
                'abbre'   => 'DW'
            ],
            [
                'id'      => 7,
                'type_id' => 4,
                'kh_name' => 'ក្រសួងមហាផ្ទៃ',
                'en_name' => '',
                'abbre'   => 'DW'
            ],
            [
                'id'      => 8,
                'type_id' => 3,
                'kh_name' => 'ក្រសួងសាធារណការ និងដឹកជញ្ជូន',
                'en_name' => '',
                'abbre'   => 'DW'
            ],
            [
                'id'      => 9,
                'type_id' => 3,
                'kh_name' => 'ក្រសួងការពារជាតិ',
                'en_name' => '',
                'abbre'   => 'DW'
            ],
            [
                'id'      => 10,
                'type_id' => 3,
                'kh_name' => 'ក្រសួងមហាផ្ទៃ',
                'en_name' => '',
                'abbre'   => 'DW'
            ],
            [
                'id'      => 11,
                'type_id' => 5,
                'kh_name' => 'នាយកដ្ឋានបច្ចេកទេស',
                'en_name' => '',
                'abbre'   => 'DW'
            ],
            [
                'id'      => 12,
                'type_id' => 5,
                'kh_name' => 'នាយកដ្ឋានផ្លូវថ្នល់',
                'en_name' => '',
                'abbre'   => 'DW'
            ],
            [
                'id'      => 13,
                'type_id' => 5,
                'kh_name' => 'នាយកដ្ឋានបច្ចេកវិទ្យា',
                'en_name' => '',
                'abbre'   => 'DW'
            ],
            [
                'id'      => 14,
                'type_id' => 1,
                'kh_name' => 'ក្រសួងសាធារណការ និងដឹកជញ្ជូន',
                'en_name' => 'Ministry of Public Work and Transpot',
                'abbre'   => 'MPWT'

            ],
            [
                'id'      => 15,
                'type_id' => 1,
                'kh_name' => 'ក្រសួងការពារជាតិ',
                'en_name' => 'Ministry of National Defense',
                'abbre'   => 'MND'
            ],
            [
                'id'      => 16,
                'type_id' => 1,
                'kh_name' => 'ក្រសួងមហាផ្ទៃ',
                'en_name' => 'Ministry of Interior',
                'abbre'   => 'MI'
            ]
        ]);
    }
}
