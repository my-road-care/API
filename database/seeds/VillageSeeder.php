<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VillageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path() . "/json/village.json";
        $json = json_decode(file_get_contents($path), true);
        
        foreach($json as $row){
            DB::table('sup_villages')->insert([
                "communes_code"     => $row['COMMUNE_CODE'],
                "name" => $row['NAME_EN'] 
            ]);   
        }
    }
}
