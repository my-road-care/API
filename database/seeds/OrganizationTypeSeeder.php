<?php

use Illuminate\Database\Seeder;

class OrganizationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['ម្ចាស់គម្រោង','អង្គភាព PIU','អនាប័ក​ ប្រតិបត្តិ', 'ក្របខណ្ឌ', 'អ្នកត្រួតពិនិត្យ'];
        foreach($data as $key=>$row){
            DB::table('sup_organizations_type')->insert(
                [
                    'id' => $key+1,
                    'name' => $row
                ],
            );
        }
    }
}
