<?php

use Illuminate\Database\Seeder;

class BudgetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sup_budget')->insert([
            "name"=>"ជំពូក ២១"
        ]);
        DB::table('sup_budget')->insert([
            "name"=>"ជំពូក ៦១"
        ]);
    }
}
