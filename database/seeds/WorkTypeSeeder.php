<?php

use App\Model\Supervision\Setup\ProjectType;
use Illuminate\Database\Seeder;

class WorkTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WorkType::insert([
            [
                "id"   => 1,
                "name" => "សាងសង់ថ្មី"
            ],[
                "id"   => 2,
                "name" => "ជួសជុលឡើងវិញ"
            ]
        ]);
    }
}
