<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "Field Density Test (Sandcone/Nuclear)",
            "LWD",
            "BB Test",
            "Proof Rolling",
            "FWD",
            "Coring Thickness",        
            "Thickness",
            "Coring Thickness",
            "FDT",
            "Penetration Test",
            "Application Volume",
            "Slump Test",
            "Temperature",
            "Strength Test",
            "Rebar Spacing",
            "Form Work"];
        foreach($data as $key => $row){
            DB::table('sup_testing_method')->insert([
                "name"=>$data[$key]
            ]);
        }
    }
}
