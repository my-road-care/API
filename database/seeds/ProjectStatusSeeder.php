<?php

use App\Model\Supervision\Setup\Status;
use App\Model\Supervision\Setup\ProjectType;
use Illuminate\Database\Seeder;

class ProjectStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectStatus::insert([
            [
                "id"      => 1,
                "kh_name" => "គ្រោងសាងសង់",
                "en_name" => "Pendding",
                "color"   => "#ffcc00"
            ],[
                "id"   => 2,
                "kh_name" => "កំពុងសាងសង់",
                "en_name" => "In Progress",
                "color"   => "#0909FF"
            ],[
                "id"   => 3,
                "kh_name" => "បានបញ្ចប់",
                "en_name" => "Finish",
                "color"   => "#12AD2B"
            ]
        ]);
    }
}
