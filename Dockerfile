FROM lorisleiva/laravel-docker:7.2

# Copy all source codes to docker container
COPY ./ /apps/laravel

WORKDIR /apps/laravel

RUN rm -f composer.lock

# Install dependencies
RUN composer install

RUN cp .env.example .env

RUN php artisan key:generate

CMD php artisan config:cache && php artisan --host=0.0.0.0 serve
