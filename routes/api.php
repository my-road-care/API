<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {

    Route::get('/', function () {
        return response()->json(['message'=>'Welcome to Road Care API'], 200);
    });
    
    $api->group(['prefix' => 'seeding'], function ($api) {
        require(__DIR__.'/Api/V1/Seeding/main.php');
    });

    $api->group(['prefix' => 'sup', 'middleware' => 'api.auth'], function ($api) {
        require(__DIR__.'/Api/V1/Supervision/main.php');
    });

	$api->group(['prefix' => 'auth', 'middleware' => 'cors'], function ($api) {
        require(__DIR__.'/Api/V1/Auth/main.php');
    });
	
    $api->group(['prefix' => 'cp', 'middleware' => 'cors'], function ($api) {
        require(__DIR__.'/Api/V1/CP/main.php');
    });

    $api->group(['prefix' => 'mo', 'middleware' => 'cors'], function ($api) {
        require(__DIR__.'/Api/V1/MO/main.php');
    });

    $api->group(['prefix' => 'mt', 'middleware' => 'cors'], function ($api) {
        require(__DIR__.'/Api/V1/MT/main.php');
    });

    $api->group(['prefix' => 'bot', 'middleware' => 'cors'], function ($api) {
        require(__DIR__.'/Api/V1/Bot/main.php');
    });

    $api->group(['prefix' => 'sup/authority', 'middleware' => 'api.auth'], function ($api) {
        require(__DIR__ . '/Api/V1/Supervision/authority.php');
    });

    $api->group(['prefix' => 'public', 'middleware' => 'cors'], function ($api) {
        require(__DIR__.'/Api/V1/Public/main.php');
    });

    $api->group(['middleware' => ['cors']], function ($api) {
        require(__DIR__.'/Api/V1/Client/main.php');
    });
              
    $api->group(['middleware' => ['cors', 'jwt.verify']], function ($api) {
        $api->get('/check-token',                                 ['uses' => 'App\Api\V1\Controllers\CheckToken\Controller@checkToken']);
    });

    //============================================ Dashboard
    $api->group(['prefix' => 'dashboard','middleware' => 'cors'], function ($api) {
        $api->get('/', ['uses' => 'App\Api\V1\Controllers\Supervision\Dashboard\DashboardController@index']);

    });

    /** Invite Friend */
    $api->get('/invite-friend',                                 ['uses' => 'App\Api\V1\Controllers\PublicData\Controller@inviteFriend']);

    /** Evaluate Criteria */
    $api->get('/evaluate-criteria',                             ['uses' => 'App\Api\V1\Controllers\PublicData\Controller@evaluateCriteria']);

    /** Test Sending Telegram */
    $api->get('/test-telegram',                             	['uses' => 'App\Api\V1\Controllers\PublicData\Controller@testTelegram']);

    // App Info
    $api->get('/app-info', 										'\App\Api\V1\Controllers\Client\Gen\Controller@appInfo'); 


    //============================================ Authority
	$api->group(['prefix' => 'authority', 'middleware' => ['api.auth','authority']], function ($api) {

        //Account 
        $api->group(['prefix' => 'user', 'namespace' => 'App\Api\V1\Controllers\Supervision\Authorities\Accounts'], function ($api) {
            $api->post('/update-status',    ['uses' => 'AccountController@updateStatus']);
            $api->get('/role',              ['uses' => 'AccountController@GetRole']);
            $api->get('/supervision',       ['uses' => 'AccountController@getSVUser']);
            $api->get('/setup',             ['uses' => 'AccountController@getAuthorithySetup']);
            $api->get('/',                  ['uses' => 'AccountController@getUser']);
            $api->get('/organization',      ['uses' => 'AccountController@getOrganization']);
            $api->post('/update',           ['uses' => 'AccountController@update']);
            $api->post('/',                 ['uses' => 'AccountController@create']);
            $api->post('/update-password',  ['uses' => 'AccountController@updatePassword']);
            $api->delete('/',               ['uses' => 'AccountController@delete']);
        });

        //permission
        $api->group(['prefix' => 'permission', 'namespace' => 'App\Api\V1\Controllers\Supervision\Authorities\Permissions'], function ($api) {
            $api->get('/',  ['uses' => 'PermissionController@ListingPerPart']);
            $api->post('/',  ['uses' => 'PermissionController@update']);
        });


        //Role
        $api->group(['prefix' => 'role', 'namespace' => 'App\Api\V1\Controllers\Supervision\Authorities\Types'], function ($api) {
            $api->get('/',  ['uses' => 'TypeController@listing']);
        });

	});
    
    

    

    // migration

    // $api->get('/CreateProjectInfo','\App\Api\V1\Controllers\Migrations\Project@createProject');
    // $api->get('/ProjectFullProcess/{id}','\App\Api\V1\Controllers\Migrations\Project@ProjectFullProcess');

    // $api->get('/work/{id}','\App\Api\V1\Controllers\Migrations\ProjectInfo@createWork');
    // $api->get('/projectplan/{id}','\App\Api\V1\Controllers\Migrations\ProjectInfo@createProjectPlan');
    // $api->get('/projectprocurment/{id}','\App\Api\V1\Controllers\Migrations\ProjectInfo@createProjectProcurement');
    // $api->get('/CreateStandartTest/{id}','\App\Api\V1\Controllers\Migrations\ProjectInfo@CreateStandartTest');
    $api->get('/data_standard', 	'\App\Http\Controllers\Json\BindData@migrate');
    $api->get('/data-test', 	'\App\Http\Controllers\Json\JSONController@index'); 
    // $api->get('/generate-pk', 	'\App\Http\Controllers\Json\JSONController@generatePK'); 

});