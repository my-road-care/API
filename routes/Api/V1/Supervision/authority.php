<?php

//Account 
$api->group(['prefix' => 'user', 'namespace' => 'App\Api\V1\Controllers\Supervision\Authorities\Accounts'], function ($api) {
    
    $api->get('/role',              ['uses' => 'AccountController@GetRole']);
    $api->get('/supervision',       ['uses' => 'AccountController@getSVUser']);
    $api->get('/setup',             ['uses' => 'AccountController@getAuthorithySetup']);
    $api->get('/',                  ['uses' => 'AccountController@getUser']);
    $api->get('/organization',      ['uses' => 'AccountController@getOrganization']);
    $api->post('/update',           ['uses' => 'AccountController@update']);
    $api->post('/',                 ['uses' => 'AccountController@create']);
    $api->post('/update-password',  ['uses' => 'AccountController@updatePassword']);
    $api->delete('/',               ['uses' => 'AccountController@delete']);
    $api->get('/system-role',       ['uses' => 'AccountController@getSystemRole']);
});

//permission
$api->group(['prefix' => 'permission', 'middleware' => 'permission', 'namespace' => 'App\Api\V1\Controllers\Supervision\Authorities\Permissions'], function ($api) {
    $api->get('/',  ['uses' => 'PermissionController@ListingPerPart']);
    $api->post('/',  ['uses' => 'PermissionController@update']);
});


//Role
$api->group(['prefix' => 'role', 'namespace' => 'App\Api\V1\Controllers\Supervision\Authorities\Types'], function ($api) {
    $api->get('/',  ['uses' => 'TypeController@listing']);
});