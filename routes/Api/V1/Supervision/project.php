<?php

// =====================>> Listing & Create
$api->group(['namespace' => 'App\Api\V1\Controllers\Supervision\Project'], function ($api) {

    $api->get('/setup',                      ['uses' => 'ProjectController@setup']);
    $api->get('/setup/provinces/{id}',       ['uses' => 'ProjectController@getProvince']);
    $api->get('/data-setup',                 ['uses' => 'ProjectController@dataSetup']);
    $api->get('/data-setup/offline-mode',    ['uses' => 'ProjectController@setupOffline']);
    $api->get('/',                           ['uses' => 'ProjectController@listProject']);
    $api->get('/notification',               ['uses' => 'ProjectController@getNotification']);
    $api->get('/check-code',                 ['uses' => 'ProjectController@checkCode']);
    $api->get('/get-code/{projectId}',       ['uses' => 'ProjectController@getCode']);
    $api->post('/create',                    ['uses' => 'ProjectController@createProject']);
    $api->delete('{projectId}/delete',       ['uses' => 'ProjectController@deleteProject']);

    // System Report
    $api->get('{projectId}/report/system-report/setup',      ['uses' => 'SystemReportController@setup']);
    $api->post('{projectId}/report/system-report',           ['uses' => 'SystemReportController@listing']);
    $api->get('{projectId}/report/delete-file',              ['uses' => 'SystemReportController@deleteFile']);

    // Check which part this user can access 
    $api->get('/{projectId}/permission',          ['uses' => 'ProjectController@checkProjectPermission']);

    // Give Approval for Technical Information
    $api->get('/{projectId}/approve',             ['uses' => 'ProjectController@approveTechnicalInfo']);

});

// =====================>>  Info
$api->group(['namespace' => 'App\Api\V1\Controllers\Supervision\Project\Info'], function ($api) {

    // Statistic
    $api->get('{projectId}/info/statistic', ['uses' => 'ProjectStatisticController@getStatistic']);
    $api->get('{projectId}/info/approval',  ['uses' => 'ProjectStatisticController@approve']);

    // General
    $api->get('{projectId}/info/general',      ['uses' => 'ProjectInfoController@getGeneral']);
    $api->post('{projectId}/info/main',        ['uses' => 'ProjectInfoController@updateMainInfo']);
    $api->post('{projectId}/info/procedure',   ['uses' => 'ProjectInfoController@updateProcedureInfo']);
    $api->post('{projectId}/info/date',        ['uses' => 'ProjectInfoController@updateDateInfo']);

    // Procurement
    $api->get('{projectId}/info/procurement',                   ['uses' => 'ProjectInfoController@getProcurement']);
    $api->post('{projectId}/info/procurements',                 ['uses' => 'ProjectProcurementController@create']);
    $api->post('{projectId}/info/procurement/{id}',             ['uses' => 'ProjectProcurementController@update']);
    $api->delete('{projectId}/info/procurement/{id}',           ['uses' => 'ProjectProcurementController@delete']);

    // Working Structure
    $api->get('{projectId}/info/working-structure',             ['uses' => 'ProjectWorkingStructureController@listing']);
    $api->get('get-role',                                       ['uses' => 'ProjectWorkingStructureController@getRole']);
    $api->get('get-user',                                       ['uses' => 'ProjectWorkingStructureController@getUser']);
    $api->get('get-sys',                                        ['uses' => 'ProjectWorkingStructureController@getSysRole']);
    $api->post('delete',                                        ['uses' => 'ProjectWorkingStructureController@delete' ]);
    $api->post('update',                                        ['uses' => 'ProjectWorkingStructureController@update' ]);
    $api->post('{projectId}/info/working-structures',           ['uses' => 'ProjectWorkingStructureController@create' ]);
    $api->post('{projectId}/info/working-structures/test',      ['uses' => 'ProjectWorkingStructureController@testFirbase' ]);
    $api->post('{projectId}/info/working-structures-user',      ['uses' => 'ProjectWorkingStructureController@createUserProject' ]);
    $api->delete('{projectId}/info/working-structures/{id}',    ['uses' => 'ProjectWorkingStructureController@delete' ]);

    $api->post('{projectId}/info/working-structures/test',      ['uses' => 'ProjectWorkingStructureController@testFirebase' ]);

});


// =====================>>  Technical
$api->group(['namespace' => 'App\Api\V1\Controllers\Supervision\Project\Technical'], function ($api) {

    // Listing
    $api->get('{projectId}/technical', ['uses' => 'TechListingController@index']);

    // Work Diagram
    $api->get('{projectId}/technical/diagram/contrction-works',         ['uses' => 'TechDiagramController@dataSetup']);
    $api->get('{projectId}/technical/diagram/artistic-works',           ['uses' => 'TechDiagramController@dataSetupArt']);
    $api->get('{projectId}/technical/diagram',                          ['uses' => 'TechDiagramController@getDiagram']);
    $api->post('{projectId}/technical/diagram',                         ['uses' => 'TechDiagramController@create']);
    $api->post('{projectId}/technical/diagram/add-artistic',            ['uses' => 'TechDiagramController@createArtistic']);

    // $api->get('{projectId}/technical/diagram-update',                   ['uses' => 'ProjectDiagramController@updateTest']);

    // Progress
    $api->get('{projectId}/progress-pk',                  ['uses' => 'TechProgressController@getPks']);
    $api->get('{projectId}/progress-info/{pkId}',         ['uses' => 'TechProgressController@progressInfo']);
    $api->get('{projectId}/progress-check',               ['uses' => 'TechProgressController@progressCheck']);
    $api->post('{projectId}/progress-update',             ['uses' => 'TechProgressController@progressUpdate']);

    // Testing
    $api->get('{projectId}/testing-info/{pkId}',         ['uses' => 'TechTestingQualityController@testingInfo']);
    $api->get('{projectId}/testing-method/{pkId}',       ['uses' => 'TechTestingQualityController@getTestingMethod']);
    $api->get('{projectId}/testing-check',               ['uses' => 'TechTestingQualityController@testingCheck']);
    $api->post('{projectId}/testing-update',             ['uses' => 'TechTestingQualityController@updateTest']);

    // Privew Side
    $api->get('{projectId}/technical/diagram/side/{sideId}',                            ['uses' => 'TechSideController@preview']);
    $api->post('{projectId}/technical/diagram/side/{sideId}/update-progress',           ['uses' => 'TechSideController@updateProgress']);
    $api->post('{projectId}/technical/diagram/side/{sideId}/add-quality-test',          ['uses' => 'TechSideController@addQualityTest']);

    // Delete History or Test Record
    $api->delete('{projectId}/technical/record/{id}/delete', ['uses' => 'TechSideController@deleteHistory']);

    // Offline Mode
    $api->get('{projectId}/technical/list-testing-standard', ['uses' => 'TechSideController@listTestingStandard']);


    // Testing Standard
    $api->get('{projectId}/technical/testing-standards',                                    ['uses' => 'TechTestingStandardController@listing']);
 
    $api->get('{projectId}/technical/testing-standards/data-setup-testing-standards',       ['uses' => 'TechTestingStandardController@dataSetupTestingStandards']);
    $api->get('{projectId}/technical/testing-standards/sort',                               ['uses' => 'TechTestingStandardController@sortDataSetupTestingStandards']);
    $api->get('{projectId}/technical/testing-standards/{standardId}',                       ['uses' => 'TechTestingStandardController@get']);
    $api->post('{projectId}/technical/testing-standards',                                   ['uses' => 'TechTestingStandardController@create']);
    $api->post('{projectId}/technical/testing-standards/{standardId}',                      ['uses' => 'TechTestingStandardController@update']);
    $api->delete('{projectId}/technical/testing-standards/{standardId}',                    ['uses' => 'TechTestingStandardController@delete']);

    // Plan
    $api->get('{projectId}/technical/plans',                ['uses' => 'TechPlanController@listing']);
    $api->get('{projectId}/technical/plans/{planId}',       ['uses' => 'TechPlanController@get']);
    $api->post('{projectId}/technical/plans',               ['uses' => 'TechPlanController@create']);
    $api->post('{projectId}/technical/plans/{planId}',      ['uses' => 'TechPlanController@update']);
    $api->delete('{projectId}/technical/plans/{planId}',    ['uses' => 'TechPlanController@delete']);

});

// =====================>>  Report
$api->group(['namespace' => 'App\Api\V1\Controllers\Supervision\Project\Report'], function ($api) {

    // Testing Report
    $api->get('{projectId}/report/testing-report/setup',            ['uses' => 'ReportTestingController@dataSetup']);
    $api->get('{projectId}/report/testing-report',                  ['uses' => 'ReportTestingController@listing']);
    $api->get('{projectId}/report/testing-report/{testingTrxID}',   ['uses' => 'ReportTestingController@viewTesting']);
    $api->get('{projectId}/report/testing-report-chart',            ['uses' => 'ReportTestingController@getTestingChart']);

    // Letter
    $api->get('{projectId}/report/letter/setup' ,       ['uses' => 'ReportLetterController@dataSetup']);
    $api->get('{projectId}/report/letter' ,             ['uses' => 'ReportLetterController@listing']);
    $api->get('{projectId}/report/letter/{id}' ,        ['uses' => 'ReportLetterController@preview']);
    $api->post('{projectId}/report/letter',             ['uses' => 'ReportLetterController@create']);
    $api->post('{projectId}/report/letter/{id}',        ['uses' => 'ReportLetterController@update']);
    $api->delete('{projectId}/report/letter/{id}',      ['uses' => 'ReportLetterController@delete']);

    // Picture
    $api->get('{projectId}/report/picture/setup',               ['uses' => 'ReportProgressController@dataSetup']);
    $api->get('{projectId}/report/picture',                     ['uses' => 'ReportProgressController@listing']);
    $api->get('{projectId}/report/picture/{progressTrxID}',     ['uses' => 'ReportProgressController@viewProgress']);
    $api->post('{projectId}/report/picture/getGeneralInfo',     ['uses' => 'ReportProgressController@getdataJsreport']);    

    // References Report
    $api->get('{projectId}/report/references-report',           ['uses' => 'ReportReferencesController@listing']);
    $api->post('{projectId}/report/references-report/create',   ['uses' => 'ReportReferencesController@create']);
    $api->get('{projectId}/report/references-report/{id}',      ['uses' => 'ReportReferencesController@preview']);
    $api->post('{projectId}/report/references-report/{id}',     ['uses' => 'ReportReferencesController@update']);
    $api->delete('{projectId}/report/references-report/{id}',   ['uses' => 'ReportReferencesController@delete']);


});
