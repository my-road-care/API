<?php

/**
 * -----------------------------------------------------
 * Supervision
 * -----------------------------------------------------
 */

 //=============================================== Check Supervision User
$api->group(['middleware' => 'checkSuppervisionUser'], function ($api) {	

	

	//============================================ Project
	$api->group(['prefix' => 'projects', 'middleware' => 'ProjectPermission'], function ($api) {
		require(__DIR__ . '/project.php');
	});

	//============================================ Report
	$api->group(['prefix' => 'reports'], function ($api) {
		require(__DIR__ . '/report.php');
	});

	//============================================ Authority
	$api->group(['prefix' => 'authority'], function ($api) {
		require(__DIR__ . '/authority.php');
	});

	//============================================ Organization
	$api->group(['prefix' => 'organization'], function ($api) {
		require(__DIR__ . '/organization.php');
	});

	//============================================ Setting
	$api->group(['prefix' => 'setup'], function ($api) {
		require(__DIR__ . '/setting.php');
	});

	//=========================================== Sup Dashboard	
	$api->group(['namespace' => 'App\Api\V1\Controllers\Supervision\Project'], function ($api) {
		$api->get('/dashboard',  ['uses' => 'DashboardController@dashboard']);
	});

});







