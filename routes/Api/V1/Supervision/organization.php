<?php

$api->group(['namespace' => 'App\Api\V1\Controllers\Supervision\Organization'], function ($api) {
    $api->get('/data-setup',    ['uses' => 'OrganizationController@dataSetup']);
    $api->get('/',              ['uses' => 'OrganizationController@listing']);
    $api->post('/',             ['uses' => 'OrganizationController@create']);
    $api->delete('/{id}',           ['uses' => 'OrganizationController@delete']);
    $api->post('/update/{id}',  ['uses' => 'OrganizationController@update']);
});
