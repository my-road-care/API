<?php

$api->group(['namespace' => 'App\Api\V1\Controllers\Supervision\Dashboard'], function ($api) {
    $api->get('/', ['uses' => 'DashboardController@index']);
});
