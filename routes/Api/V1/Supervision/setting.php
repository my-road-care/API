<?php

$api->group(['prefix' => 'works', 'namespace' => 'App\Api\V1\Controllers\Supervision\Setting\Works'], function ($api) {
    $api->get('/show',           ['uses' => 'WorksController@show']);
    $api->get('/',               ['uses' => 'WorksController@listing']);
    $api->post('testing-method', ['uses' => 'WorksController@TesthingMethod']);
    $api->post('/',              ['uses' => 'WorksController@create']);
    $api->delete('/',            ['uses' => 'WorksController@delete']);
    $api->post('/update',        ['uses' => 'WorksController@update']);
});

$api->group(['prefix' => 'workstype', 'namespace' => 'App\Api\V1\Controllers\Supervision\Setting\Works'], function ($api) {
    $api->get('/',          ['uses' => 'WorksTypeController@listing']);
});

$api->group(['prefix' => 'worksgroup', 'namespace' => 'App\Api\V1\Controllers\Supervision\Setting\Works'], function ($api) {
    $api->get('/',          ['uses' => 'WorksGroupController@listing']);
});

$api->group(['prefix' => 'testing-methods-types', 'namespace' => 'App\Api\V1\Controllers\Supervision\Setting\TestingMethod'], function ($api) {
    $api->get('/',          ['uses' => 'TestingMethodTypeController@listing']);
});

$api->group(['prefix' => 'testing-methods', 'namespace' => 'App\Api\V1\Controllers\Supervision\Setting\TestingMethod'], function ($api) {
    $api->get('/',              ['uses' => 'TestingMethodController@listing']);
    $api->get('/show',          ['uses' => 'TestingMethodController@show']);
    $api->post('/add-param',    ['uses' => 'TestingMethodController@addParam']);
    $api->post('/delete-param', ['uses' => 'TestingMethodController@deleteParam']);
    $api->post('/',             ['uses' => 'TestingMethodController@create']);
    $api->delete('/',           ['uses' => 'TestingMethodController@delete']);
    $api->post('/update',       ['uses' => 'TestingMethodController@update']);
  
});

// ===============================================================================> testing Standards
$api->group(['prefix' => 'testing-methods', 'namespace' => 'App\Api\V1\Controllers\Supervision\Setting\TestingStandard'], function ($api) {
    $api->get('/group',           ['uses' => 'TestingStandardController@getGroup']);
    $api->get('/group/{id}',      ['uses' => 'TestingStandardController@getStandard']);
    $api->post('/group/update',   ['uses' => 'TestingStandardController@updateStandard']);
    $api->post('/group/create',   ['uses' => 'TestingStandardController@createStandard']);
    $api->post('/group/delete',   ['uses' => 'TestingStandardController@delete']);
});

$api->group (['prefix' => 'budget-year', 'namespace' => 'App\Api\V1\Controllers\Supervision\Setting\BudgetYear'], function ($api) {

    $api->get('/',          ['uses' => 'BudgetYearController@listing']);
    $api->post('/',         ['uses' => 'BudgetYearController@create']);
    $api->delete('/{id}',   ['uses' => 'BudgetYearController@delete']);
    $api->post('/{id}',     ['uses' => 'BudgetYearController@update']);
});
