<?php

	
	$api->group(['namespace' => 'App\Api\V1\Controllers\PublicData'], function($api) {

		
		$api->get('/reports', 										['uses' => 'Controller@reports']);
		$api->get('/reports/{id}', 									['uses' => 'Controller@view']);
		$api->get('/invite-friend', 								['uses' => 'Controller@inviteFriend']);
		$api->get('/monthly-reports', 								['uses' => 'Controller@monthlyReport']);
		// Public
		$api->get('/daily-report', 									['uses' => 'DialyReportController@index']);
		$api->get('/weekly-reports', 								['uses' => 'DialyReportController@weeklyReports']);
		$api->get('/monthly-report', 								['uses' => 'DialyReportController@monthlyReports']);

		$api->get('/get-report/{report_id}', 						['uses' => 'DialyReportController@getReportID']);
	});

