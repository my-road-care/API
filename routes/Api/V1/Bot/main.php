<?php

	
	$api->group(['namespace' => 'App\Api\V1\Controllers\Bot'], function($api) {

		
		$api->post('/', 										['uses' => 'Controller@webhook']);
		$api->get('/send', 										['uses' => 'Controller@send']);

		
	});

