<?php

$controller = 'Notification\Controller@';

$api->get('/', 				['uses' => $controller.'list']);
$api->post('/', 			['uses' => $controller.'post']);
$api->get('/users', 		['uses' => $controller.'users']);
$api->get('/send', 			['uses' => $controller.'send']);


