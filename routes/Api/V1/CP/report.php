<?php

$controller = 'Report\Controller@';

$api->get('/province', 		['uses' => $controller.'province']);
$api->get('/mts', 		['uses' => $controller.'mts']);
$api->post('/send', 		['uses' => $controller.'send']);
$api->get('/monthly', 		['uses' => $controller.'monthly']);