<?php

$controller = 'Reporter\Controller@';
$api->get('/list', 			    ['uses' => $controller.'list']);
$api->get('/view/{id}', 	    ['uses' => $controller.'view']);
$api->delete('/{id}', 		    ['uses' => $controller.'delete']);
$api->get('/{id}/reports', 		['uses' => $controller.'reports']);
$api->post('/change-password',  ['uses' => $controller.'changePassword']);

$controller = 'Reporter\ScoreController@';
$api->get('/{id}/scores/list', 					['uses' => $controller.'list']);

$controller = 'Reporter\ReportController@';
$api->get('/{id}/reports/list', 				['uses' => $controller.'list']);