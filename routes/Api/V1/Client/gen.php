<?php

$controller = 'Gen\Controller@';
$api->get('/roads', 										['uses' => $controller.'roads']);
$api->get('/provinces', 									['uses' => $controller.'provinces']);
$api->get('/maintence-codes', 								['uses' => $controller.'maintenceCodes']);
$api->get('/maintence-coded', 								['uses' => $controller.'maintenceCoded']);
$api->get('/mos', 											['uses' => $controller.'mos']); 
$api->get('/mts', 											['uses' => $controller.'mts']); 

$api->get('/permissions', 									['uses' => $controller.'permissions']); 
$api->get('/statuses', 										['uses' => $controller.'statuses']); 
// $api->get('/app-info', 										['uses' => $controller.'appInfo']); 
$api->get('/what-news', 									['uses' => $controller.'whatNews']); 