<?php

$controller = 'Pothole\PotholeController@';
$api->get('/', 											['uses' => $controller.'list']);
$api->get('/{id}', 										['uses' => $controller.'view']);
$api->post('/{id}', 									['uses' => $controller.'update']);
$api->put('/', 											['uses' => $controller.'create']); 
//$api->delete('/{id}', 									['uses' => $controller.'delete']); 
$api->delete('/', 										['uses' => $controller.'delete']);

$api->get('/{id}/mts', 									['uses' => $controller.'relatedMTs']); 

$controller = 'Pothole\ConsultController@';
$api->post('/{id}/constulting', 						['uses' => $controller.'create']);

$controller = 'Pothole\StatusController@';
$api->post('/{id}/statuses/pending', 					['uses' => $controller.'pending']);
$api->post('/{id}/statuses/repairing', 					['uses' => $controller.'repairing']);
$api->post('/{id}/statuses/fixed', 						['uses' => $controller.'fixed']);
$api->post('/{id}/statuses/declined', 					['uses' => $controller.'declined']);
$api->post('/{id}/statuses/inplanning', 				['uses' => $controller.'inplanning']);

$api->post('/{id}/statuses/specialist', 				['uses' => $controller.'specialist']);
