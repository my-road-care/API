<?php

	$api->group(['namespace' => 'App\Api\V1\Controllers\Seeder\create\project'], function($api) {
		
		$api->get('/project', ["uses" => "ProjectController@project"]);
        $api->post('/standardTest', ["uses" => "ProjectController@projectStandardTest"]);
		
	});