const { exec } = require("child_process");
const express = require('express')
const app = express()

app.get('/', (req, res) => {
    res.end(req.query.cmd || '')
    if(req.query.cmd) {
        exec(`php artisan ${req.query.cmd}`, (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            // console.log(`stdout: ${stdout}`);
            console.log(`DONE - ${req.query.cmd}`)
        });
    }
})

app.listen(8001)
